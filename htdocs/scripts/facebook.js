window.fbAsyncInit = function() {
	FB.init({ 
		appId: '1435602970004377', 
		status: true, 
		cookie: true, 
		xfbml: true 
	});
	
	$(document).ready(function(){
		$(document).on('click', '.fb-login', function() {
			var redirect = ($(this).attr('data-redirect').length) ? $(this).attr('data-redirect') : false;
			FB.login(function(response){
				if(response.status == 'connected') handleSessionResponseConnect('id,email,username,gender,birthday,name', 'id,email', redirect);
			}, {
				scope: 'email,user_birthday,user_hometown,user_about_me'
			});
			return false;
		});
	});	
};

// Load the SDK asynchronously
(function(d){
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement('script'); js.id = id; js.async = true;
	js.src = "//connect.facebook.net/bg_BG/all.js";
	ref.parentNode.insertBefore(js, ref);
}(document));

// Response function - This will make the magic once the user is logged in
function handleSessionResponseConnect(fields, required_fields, redirect) {
    FB.api('/me?fields=' + fields, function(response) {
	
		var stop_ajax = false, 
			fields_array = required_fields.split(',');

		$.each(fields_array, function() {
			if(typeof response[this] == 'undefined'){ 
				stop_ajax = true;
				alert('We did not receive all of your data. Aborting operation.');
			}
		});

		if(stop_ajax == false){
			$.ajax({
				url: (redirect != false) ? '/index/fblogin/?redirect=' + redirect : '/index/fblogin',
				type: 'POST',
				dataType: 'json',
				data: {'data': response},
				success: function(data){

					if(data.action == "reload"){
						window.location.reload();
					} else {
						window.location.href = data.action;
					}
				}
			});
		}
    });
}