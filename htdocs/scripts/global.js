var System = {

	init: function() {
	
		// Get page name
		// var page = $('body').attr('data-page');
		
		var idCurrentLang = $('#idCurrentLang').val();
		
		if($('.flexslider').length){
			$('.flexslider').flexslider({
	          	animation: "fade",
	          	controlNav: true,
	          	directionNav: false,
	          	slideshow: true,
	          	slideshowSpeed: 4000
	        });
		}
		/*
		if($('#container').length && !$('.viewProd').length){
			$('#container').masonry({
				itemSelector: '.box',
				columnWidth: 260
			});
		}
		*/
		if($('.box').length){
			$(".box img").mouseenter(function (){
				var size = $(this).parent().height();
				$(this).parent().find(".hover").css({"height": size - 35});
	
			});
		}
		
		if($('#modal').length){
			$(document).on('click', '#modal', function() {
		    	$('#modal_box').modal();
		        return false;
		    });
		}
		
		if($('#order_btn').length){
			System.loadOrderPopup();
		}
				
		if( $('.tipsyLoad').length ){
			$('.tipsyLoad').tipsy({ gravity: 's' });
		}
		
		if( $('.deleteCartRow').length){
			System.deleteCartRow(idCurrentLang);
		}
		
		if( $('#sendCart').length ){
			System.sendCartData();
		}
		
		if( $('.minusCol').length){
			System.minusCol(idCurrentLang);
		}
		
		if( $('.plusCol').length){
			System.plusCol(idCurrentLang);
		}
		
		if($('#backButton').length){
			$(document).on('click', '#backButton', function() {
				history.back();
			});
		}
				
		if ($('.tabs').length) {
			$('.tabsBtn').click(function(){
				var tab_id = $(this).attr('data-tab');

				$('.tabsBtn').removeClass('active');
				$('.content').removeClass('current');

				$(this).addClass('active');
				$("#" + tab_id).addClass('current');
			});
		}
		
	},
	
	loadOrderPopup:function(){
		$(document).on('click', '#order_btn', function() {
			var title = $(this).attr('rel');
			alert(title);
		});
	},
	
	sendCartData: function(){
		
		$(document).on('click', '#sendCart', function() {
			
			var mphone 		= $('#customer-main-phone').val();
			var mcity  		= $('#customer-main-city').val();  
			var madress 	= $('#customer-main-address').val();	
			var mname		= $('#customer-main-name').val();
			var memail		= $('#customer-main-email').val();
			
			if(	mname.length<3 || 
				memail.length<3 || 
				mphone.length<3 || 
				mcity.length<2 || 
				madress.length<5
			){
				alert('Всички полета със звездичка са задължителни!');
			}
			else if(System.checkEmail(memail)==false){
				alert('Email адреса е невалиден!');
				return false;
			}
		/*	else if($('#agree').prop('checked') != true){
				alert('Прочетете условията за ползване!');
			}
		*/	
			else{
				// Submit form
				$('#cartForm').submit();
			}
			
		});
	},
	
	checkEmail: function(str){
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  		return regex.test(str);
	},
	
	deleteCartRow: function(idCurrentLang){
		
		$(document).on('click', '.deleteCartRow', function() {
			var idcart = $(this).attr('rel');
			
			if (confirm('Сигурни ли сте, че искате да изтриете този артикул от поръчката?')) {
				
				$.ajax({
					type: 'post',
					dataType: 'json',
					data: { idcart: idcart },				
					url: '/' + idCurrentLang + '/ajax/removeFromCart',
					success: function(result){
						$('#row_'+idcart).fadeOut().remove();
						
						$("#totalPriceCart").html(result.price + ' лв');
						$("#cartprice").html(result.price + ' лв');
						$("#cartprod").html(result.col);
					}
				});
			}
		});
	},
	
	minusCol: function(idCurrentLang){
		$(document).on('click', '.minusCol', function() {
			var idop   = $(this).attr('rel');
			var pCount = parseInt($('#quantity_'+idop).val());
			
			if(pCount == 1){
				return false;
			}
			
			pCount = pCount-1;
			$('#quantity_'+idop).val(pCount);
			
			$.ajax({
				type: 'post',
				dataType: 'json',
				data: { idop: idop, count:pCount },
				url: '/' + idCurrentLang + '/ajax/updateProductCart',
				success: function(result){
				
					if(result.promoInfo){
						$('#idPriceInfo').html('Цена:<br /><span style="color:orange;">Отстъпка:</span><br /><strong>Крайна цена:</strong>');
						$('#totalPriceCart').html(result.totalPricePercent + ' лв. <br /><span style="color:orange;"> - ' + result.promoInfo.percent + '%</span><br /><strong>' + result.price + '</strong> лв.');
					}
					else {
						$('#idPriceInfo').html('<strong>Крайна цена:</strong>');
						$('#totalPriceCart').html(result.price + ' лв.');
					}
					
					$('#cartprice').html(result.price + ' лв.');
					$('#totalProduct_' + idop).html(result.prodPrice + ' лв.')
					$("#cartprod").html(result.col);
				}
			});
		});
	},
	
	plusCol: function(idCurrentLang){
		$(document).on('click', '.plusCol', function() {
			var idop   = $(this).attr('rel');
			var pCount = parseInt($('#quantity_'+idop).val());
			
			pCount = pCount+1;
			$('#quantity_'+idop).val(pCount);
			
			$.ajax({
				type: 'post',
				dataType: 'json',
				data: { idop: idop, count:pCount },
				url: '/' + idCurrentLang + '/ajax/updateProductCart',
				success: function(result){
				
					if(result.promoInfo){
						$('#idPriceInfo').html('Цена:<br /><span style="color:orange;">Отстъпка:</span><br /><strong>Крайна цена:</strong>');
						$('#totalPriceCart').html(result.totalPricePercent + ' лв. <br /><span style="color:orange;"> - ' + result.promoInfo.percent + '%</span><br /><strong>' + result.price + '</strong> лв.');
					}
					else {
						$('#idPriceInfo').html('<strong>Крайна цена:</strong>');
						$('#totalPriceCart').html(result.price + ' лв.');
					}
					
					$("#cartprod").html(result.col);
					$('#cartprice').html(result.price + ' лв.');
					$('#totalProduct_' + idop).html(result.prodPrice + ' лв.')
				}
			});
		});
	},
	
	changeModelPrice: function(idCurrentLang){
		$(document).on('change', '#idmodel', function() {
			
			var idmodel 	= $(this).val();
			var idproduct 	= $('#prodid').val();
			
			if(idmodel >0){
				
				$.ajax({
						type: 'post',
						dataType: 'json',
						data: { idproduct: idproduct, idmodel:idmodel},				
						url: '/' + idCurrentLang + '/ajax/modelPrice',
						success: function(result){
							if(result.priceModel) {
								$('#priceProduct').html(result.priceModel);
								$('#nalichnostinfo').html(result.nalichnost);
								
								if(result.active == 2) {
									$('#btnreqview').fadeOut();
									$('#btnorderview').fadeIn();
								}
								else if(result.active == 3 || result.active==1){
									$('#btnorderview').fadeOut();
									$('#btnreqview').fadeIn();
								}
							}
						},
						error: function(error){
						}
					});
			}
			else{
				Alert('Изберете модел за да поръчате!');
			}
		});
	},
	
	checkRegisterData: function(){
		$(document).on('click', '#registrationBtn', function() {
			
			var email = $('#pemail').val();
			var pass  = $('#pass').val();
			var repass = $('#repass').val();
			
			if(email.length < 3 || pass.length < 4 || pass.length < 4) {
				alert('* Indicates Required Fields!');
				return false;
			}
			else {
				$('#registration').submit();
			}
		});
	},
	
	closeOverlay: function(){
	
		// Close popup screen
		$(document).on('click', '.badge-overlay-close', function() {
			$('.overlay-scroll-container').addClass('hide');
			if( !$('#login_section').hasClass('hide') ) $('#login_section').addClass('hide');
			if( !$('#signup_section').hasClass('hide') ) $('#signup_section').addClass('hide');
			if( !$('#report_section').hasClass('hide') ) $('#report_section').addClass('hide');
			if( !$('#modal-upload').hasClass('hide') ) $('#modal-upload').addClass('hide');
		});
	}
}

function checkFields(){
	
	var success = 1;
			
	$(".pp_inline .required_contact").each(function(){
		var check = $(this).val();
		
		if(check.length < 2){
			success = 2;
		}
	});
	
	if(success == 2){
		alert('Всички полета с * са задължителни!');
		return false;
	}
	else {
		return true;
	}
}

$(document).ready(System.init);