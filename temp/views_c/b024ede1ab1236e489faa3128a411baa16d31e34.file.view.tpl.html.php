<?php /* Smarty version Smarty-3.0.8, created on 2019-09-11 16:27:57
         compiled from "/home/balkanec/public_html/evinarts.com/application/views/news/view.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:7035336885d78f65d695031-74582257%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b024ede1ab1236e489faa3128a411baa16d31e34' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/application/views/news/view.tpl.html',
      1 => 1568208476,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7035336885d78f65d695031-74582257',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
prettyPhoto/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>

<section>
	<div class="container">
		
		<h3 class="h3Margin"><?php echo $_smarty_tpl->getVariable('infoNews')->value['title'];?>
</h3>
		<div class="newsbox">
			<div class="news_box">
			<?php if ($_smarty_tpl->getVariable('infoNews')->value['picture']){?>
				<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
news/big/<?php echo $_smarty_tpl->getVariable('infoNews')->value['picture'];?>
" rel="prettyPhoto[gallery1]">
					<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
news/big/<?php echo $_smarty_tpl->getVariable('infoNews')->value['picture'];?>
" style="float:left; padding:0 10px 10px 10px; max-width:300px;" />
				</a>
			<?php }?>
	        <?php echo $_smarty_tpl->getVariable('infoNews')->value['content'];?>

	    </div>
	   		<br style="clear:both;" />
	   		
	   		<?php if ($_smarty_tpl->getVariable('infoNews')->value['images']){?>
				<h3 class="h3Margin"><?php echo $_smarty_tpl->getVariable('lang')->value['gallery'];?>
</h3>
				<div class="gallery_wrapper_1">
					<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('infoNews')->value['images']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
		           		<div class="gallery_1" style="padding:0px; padding-right: 10px;">
		           			<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
news/big/<?php echo $_smarty_tpl->tpl_vars['v']->value['picture'];?>
" rel="prettyPhoto[gallery1]">
								<img border="0" alt="" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
news/thumb/<?php echo $_smarty_tpl->tpl_vars['v']->value['picture'];?>
">
							</a>
						</div>
						<?php if ((($_smarty_tpl->tpl_vars['k']->value+1)%4==0)){?></div><div class="gallery_wrapper_1"><?php }?>
		            <?php }} ?>
				</div>
					
					<script type="text/javascript">
						$(document).ready(function() {
							$("a[rel^='prettyPhoto']").prettyPhoto({
								animation_speed:'normal',
								theme:'dark_rounded',
								deeplinking: false,
								hideflash: true, 
								autoplay_slideshow: false, 
								overlay_gallery:false, 
								slideshow:false,
								social_tools:'',
							});
						});
					</script>
				
				
			<?php }?>
	   		
		</div>
		
		<div class="clearfix"></div>
		<div class="share">
			<span><?php echo $_smarty_tpl->getVariable('lang')->value['page_share'];?>
</span>

			<div class="fb">
				<div class="fb-like" data-href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('facebook')->value['link'];?>
" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
			</div>
			<div class="google">
				<div class="g-plusone" data-size="medium"></div>			
			</div>
			<a href="javascript:history.back();" class="back"><?php echo $_smarty_tpl->getVariable('lang')->value['page_back'];?>
</a>
		</div>			
		
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1420638294889507&version=v2.0";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		
			<!-- Place this tag after the last +1 button tag. -->
			<script type="text/javascript">
			  window.___gcfg = {lang: 'en'};
		
			  (function() {
				var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
				po.src = 'https://apis.google.com/js/platform.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		
		
	</div>
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>