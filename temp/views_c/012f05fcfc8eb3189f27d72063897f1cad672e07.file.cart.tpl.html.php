<?php /* Smarty version Smarty-3.0.8, created on 2019-09-10 18:49:24
         compiled from "/home/balkanec/public_html/evinarts.com/application/views/cart/cart.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:9108001115d77c6046f4cf8-14378310%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '012f05fcfc8eb3189f27d72063897f1cad672e07' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/application/views/cart/cart.tpl.html',
      1 => 1568130563,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9108001115d77c6046f4cf8-14378310',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<section>
	<div class="container">
		<h3 class="h3Margin"><?php echo $_smarty_tpl->getVariable('lang')->value['order_title'];?>
</h3>	
		<div class="textpage_content">
			<div class="innercart clearfix">
				<?php if ($_smarty_tpl->getVariable('productInCart')->value){?>
					<table id="basket" cellpadding="0" cellspacing="0" class="clearfix">
						<thead>
							<tr class="cartheader">
								<th><?php echo $_smarty_tpl->getVariable('lang')->value['forder_number'];?>
</th>
								<th class="name" colspan="2"><?php echo $_smarty_tpl->getVariable('lang')->value['forder_prod'];?>
</th>
								<th class="total"><?php echo $_smarty_tpl->getVariable('lang')->value['forder_price'];?>
</th>
								<th class="remove"><?php echo $_smarty_tpl->getVariable('lang')->value['forder_options'];?>
</th>
							</tr>
						</thead>
						<tbody>
							<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('productInCart')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['product']->key;
?>
								<tr id="row_<?php echo $_smarty_tpl->tpl_vars['product']->value['id'];?>
">
									<td align="center"><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
.</td>
									<td class="image">
										<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/<?php echo $_smarty_tpl->tpl_vars['product']->value['alias'];?>
-<?php echo $_smarty_tpl->tpl_vars['product']->value['idproduct'];?>
"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
thumb/<?php echo $_smarty_tpl->tpl_vars['product']->value['picture'];?>
" alt="" width="68" /></a>
									</td>
									<td class="name">
										<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/<?php echo $_smarty_tpl->tpl_vars['product']->value['alias'];?>
-<?php echo $_smarty_tpl->tpl_vars['product']->value['idproduct'];?>
"><?php echo $_smarty_tpl->tpl_vars['product']->value['title'];?>
</a>
									</td>
									<td class="price" align="center">
										<?php if ($_SESSION['currency']!='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_SESSION['currency']];?>
<?php }?><?php echo $_smarty_tpl->tpl_vars['product']->value['price'];?>
 <?php if ($_SESSION['currency']=='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_SESSION['currency']];?>
<?php }?>
									</td>
									<td class="remove">
										<a href="javascript:void(0);" class="deleteCartRow tipsyLoad" rel="<?php echo $_smarty_tpl->tpl_vars['product']->value['id'];?>
" title="<?php echo $_smarty_tpl->getVariable('lang')->value['forder_delete'];?>
"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
remove.png" alt="" /></a>
									</td>
								</tr>
							<?php }} ?>
						</tbody>
						<tfoot>
							<tr class="cartfooter">
								<td colspan="2">
									<a class="btn_back orderedit" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
">
										<span><?php echo $_smarty_tpl->getVariable('lang')->value['forder_continue'];?>
</span>
									</a>
								</td>
								<td class="price" style="text-align:right;" colspan="2">
									<strong><?php echo $_smarty_tpl->getVariable('lang')->value['forder_total_price'];?>
:</strong>
								</td>
								<td class="total" id="totalPriceCart" style="text-align:center;">
									<?php if ($_SESSION['currency']!='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_SESSION['currency']];?>
<?php }?> <strong><?php echo $_smarty_tpl->getVariable('totalPrice')->value;?>
</strong> <?php if ($_SESSION['currency']=='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_SESSION['currency']];?>
<?php }?>
								</td>
							</tr>
						</tfoot>
					</table>
				<?php }else{ ?>
					<p style="width:728px; color:red; padding-top:10px;"><?php echo $_smarty_tpl->getVariable('lang')->value['forder_empty'];?>
</p>
				<?php }?>
			</div>
			<div class="cartfooter mobile">
				<a class="btn_back orderedit" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
">
					<span><?php echo $_smarty_tpl->getVariable('lang')->value['forder_continue'];?>
</span>
				</a>
				<div class="total" id="totalPriceCart">
					<strong><?php echo $_smarty_tpl->getVariable('lang')->value['forder_total_price'];?>
:</strong>
					<?php if ($_SESSION['currency']!='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_SESSION['currency']];?>
<?php }?> <strong><?php echo $_smarty_tpl->getVariable('totalPrice')->value;?>
</strong> <?php if ($_SESSION['currency']=='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_SESSION['currency']];?>
<?php }?>
				</div>
			</div>
				
			<?php if ($_smarty_tpl->getVariable('productInCart')->value){?>
							
				<div class="checkout_grid container">
					<div class="pull-left">
						<div class="box_holder">
							<div class="b_head">
								<h2 class="i-file"><?php echo $_smarty_tpl->getVariable('lang')->value['forder_metod_dostavka'];?>
</h2>
							</div><!--box head-->
							<div class="b_body dostavkabox">
								<?php echo $_smarty_tpl->getVariable('lang')->value['dostavka_text'];?>

							</div>
							
						</div>
						<br />
					</div>
					<div class="pull-right">
						<form method="post" action="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/cart/order" name="cartForm" id="cartForm">
							<div class="box_holder">
								<div class="b_head">
									<h2 class="i-file"><?php echo $_smarty_tpl->getVariable('lang')->value['forder_info_delivery'];?>
</h2>
								</div><!--box head-->
								<div class="b_body">
									<div class="f_row">
										<input type="text" id="customer-main-name" name="customer[fullName]" value="" value="" class="inp_text f-name" placeholder="<?php echo $_smarty_tpl->getVariable('lang')->value['forder_ime_fam'];?>
">
									</div>
									<div class="f_row">
										<input type="text"id="customer-main-city" name="customer[city]" value="" class="inp_text f-city" placeholder="<?php echo $_smarty_tpl->getVariable('lang')->value['forder_naseleno_mqsto'];?>
" /> 
									</div>
																
									<div class="f_row">
										<input type="text" id="customer-main-address" name="customer[address]" class="inp_text f-adress" placeholder="<?php echo $_smarty_tpl->getVariable('lang')->value['forder_address'];?>
" />
									</div>
									<div class="f_row">     
										<input type="text" id="customer-main-phone" name="customer[phone]" value="" class="inp_text f-phone " placeholder="<?php echo $_smarty_tpl->getVariable('lang')->value['contact_phone'];?>
" />
									</div>
									<div class="f_row">
										<input type="text" id="customer-main-email" name="customer[email]" value="" class="inp_text f-mail" placeholder="E-mail" />
									</div>
									<div class="f_row">
										<textarea class="inp_textarea" placeholder="<?php echo $_smarty_tpl->getVariable('lang')->value['forder_final'];?>
" name="customer[description]"></textarea>
									</div>
									
									<a href="javascript:void(0)" class="btn_order big clear" name="sendCart" id="sendCart"><span><?php echo $_smarty_tpl->getVariable('lang')->value['view_order'];?>
</span></a>
								</div><!--box body-->
							</div><!--box-->
						</form>
					</div><!--left clm-->
				</div>			    
			<?php }?>
			<div class="clearfix"></div>
		</div>
	</div>
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>