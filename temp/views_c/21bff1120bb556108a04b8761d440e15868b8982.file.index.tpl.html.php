<?php /* Smarty version Smarty-3.0.8, created on 2019-11-18 07:26:00
         compiled from "/home/balkanec/public_html/evinarts.com/application/views/index.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:14261076545dd22b68d2fc84-16431223%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '21bff1120bb556108a04b8761d440e15868b8982' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/application/views/index.tpl.html',
      1 => 1574054759,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14261076545dd22b68d2fc84-16431223',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/home/balkanec/public_html/evinarts.com/library/smarty/plugins/modifier.escape.php';
if (!is_callable('smarty_modifier_strip_tags')) include '/home/balkanec/public_html/evinarts.com/library/smarty/plugins/modifier.strip_tags.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/balkanec/public_html/evinarts.com/library/smarty/plugins/modifier.truncate.php';
if (!is_callable('smarty_modifier_date_bg')) include '/home/balkanec/public_html/evinarts.com/library/smarty/plugins/modifier.date_bg.php';
?><?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<section class="galeria">
	<div class="container">
		
		<div class="flexslider">
			<ul class="slides">
				<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('listPicture')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
				  	<li>
		                <a href="<?php echo $_smarty_tpl->getVariable('cofig')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/<?php echo $_smarty_tpl->tpl_vars['v']->value['alias'];?>
-<?php echo $_smarty_tpl->tpl_vars['v']->value['idproduct'];?>
" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['v']->value['title']);?>
">
		                  <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
big/<?php echo $_smarty_tpl->tpl_vars['v']->value['picture'];?>
" alt="" />
		                  <p><?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>
 / <span><?php echo smarty_modifier_truncate(smarty_modifier_strip_tags($_smarty_tpl->tpl_vars['v']->value['content']),30,'...','UTF-8');?>
</span></p>
		                </a>
	              	</li>
              	<?php }} ?>
			</ul>
		</div>

		<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/<?php echo $_smarty_tpl->getVariable('listKartichki')->value['alias'];?>
-<?php echo $_smarty_tpl->getVariable('listKartichki')->value['idproduct'];?>
" class="right_index_box">
			
			<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
big/<?php echo $_smarty_tpl->getVariable('listKartichki')->value['picture'];?>
" alt="" />
			<p><?php echo $_smarty_tpl->getVariable('listKartichki')->value['title'];?>
</p>
		</a>
	</div>
		
</section>
<section class="shortParagraph">
	<div class="container">
		<p class="txtItalic"><?php echo $_smarty_tpl->getVariable('lang')->value['index_text_1'];?>
</p>
	</div>
</section>

<section class="aboutme">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-xs-12">
				<h3><?php echo $_smarty_tpl->getVariable('lang')->value['index_za_men'];?>
</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-5 col-md-5 ">
				<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
htdocs/images/profile-new.jpg" alt="" class="img-fluid" />
			</div>
			<div class="col-lg-7 col-md-7 pl-5">
					<h1><?php echo $_smarty_tpl->getVariable('lang')->value['avt_name_index'];?>
</h1>
					<p><?php echo $_smarty_tpl->getVariable('lang')->value['index_text_2'];?>
</p>
					<div class="row">
						<div class="col-lg-12">
				<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/contacts" class="button1 floatL"><?php echo $_smarty_tpl->getVariable('lang')->value['index_pishete_mi'];?>
</a>
				<a href="https://www.facebook.com/evinarts" class="button1 floatL" target="_blank"><?php echo $_smarty_tpl->getVariable('lang')->value['index_facebook'];?>
</a>
			</div>
					</div>
			</div>
		</div><!-- row -->
	</div>
</section>

<section class="map"><!-- past myTechniques -->
	<div class="container">
		<h3><?php echo $_smarty_tpl->getVariable('lang')->value['forder_address'];?>
</h3>
		<?php if ($_smarty_tpl->getVariable('lang')->value['lg']=='bg'){?>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2932.3776644258387!2d23.347301615755676!3d42.695723421829285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40aa8584c0ea1701%3A0x2f2a5d2e45396c43!2z0YPQuy4g4oCe0JDQu9C10LrQviDQmtC-0L3RgdGC0LDQvdGC0LjQvdC-0LLigJwgMzUsIDE1MDUg0J7QsdC-0YDQuNGJ0LUsINCh0L7RhNC40Y8!5e0!3m2!1sbg!2sbg!4v1566188330506!5m2!1sbg!2sbg" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
		<?php }else{ ?>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2932.3776644258387!2d23.347301615755676!3d42.695723421829285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40aa8584c0ea1701%3A0x2f2a5d2e45396c43!2z0YPQuy4g4oCe0JDQu9C10LrQviDQmtC-0L3RgdGC0LDQvdGC0LjQvdC-0LLigJwgMzUsIDE1MDUg0J7QsdC-0YDQuNGJ0LUsINCh0L7RhNC40Y8!5e0!3m2!1sbg!2sbg!4v1566188330506!5m2!1sen!2sen" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
		<?php }?>
	</div>
</section>

<section class="appear">
	<div class="container">
		<h3 class="h3Margin"><?php echo $_smarty_tpl->getVariable('lang')->value['index_posledni_izqvi'];?>
</h3>
		<div class="flex">
		<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('listNews')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
			<div class="newsPhoto" style='background: url("<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
news/thumb/<?php echo $_smarty_tpl->tpl_vars['v']->value['picture'];?>
") no-repeat center center'></div>

			<figure class="newsHolder">
				<h4><?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>
</h4>
				<h5><?php echo smarty_modifier_date_bg($_smarty_tpl->tpl_vars['v']->value['date_added']);?>
</h5>
				<p><?php echo smarty_modifier_truncate(smarty_modifier_strip_tags($_smarty_tpl->tpl_vars['v']->value['content']),200,'...','UTF-8');?>
</p>
			</figure>
		<?php }} ?>
		</div><!-- flex -->
	</div>
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>