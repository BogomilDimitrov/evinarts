<?php /* Smarty version Smarty-3.0.8, created on 2019-11-18 07:45:08
         compiled from "/home/balkanec/public_html/evinarts.com/application/views/news/browse.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:6713207335dd22fe44dcce8-21227997%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9f1ee01ea53eade2c2d7a9749a5b3b012598606e' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/application/views/news/browse.tpl.html',
      1 => 1574055907,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6713207335dd22fe44dcce8-21227997',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<section>
	<div class="container">
		<h3 class="h3Margin"><?php if ($_smarty_tpl->getVariable('upcoming')->value==2){?><?php echo $_smarty_tpl->getVariable('lang')->value['menu_pred_title'];?>
<?php }else{ ?><?php echo $_smarty_tpl->getVariable('lang')->value['index_posledni_izqvi'];?>
<?php }?></h3>
		
			<div id="container_inner">
			<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('listNews')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
				<div class="newsbox">
					<?php if ($_smarty_tpl->tpl_vars['v']->value['picture']){?>	
						<a title="<?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>
" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/news/<?php echo $_smarty_tpl->tpl_vars['v']->value['alias'];?>
-<?php echo $_smarty_tpl->tpl_vars['v']->value['idnews'];?>
">
							<img border="0" alt="<?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>
" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
news/thumb/<?php echo $_smarty_tpl->tpl_vars['v']->value['picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>
" />
						</a>
					<?php }?>
					<div class="newsinfo">
						<a class="title" title="<?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>
" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/news/<?php echo $_smarty_tpl->tpl_vars['v']->value['alias'];?>
-<?php echo $_smarty_tpl->tpl_vars['v']->value['idnews'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>
</a><br /><br />
						<?php echo $_smarty_tpl->tpl_vars['v']->value['content'];?>

					</div>
				</div>
				<div class="h-dots"></div>
			<?php }} ?>
			</div>
			
			<div class="pagination">
				<div class="pager">
					<?php echo $_smarty_tpl->getVariable('lang')->value['page_number'];?>

					<span><?php echo $_smarty_tpl->getVariable('currentPage')->value;?>
</span>
					<?php echo $_smarty_tpl->getVariable('lang')->value['page_ot'];?>
 <span class="static"><?php echo $_smarty_tpl->getVariable('maxPage')->value;?>
</span>
					<a href="<?php if ($_smarty_tpl->getVariable('next_link')->value){?><?php echo $_smarty_tpl->getVariable('next_link')->value;?>
<?php }else{ ?>javascript:void(0)<?php }?>" class="btn_next" title="<?php echo $_smarty_tpl->getVariable('lang')->value['page_next_page'];?>
"></a>
					<a href="<?php if ($_smarty_tpl->getVariable('prev_link')->value){?><?php echo $_smarty_tpl->getVariable('prev_link')->value;?>
<?php }else{ ?>javascript:void(0)<?php }?>" class="btn_prev" title="<?php echo $_smarty_tpl->getVariable('lang')->value['page_prev_page'];?>
"></a>
				</div>
			</div><!-- pagination -->
		<br style="clear:both" /><br />	
	</div>
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>