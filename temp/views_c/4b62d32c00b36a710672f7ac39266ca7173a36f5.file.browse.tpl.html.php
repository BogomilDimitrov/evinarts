<?php /* Smarty version Smarty-3.0.8, created on 2019-09-10 14:16:25
         compiled from "/home/balkanec/public_html/evinarts.com/application/views/gallery/browse.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:3966576745d7786099f8d12-37768182%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4b62d32c00b36a710672f7ac39266ca7173a36f5' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/application/views/gallery/browse.tpl.html',
      1 => 1568114182,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3966576745d7786099f8d12-37768182',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/home/balkanec/public_html/evinarts.com/library/smarty/plugins/modifier.escape.php';
if (!is_callable('smarty_modifier_strip_tags')) include '/home/balkanec/public_html/evinarts.com/library/smarty/plugins/modifier.strip_tags.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/balkanec/public_html/evinarts.com/library/smarty/plugins/modifier.truncate.php';
?><?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<script type="text/javascript">
	$(window).load(function() {
		$('#container').masonry({
			itemSelector: '.box',
			percentPosition: true,
			gutter: 10
		});
	});
</script>


<section class="galeria bSizing">
	<div class="container">
		<?php $_template = new Smarty_Internal_Template("elements/left_menu.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
		
		<div class="thumbsWidth">
			<ul class="breadcrumb bSizing">
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['breadcrumb_home'];?>
</a></li>
			    <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/<?php echo $_smarty_tpl->getVariable('page')->value;?>
"><?php echo $_smarty_tpl->getVariable('sectionTitle')->value;?>
</a></li> 
			    <?php if ($_smarty_tpl->getVariable('categoryInfo')->value){?>
			    	<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/<?php echo $_smarty_tpl->getVariable('page')->value;?>
/<?php echo $_smarty_tpl->getVariable('categoryInfo')->value['alias'];?>
-<?php echo $_smarty_tpl->getVariable('categoryInfo')->value['idcategory'];?>
"><?php echo $_smarty_tpl->getVariable('categoryInfo')->value['title'];?>
</a></li>
			    <?php }?>
			</ul>
			<div id="container">
				
				<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('listGallery')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
					<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/<?php echo $_smarty_tpl->tpl_vars['v']->value['alias'];?>
-<?php echo $_smarty_tpl->tpl_vars['v']->value['idproduct'];?>
" class="box">
						<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
thumb/<?php echo $_smarty_tpl->tpl_vars['v']->value['picture'];?>
" alt="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['v']->value['title']);?>
" style="max-width:235px;" />
						<div class="hover"> 
							<p>
								<span><?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>

									<b><?php echo smarty_modifier_truncate(smarty_modifier_strip_tags($_smarty_tpl->tpl_vars['v']->value['content']),30,'...','UTF-8');?>
</b>
								<?php if ($_smarty_tpl->tpl_vars['v']->value['price_1']){?>
									<span class="price">
										<?php if ($_SESSION['currency']!='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_SESSION['currency']];?>
<?php }?> <?php echo $_smarty_tpl->tpl_vars['v']->value['price_1'];?>
 <?php if ($_SESSION['currency']=='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_SESSION['currency']];?>
<?php }?>
									</span>
								<?php }?>
								</span>
							</p>
						</div>
					</a>
				<?php }} ?>
				
			</div>

			<div class="pagination">
				<div class="pager">
					<?php echo $_smarty_tpl->getVariable('lang')->value['page_number'];?>

					<span><?php echo $_smarty_tpl->getVariable('currentPage')->value;?>
</span>
					<?php echo $_smarty_tpl->getVariable('lang')->value['page_ot'];?>
 <span class="static"><?php echo $_smarty_tpl->getVariable('maxPage')->value;?>
</span>
					<a href="<?php if ($_smarty_tpl->getVariable('next_link')->value){?><?php echo $_smarty_tpl->getVariable('next_link')->value;?>
<?php }else{ ?>javascript:void(0)<?php }?>" class="btn_next" title="<?php echo $_smarty_tpl->getVariable('lang')->value['page_next_page'];?>
"></a>
					<a href="<?php if ($_smarty_tpl->getVariable('prev_link')->value){?><?php echo $_smarty_tpl->getVariable('prev_link')->value;?>
<?php }else{ ?>javascript:void(0)<?php }?>" class="btn_prev" title="<?php echo $_smarty_tpl->getVariable('lang')->value['page_prev_page'];?>
"></a>
				</div>
			</div><!-- pagination -->

		</div>
	</div>	
</section>
		
<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>