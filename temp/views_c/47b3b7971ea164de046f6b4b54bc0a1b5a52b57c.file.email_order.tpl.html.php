<?php /* Smarty version Smarty-3.0.8, created on 2019-09-10 19:00:08
         compiled from "/home/balkanec/public_html/evinarts.com/application/views/pages/email_order.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:4686214515d77c888a519d3-96299702%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '47b3b7971ea164de046f6b4b54bc0a1b5a52b57c' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/application/views/pages/email_order.tpl.html',
      1 => 1437064875,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4686214515d77c888a519d3-96299702',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<body style="height:100%;background: #F0F0F0;color:#333;font:1em/1.5 Arial,sans-serif;padding:0 0 50px;margin:0;">
<table width="100%" height="60" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border:0;text-align:left;background:#F4F4F4;">
<tbody>
<tr>
<td height="60">
<table width="820" align="center" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td>
<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
">
<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
htdocs/images/logo.png" style="width:100px;height:101px;margin-top:6px;display:block;" />
</a>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
	
<table width="820" align="center" cellpadding="0" cellspacing="0" style="margin-top:25px;">
<tbody>
<tr>
<td style="padding:10px;-webkit-border-radius:10px;-moz-border-radius:10px;border-radius:10px;background:rgba(0,0,0,0.07);">

<table width="100%" style="-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;padding:0 50px 30px;background-color:white;">
	<tbody>
	<tr>
		<td colspan="2"><b><?php echo $_smarty_tpl->getVariable('lang')->value['forder_client'];?>
</b></td>
		<td width="100">&nbsp;</td>
		<td colspan="2"><b><?php echo $_smarty_tpl->getVariable('lang')->value['forder_info_delivery'];?>
</b></td>
	</tr>
	<tr>
		<td><?php echo $_smarty_tpl->getVariable('lang')->value['forder_data'];?>
:</td>
		<td><b><?php echo $_smarty_tpl->getVariable('email')->value['date_order'];?>
</b></td>
		<td width="100">&nbsp;</td>
		<td><?php echo $_smarty_tpl->getVariable('lang')->value['contact_name'];?>
:</td>
		<td><b><?php echo $_smarty_tpl->getVariable('email')->value['shipping']['name'];?>
</b></td>
	</tr>
	
	<tr>
		<td><?php echo $_smarty_tpl->getVariable('lang')->value['forder_porychka'];?>
:</td>
		<td><b>#<?php echo $_smarty_tpl->getVariable('email')->value['idorder'];?>
</b></td>
		<td width="100">&nbsp;</td>
		<td>Email:</td>
		<td><b><?php echo $_smarty_tpl->getVariable('email')->value['shipping']['email'];?>
</b></td>
	</tr>	
	<tr>
		<td></td>
		<td></td>
		<td width="100">&nbsp;</td>
		<td><?php echo $_smarty_tpl->getVariable('lang')->value['forder_address'];?>
:</td>
		<td><b><?php echo $_smarty_tpl->getVariable('email')->value['shipping']['address'];?>
</b></td>
	</tr>
	
	<tr>
		<td>Email:</td>
		<td><b>evin@abv.bg</b></td>
		<td width="100">&nbsp;</td>
		<td><?php echo $_smarty_tpl->getVariable('lang')->value['cart_city'];?>
:</td>
		<td><b><?php echo $_smarty_tpl->getVariable('email')->value['shipping']['city'];?>
</b></td>
	</tr>
		
	<tr>
		<td><?php echo $_smarty_tpl->getVariable('lang')->value['forder_contact_phone'];?>
:</td>
		<td><b>+359 889 44 44 26</b></td>
		<td width="100">&nbsp;</td>
		<td><?php echo $_smarty_tpl->getVariable('lang')->value['contact_phone'];?>
:</td>
		<td><b><?php echo $_smarty_tpl->getVariable('email')->value['shipping']['phone'];?>
</b></td>
	</tr>
	
	<tr>
		<td></td>
		<td></td>
		<td width="100">&nbsp;</td>
		<td><?php echo $_smarty_tpl->getVariable('lang')->value['forder_final'];?>
:</td>
		<td><b><?php echo $_smarty_tpl->getVariable('email')->value['description'];?>
</b></td>
	</tr>
	</tbody>
</table>

<h2><?php echo $_smarty_tpl->getVariable('lang')->value['order_title'];?>
 #<?php echo $_smarty_tpl->getVariable('email')->value['idorder'];?>
:</h2>
<table width="100%" style="-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;padding:5px 50px 30px;background-color:white;border:1px solid #999;">
<tbody>
	<tr>
		<th><?php echo $_smarty_tpl->getVariable('lang')->value['forder_prod'];?>
</th>
		<th><?php echo $_smarty_tpl->getVariable('lang')->value['forder_col'];?>
</th>
		<th><?php echo $_smarty_tpl->getVariable('lang')->value['forder_price'];?>
</th>
		<th><?php echo $_smarty_tpl->getVariable('lang')->value['forder_total_price'];?>
</th>
	</tr>
	
	<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('email')->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
		<tr>
			<td><?php echo $_smarty_tpl->tpl_vars['k']->value;?>
. <?php echo $_smarty_tpl->tpl_vars['v']->value['productName'];?>
</td>
			<td align="center"><?php echo $_smarty_tpl->tpl_vars['v']->value['quantity'];?>
</td>
			<td align="center"><?php if ($_smarty_tpl->getVariable('email')->value['currency']!='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_smarty_tpl->getVariable('email')->value['currency']];?>
<?php }?> <?php echo $_smarty_tpl->tpl_vars['v']->value['price'];?>
 <?php if ($_smarty_tpl->getVariable('email')->value['currency']=='leva'){?>лв.<?php }?></td>
			<td align="center"><?php if ($_smarty_tpl->getVariable('email')->value['currency']!='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_smarty_tpl->getVariable('email')->value['currency']];?>
<?php }?> <?php echo $_smarty_tpl->tpl_vars['v']->value['totalPrice'];?>
 <?php if ($_smarty_tpl->getVariable('email')->value['currency']=='leva'){?>лв.<?php }?></td>
		</tr>
	<?php }} ?>
	
	<tr>
		<td colspan="3" align="right"><b><?php echo $_smarty_tpl->getVariable('lang')->value['forder_total_price'];?>
:</b></td>
		<td align="center"><?php if ($_smarty_tpl->getVariable('email')->value['currency']!='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_smarty_tpl->getVariable('email')->value['currency']];?>
<?php }?> &nbsp;<?php echo $_smarty_tpl->getVariable('email')->value['total'];?>
 <?php if ($_smarty_tpl->getVariable('email')->value['currency']=='leva'){?>лв.<?php }?></td>
	</tr>
</tbody>
</table>
<br />
<table width="100%" style="-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;padding:10px 50px 30px;background-color:white;">
<tbody>
<tr>
<td>
<?php if ((!empty($_smarty_tpl->getVariable('email',null,true,false)->value['title']))){?>
<h1 style="text-align:center;<?php if ((!empty($_smarty_tpl->getVariable('email',null,true,false)->value['slogan']))){?>margin-bottom:.4em;<?php }else{ ?>margin-bottom:30px;<?php }?>font-size:1.5em;">
<?php echo $_smarty_tpl->getVariable('email')->value['title'];?>

</h1>
<?php }?>
<?php if ((!empty($_smarty_tpl->getVariable('email',null,true,false)->value['slogan']))){?>
<p style="text-align:center;color: #999;margin-bottom:30px;">
<?php echo $_smarty_tpl->getVariable('email')->value['slogan'];?>

</p>
<?php }?>
</td>
</tr>
<tr>
<td>
<?php if ((!empty($_smarty_tpl->getVariable('email',null,true,false)->value['content']))){?><?php echo $_smarty_tpl->getVariable('email')->value['content'];?>
<?php }?>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>