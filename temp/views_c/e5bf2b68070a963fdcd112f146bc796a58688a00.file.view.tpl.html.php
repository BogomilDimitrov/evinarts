<?php /* Smarty version Smarty-3.0.8, created on 2019-09-10 15:47:27
         compiled from "/home/balkanec/public_html/evinarts.com/application/views/gallery/view.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:4706458685d779b5f6371a7-87581875%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e5bf2b68070a963fdcd112f146bc796a58688a00' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/application/views/gallery/view.tpl.html',
      1 => 1568119645,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4706458685d779b5f6371a7-87581875',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/home/balkanec/public_html/evinarts.com/library/smarty/plugins/modifier.escape.php';
if (!is_callable('smarty_modifier_strip_tags')) include '/home/balkanec/public_html/evinarts.com/library/smarty/plugins/modifier.strip_tags.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/balkanec/public_html/evinarts.com/library/smarty/plugins/modifier.truncate.php';
?><?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<script src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
jquery.add2cart.js?a=15" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_css'];?>
lightbox.css" />
<script  type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
lightbox.js"></script>

<section class="galeria bSizing viewProd">
	<div class="container">
		<?php $_template = new Smarty_Internal_Template("elements/left_menu.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
		
		<div class="thumbsWidth">
			<ul class="breadcrumb bSizing">
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['breadcrumb_home'];?>
</a></li>
			    <?php if ($_smarty_tpl->getVariable('idmaincat')->value==24){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/<?php echo $_smarty_tpl->getVariable('page')->value;?>
"><?php echo $_smarty_tpl->getVariable('sectionTitle')->value;?>
</a></li>
				<?php }elseif($_smarty_tpl->getVariable('idmaincat')->value==14){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/<?php echo $_smarty_tpl->getVariable('page')->value;?>
"><?php echo $_smarty_tpl->getVariable('sectionTitle')->value;?>
</a></li>
			    <?php }else{ ?>
			    	<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/<?php echo $_smarty_tpl->getVariable('page')->value;?>
"><?php echo $_smarty_tpl->getVariable('sectionTitle')->value;?>
</a></li>
			    	<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/<?php echo $_smarty_tpl->getVariable('page')->value;?>
/<?php echo $_smarty_tpl->getVariable('categoryInfo')->value['alias'];?>
-<?php echo $_smarty_tpl->getVariable('categoryInfo')->value['idcategory'];?>
"><?php echo $_smarty_tpl->getVariable('categoryInfo')->value['title'];?>
</a></li>
			    <?php }?>
			    <li><span><?php echo $_smarty_tpl->getVariable('productInfo')->value['title'];?>
</span></li>
			</ul>
			
			<div id="container">
				
				<div class="item_wrap">
					<div class="image_wrap">
						<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
big/<?php echo $_smarty_tpl->getVariable('productInfo')->value['picture'];?>
" data-lightbox="item" data-title="<?php echo smarty_modifier_escape($_smarty_tpl->getVariable('productInfo')->value['title']);?>
" class="zoom"></a>
						<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
big/<?php echo $_smarty_tpl->getVariable('productInfo')->value['picture'];?>
" alt="" class="main_img" id="product_air" />
					</div>
					<div class="flex">
						<div class="info">
							<h4><?php echo $_smarty_tpl->getVariable('productInfo')->value['title'];?>
</h4>
							<?php if ($_smarty_tpl->getVariable('productInfo')->value['razmeri']){?><span class="size"><?php echo $_smarty_tpl->getVariable('productInfo')->value['razmeri'];?>
 <?php echo $_smarty_tpl->getVariable('lang')->value['view_sm'];?>
</span><?php }?>
							<p><?php echo $_smarty_tpl->getVariable('productInfo')->value['content'];?>
</p>

							<a href="javascript:history.back();" class="backbtn"><?php echo $_smarty_tpl->getVariable('lang')->value['page_back'];?>
</a>
						</div>
						<div class="buy_wrap">
							<?php if ($_smarty_tpl->getVariable('productInfo')->value['price_1']){?>
								<div class="price"><?php echo $_smarty_tpl->getVariable('lang')->value['price'];?>
: <span>
									<?php if ($_SESSION['currency']!='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_SESSION['currency']];?>
<?php }?> 
									<?php if ($_smarty_tpl->getVariable('productInfo')->value['status']=='promo'){?><span class="oldprice"><?php if ($_SESSION['currency']!='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_SESSION['currency']];?>
<?php }?> <?php echo $_smarty_tpl->getVariable('productInfo')->value['price_2'];?>
 <?php if ($_SESSION['currency']=='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_SESSION['currency']];?>
<?php }?><span class="oldline"></span> </span><?php }?> 
									<?php echo $_smarty_tpl->getVariable('productInfo')->value['price_1'];?>
 <?php if ($_SESSION['currency']=='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_SESSION['currency']];?>
<?php }?>
									</span>
								</div>
								
								<input type="button" value="<?php echo $_smarty_tpl->getVariable('lang')->value['view_order'];?>
" onClick="$.add2cart('product_air', 'cartprod')" name="buy"/>
								<input type="hidden" name="idproduct" id="prodid" value="<?php echo $_smarty_tpl->getVariable('productInfo')->value['idproduct'];?>
" />
							
							<?php }elseif($_smarty_tpl->getVariable('productInfo')->value['active']==1){?>
								<div class="price"><?php echo $_smarty_tpl->getVariable('lang')->value['view_zaqvka'];?>
<br /><br /></div>
							<?php }else{ ?>
								<div class="price"><?php echo $_smarty_tpl->getVariable('lang')->value['view_zaqvka'];?>
<br /><br /></div>
							<?php }?>
							<div class="social">
								<div class="fb_btn">
									<div class="fb-like" data-href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/<?php echo $_smarty_tpl->getVariable('facebook')->value['link'];?>
" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
								</div><!-- fb_btn -->
								<div class="gp_btn">
									<div class="g-plusone" data-size="medium"></div>
								</div>
							</div><!-- social -->
							
							<?php if (count($_smarty_tpl->getVariable('productInfo')->value['images'])>1){?>
								<div style="height:0px; overflow:hidden;">
									<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('productInfo')->value['images']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
										<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
big/<?php echo $_smarty_tpl->tpl_vars['v']->value['picture'];?>
" data-lightbox="item"></a>
									<?php }} ?>
								</div>
							<?php }?>
							
						</div>
					</div>
					<a href="javascript:history.back();" class="backbtn mobile">Назад</a>
				</div>
			</div>
		</div>
	</div>	
</section>

<?php if ($_smarty_tpl->getVariable('sameProduct')->value){?>
	<section class="galeria more" style="min-height: 220px;">
		<div class="container">
			<div class="inner">
				<h3 class="h3Margin small"><?php echo $_smarty_tpl->getVariable('lang')->value['view_same'];?>
</h3>
				
				<div class="more_item">
					<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('sameProduct')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
						<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/<?php echo $_smarty_tpl->tpl_vars['v']->value['alias'];?>
-<?php echo $_smarty_tpl->tpl_vars['v']->value['idproduct'];?>
" class="<?php if ($_smarty_tpl->tpl_vars['k']->value==2){?>last <?php }?>box">
							<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
thumb/<?php echo $_smarty_tpl->tpl_vars['v']->value['picture'];?>
">
							<div class="hover">
								<p>
									<span><?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>

										<b><?php echo smarty_modifier_truncate(smarty_modifier_strip_tags($_smarty_tpl->tpl_vars['v']->value['content']),30,'...','UTF-8');?>
</b>
										<?php if ($_smarty_tpl->tpl_vars['v']->value['price_1']){?><span class="price"><?php if ($_SESSION['currency']!='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_SESSION['currency']];?>
<?php }?> <?php echo $_smarty_tpl->tpl_vars['v']->value['price_1'];?>
 <?php if ($_SESSION['currency']=='leva'){?><?php echo $_smarty_tpl->getVariable('config')->value['currency'][$_SESSION['currency']];?>
<?php }?></span><?php }?>
								</p>
							</div>
						</a>
					<?php }} ?>
				</div>
			</div><!-- inner -->
		</div>
	</section>
<?php }else{ ?>
	<br /><br />
<?php }?>

<div id="fb-root"></div>

<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=463900467095416&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>



<!-- Place this tag after the last +1 button tag. -->
	<script type="text/javascript">
	  window.___gcfg = {lang: 'en'};

	  (function() {
		var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		po.src = 'https://apis.google.com/js/platform.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	  })();
	</script>
		

		
<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>