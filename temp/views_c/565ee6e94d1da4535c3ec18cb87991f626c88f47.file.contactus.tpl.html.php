<?php /* Smarty version Smarty-3.0.8, created on 2019-09-10 12:00:01
         compiled from "/home/balkanec/public_html/evinarts.com/application/views/pages/contactus.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:20991469975d776611a366b7-67283652%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '565ee6e94d1da4535c3ec18cb87991f626c88f47' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/application/views/pages/contactus.tpl.html',
      1 => 1568106000,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20991469975d776611a366b7-67283652',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
	<div class="container">
		<h3 class="h3Margin"><?php echo $_smarty_tpl->getVariable('lang')->value['contact_men'];?>
</h3>
		<div class="row mb-5">
			<div class="col-lg-6 col-md-4 col-sm-12">
				<b><?php echo $_smarty_tpl->getVariable('lang')->value['contact_name'];?>
:</b> <?php echo $_smarty_tpl->getVariable('lang')->value['avt_name'];?>
<br /><br />
				<b><?php echo $_smarty_tpl->getVariable('lang')->value['contact_phone'];?>
:</b> +359 884 90 67 21,<br /> +359 889 44 44 26<br /><br />
				<b>Email:</b> evinarts@gmail.com, evin@evinarts.com <br /><br />
				<strong><?php echo $_smarty_tpl->getVariable('lang')->value['forder_address'];?>
:</strong> <?php echo $_smarty_tpl->getVariable('lang')->value['contact_address_info'];?>
				
			</div>
			<div class="col-lg-6 col-md-8 col-sm-12">
				<strong><?php echo $_smarty_tpl->getVariable('lang')->value['map'];?>
:</strong><br /><br />
				<div class="map_wrap">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2932.3776644258387!2d23.347301615755676!3d42.695723421829285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40aa8584c0ea1701%3A0x2f2a5d2e45396c43!2z0YPQuy4g4oCe0JDQu9C10LrQviDQmtC-0L3RgdGC0LDQvdGC0LjQvdC-0LLigJwgMzUsIDE1MDUg0J7QsdC-0YDQuNGJ0LUsINCh0L7RhNC40Y8!5e0!3m2!1sbg!2sbg!4v1566188330506!5m2!1sbg!2sbg" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>