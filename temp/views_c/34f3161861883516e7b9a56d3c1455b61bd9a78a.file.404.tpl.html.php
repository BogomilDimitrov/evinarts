<?php /* Smarty version Smarty-3.0.8, created on 2019-09-10 12:49:19
         compiled from "/home/balkanec/public_html/evinarts.com/application/views/pages/errors/404.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:12214720875d77719f285f99-30514198%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '34f3161861883516e7b9a56d3c1455b61bd9a78a' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/application/views/pages/errors/404.tpl.html',
      1 => 1568108956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12214720875d77719f285f99-30514198',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<section>
	<div class="container">
		<h4><?php echo $_smarty_tpl->getVariable('lang')->value['error_interface_404_title'];?>
</h4>
		<p class="txtItalic"><?php echo $_smarty_tpl->getVariable('lang')->value['error_interface_404_message'];?>
</p>
		<p><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
" style="font-weight: bold;"><?php echo $_smarty_tpl->getVariable('lang')->value['interface_return_to_homepage'];?>
</a></p>
	</div>
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>