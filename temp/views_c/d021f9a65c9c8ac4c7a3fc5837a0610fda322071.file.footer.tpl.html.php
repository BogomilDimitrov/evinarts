<?php /* Smarty version Smarty-3.0.8, created on 2019-09-10 12:26:55
         compiled from "/home/balkanec/public_html/evinarts.com/application/views/footer.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:13710667095d776c5f50e385-83074151%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd021f9a65c9c8ac4c7a3fc5837a0610fda322071' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/application/views/footer.tpl.html',
      1 => 1568107607,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13710667095d776c5f50e385-83074151',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
	<footer>
		<div class="container">
			<h3 class="h3Margin"><?php echo $_smarty_tpl->getVariable('lang')->value['kontakti'];?>
</h3>

			<div class="row">
				<div class="col-lg-6 col-md-6 col-xs-12">
					<div class="flex">
						<div class="phones txt-left">
							<p>+359 889 44 44 26</p>
							<p>+359 884 90 67 21</p>
						</div>
						<div class="emails txt-left">
							<p>evin@evinarts.com</p>
							<p>evinarts@gmail.com</p>
						</div>
					</div>
					<div class="clearfix">
						<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
" style="color:#9ee2d8;"><?php echo $_smarty_tpl->getVariable('lang')->value['breadcrumb_home'];?>
</a> &nbsp;|&nbsp; 
						<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/terms" style="color:#9ee2d8;"><?php echo $_smarty_tpl->getVariable('lang')->value['obshti_uslovia'];?>
</a> &nbsp;|&nbsp; 
						<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/sitemap" style="color:#9ee2d8;"><?php echo $_smarty_tpl->getVariable('lang')->value['sitemap'];?>
</a>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-xs-12">
					<h4><?php echo $_smarty_tpl->getVariable('lang')->value['contact_title'];?>
</h4>
					<form id="contact_form" action="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/contacts" name="sendmessage" method="POST" enctype="multipart/form-data">
						<div class="row">
							<div class="col-lg-3 col-md-4 col-sm-12 pl-0"><label for="name"><?php echo $_smarty_tpl->getVariable('lang')->value['contact_name'];?>
*</label></div>
							<div class="col-lg-9 col-md-8 col-sm-12 pl-0"><input id="name" class="input" name="name" type="text" value="" size="30" /></div>
						</div>
						<div class="row">
							<div class="col-lg-3 col-md-4 col-sm-12 pl-0"><label for="email">Email*</label></div>
							<div class="col-lg-9 col-md-8 col-sm-12 pl-0"><input id="email" class="input" name="email" type="text" value="" size="30" /></div>
						</div>
						<div class="row">
							<div class="col-lg-3 col-md-4 col-sm-12 pl-0"><label for="email"><?php echo $_smarty_tpl->getVariable('lang')->value['contact_phone'];?>
*</label></div>
							<div class="col-lg-9 col-md-8 col-sm-12 pl-0"><input id="email" class="input" name="phone" type="text" value="" size="30" /></div>
						</div>
						<div class="row">
							<div class="col-lg-3 col-md-4 col-sm-12 pl-0"><label for="message"><?php echo $_smarty_tpl->getVariable('lang')->value['contact_comentar'];?>
*</label></div>
							<div class="col-lg-9 col-md-8 col-sm-12 pl-0"><textarea id="message" class="input" name="comment" rows="7" cols="30"></textarea></div>
						</div>
						<div class="row">
							<div class="col-12"><input id="submit_button" name="sendForm" type="submit" value="<?php echo $_smarty_tpl->getVariable('lang')->value['contact_send'];?>
" /></div>
						</div>
					</form>
				</div>
			</div><!-- row -->
		</div>
	</footer>


	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50453900-1', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>