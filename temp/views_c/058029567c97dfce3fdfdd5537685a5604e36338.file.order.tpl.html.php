<?php /* Smarty version Smarty-3.0.8, created on 2019-10-23 17:18:10
         compiled from "/home/balkanec/public_html/evinarts.com/application/views/cart/order.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:13725414555db06122683eb1-17096077%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '058029567c97dfce3fdfdd5537685a5604e36338' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/application/views/cart/order.tpl.html',
      1 => 1568133698,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13725414555db06122683eb1-17096077',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<section>
	<div class="container">
		
		<h3 class="h3Margin"><?php echo $_smarty_tpl->getVariable('lang')->value['forder_porychka'];?>
</h1>
		<div class="">					
			<div id="finish_order">
				<div class="thanks"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
htdocs/images/tick.png" /> <p><?php echo $_smarty_tpl->getVariable('lang')->value['forder_success'];?>
</p></div>
			</div>
			<div id="finish_order_box">
				<div class="order_box">
					<h4><?php echo $_smarty_tpl->getVariable('lang')->value['forder_porychka'];?>
</h4>
					<div class="order_info">
						<table class="order_info">
							<tr>
								<td><b><?php echo $_smarty_tpl->getVariable('lang')->value['forder_number'];?>
:</b></td>
								<td><span><?php echo $_smarty_tpl->getVariable('idorder')->value;?>
</span></td>
							</tr>
							<tr>
								<td><b><?php echo $_smarty_tpl->getVariable('lang')->value['forder_status'];?>
:</b></td>
								<td><span><?php echo $_smarty_tpl->getVariable('lang')->value['forder_new'];?>
</span></td>
							</tr>
							<tr>
								<td><b><?php echo $_smarty_tpl->getVariable('lang')->value['forder_data'];?>
</b></td>
								<td><span><?php echo $_smarty_tpl->getVariable('date_order')->value;?>
</span></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="order_box">
					<h4><?php echo $_smarty_tpl->getVariable('lang')->value['forder_client'];?>
</h4>
					<div class="order_info">
						<table class="order_info">
							<tr>
								<td><b><?php echo $_smarty_tpl->getVariable('lang')->value['contact_name'];?>
:</b></td>
								<td><span><?php echo $_smarty_tpl->getVariable('orderInfo')->value['fullName'];?>
</span></td>
							</tr>
							<tr>
								<td><b>Email:</b></td>
								<td><span><?php echo $_smarty_tpl->getVariable('orderInfo')->value['email'];?>
</span></td>
							</tr>
							<tr>
								<td><b><?php echo $_smarty_tpl->getVariable('lang')->value['contact_phone'];?>
:</b></td>
								<td><span><?php echo $_smarty_tpl->getVariable('orderInfo')->value['phone'];?>
</span></td>
							</tr>
							<tr>
								<td colspan="2"><?php echo $_smarty_tpl->getVariable('orderInfo')->value['address'];?>
</td>
							</tr>
							<tr>
								<td><b><?php echo $_smarty_tpl->getVariable('lang')->value['cart_city'];?>
:</b></td>
								<td><span><?php echo $_smarty_tpl->getVariable('orderInfo')->value['city'];?>
</span></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="order_box">
					<h4><?php echo $_smarty_tpl->getVariable('lang')->value['forder_address_send'];?>
</h4>
					<div class="order_info">
			
						<table class="order_info">
							<tr>
								<td><b><?php echo $_smarty_tpl->getVariable('lang')->value['contact_name'];?>
:</b></td>
								<td><span><?php echo $_smarty_tpl->getVariable('orderInfo')->value['fullName'];?>
</span></td>
							</tr>
							<tr>
								<td><b>Email:</b></td>
								<td><span><?php echo $_smarty_tpl->getVariable('orderInfo')->value['email'];?>
</span></td>
							</tr>
							<tr>
								<td><b><?php echo $_smarty_tpl->getVariable('lang')->value['contact_phone'];?>
:</b></td>
								<td><span><?php echo $_smarty_tpl->getVariable('orderInfo')->value['phone'];?>
</span></td>
							</tr>
							<tr>
								<td colspan="2"><?php echo $_smarty_tpl->getVariable('orderInfo')->value['address'];?>
</td>
							</tr>
							<tr>
								<td><b><?php echo $_smarty_tpl->getVariable('lang')->value['cart_city'];?>
:</b></td>
								<td><span><?php echo $_smarty_tpl->getVariable('orderInfo')->value['city'];?>
</span></td>
							</tr>
						</table>
					</div>
				</div>
			
				<br />
				<?php if ($_smarty_tpl->getVariable('orderInfo')->value['description']){?>
					<div class="order_box">
						<h4><?php echo $_smarty_tpl->getVariable('lang')->value['forder_final'];?>
: </h4>
						<div class="order_info">
							<?php echo $_smarty_tpl->getVariable('orderInfo')->value['description'];?>

						</div>
					</div>
				<?php }?>
				<div class="clearfix">&nbsp;</div>
			</div>
		</div>
	</div>

</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>