<?php /* Smarty version Smarty-3.0.8, created on 2019-11-18 07:43:04
         compiled from "/home/balkanec/public_html/evinarts.com/application/views/header.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:17702311915dd22f6886b481-66752922%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5bcd5f67896a8e68b7fca2727f625d9e26e44eb8' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/application/views/header.tpl.html',
      1 => 1574055782,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17702311915dd22f6886b481-66752922',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/home/balkanec/public_html/evinarts.com/library/smarty/plugins/modifier.escape.php';
?><!doctype html>
<html class="has-jgallery has-visible-jgallery">
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-language" content="<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
" />
			
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<title><?php if (smarty_modifier_escape($_smarty_tpl->getVariable('meta')->value['title'])){?> <?php echo smarty_modifier_escape($_smarty_tpl->getVariable('meta')->value['title']);?>
 -<?php }?> <?php echo $_smarty_tpl->getVariable('lang')->value['meta_head_title'];?>
 evinarts.com</title>
		
		<meta name="description" content="<?php echo smarty_modifier_escape($_smarty_tpl->getVariable('meta')->value['description']);?>
" />
		<meta name="keywords" content="<?php echo smarty_modifier_escape($_smarty_tpl->getVariable('meta')->value['keywords']);?>
" />
		
		<meta name="robots" content="all,index,follow" />
		<meta name="GOOGLEBOT" content="index,follow" />
		
		<!-- <link rel="shortcut icon" href="../favicon.ico">  -->

		<link rel="icon" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
htdocs/images/favicon.png" type="image/png"/>
		<link rel="shortcut icon" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
htdocs/images/favicon.png" type="image/png"/>

		<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400italic,400,700,300&subset=latin,cyrillic' rel='stylesheet' type='text/css' />
		<link href='https://www.google.com/fonts#QuickUsePlace:quickUse/Family:Roboto+Slab:400,300' rel='stylesheet' type='text/css' />
		<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_css'];?>
bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_css'];?>
bootstrap-grid.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_css'];?>
bootstrap-reboot.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_css'];?>
style.css?a=3" />
		<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_css'];?>
tipsy.css" />
		
		<script  type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
jquery-1.11.1.js"></script>
		<script  type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
jquery.flexslider.js"></script>
		<script  type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
masonry.pkgd.min.js"></script>

		<!-- Bootstrap v4.3.1  -->
		<script  type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
bootstrap.min.js"></script>
		<script  type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
bootstrap.bundle.min.js"></script>
		
		<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
jquery.tipsy.js"></script>
		<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
global.js?a=5"></script>
		
		<?php if ($_smarty_tpl->getVariable('facebook')->value){?>
			<meta property="og:title" content="<?php echo $_smarty_tpl->getVariable('facebook')->value['name'];?>
" />
			<meta property="og:type" content="website" />
			<meta property="og:url" content="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/<?php echo $_smarty_tpl->getVariable('facebook')->value['link'];?>
" />
			<meta property="og:image" content="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
data/<?php echo $_smarty_tpl->getVariable('facebook')->value['picture'];?>
" />
			<meta property="og:site_name" content="<?php echo $_smarty_tpl->getVariable('config')->value['site_name'];?>
" />
			<meta property="fb:app_id" content="463900467095416" />
			<meta property="og:locale" content="bg_BG" />
		<?php }?>
		<?php if ($_smarty_tpl->getVariable('lang')->value['lg']=='fr'||$_smarty_tpl->getVariable('lang')->value['lg']=='en'){?>
		<style>
			.mainNavLink {
				padding:30px 16px 0 !important;
			}
		</style>
		<?php }?>
		<meta name="google-site-verification" content="nXJG-MQ77dj40S8hEoCuLaSE83-JcPXjJzoPYEqbjFI" />
	</head>
<body>
	<header>
		<div class="container">
			<nav class="navbar navbar-light navbar-expand-lg navbar-expand-md">
				<ul class="floatR" id="landNav">
					<li class="mainNav">
						<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/cart" class="basket" id="cartbasket">
							<span class="txtTitle"><?php echo $_smarty_tpl->getVariable('lang')->value['header_cart'];?>
</span>
							<span class="iconBasket"></span>
							<span class="itemCount" id="cartprod"><?php echo $_smarty_tpl->getVariable('cartInfo')->value['col'];?>
</span>
						</a>
					</li>
					<li class="mainNav"><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
bg" class="languages <?php if ($_smarty_tpl->getVariable('lang')->value['lg']=='bg'){?>active<?php }?>"><span class="BG"></span><span class="txtTitle">БГ</span></a></li>
					<li class="mainNav"><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
en" class="languages <?php if ($_smarty_tpl->getVariable('lang')->value['lg']=='en'){?>active<?php }?>"><span class="EN"></span><span class="txtTitle">EN</span></a></li>
					<li class="mainNav"><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
fr" class="languages-last <?php if ($_smarty_tpl->getVariable('lang')->value['lg']=='fr'){?>active<?php }?>"><span class="FR"></span><span class="txtTitle">FR</span></a></li>
				</ul>
				<input type="hidden" name="currentLang" value="<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
" id="idCurrentLang" />

				<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
" class="logo_mobile">
					<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
htdocs/images/logo_new.png">
				</a>
				
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			    <span></span>
			    <span></span>
			    <span></span>
			  </button>

			  <div class="collapse navbar-collapse" id="navbarNav">
					<ul id="mainNav">
						<li class="mainNav navMargins"><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
" class="mainNavLink <?php if ($_smarty_tpl->getVariable('page')->value=='home'){?> selectedItem<?php }?>"><?php echo $_smarty_tpl->getVariable('lang')->value['home'];?>
</a></li>
						<li class="mainNav navMargins"><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/aboutus" class="mainNavLink <?php if ($_smarty_tpl->getVariable('page')->value=='aboutus'){?> selectedItem<?php }?>"><?php echo $_smarty_tpl->getVariable('lang')->value['za_nas'];?>
</a></li>
						<li class="mainNav navMargins"><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/gallery" class="mainNavLink <?php if ($_smarty_tpl->getVariable('page')->value=='gallery'){?>selectedItem<?php }?>"><?php echo $_smarty_tpl->getVariable('lang')->value['gallery'];?>
</a></li>
						<li class="mainNav navMargins"><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
" class="logo"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
htdocs/images/logo_new.png"></a></li>
						<li class="mainNav navMargins"><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/decorations" class="mainNavLink <?php if ($_smarty_tpl->getVariable('page')->value=='decorations'){?>selectedItem<?php }?>"><?php echo $_smarty_tpl->getVariable('lang')->value['header_dekoracii'];?>
</a></li>
						<li class="mainNav navMargins hassub">
							<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/news" class="mainNavLink <?php if ($_smarty_tpl->getVariable('page')->value=='news'){?> selectedItem<?php }?>"><?php echo $_smarty_tpl->getVariable('lang')->value['header_events'];?>
</a>
							<ul class="subnav">
								<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/news/index/up/2"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_pred'];?>
</a></li>
								<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/news/index/up/1"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_last'];?>
</a></li>
							</ul>
						</li>
						<li class="mainNav navMargins"><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
<?php echo $_smarty_tpl->getVariable('lang')->value['lg'];?>
/contacts" class="mainNavLink <?php if ($_smarty_tpl->getVariable('page')->value=='contacts'){?>selectedItem<?php }?>"><?php echo $_smarty_tpl->getVariable('lang')->value['kontakti'];?>
</a></li>
					</ul>
				</div>
			</nav>
		</div>
	</header>
