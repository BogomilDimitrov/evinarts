<?php

	// Configuration
	ini_set('memory_limit', '1164M');
	ini_set('session.gc_maxlifetime', 3*60*60); // 3 hours
	ini_set('display_errors', 0); 
	ini_set('zend.ze1_compatibility_mode', 'off');
	
	// Developer mode
	$developers_ips = array('84.22.27.138');
	
	// Define root path
	define('ROOT_PATH', dirname(__FILE__) . '/');

	// Start the session
	session_start();
			
	// Load main framework classes
	require_once(ROOT_PATH . 'library/core/Controller.class.php');
	require_once(ROOT_PATH . 'library/core/Filter.class.php');
	require_once(ROOT_PATH . 'library/core/Database.class.php');
	require_once(ROOT_PATH . 'library/core/Registry.class.php');
	require_once(ROOT_PATH . 'library/core/Loader.class.php');
	require_once(ROOT_PATH . 'library/core/Language.class.php');
	require_once(ROOT_PATH . 'library/smarty/Smarty.class.php');

	// Load config and language files
	require_once(ROOT_PATH . 'application/config.php');
	
	// Start the template system
	$smarty = new Smarty();
	$smarty->template_dir = ROOT_PATH . 'application/views/';
	$smarty->compile_dir = ROOT_PATH . 'temp/views_c/';
	$smarty->config_dir = ROOT_PATH . 'temp/views_c/';
	$smarty->cache_dir = ROOT_PATH . 'temp/cache/';
	$smarty->debugging = false;
	$smarty->caching = false;
	$smarty->compile_check = true;
	$smarty->debugging = false;
	$smarty->cache_lifetime = 3600;

	// Initialize db object for the queries
	$db = new Database($db_config);
	
	// Initialize memcache

	// Start the registry
	Registry::set($db, 'db');
	Registry::set($memcache, 'memcache');
	Registry::set($smarty, 'smarty');
	Registry::set($config, 'config');
	
	// Load needed classes
	Loader::loadClass('System');
	Loader::loadClass('Crypt');
	//Loader::loadClass('Users');
	Loader::loadClass('Common');
	Loader::loadClass('Cart');
	
	// Parse url
	$rq = urldecode($_SERVER['REQUEST_URI']);
		
	// Get only first parameter
	$params = explode('/', substr($rq, 1));
	
	// Define language
	$lang_array = array('bg','en', 'fr');
	
	if(empty($params[0])) $params[0] = 'bg';
	
	// Define Language
	$langParam = (in_array($params[0], $lang_array))? $params[0] : 'bg';
	
	// Load the language file
	Registry::set(Language::load($langParam), 'lang');
	Registry::set($langParam, 'lg');
	
	// Дефинираме валутата
	if($langParam == 'bg') $_SESSION['currency'] = 'leva';
	else $_SESSION['currency'] = 'eur';
	
	// Assign system variables
	$smarty->assign('current_url', urlencode(substr($_SERVER['REQUEST_URI'], 1)));
	
	if (in_array($_SERVER['REMOTE_ADDR'], $developers_ips)){
		
		$smarty->assign('debug', true);
		
		// Override configs
		ini_set('display_errors', 1);
		
		// Run SQL Debugger
		$db->showSQLDebugger();
	}	
	
	// Calculate cart price 
	$cartInfo = Cart::getColPrice(session_id());
	$smarty->assign('cartInfo', $cartInfo);
	
	// Start the controllers management
	Loader::loadClass('Router');
	$controller = new Controller();
	$controller->start(Router::checkURL());
?>