<?php
// Localhost
$db_config = array(
    'host'        	=> 'localhost',
    'username'      => 'balkanec_evinart',
    'password'      => '123evin321',
    'database'      => 'balkanec_evinnew',
    'server_type'   => 'MySQL',
    'charset'       => 'utf8',
    'debugger'      => false
);

$config = array(
    'site_url'      => 'http://eadmin.evinarts.com/',
    'site_img'      => 'http://eadmin.evinarts.com/htdocs/images/',
    'site_css'      => 'http://eadmin.evinarts.com/htdocs/styles/',
    'site_js'       => 'http://eadmin.evinarts.com/htdocs/scripts/',
    'site_swf'      => 'http://eadmin.evinarts.com/htdocs/swf/',
	'site_data'     => 'https://evinarts.com/data/',
	'site_dir'      => '/home/balkanec/public_html/evinarts.com/eadmin/',
	'site_email'	=> 'no-reply@evinarts.com',
	'userarea_url'	=> 'https://evinarts.com/',
	'userarea_img'	=> 'https://evinarts.com/htdocs/images/',
	'userarea_css'	=> 'https://evinarts.com/htdocs/styles',
	'userarea_js'	=> 'https://evinarts.com/htdocs/scripts,',
	'userarea_swf'	=> 'https://evinarts.com/htdocs/swf/',
	'userarea_dir'	=> '/home/balkanec/public_html/evinarts.com/',
	'site_name'		=> 'Evinarts.com',
	'admin_url'		=> 'http://eadmin.evinarts.com/',
	'use_smtp'		=> false,
	'smtp_host'		=> '',
	'smtp_port'		=> 25,
	'smtp_user'		=> '',
	'smtp_pass'		=> '',
	'memcache_port'	=> 11211
);

// Site cache configuration array
/*$cache = array(
    'dir'           => '/var/www/vhosts/iranian.mvpower.net/temp/cache/',
    'lifetime'      => 600,
    'suffix'        => '_cache.txt'
);*/

// Settings
mb_internal_encoding('UTF-8');
?>