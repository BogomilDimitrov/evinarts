<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved
 *
 * Products controller
 *
 * @file            productsController.php
 * @category        Application
 * @author          Elenko Ivanow
 *
 */
Class ProductsController extends Controller {

    // Database object
    private $db = null;
    // Config array
    private $config = null;
    // Smarty object
    private $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;

    // If actions is not found
    public function _call() {
        System::redirect($this->config['site_url']);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // Get variables
        $this->db = Registry::get('db');
        $this->config = Registry::get('config');
        $this->smarty = Registry::get('smarty');
        $this->lang = Registry::get('lang');
        $this->cache = Registry::get('cache');
        $this->router = Registry::get("router");

        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);

        // If not logged - redirect to login page
        if (empty($_SESSION['admin'])) {
            System::redirect($this->config['site_url'] . "index/login");
        }

        // Check for messages or any post data
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
        if (!empty($_SESSION['post'])) {
            $this->smarty->assign("post", $_SESSION['post']);
            unset($_SESSION['post']);
        }
    }
	
    // Browse Products
    public function browseAction() {
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}

        // Load needed classes
		Loader::loadClass('Products');
		Loader::loadClass('Search');
        Loader::loadClass('Pager');
		
		// Get search query if any
        $query = (!empty($_POST['search'])) ? Filter::sanitize($_POST['search']) : Filter::sanitize(urldecode($this->getParam("search")));

        // If we're searching for something
        if (!empty($query)) {

            // Construct search query
            $url = $this->config['site_url'] . 'products/browse/search/' . urlencode($query) . '/page/[page]/';

            // Count posts
            $count = count(Search::searchProducts($query, null, null, 'GROUP BY p.idproduct'));
        }

        // If simply browsing the posts
        else {

            // Construct search query
             $url = $this->config['site_url'] . "products/browse/page/[page]/";

            // Count posts
            $count = Products::countProducts();
        }
		
        // Parameters for pagination
        $page = (int) $this->getParam("page");
        $page = (!empty($page)) ? $this->getParam("page") : 1;
        $this->smarty->assign("page", $page);
        $show = (int) $this->getParam("show", 20);
        
        // If posting - set page to first one
        if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
            $page = 1;
        }

        $pager = new Pager();
        $pager->replace = "[page]";
        $pager->page = $page;
        $pager->rows_per_page = $show;
        $pager->link = $url;

		// Get pager data
		$return = $pager->paginate($count);
		
		// Get Products
		 if (!empty($query)) {

            // Get products
            $products = Search::searchProducts($query, array('by' => 'p.date_added', 'type' => 'DESC'), array('from' => $pager->limit, 'count' => $pager->rows_per_page), 'GROUP BY p.idproduct');
        }

        // If simply browsing the products
        else {

            // Get products
            $products = Products::getProducts( array(), array('by' => 'p.idproduct', 'type' => 'DESC'), array('from' => $pager->limit, 'count' => $pager->rows_per_page) );
        }

        // Construct parse data
        $data = array(
			'page'		=> 'products all',
			'page_title'=> 'Всички картини',
            'products' 	=> $products,
            'pager' 	=> $return[0],
			'search'	=> $query,
        );

        // Assign values and display template
        $this->smarty->assign("data", $data);
        $this->smarty->display("products/browse.tpl.html");
    }
	
	// Browse by type products
    public function viewAction() {
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}
		
		// Get type 
		$typeView = $this->getParam('view');
		
        // Load needed classes
		Loader::loadClass("Products");
        Loader::loadClass("Pager");

        // Parameters for pagination
        $page = (int) $this->getParam("page");
        $page = (!empty($page)) ? $this->getParam("page") : 1;
        $this->smarty->assign("page", $page);
        $show = (int) $this->getParam("show", 20);

        // Set pagination url
        $url = $this->config['site_url'] . "products/view/" . $typeView . "/page/[page]/";
        

        // If posting - set page to first one
        if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
            $page = 1;
        }

        $pager = new Pager();
        $pager->replace = "[page]";
        $pager->page = $page;
        $pager->rows_per_page = $show;
        $pager->link = $url;

		if($typeView == 'new'){
			$condition = array('p.status' => 'new');
		}
		else if($typeView == 'promo'){
			$condition = array('p.status' => "in['promo','20'");
		}
		else if($typeView == 'inactive'){
			$condition = array('p.active' => "0");
		}
		
		// Count results
		$count = Products::countProducts($condition);

		// Get pager data
		$return = $pager->paginate($count);

		// Get Products
		$products = Products::getProducts( $condition, array('by' => 'p.idproduct', 'type' => 'DESC'), array('from' => $pager->limit, 'count' => $pager->rows_per_page) );

        // Construct parse data
        $data = array(
			'page'		=> 'products all',
			'page_title'=> 'Всички продукти',
            'products' 	=> $products,
            'pager' 	=> $return[0]
        );

        // Assign values and display template
        $this->smarty->assign("data", $data);
        $this->smarty->display("products/browse.tpl.html");
    }
	
	// Edit lang pages
	public function editlangAction(){
		
		// Get id
        $idproduct 	= $this->getParam('idproduct');
		$langsel 	= $this->getParam('langsel');

        // Load needed classes
        Loader::loadClass('Products');
			
		// Load needed classes
        Loader::loadClass('Common');
        Loader::loadClass('Filter');

        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
        	
			// Edit array
			$data = array(
				'title_' . $langsel 			=> Filter::sanitize($_POST['title_' . $langsel]),
				'alias_'. $langsel				=> Common::createURLstring($_POST['title_' . $langsel]),
				
				'content_' . $langsel 			=> Filter::sanitize($_POST['content_' . $langsel], 'p,strong,em,span,a,img,ul,ol,li,br,blockquote,embed,iframe,object,param,table,tbody,tr,td,th,style,class', true),
				'meta_title_' . $langsel		=> Filter::sanitize($_POST['meta_title_' . $langsel]),
				'meta_keywords_' . $langsel 	=> Filter::sanitize($_POST['meta_keywords_' . $langsel]),
				'meta_description_' . $langsel 	=> Filter::sanitize($_POST['meta_description_' . $langsel]),
			);

            // Update database
            $this->db->update('products', $data, "idproduct = ". $idproduct, false);

            // Assign success message
            $this->smarty->assign('success', $this->lang['changes_saved_successfully']);
		}
		
        // Get pages data
        $product = reset(Products::getProducts(array('idproduct' => $idproduct)));
		
		$product['title']				= $product['title_' . $langsel];
		$product['content']				= $product['content_' . $langsel];
		$product['meta_title']			= $product['meta_title_' . $langsel];
		$product['meta_keywords']		= $product['meta_keywords_' . $langsel];
		$product['meta_description']	= $product['meta_description_' . $langsel];
		
        // If no data - redirect to index page
        if (empty($product)) {

            // Redirect to index page
            System::redirect($this->config['site_url'] . 'products/browse');
        }
		
		// Assign values and display template
        $this->smarty->assign('product', $product);
		$this->smarty->assign('page_title', 'Продукти езици');
		$this->smarty->assign('langsel', $langsel);
		
        $this->smarty->display('products/editLang.tpl.html');
	}
	
    // Edit product
    public function editAction() {

        // Get idproduct
        $idproduct = $this->getParam('idproduct');

        // Load needed classes
        Loader::loadClass('Products');

        // Get product data
        $product = reset(Products::getProducts(array('idproduct' => $idproduct)));

        // If the username not given - redirect to index page
        if (empty($product)) {

            // Redirect to index page
            System::redirect($this->config['site_url']);
        }

        // Load needed classes
		Loader::loadClass('Category');
		Loader::loadClass('Common');
        Loader::loadClass('Filter');
	
        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
				
			$error = '';
			
			// Post data
			$post_data = array(
				'idcategory'	=> $_POST['idcategory'],
				'title_bg' 		=> Filter::sanitize($_POST['title_bg']),
				'content_bg' 	=> Filter::sanitize($_POST['content_bg'], 'p,strong,em,span,a,img,ul,ol,li,br,blockquote,embed,iframe,object,param,table,tbody,tr,td,th,style,class', true),
				'price_1'		=> $_POST['price_1'],
				'price_2'		=> $_POST['price_2'],
				'razmeri'		=> $_POST['razmeri'],
				'status'		=> $_POST['status'],
				'active' 		=> $_POST['active'],
			);
						
            // If all fields are ok
            if (empty($error)) {
			
				// Edit array
				$send_data = array(
					'idcategory'	=> $post_data['idcategory'],
					'title_bg' 		=> $post_data['title_bg'],
					'alias_bg'		=> Common::createURLstring($_POST['title_bg']),
					'content_bg' 	=> $post_data['content_bg'],
					'price_1'		=> $post_data['price_1'],
					'price_2'		=> $post_data['price_2'],
					'razmeri'		=> $post_data['razmeri'],
					'status'		=> $post_data['status'],
					'active' 		=> $post_data['active'],
				);

                // Update database
                $this->db->update('products', $send_data, "idproduct = '" . $product['idproduct'] . "'", false);

                // Get product data
                $product = reset(Products::getProducts(array('idproduct' => $product['idproduct'])));

                // Assign success message
                $this->smarty->assign('success', $this->lang['changes_saved_successfully']);
            }
        }
		
		// Get category
		$category = Category::getCategory(array('idparrent' => 0, 'active' => 1), array('by' => 'position', 'type' => 'ASC'));
			
		foreach($category as $key=>$cat) {
			$category[$key]['sub_category'] = Category::getCategory(array('idparrent' => $cat['idcategory']), array('by' => 'position', 'type' => 'ASC'));
		}
		
		$data = array(
			'product'		=> $product,
			'page_title'	=> 'Редактиране на продукт',
			'category_list' => $category,
		);
		
        // Assign values and display template
        $this->smarty->assign($data);
        $this->smarty->display('products/edit.tpl.html');
    }
	
	// Edit meta data
    public function editMetaAction() {

        // Get idproduct
        $idproduct = $this->getParam('idproduct');

        // Load needed classes
        Loader::loadClass('Products');

        // Get product data
        $product = reset(Products::getProducts(array('idproduct' => $idproduct)));

        // If the username not given - redirect to index page
        if (empty($product)) {

            // Redirect to index page
            System::redirect($this->config['site_url']);
        }

        // Load needed classes
        Loader::loadClass('Filter');
	
        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
				
			$error = '';
			
			// Post data
			$post_data = array(
				'meta_title_bg' 		=> Filter::sanitize($_POST['meta_title_bg']),
				'meta_description_bg'	=> Filter::sanitize($_POST['meta_description_bg']),
				'meta_keywords_bg'		=> Filter::sanitize($_POST['meta_keywords_bg']),
			);
						
            // If all fields are ok
            if (empty($error)) {
			
				// Edit array
				$send_data = array(
					'meta_title_bg' 		=> $post_data['meta_title_bg'],
					'meta_description_bg'	=> $post_data['meta_description_bg'],
					'meta_keywords_bg'		=> $post_data['meta_keywords_bg'],
				);

                // Update database
                $this->db->update('products', $send_data, "idproduct = '" . $product['idproduct'] . "'", false);

                // Get product data
                $product = reset(Products::getProducts(array('idproduct' => $product['idproduct'])));

                // Assign success message
                $this->smarty->assign('success', $this->lang['changes_saved_successfully']);
            }
        }
		
		$data = array(
			'product'		=> $product,
			'page_title'	=> 'Редактиране на продукт - Мета данни',
		);
		
        // Assign values and display template
        $this->smarty->assign($data);
        $this->smarty->display('products/edit_meta.tpl.html');
    }
	
	// Edit meta data
    public function editSizeAction() {

        // Get idproduct
        $idproduct = $this->getParam('idproduct');

        // Load needed classes
        Loader::loadClass('Products');

        // Get product data
        $product = reset(Products::getProducts(array('idproduct' => $idproduct)));

        // If the username not given - redirect to index page
        if (empty($product)) {

            // Redirect to index page
            System::redirect($this->config['site_url']);
        }

        // Load needed classes
        Loader::loadClass('Filter');
	
        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
				
			$error = '';
			
			// Edit array
			$send_data = array(
				'size' 		=> '',
				'colors'	=> '',
			);
			
			// Update size
			if($_POST['size']) $send_data['size'] = implode(',', $_POST['size']);
			
			// Update color
			if($_POST['color']) $send_data['colors'] = implode(',', $_POST['color']);

			// Update database
			$this->db->update('products', $send_data, "idproduct = '" . $product['idproduct'] . "'", false);

			// Get product data
			$product = reset(Products::getProducts(array('idproduct' => $product['idproduct'])));

			// Assign success message
			$this->smarty->assign('success', $this->lang['changes_saved_successfully']);
        }
		
		// Explode size
		if($product['size']){
			$size_list = explode(',', $product['size']);
			foreach($size_list as $k=>$v) 
				$product['size_list'][$v] = $v;
		}
		
		// Explode color
		if($product['colors']){
			$color_list = explode(',', $product['colors']);
			foreach($color_list as $k=>$v)
				$product['color_list'][$v] = $v;
		}
		
		// Всички размери
		$size_list = $this->db->fetchAll("SELECT * FROM `size` ORDER BY `position` ASC");
		foreach($size_list as $k=>$v){
			$size[$v['idsize']] = $v['title'];
		}
		
		$data = array(
			'page_title'	=> 'Редактиране на продукт - Размери',
			'product'		=> $product,
			'size'			=> $size,
		);
		
        // Assign values and display template
        $this->smarty->assign($data);
        $this->smarty->display('products/edit_size.tpl.html');
    }
	
	// Edit picture
    public function editPictureAction() {

        // Get idproduct
        $idproduct = $this->getParam('idproduct');

        // Load needed classes
        Loader::loadClass('Products');

        // Get product data
        $product = reset(Products::getProducts(array('idproduct' => $idproduct)));

        // If the username not given - redirect to index page
        if (empty($product)) {

            // Redirect to index page
            System::redirect($this->config['site_url']);
        }

        // Load needed classes
        Loader::loadClass('Filter');
	
        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
				
			$error = '';
			
			// Post data
			$post_data = array(
				'meta_title' 		=> Filter::sanitize($_POST['meta_title']),
				'meta_description'	=> Filter::sanitize($_POST['meta_description']),
				'meta_keywords'		=> Filter::sanitize($_POST['meta_keywords']),
			);
						
            // If all fields are ok
            if (empty($error)) {
			
				// Edit array
				$send_data = array(
					'meta_title' 		=> $post_data['meta_title'],
					'meta_description'	=> $post_data['meta_description'],
					'meta_keywords'		=> $post_data['meta_keywords'],
				);

                // Update database
                $this->db->update('products', $send_data, "idproduct = '" . $product['idproduct'] . "'", false);

                // Get product data
                $product = reset(Products::getProducts(array('idproduct' => $product['idproduct'])));

                // Assign success message
                $this->smarty->assign('success', $this->lang['changes_saved_successfully']);
            }
        }
		
		// Get all pictures
		$pictures = $this->db->fetchAll("SELECT * FROM `products_picture` WHERE `idproduct`=" . $product['idproduct']);
		
		$data = array(
			'product'		=> $product,
			'page_title'	=> 'Редактиране на продукт - Снимки',
			'pictures'		=> $pictures,
		);
		
        // Assign values and display template
        $this->smarty->assign($data);
        $this->smarty->display('products/edit_picture.tpl.html');
    }
	
	// Add product
	public function addAction(){
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}
	
		// Load needed classes
		Loader::loadClass('Category');
        Loader::loadClass('Common');
        Loader::loadClass('Filter');
        
        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
				
			$error = '';
			
			// Post data
			$post_data = array(
				'idcategory'	=> $_POST['idcategory'],
				'title_bg' 		=> Filter::sanitize($_POST['title_bg']),
				'content_bg' 	=> Filter::sanitize($_POST['content_bg'], 'p,strong,em,span,a,img,ul,ol,li,br,blockquote,embed,iframe,object,param,table,tbody,tr,td,th,style,class', true),
				'price_1'		=> $_POST['price_1'],
				'price_2'		=> $_POST['price_2'],
				'razmeri'		=> $_POST['razmeri'],
				'status'		=> $_POST['status'],
				'active' 		=> $_POST['active'],
			);
			
            // If all fields are ok
            if (empty($error)) {
			
				// Edit array
				$data = array(
					'idcategory'	=> $post_data['idcategory'],
					'title_bg' 		=> $post_data['title_bg'],
					'alias_bg'		=> Common::createURLstring($_POST['title_bg']),
					'content_bg' 	=> $post_data['content_bg'],
					'price_1'		=> $post_data['price_1'],
					'price_2'		=> $post_data['price_2'],
					'razmeri'		=> $post_data['razmeri'],
					'status'		=> $post_data['status'],
					'active' 		=> $post_data['active'],
					'meta_title_bg'	=> $post_data['title_bg'],
					'date_added'	=> date('Y-m-d H:i:s'),
				);
								
                // Update database
                $this->db->insert('products', $data);

                // Assign success message
				$_SESSION['success'] = $this->lang['changes_saved_successfully'];
				
				// Redirect back
				if($this->db->lastInsertId())
					System::redirect($this->config['site_url'] . "products/edit/idproduct/" . $this->db->lastInsertId() . "/");
            }

            // If we have errors
            else {

                // Assign error message
                $this->smarty->assign('error', $error);
            }
        }
		
		// Get category
		$category = Category::getCategory(array('idparrent' => 0, 'active' => 1), array('by' => 'position', 'type' => 'ASC'));
			
		foreach($category as $key=>$cat) {
			$category[$key]['sub_category'] = Category::getCategory(array('idparrent' => $cat['idcategory']), array('by' => 'position', 'type' => 'ASC'));
		}
		
		 // Construct parse data
        $data = array(
			'page'			=> 'products all',
			'page_title'	=> 'Добавяне на продукт',
            'category_list' => $category,
        );

        // Assign values and display template
        $this->smarty->assign($data);
        $this->smarty->display('products/add.tpl.html');
	}
	
    // Worker
    public function workerAction() {
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}

        // Get params
        $type = $this->getParam('type');
        $idtarget = (int) $this->getParam('idtarget');

        // If no params
        if (empty($type) || empty($idtarget)) {

            // Assign error message
            $_SESSION['error'] = 'Parameters missing!';

            // Redirect back
            System::redirect((!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $this->config['site_url']);
        }

        // Switch between object types
        switch ($type) {

            // Delete products
            case 'delete-products':

                // Get product id
                $idproduct = $idtarget;

                // Delete product
                if ($this->db->delete('products', "idproduct = '" . $idproduct . "'")) {
                    
					// Assign success message
                    $_SESSION['success'] = "Данните са изтрити успешно!";

                    // Redirect back
                    System::redirect((!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $this->config['site_url']);
                }

                // If not deleted
                else {

                    // Assign error message
                    $_SESSION['error'] = "Данните немогат да бъдат изтрити";

                    // Redirect to index page
                    System::redirect($this->config['site_url']);
                }
				
            break;
			
			case 'delete-picture':
			
				// Get product id
                $idproduct = $idtarget;

				// Get picture id
				$idpicture = (int) $this->getParam('idpicture');
				
				// Product info
				$productInfo = $this->db->fetchRow("SELECT * FROM `products` WHERE `idproduct`=" . $idproduct);
				
				// Picture info
				$pictureInfo = $this->db->fetchRow("SELECT * FROM `products_picture` WHERE `idpicture`=" . $idpicture);
				
                // Delete product
                if ($this->db->delete('products_picture', "idpicture = '" . $idpicture . "'")) {
                    
					if($productInfo['picture'] == $pictureInfo['picture']){
						// изтриване на главната снимка
						$this->db->query("UPDATE `products` SET `picture`='' WHERE `idproduct`=" . $idproduct);
					}
					
					// Assign success message
                    $_SESSION['success'] = "Снимката е изтрита успешно!";

                    // Redirect back
                    System::redirect( $this->config['site_url'].'products/editPicture/idproduct/' . $idproduct );
                }

                // If not deleted
                else {

                    // Assign error message
                    $_SESSION['error'] = "Снимката не може да бъде изтрита!";

                    // Redirect to index page
                    System::redirect($this->config['site_url']);
                }
				
			break;
        }
    }
	
}
?>