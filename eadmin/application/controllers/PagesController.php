<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved
 *
 * Pages controller
 *
 * @file            pagesController.php
 * @category        Application
 * @author          Elenko Ivanow
 *
 */
Class PagesController extends Controller {

    // Database object
    private $db = null;
    // Config array
    private $config = null;
    // Smarty object
    private $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;

    // If actions is not found
    public function _call() {
        System::redirect($this->config['site_url']);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // If no admin logged in
        if (empty($_SESSION['admin'])) {

            // Redirect to login page
            System::redirect($this->config['site_url']);
        }

        // Get variables
        $this->db = Registry::get('db');
        $this->config = Registry::get('config');
        $this->smarty = Registry::get('smarty');
        $this->lang = Registry::get('lang');
        $this->cache = Registry::get('cache');
        $this->router = Registry::get("router");

        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);

        // If not logged - redirect to login page
        if (empty($_SESSION['admin'])) {
            System::redirect($this->config['site_url'] . "index/login");
        }

        // Check for messages or any post data
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
        if (!empty($_SESSION['post'])) {
            $this->smarty->assign("post", $_SESSION['post']);
            unset($_SESSION['post']);
        }
    }
	
    // Browse pages
    public function browseAction() {
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}

        // Load needed classes
        Loader::loadClass("Pager");
		Loader::loadClass("Pages");

        // Parameters for pagination
        $page = (int) $this->getParam("page");
        $page = (!empty($page)) ? $this->getParam("page") : 1;
        $this->smarty->assign("page", $page);
        $show = (int) $this->getParam("show", 20);

        // Set pagination url
        $url = $this->config['site_url'] . "pages/browse/page/[page]/";
        

        // If posting - set page to first one
        if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
            $page = 1;
        }

        $pager = new Pager();
        $pager->replace = "[page]";
        $pager->page = $page;
        $pager->rows_per_page = $show;
        $pager->link = $url;


		// Count results
		$count = Pages::countPages();

		// Get pager data
		$return = $pager->paginate($count);

		// Get Pages
		$pagesList = Pages::getPages(array(), array('by' => 'id', 'type' => 'DESC'), array('from' => $pager->limit, 'count' => $pager->rows_per_page) );
        
        // Construct parse data
        $data = array(
			'page'		=> 'pages all',
			'page_title'=> 'Статични страници',
            'pagesList' => $pagesList,
            'pager' 	=> $return[0]
        );

        // Assign values and display template
        $this->smarty->assign("data", $data);
        $this->smarty->display("pages/browse.tpl.html");
    }
	
	// Edit lang pages
	public function editlangAction(){
		
		// Get id
        $id 		= $this->getParam('id');
		$langsel 	= $this->getParam('langsel');

        // Load needed classes
        Loader::loadClass('Pages');
			
		// Load needed classes
        Loader::loadClass('Common');
        Loader::loadClass('Filter');

        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
        	
			// Edit array
			$data = array(
				'title_' . $langsel 			=> Filter::sanitize($_POST['title_' . $langsel]),
				'content_' . $langsel 			=> Filter::sanitize($_POST['content_' . $langsel], 'p,strong,em,span,a,img,ul,ol,li,br,blockquote,embed,iframe,object,param,table,tbody,tr,td,th,style,class', true),
				'meta_title_' . $langsel		=> Filter::sanitize($_POST['meta_title_' . $langsel]),
				'meta_keywords_' . $langsel 	=> Filter::sanitize($_POST['meta_keywords_' . $langsel]),
				'meta_description_' . $langsel 	=> Filter::sanitize($_POST['meta_description_' . $langsel]),
			);

            // Update database
            $this->db->update('pages', $data, "id = ". $id, false);

            // Assign success message
            $this->smarty->assign('success', $this->lang['changes_saved_successfully']);
		}
		
        // Get pages data
        $pageInfo = reset(Pages::getPages(array('id' => $id)));
		
		$pageInfo['title']				= $pageInfo['title_' . $langsel];
		$pageInfo['content']			= $pageInfo['content_' . $langsel];
		$pageInfo['meta_title']			= $pageInfo['meta_title_' . $langsel];
		$pageInfo['meta_keywords']		= $pageInfo['meta_keywords_' . $langsel];
		$pageInfo['meta_description']	= $pageInfo['meta_description_' . $langsel];
		
        // If no data - redirect to index page
        if (empty($pageInfo)) {

            // Redirect to index page
            System::redirect($this->config['site_url']);
        }
		
		// Assign values and display template
        $this->smarty->assign('pageInfo', $pageInfo);
		$this->smarty->assign('langsel', $langsel);
		
        $this->smarty->display('pages/editLang.tpl.html');
	}
	
    // Edit pages
    public function editAction() {

        // Get id
        $id = $this->getParam('id');

        // Load needed classes
        Loader::loadClass('Pages');

        // Get pages data
        $pageInfo = reset(Pages::getPages(array('id' => $id)));

        // If no data - redirect to index page
        if (empty($pageInfo)) {

            // Redirect to index page
            System::redirect($this->config['site_url']);
        }

        // Load needed classes
        Loader::loadClass('Common');
        Loader::loadClass('Filter');

        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
				
			$error = '';
			
			// Post data
			$post_data = array(
				'title_bg' 				=> Filter::sanitize($_POST['title_bg']),
				'content_bg' 			=> Filter::sanitize($_POST['content_bg'], 'p,strong,em,span,a,img,ul,ol,li,br,blockquote,embed,iframe,object,param,table,tbody,tr,td,th,style,class', true),
				'meta_title_bg' 		=> Filter::sanitize($_POST['meta_title_bg']),
				'meta_keywords_bg' 		=> Filter::sanitize($_POST['meta_keywords_bg']),
				'meta_description_bg' 	=> Filter::sanitize($_POST['meta_description_bg']),
			);
						
            // If all fields are ok
            if (empty($error)) {
			
				// Edit array
				$data = array(
					'title_bg' 			=> $post_data['title_bg'],
					'content_bg' 		=> $post_data['content_bg'],
					'meta_title_bg'			=> $post_data['meta_title_bg'],
					'meta_keywords_bg' 		=> $post_data['meta_keywords_bg'],
					'meta_description_bg' 	=> $post_data['meta_description_bg'],
				);

                // Update database
                $this->db->update('pages', $data, "id = '" . $pageInfo['id'] . "'", false);

                // Get pages data
                $pageInfo = reset(Pages::getPages(array('id' => $pageInfo['id'])));

                // Assign success message
                $this->smarty->assign('success', $this->lang['changes_saved_successfully']);
            }

            // If we have errors
            else {

                // If posting merge posted data with predefined one
                if (!empty($post_data)) $pageInfo = array_merge($pageInfo, $post_data);

                // Assign error message
                $this->smarty->assign('error', $error);
            }
        }
	
        // Assign values and display template
        $this->smarty->assign('pageInfo', $pageInfo);
        $this->smarty->display('pages/edit.tpl.html');
    }	
}
?>