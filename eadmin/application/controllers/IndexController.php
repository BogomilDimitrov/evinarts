<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2012 All Rights Reserved
 *
 * Index controller
 *
 * @file            IndexController.php
 * @category        Application
 * @author          Jordan Trifonov
 *
 */
Class IndexController extends Controller {

    // Database object
    private $db = null;
    // Config array
    private $config = null;
    // Smarty object
    private $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;

    // If actions is not found
    public function _call() {
        System::redirect($this->config['site_url']);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // Get variables
        $this->db = Registry::get('db');
        $this->config = Registry::get('config');
        $this->system_config = Registry::get('system_config');
        $this->smarty = Registry::get('smarty');
        $this->lang = Registry::get('lang');
        $this->cache = Registry::get('cache');
        $this->router = Registry::get("router");

        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);
		
        // Check for messages or any post data
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
        if (!empty($_SESSION['post'])) {
            $this->smarty->assign("post", $_SESSION['post']);
            unset($_SESSION['post']);
        }
    }

    // Index action
    public function indexAction() {

        // If not logged - redirect to login page
        if (empty($_SESSION['admin'])) {

            // Redirect to login page
            System::redirect($this->config['site_url'] . 'index/login');
        }

        // Load needed classes
        Loader::loadClass('Posts');
		Loader::loadClass('Users');
		
		// count users
		$data['countusers'] = Users::countUsers();
		
		// count new orders
		//$data['countphotos'] = Posts::countPosts();
		
        // Assign values and display template
        $this->smarty->assign("data", $data);
        $this->smarty->display("index.tpl.html");
    }

    // Login action
    public function loginAction() {

        // If we have a cookie
        if (!empty($_COOKIE['admin'])) {
            $user_data = unserialize(base64_decode($_COOKIE['admin']));
            $_SESSION['admin'] = $user_data;
            System::redirect($this->config['site_url']);
        }

        // If logged - redirect to index page
        if (!empty($_SESSION['admin'])) {
            System::redirect($this->config['site_url']);
        }

        // If trying to login
        if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {

            // Load users model
            Loader::loadClass("Users");
            Loader::loadClass("Crypt");

            // Put data in array
            $data = array(
                "username" 	=> $_POST['username'],
                "password" 	=> Crypt::encrypt($_POST['password']),
                "type" 		=> "!user"
            );

            // Try to get the user
            $user_data = Users::getUsers($data);

            // If the data is correct
            if (!empty($user_data)) {
                $_SESSION['admin'] = reset($user_data);

                if (isset($_POST['rememberme'])) {
                    setcookie("admin", base64_encode(serialize(reset($user_data))), time() + 60 * 60 * 24 * 365);
                }

                System::redirect($this->config['site_url']);
            } else {
                $this->smarty->assign("error", "Wrong username or password");
            }
        }

        // Assign values and display template
        $this->smarty->display("login.tpl.html");
    }

    // Logout action
    public function logoutAction() {

        // Destroy all sessions
        session_destroy();

        // Unset the sessions
        unset($_SESSION['admin']);

        // Remove cookies
        setcookie("admin", "", time() - 3600);

        // Redirect back to the main page
        System::redirect($this->config['site_url']);
    }
}
?>