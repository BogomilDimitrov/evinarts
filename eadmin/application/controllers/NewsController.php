<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2014 All Rights Reserved
 *
 * News controller
 *
 * @file            newsController.php
 * @category        Application
 * @author          Elenko Ivanow
 *
 */
Class NewsController extends Controller {

    // Database object
    private $db = null;
    // Config array
    private $config = null;
    // Smarty object
    private $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;

    // If actions is not found
    public function _call() {
        System::redirect($this->config['site_url']);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // Get variables
        $this->db = Registry::get('db');
        $this->config = Registry::get('config');
        $this->smarty = Registry::get('smarty');
        $this->lang = Registry::get('lang');
        $this->cache = Registry::get('cache');
        $this->router = Registry::get("router");

        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);
		
		// Дефинираме типа на секцията
		$this->arrayTypeNews = array('news' => 'Новини');
		
        // If not logged - redirect to login page
        if (empty($_SESSION['admin'])) {
            System::redirect($this->config['site_url'] . "index/login");
        }

        // Check for messages or any post data
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
        if (!empty($_SESSION['post'])) {
            $this->smarty->assign("post", $_SESSION['post']);
            unset($_SESSION['post']);
        }
    }
	
    // Browse News
    public function browseAction() {
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}
		
        // Load needed classes
		Loader::loadClass('News');
		Loader::loadClass('Search');
        Loader::loadClass('Pager');
		
		// Get search query if any
        $query = (!empty($_POST['search'])) ? Filter::sanitize($_POST['search']) : Filter::sanitize(urldecode($this->getParam("search")));

        // If we're searching for something
        if (!empty($query)) {

            // Construct search query
            $url = $this->config['site_url'] . 'news/browse/search/' . urlencode($query) . '/page/[page]/';

            // Count posts
            $count = count(Search::searchNews($query, null, null, 'GROUP BY p.idnews'));
        }

        // If simply browsing the posts
        else {

            // Construct search query
             $url = $this->config['site_url'] . "news/browse/page/[page]/";

            // Count posts
            $count = News::countNews(array());
        }
		
        // Parameters for pagination
        $page = (int) $this->getParam("page");
        $page = (!empty($page)) ? $this->getParam("page") : 1;
        $this->smarty->assign("page", $page);
        $show = (int) $this->getParam("show", 20);
        
        // If posting - set page to first one
        if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
            $page = 1;
        }

        $pager = new Pager();
        $pager->replace = "[page]";
        $pager->page = $page;
        $pager->rows_per_page = $show;
        $pager->link = $url;

		// Get pager data
		$return = $pager->paginate($count);
		
		// Get News
		 if (!empty($query)) {

            // Get news
            $news = Search::searchNews($query, array('by' => 'n.idnews', 'type' => 'DESC'), array('from' => $pager->limit, 'count' => $pager->rows_per_page), 'GROUP BY n.idnews');
        }

        // If simply browsing the news
        else {

            // Get news
            $news = News::getNews( array(), array('by' => 'p.idnews', 'type' => 'DESC'), array('from' => $pager->limit, 'count' => $pager->rows_per_page) );
        }

        // Construct parse data
        $data = array(
			'page'		=> 'news all',
			'page_title'=> 'Всички Новини',
            'news' 		=> $news,
            'pager' 	=> $return[0],
			'search'	=> $query,
        );

        // Assign values and display template
        $this->smarty->assign($data);
        $this->smarty->display("news/browse.tpl.html");
    }
	
	// Browse by type products
    public function viewAction() {
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}
		
		// Get type 
		$typeView = $this->getParam('view');
		
        // Load needed classes
		Loader::loadClass("Products");
        Loader::loadClass("Pager");

        // Parameters for pagination
        $page = (int) $this->getParam("page");
        $page = (!empty($page)) ? $this->getParam("page") : 1;
        $this->smarty->assign("page", $page);
        $show = (int) $this->getParam("show", 20);

        // Set pagination url
        $url = $this->config['site_url'] . "products/view/" . $typeView . "/page/[page]/";
        

        // If posting - set page to first one
        if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
            $page = 1;
        }

        $pager = new Pager();
        $pager->replace = "[page]";
        $pager->page = $page;
        $pager->rows_per_page = $show;
        $pager->link = $url;

		if($typeView == 'new'){
			$condition = array('p.status' => 'new');
		}
		else if($typeView == 'promo'){
			$condition = array('p.status' => "in['promo','20'");
		}
		else if($typeView == 'inactive'){
			$condition = array('p.active' => "0");
		}
		
		// Count results
		$count = Products::countProducts($condition);

		// Get pager data
		$return = $pager->paginate($count);

		// Get Products
		$products = Products::getProducts( $condition, array('by' => 'p.idproduct', 'type' => 'DESC'), array('from' => $pager->limit, 'count' => $pager->rows_per_page) );

        // Construct parse data
        $data = array(
			'page'		=> 'products all',
			'page_title'=> 'Всички продукти',
            'products' 	=> $products,
            'pager' 	=> $return[0]
        );

        // Assign values and display template
        $this->smarty->assign("data", $data);
        $this->smarty->display("products/browse.tpl.html");
    }
	
	// Edit lang pages
	public function editlangAction(){
		
		// Get id
        $idnews 		= $this->getParam('idnews');
		$langsel 	= $this->getParam('langsel');

        // Load needed classes
        Loader::loadClass('News');
			
		// Load needed classes
        Loader::loadClass('Common');
        Loader::loadClass('Filter');

        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
        	
			// Edit array
			$data = array(
				'title_' . $langsel 			=> Filter::sanitize($_POST['title_' . $langsel]),
				'alias_'. $langsel				=> Common::createURLstring($_POST['title_' . $langsel]),
				
				'content_' . $langsel 			=> Filter::sanitize($_POST['content_' . $langsel], 'p,strong,em,span,a,img,ul,ol,li,br,blockquote,embed,iframe,object,param,table,tbody,tr,td,th,style,class', true),
				'meta_title_' . $langsel		=> Filter::sanitize($_POST['meta_title_' . $langsel]),
				'meta_keywords_' . $langsel 	=> Filter::sanitize($_POST['meta_keywords_' . $langsel]),
				'meta_description_' . $langsel 	=> Filter::sanitize($_POST['meta_description_' . $langsel]),
			);

            // Update database
            $this->db->update('news', $data, "idnews = ". $idnews, false);

            // Assign success message
            $this->smarty->assign('success', $this->lang['changes_saved_successfully']);
		}
		
        // Get pages data
        $news = reset(News::getNews(array('idnews' => $idnews)));
		
		$news['title']				= $news['title_' . $langsel];
		$news['content']			= $news['content_' . $langsel];
		$news['meta_title']			= $news['meta_title_' . $langsel];
		$news['meta_keywords']		= $news['meta_keywords_' . $langsel];
		$news['meta_description']	= $news['meta_description_' . $langsel];
		
        // If no data - redirect to index page
        if (empty($news)) {

            // Redirect to index page
            System::redirect($this->config['site_url'] . 'news/browse');
        }
		
		// Assign values and display template
        $this->smarty->assign('news', $news);
		$this->smarty->assign('langsel', $langsel);
		
        $this->smarty->display('news/editLang.tpl.html');
	}
	
    // Edit news
    public function editAction() {

        // Get idnews
        $idnews = $this->getParam('idnews');
		
        // Load needed classes
        Loader::loadClass('News');

        // Get news data
        $news = reset(News::getNews(array('idnews' => $idnews)));

        // If the username not given - redirect to index page
        if (empty($news)) {

            // Redirect to index page
            System::redirect($this->config['site_url'] . 'news');
        }

        // Load needed classes
		Loader::loadClass('Common');
        Loader::loadClass('Filter');
	
        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
				
			$error = '';
			
			// Post data
			$post_data = array(
				'title_bg' 		=> Filter::sanitize($_POST['title_bg']),
				'content_bg' 	=> Filter::sanitize($_POST['content_bg'], 'p,strong,em,span,a,img,ul,ol,li,br,blockquote,embed,iframe,object,param', true),
				'status'		=> $_POST['status'],
				'upcoming'		=> $_POST['upcoming'],
			);
			
            // If all fields are ok
            if (empty($error)) {
				
				// Edit array
				$data = array(
					'alias_bg'		=> Common::createURLstring($_POST['title_bg']),
					'title_bg' 		=> $post_data['title_bg'],
					'content_bg' 	=> $post_data['content_bg'],
					'status'		=> $post_data['status'],
					'upcoming'		=> $post_data['upcoming'],
				);
				
				if($_POST['date_added']){
					$data['date_added'] = $_POST['date_added'];
				}
				
                // Update database
                $this->db->update('news', $data, "idnews = '" . $news['idnews'] . "'", false);

                // Get news data
                $news = reset(News::getNews(array('idnews' => $news['idnews'])));
				
                // Assign success message
                $this->smarty->assign('success', $this->lang['changes_saved_successfully']);
            }
        }
		
		$data = array(
			'news'			=> $news,
			'page_title'	=> 'Редактиране на Новини',
		);
		
        // Assign values and display template
        $this->smarty->assign($data);
        $this->smarty->display('news/edit.tpl.html');
    }
	
	// Edit meta data
    public function editMetaAction() {

        // Get id news
        $idnews = $this->getParam('idnews');
		
        // Load needed classes
        Loader::loadClass('News');

        // Get news data
        $news = reset(News::getNews(array('idnews' => $idnews)));

        // If the username not given - redirect to index page
        if (empty($news)) {

            // Redirect to index page
            System::redirect($this->config['site_url'] . 'news');
        }

        // Load needed classes
        Loader::loadClass('Filter');
	
        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
				
			$error = '';
			
			// Post data
			$post_data = array(
				'meta_title_bg' 		=> Filter::sanitize($_POST['meta_title_bg']),
				'meta_description_bg'	=> Filter::sanitize($_POST['meta_description_bg']),
				'meta_keywords_bg'		=> Filter::sanitize($_POST['meta_keywords_bg']),
			);
						
            // If all fields are ok
            if (empty($error)) {
			
                // Update database
                $this->db->update('news', $post_data, "idnews = '" . $news['idnews'] . "'", false);

                // Get news data
                $news = reset(News::getNews(array('idnews' => $news['idnews'])));

                // Assign success message
                $this->smarty->assign('success', $this->lang['changes_saved_successfully']);
            }
        }
		
		$data = array(
			'news'			=> $news,
			'page_title'	=> 'Редактиране на Новини - Мета данни',
		);
		
        // Assign values and display template
        $this->smarty->assign($data);
        $this->smarty->display('news/edit_meta.tpl.html');
    }
	
	// Edit picture
    public function editPictureAction() {

        // Get id news
        $idnews 	= $this->getParam('idnews');

        // Load needed classes
        Loader::loadClass('News');

        // Get news data
        $news = reset(News::getNews(array('idnews' => $idnews)));

        // If the username not given - redirect to index page
        if (empty($news)) {

            // Redirect to index page
            System::redirect($this->config['site_url'] . 'news');
        }

        // Load needed classes
        Loader::loadClass('Filter');
		
		// Get all pictures
		$pictures = $this->db->fetchAll("SELECT * FROM `news_picture` WHERE `idnews`=" . $news['idnews']);

		$data = array(
			'news'		=> $news,
			'page_title'	=> 'Редактиране на Новина - Снимки',
			'pictures'		=> $pictures,
		);
		
        // Assign values and display template
        $this->smarty->assign($data);
        $this->smarty->display('news/edit_picture.tpl.html');
    }
	
	// Add news
	public function addAction(){
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}
		
		// Load needed classes
        Loader::loadClass('Common');
        Loader::loadClass('Filter');
        
        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
				
			$error = '';
			
			// Post data
			$post_data = array(
				'title_bg' 		=> Filter::sanitize($_POST['title_bg']),
				'content_bg' 	=> Filter::sanitize($_POST['content_bg'], 'p,strong,em,span,a,img,ul,ol,li,br,blockquote,embed,iframe,object,param', true),
				'status'		=> $_POST['status'],
			);
			
            // If all fields are ok
            if (empty($error)) {
			
				// Edit array
				$data = array(
					'alias_bg'		=> Common::createURLstring($_POST['title_bg']),
					'title_bg' 		=> $post_data['title_bg'],
					'content_bg' 	=> $post_data['content_bg'],
					'meta_title_bg'	=> $post_data['title_bg'],
					'status'		=> $post_data['status'],
					'iduser'		=> $_SESSION['admin']['iduser'],
					'ip_added'		=> $_SERVER['REMOTE_ADDR'],
					'date_added' 	=> date('Y-m-d H:i:s'),
				);
				
				if($_POST['date_added']){
					$data['date_added'] = $_POST['date_added']; 
				}
				
                // Update database
                $this->db->insert('news', $data);

                // Assign success message
				$_SESSION['success'] = $this->lang['changes_saved_successfully'];
				
				// Redirect back
				System::redirect($this->config['site_url'] . "news/edit/idnews/" . $this->db->lastInsertId() . "/");
            }

            // If we have errors
            else {

                // Assign error message
                $this->smarty->assign('error', $error);
            }
        }
		
		 // Construct parse data
        $data = array(
			'page'			=> 'news all',
			'page_title'	=> 'Добавяне на Новини',
        );

        // Assign values and display template
        $this->smarty->assign($data);
        $this->smarty->display('news/add.tpl.html');
	}
	
    // Worker
    public function workerAction() {
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}

        // Get params
        $type = $this->getParam('type');
        $idtarget = (int) $this->getParam('idtarget');

        // If no params
        if (empty($type) || empty($idtarget)) {

            // Assign error message
            $_SESSION['error'] = 'Parameters missing!';

            // Redirect back
            System::redirect((!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $this->config['site_url']);
        }

        // Switch between object types
        switch ($type) {
			
            // Delete news
            case 'delete-news':

                // Get news id
                $idnews = $idtarget;

                // Delete news
                if ($this->db->delete('news', "idnews = '" . $idnews . "'")) {
                    
					// Assign success message
                    $_SESSION['success'] = "Данните са изтрити успешно!";

                    // Redirect back
                    System::redirect((!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $this->config['site_url']);
                }

                // If not deleted
                else {

                    // Assign error message
                    $_SESSION['error'] = "Данните немогат да бъдат изтрити";

                    // Redirect to index page
                    System::redirect($this->config['site_url']);
                }
				
            break;
			
			case 'delete-picture':
			
				// Get news id
                $idnews = $idtarget;

				// Get picture id
				$idpicture = (int) $this->getParam('idpicture');
				
				// News info
				$newsInfo = $this->db->fetchRow("SELECT * FROM `news` WHERE `idnews`=" . $idnews);
				
				// Picture info
				$pictureInfo = $this->db->fetchRow("SELECT * FROM `news_picture` WHERE `idpicture`=" . $idpicture);
				
                // Delete product
                if ($this->db->delete('news_picture', "idpicture = '" . $idpicture . "'")) {
                    
					if($newsInfo['picture'] == $pictureInfo['picture']){
						// изтриване на главната снимка
						$this->db->query("UPDATE `news` SET `picture`='' WHERE `idnews`=" . $idnews);
					}
					
					// Assign success message
                    $_SESSION['success'] = "Снимката е изтрита успешно!";

                    // Redirect back
                    System::redirect( $this->config['site_url'] . 'news/editPicture/idnews/' . $idnews );
                }

                // If not deleted
                else {

                    // Assign error message
                    $_SESSION['error'] = "Снимката не може да бъде изтрита!";

                    // Redirect to index page
                    System::redirect($this->config['site_url'] . 'news/editPicture/idnews/' . $idnews);
                }
				
			break;
        }
    }
	
}
?>