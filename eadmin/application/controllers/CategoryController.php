<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved
 *
 * Category controller
 *
 * @file            CategoryController.php
 * @category        Application
 * @author          Elenko Ivanow
 *
 */
Class CategoryController extends Controller {

    // Database object
    private $db = null;
    // Config array
    private $config = null;
    // Smarty object
    private $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;

    // If actions is not found
    public function _call() {
        System::redirect($this->config['site_url']);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // If no admin logged in
        if (empty($_SESSION['admin'])) {

            // Redirect to login page
            System::redirect($this->config['site_url']);
        }

        // Get variables
        $this->db = Registry::get('db');
        $this->config = Registry::get('config');
        $this->smarty = Registry::get('smarty');
        $this->lang = Registry::get('lang');
        $this->cache = Registry::get('cache');
        $this->router = Registry::get("router");

        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);

        // If not logged - redirect to login page
        if (empty($_SESSION['admin'])) {
            System::redirect($this->config['site_url'] . "index/login");
        }

        // Check for messages or any post data
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
        if (!empty($_SESSION['post'])) {
            $this->smarty->assign("post", $_SESSION['post']);
            unset($_SESSION['post']);
        }
    }
	
    // Browse Brands
    public function browseAction() {
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}

        // Load needed classes
        Loader::loadClass("Category");
		Loader::loadClass("Pager");

        // Get search query
        $search = (!empty($_POST['search'])) ? Filter::sanitize($_POST['search']) : Filter::sanitize(urldecode($this->getParam("search")));

        // Parameters for pagination
        $page = (int) $this->getParam("page");
        $page = (!empty($page)) ? $this->getParam("page") : 1;
        $this->smarty->assign("page", $page);
        $show = (int) $this->getParam("show", 20);

        // If searching for user
        if (!empty($search)) {

            // Set pagination url
            $url = $this->config['site_url'] . "category/browse/search/" . urlencode($search) . "/page/[page]/";
        }

        // Just browsing the category
        else {

            // Set pagination url
            $url = $this->config['site_url'] . "category/browse/page/[page]/";
        }

        // If posting - set page to first one
        if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
            $page = 1;
        }

        $pager = new Pager();
        $pager->replace = "[page]";
        $pager->page = $page;
        $pager->rows_per_page = $show;
        $pager->link = $url;

        // If searching for brand
        if (!empty($search)) {

			// Count results
			$count = $this->db->fetchOne("SELECT COUNT(idcategory) FROM `category` WHERE title LIKE '%" . $search . "%'");
			
			// Get pager data
			$return = $pager->paginate($count);
			
			// Get category
			$categorys = $this->db->fetchAll("SELECT b.*
											FROM `category` AS b WHERE b.title LIKE '%" . $search . "%' 
											ORDER BY b.idcategory DESC 
											LIMIT " . $pager->limit . ", " . $pager->rows_per_page);
			
        }

        // Just browsing the category
        else {

            // Count results
            $count = $this->db->fetchOne("SELECT COUNT(idcategory) FROM `category` WHERE `idparrent`=0");

            // Get pager data
            $return = $pager->paginate($count);

            // Get category
            $category = $this->db->fetchAll("SELECT b.*
											FROM `category` AS b 
											WHERE `idparrent`=0
											ORDER BY b.idcategory DESC 
											LIMIT " . $pager->limit . ", " . $pager->rows_per_page);
			
			// Get parrent category
			foreach($category as $key=>$cat) {
				$category[$key]['sub_category'] = Category::getCategory(array('idparrent' => $cat['idcategory']), array('by' => 'position', 'type' => 'ASC'));
			}
		}

        // Construct parse data
        $data = array(
			'page'			=> 'category all',
			'page_title'	=> 'Всички категории',
            'category' 		=> $category,
            'pager' 		=> $return[0],
            'search' 		=> $search
        );

        // Assign values and display template
        $this->smarty->assign("data", $data);
        $this->smarty->display("category/browse.tpl.html");
    }

    // Edit category
    public function editAction() {

        // Get idcategory
        $idcategory = $this->getParam('idcategory');

        // Load needed classes
        Loader::loadClass('Category');

        // Get category data
        $category = reset(Category::getCategory(array('idcategory' => $idcategory)));

        // If the category not given - redirect to index page
        if (empty($category)) {

            // Redirect to index page
            System::redirect($this->config['site_url']);
        }

        // Load needed classes
        Loader::loadClass('Common');
        Loader::loadClass('Filter');

        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
				
			$error = '';
			
			// Post data
			$post_data = array(
				'title_bg' 		=> Filter::sanitize($_POST['title_bg']),
				'content_bg' 	=> Filter::sanitize($_POST['content_bg']),
				'idparrent'		=> $_POST['idparrent'],
				'position' 		=> $_POST['position'],
				'active' 		=> $_POST['active'],
				'meta_title_bg' 		=> Filter::sanitize($_POST['meta_title_bg']),
				'meta_keywords_bg' 		=> Filter::sanitize($_POST['meta_keywords_bg']),
				'meta_description_bg' 	=> Filter::sanitize($_POST['meta_description_bg']),
			);
			
			
            // If all fields are ok
            if (empty($error)) {
			
				// Edit array
				$data = array(
					'title_bg' 		=> $post_data['title_bg'],
					'slug_bg'		=> Common::createURLstring($_POST['title_bg']),
					'content_bg' 	=> $post_data['content_bg'],
					'idparrent'		=> $post_data['idparrent'],
					'position'		=> $post_data['position'],
					'active' 		=> $post_data['active'],
					
					'meta_title_bg'			=> $post_data['meta_title_bg'],
					'meta_keywords_bg' 		=> $post_data['meta_keywords_bg'],
					'meta_description_bg' 	=> $post_data['meta_description_bg'],
				);

                // Update database
                $this->db->update('category', $data, "idcategory = '" . $category['idcategory'] . "'", false);

                // Get category data
                $category = reset(Category::getCategory(array('idcategory' => $category['idcategory'])));

                // Assign success message
                $this->smarty->assign('success', $this->lang['changes_saved_successfully']);
            }

            // If we have errors
            else {

                // If posting merge posted data with predefined one
                if (!empty($post_data)) $category = array_merge($category, $post_data);

                // Assign error message
                $this->smarty->assign('error', $error);
            }
        }
		
		// Main category
		$category_list = Category::getCategory(array('idparrent' => 0), array('by' => 'position', 'type' => 'ASC'));
		
		// Assign values and display template
		$this->smarty->assign('category_list', $category_list);
        $this->smarty->assign('category', $category);
		
        $this->smarty->display('category/edit.tpl.html');
    }
	
	// Edit lang pages
	public function editlangAction(){
		
		// Get idcategory
        $idcategory 		= $this->getParam('idcategory');
		$langsel 			= $this->getParam('langsel');

        // Load needed classes
        Loader::loadClass('Category');
			
		// Load needed classes
        Loader::loadClass('Common');
        Loader::loadClass('Filter');

        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
        	
			// Edit array
			$data = array (
				'title_' . $langsel 			=> Filter::sanitize($_POST['title_' . $langsel]),
				'content_' . $langsel 			=> Filter::sanitize($_POST['content_' . $langsel]),
				'meta_title_' . $langsel		=> Filter::sanitize($_POST['meta_title_' . $langsel]),
				'meta_keywords_' . $langsel 	=> Filter::sanitize($_POST['meta_keywords_' . $langsel]),
				'meta_description_' . $langsel 	=> Filter::sanitize($_POST['meta_description_' . $langsel]),
				'slug_'. $langsel				=> Common::createURLstring($_POST['title_' . $langsel]),
			);

            // Update database
            $this->db->update('category', $data, "idcategory = ". $idcategory, false);

            // Assign success message
            $this->smarty->assign('success', $this->lang['changes_saved_successfully']);
		}
		
        // Get pages data
        $category = reset(Category::getCategory(array('idcategory' => $idcategory)));
		
		$category['title']				= $category['title_' . $langsel];
		$category['content']			= $category['content_' . $langsel];
		$category['meta_title']			= $category['meta_title_' . $langsel];
		$category['meta_keywords']		= $category['meta_keywords_' . $langsel];
		$category['meta_description']	= $category['meta_description_' . $langsel];
								
        // If no data - redirect to index page
        if (empty($category)) {

            // Redirect to index page
            System::redirect($this->config['site_url'] . 'category');
        }
		
		// Assign values and display template
        $this->smarty->assign('category', $category);
		$this->smarty->assign('langsel', $langsel);
		
        $this->smarty->display('category/editLang.tpl.html');
	}
	
	// Add brand
	public function addAction(){
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}
	
		// Load needed classes
        Loader::loadClass('Category');
        Loader::loadClass('Common');
        Loader::loadClass('Filter');

        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
				
			$error = '';
			
			// Post data
			$post_data = array(
				'title_bg' 		=> Filter::sanitize($_POST['title_bg']),
				'content_bg' 	=> Filter::sanitize($_POST['content_bg']),
				'idparrent'		=> $_POST['idparrent'],
				'position'		=> $_POST['position'],
				'active' 		=> $_POST['active'],
			);			
			
            // If all fields are ok
            if (empty($error)) {
			
				// Edit array
				$data = array(
					'title_bg' 		=> $post_data['title_bg'],
					'content_bg' 	=> $post_data['content_bg'],
					'slug_bg'		=> Common::createURLstring($_POST['title_bg']),
					'idparrent'		=> $post_data['idparrent'],
					'position'		=> $post_data['position'],
					'active' 		=> $post_data['active'],
					'meta_title_bg' => $post_data['title_bg'],
				);

                // Update database
                $this->db->insert('category', $data);

                // Assign success message
				$_SESSION['success'] = $this->lang['changes_saved_successfully'];
				
				// Redirect back
				System::redirect($this->config['site_url'] . "category/edit/idcategory/" . $this->db->lastInsertId());
            }

            // If we have errors
            else {

                // If posting merge posted data with predefined one
                if (!empty($post_data)) $category = array_merge($category, $post_data);

                // Assign error message
                $this->smarty->assign('error', $error);
            }
        }
		
		// Main category
		$category_list = Category::getCategory(array('idparrent' => 0), array('by' => 'position', 'type' => 'ASC'));
		
		// Assign values and display template
		$this->smarty->assign('category_list', $category_list);
        $this->smarty->assign('category', $category);
		
        $this->smarty->display('category/add.tpl.html');
	}
	
    // Worker
    public function workerAction() {
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}

        // Get params
        $type = $this->getParam('type');
        $idtarget = (int) $this->getParam('idtarget');

        // If no params
        if (empty($type) || empty($idtarget)) {

            // Assign error message
            $_SESSION['error'] = 'Parameters missing!';

            // Redirect back
            System::redirect((!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $this->config['site_url']);
        }

        // Switch between object types
        switch ($type) {

            // Delete brand
            case 'delete-category':

                // Get brand id
                $idcategory = $idtarget;

                // Delete brand
                if ($this->db->delete('category', "idcategory = '" . $idcategory . "'")) {
                    
					// Assign success message
                    $_SESSION['success'] = "Категорията е изтрита успешно!";

                    // Redirect back
                    System::redirect((!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $this->config['site_url']);
                }

                // If not deleted
                else {

                    // Assign error message
                    $_SESSION['error'] = "Категорията не може да бъде изтрита!";

                    // Redirect to index page
                    System::redirect($this->config['site_url']);
                }
				
            break;
        }
    }
	
}

?>