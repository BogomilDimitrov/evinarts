<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved
 *
 * Users controller
 *
 * @file            UsersController.php
 * @category        Application
 * @author          Elenko Ivanow
 *
 */
Class UsersController extends Controller {

    // Database object
    private $db = null;
    // Config array
    private $config = null;
    // Smarty object
    private $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;

    // If actions is not found
    public function _call() {
        System::redirect($this->config['site_url']);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // If no admin logged in
        if (empty($_SESSION['admin'])) {

            // Redirect to login page
            System::redirect($this->config['site_url']);
        }

        // Get variables
        $this->db = Registry::get('db');
        $this->config = Registry::get('config');
        $this->smarty = Registry::get('smarty');
        $this->lang = Registry::get('lang');
        $this->cache = Registry::get('cache');
        $this->router = Registry::get("router");

        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);

        // If not logged - redirect to login page
        if (empty($_SESSION['admin'])) {
            System::redirect($this->config['site_url'] . "index/login");
        }

        // Check for messages or any post data
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
        if (!empty($_SESSION['post'])) {
            $this->smarty->assign("post", $_SESSION['post']);
            unset($_SESSION['post']);
        }
    }
	
	// New users
	public function newAction() {
		
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}

        // Load needed classes
        Loader::loadClass("Users");
        Loader::loadClass("Pager");
		
		// Parameters for pagination
        $page = (int) $this->getParam("page");
        $page = (!empty($page)) ? $this->getParam("page") : 1;
        $this->smarty->assign("page", $page);
        $show = (int) $this->getParam("show", 20);
		
		// Set pagination url
        $url = $this->config['site_url'] . "users/new/page/[page]/";
		
		$pager = new Pager();
        $pager->replace = "[page]";
        $pager->page = $page;
        $pager->rows_per_page = $show;
        $pager->link = $url;
		
		// Count results
		$count = $this->db->fetchOne("SELECT COUNT(iduser) FROM `users` WHERE active='2' ");

		// Get pager data
		$return = $pager->paginate($count);

		// Get users
		$users = $this->db->fetchAll("SELECT u.*
										FROM `users` AS u 
										WHERE u.active = '2'
										ORDER BY u.iduser DESC 
										LIMIT " . $pager->limit . ", " . $pager->rows_per_page);

		 // Construct parse data
        $data = array(
			'page'		=> 'users new',
			'page_title'=> $this->lang['users_all_new'],
            'users' 	=> $users,
            'pager' 	=> $return[0]
        );

        // Assign values and display template
        $this->smarty->assign("data", $data);
        $this->smarty->display("users/browse.tpl.html");
	}
	
    // Browse users
    public function browseAction() {
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}

        // Load needed classes
        Loader::loadClass("Users");
        Loader::loadClass("Pager");

        // Get search query
        $search = (!empty($_POST['search'])) ? Filter::sanitize($_POST['search']) : Filter::sanitize(urldecode($this->getParam("search")));

        // Parameters for pagination
        $page = (int) $this->getParam("page");
        $page = (!empty($page)) ? $this->getParam("page") : 1;
        $this->smarty->assign("page", $page);
        $show = (int) $this->getParam("show", 20);

        // If searching for user
        if (!empty($search)) {

            // Set pagination url
            $url = $this->config['site_url'] . "users/browse/search/" . urlencode($search) . "/page/[page]/";
        }

        // Just browsing the users
        else {

            // Set pagination url
            $url = $this->config['site_url'] . "users/browse/page/[page]/";
        }

        // If posting - set page to first one
        if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
            $page = 1;
        }

        $pager = new Pager();
        $pager->replace = "[page]";
        $pager->page = $page;
        $pager->rows_per_page = $show;
        $pager->link = $url;

        // If searching for user
        if (!empty($search)) {

            // Searching by email
            if (strstr($search, '@')) {

                // Count results
                $count = $this->db->fetchOne("SELECT COUNT(iduser) FROM `users` WHERE email LIKE '%" . $search . "%'");

                // Get pager data
                $return = $pager->paginate($count);

                // Get users
                $users = $this->db->fetchAll("SELECT u.*
                                            FROM `users` AS u 
                                            WHERE u.email LIKE '%" . $search . "%' 
                                            ORDER BY u.iduser DESC 
                                            LIMIT " . $pager->limit . ", " . $pager->rows_per_page);
            }
            // Searching by username
            else {

                // Count results
                $count = $this->db->fetchOne("SELECT COUNT(iduser) FROM `users` WHERE username LIKE '%" . $search . "%'");

                // Get pager data
                $return = $pager->paginate($count);
				
                // Get users
                $users = $this->db->fetchAll("SELECT u.*
												FROM `users` AS u WHERE u.username LIKE '%" . $search . "%' 
												ORDER BY u.iduser DESC 
												LIMIT " . $pager->limit . ", " . $pager->rows_per_page);
            }
        }

        // Just browsing the users
        else {

            // Count results
            $count = $this->db->fetchOne("SELECT COUNT(iduser) FROM `users`");

            // Get pager data
            $return = $pager->paginate($count);

            // Get users
            $users = $this->db->fetchAll("SELECT u.*
											FROM `users` AS u 
											ORDER BY u.iduser DESC 
											LIMIT " . $pager->limit . ", " . $pager->rows_per_page);
        }

        // Construct parse data
        $data = array(
			'page'		=> 'users all',
			'page_title'=> $this->lang['users_all_users'],
            'users' 	=> $users,
            'pager' 	=> $return[0],
            'search' 	=> $search
        );

        // Assign values and display template
        $this->smarty->assign("data", $data);
        $this->smarty->display("users/browse.tpl.html");
    }

    // Edit user profile
    public function editAction() {

        // Get userid
        $iduser = $this->getParam('iduser');

        // Load needed classes
        Loader::loadClass('Users');

        // Get user data
        $user = reset(Users::getUsers(array('iduser' => $iduser)));

        // If the username not given - redirect to index page
        if (empty($user)) {

            // Redirect to index page
            System::redirect($this->config['site_url']);
        }

        // Load needed classes
        Loader::loadClass('Users');
        Loader::loadClass('Common');
        Loader::loadClass('Filter');
        Loader::loadClass('Crypt');

        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
				
			// If user has admin permissions
			if($_SESSION['admin']['type'] == 'admin'){

				// Post data
				$post_data = array(
					'username' 		=> Filter::sanitize($_POST['username']),
					'password' 		=> $_POST['password'],
					'email' 		=> Filter::sanitize($_POST['email']),
					'fullName'		=> Filter::sanitize($_POST['fullName']),
					'phone' 		=> Filter::sanitize($_POST['phone']),
					'address' 		=> Filter::sanitize($_POST['address']),
					'city' 			=> Filter::sanitize($_POST['city']),
					'active' 		=> $_POST['active'],
				);
			
				// Check if the username is available
				$username_check = reset(Users::getUsers(array('iduser' => '!' . $user['iduser'], 'username' => $post_data['username'])));

				// Check if the email address hasn't being used by somebody else
				$email_check = reset(Users::getUsers(array('iduser' => '!' . $user['iduser'], 'email' => $post_data['email'])));

				// Check username
				if (empty($post_data['username'])) {
					$error = $this->lang['username_cannot_be_empty'];
					$mark = 'username';
				} else if (mb_strlen($post_data['username'], 'UTF-8') < 2) {
					$error = $this->lang['username_too_short'];
					$mark = 'username';
				} else if (mb_strlen($post_data['username'], 'UTF-8') > 128) {
					$error = $this->lang['username_too_long'];
					$mark = 'username';
				}  else if ($username_check) {
					$error = $this->lang['username_already_taken'];
					$mark = 'username';
				}

				// Check passwords
				if (!empty($post_data['password'])) {
					if (mb_strlen($post_data['password'], 'UTF-8') < 6) {
						$error = $this->lang['password_too_short'];
						$mark = 'password';
					} else if (mb_strlen($post_data['password'], 'UTF-8') > 128) {
						$error = $this->lang['password_too_long'];
						$mark = 'password';
					}
				}

				// Check email
				if (empty($post_data['email'])) {
					$error = $this->lang['email_cannot_be_empty'];
					$mark = 'email';
				} else if ($email_check) {
					$error = $this->lang['email_already_taken'];
					$mark = 'email';
				} else if (!Filter::checkEmail($post_data['email'])) {
					$error = $this->lang['email_not_valid'];
					$mark = 'email';
				}
			}
			
            // If all fields are ok
            if (empty($error)) {
			
				// If user has admin permissions
				if($_SESSION['admin']['type'] == 'admin'){		
					
					// Insert array
					$data = array(
						'username' 		=> $post_data['username'],
						'email' 		=> $post_data['email'],
						'fullName' 		=> $post_data['fullName'],
						'city'			=> $post_data['city'],
						'address' 		=> $post_data['address'],
						'phone' 		=> $post_data['phone'],
						'active' 		=> $post_data['active'],
					);
					
					// If user is changing password
					if (!empty($post_data['password'])){
					
						// Set variable
						$data['password'] = Crypt::encrypt($post_data['password']);
					}
				}

                // Update database
                $this->db->update('users', $data, "iduser = '" . $user['iduser'] . "'", false);

                // Get user data
                $user = reset(Users::getUsers(array('iduser' => $user['iduser'])));

                // Assign success message
                $this->smarty->assign('success', $this->lang['changes_saved_successfully']);
            }

            // If we have errors
            else {

                // If posting merge posted data with predefined one
                if (!empty($post_data)) $user = array_merge($user, $post_data);

                // Assign error message
                $this->smarty->assign('error', $error);
            }
        }
	
        // Assign values and display template
        $this->smarty->assign('user', $user);
        $this->smarty->display('users/edit.tpl.html');
    }

    // Worker
    public function workerAction() {
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}

        // Get params
        $type = $this->getParam('type');
        $idtarget = (int) $this->getParam('idtarget');

        // If no params
        if (empty($type) || empty($idtarget)) {

            // Assign error message
            $_SESSION['error'] = 'Parameters missing!';

            // Redirect back
            System::redirect((!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $this->config['site_url']);
        }

        // Switch between object types
        switch ($type) {

            // Delete user
            case 'delete-user':

                // Get user id
                $iduser = $idtarget;

                // Delete user
                if ($this->db->delete('users', "iduser = '" . $iduser . "'")) {
                    
					// Assign success message
                    $_SESSION['success'] = $this->lang['users_delete_successfully'];

                    // Redirect back
                    System::redirect((!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $this->config['site_url']);
                }

                // If not deleted
                else {

                    // Assign error message
                    $_SESSION['error'] = $this->lang['users_cannot_delete'];

                    // Redirect to index page
                    System::redirect($this->config['site_url']);
                }
				
            break;
        }
    }
	
    // Ban by IP
    public function ipbanAction() {

        // If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}

        // Load needed classes
        Loader::loadClass("Users");
        Loader::loadClass("Pager");
		
		// Parameters for pagination
        $page = (int) $this->getParam("page");
        $page = (!empty($page)) ? $this->getParam("page") : 1;
        $this->smarty->assign("page", $page);
        $show = (int) $this->getParam("show", 20);
		
		// Set pagination url
        $url = $this->config['site_url'] . "users/new/page/[page]/";
		
		$pager = new Pager();
        $pager->replace = "[page]";
        $pager->page = $page;
        $pager->rows_per_page = $show;
        $pager->link = $url;
		
		// Count results
		$count = $this->db->fetchOne("SELECT COUNT(iduser) FROM `users` WHERE active='0' ");

		// Get pager data
		$return = $pager->paginate($count);

		// Get users
		$users = $this->db->fetchAll("SELECT u.*
										FROM `users` AS u 
										WHERE u.active = '0'
										ORDER BY u.iduser DESC 
										LIMIT " . $pager->limit . ", " . $pager->rows_per_page);

		 // Construct parse data
        $data = array(
			'page'		=> 'users ban',
            'page_title'=> $this->lang['users_all_ban'],
			'users' 	=> $users,
            'pager' 	=> $return[0]
        );

        // Assign values and display template
        $this->smarty->assign("data", $data);
        $this->smarty->display("users/browse.tpl.html");
    }
}

?>