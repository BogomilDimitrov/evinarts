<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved
 *
 * Faq controller
 *
 * @file            faqController.php
 * @category        Application
 * @author          Elenko Ivanow
 *
 */
Class FaqController extends Controller {

    // Database object
    private $db = null;
    // Config array
    private $config = null;
    // Smarty object
    private $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;

    // If actions is not found
    public function _call() {
        System::redirect($this->config['site_url']);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // If no admin logged in
        if (empty($_SESSION['admin'])) {

            // Redirect to login page
            System::redirect($this->config['site_url']);
        }

        // Get variables
        $this->db = Registry::get('db');
        $this->config = Registry::get('config');
        $this->smarty = Registry::get('smarty');
        $this->lang = Registry::get('lang');
        $this->cache = Registry::get('cache');
        $this->router = Registry::get("router");

        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);

        // If not logged - redirect to login page
        if (empty($_SESSION['admin'])) {
            System::redirect($this->config['site_url'] . "index/login");
        }

        // Check for messages or any post data
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
        if (!empty($_SESSION['post'])) {
            $this->smarty->assign("post", $_SESSION['post']);
            unset($_SESSION['post']);
        }
    }
	
    // Browse Faq
    public function browseAction() {
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}

        // Load needed classes
        Loader::loadClass("Pager");

        // Parameters for pagination
        $page = (int) $this->getParam("page");
        $page = (!empty($page)) ? $this->getParam("page") : 1;
        $this->smarty->assign("page", $page);
        $show = (int) $this->getParam("show", 20);

        // Set pagination url
        $url = $this->config['site_url'] . "faq/browse/page/[page]/";
        

        // If posting - set page to first one
        if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
            $page = 1;
        }

        $pager = new Pager();
        $pager->replace = "[page]";
        $pager->page = $page;
        $pager->rows_per_page = $show;
        $pager->link = $url;


		// Count results
		$count = $this->db->fetchOne("SELECT COUNT(idfaq) FROM `faq`");

		// Get pager data
		$return = $pager->paginate($count);

		// Get Faq
		$faq = $this->db->fetchAll("SELECT f.*
										FROM `faq` AS f 
										ORDER BY f.idfaq DESC 
										LIMIT " . $pager->limit . ", " . $pager->rows_per_page);
        

        // Construct parse data
        $data = array(
			'page'		=> 'faq all',
			'page_title'=> 'Полезно',
            'faq' 		=> $faq,
            'pager' 	=> $return[0]
        );

        // Assign values and display template
        $this->smarty->assign("data", $data);
        $this->smarty->display("faq/browse.tpl.html");
    }
	
	// Edit lang faq
	public function editlangAction(){
		
		// Get id
        $idfaq 		= $this->getParam('idfaq');
		$langsel 	= $this->getParam('langsel');

        // Load needed classes
        Loader::loadClass('Faq');
			
		// Load needed classes
        Loader::loadClass('Common');
        Loader::loadClass('Filter');

        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
        	
			// Edit array
			$data = array(
				'title_' . $langsel 			=> Filter::sanitize($_POST['title_' . $langsel]),
				'content_' . $langsel 			=> Filter::sanitize($_POST['content_' . $langsel]),
			);

            // Update database
            $this->db->update('faq', $data, "idfaq = ". $idfaq, false);

            // Assign success message
            $this->smarty->assign('success', $this->lang['changes_saved_successfully']);
		}
		
        // Get pages data
        $faq = reset(Faq::getFaq(array('idfaq' => $idfaq)));
		
		$faq['title']		= $faq['title_' . $langsel];
		$faq['content']		= $faq['content_' . $langsel];
		
        // If no data - redirect to index page
        if (empty($faq)) {

            // Redirect to index page
            System::redirect($this->config['site_url'] . 'faq/browse');
        }
		
		// Assign values and display template
        $this->smarty->assign('faq', $faq);
		$this->smarty->assign('langsel', $langsel);
		
        $this->smarty->display('faq/editLang.tpl.html');
	}
	
    // Edit faq
    public function editAction() {

        // Get idfaq
        $idfaq = $this->getParam('idfaq');

        // Load needed classes
        Loader::loadClass('Faq');

        // Get faq data
        $faq = reset(Faq::getFaq(array('idfaq' => $idfaq)));

        // If the username not given - redirect to index page
        if (empty($faq)) {

            // Redirect to index page
            System::redirect($this->config['site_url']);
        }

        // Load needed classes
        Loader::loadClass('Common');
        Loader::loadClass('Filter');

        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
				
			$error = '';
			
			// Post data
			$post_data = array(
				'title_bg' 		=> Filter::sanitize($_POST['title_bg']),
				'content_bg' 	=> Filter::sanitize($_POST['content_bg']),
				'position' 		=> $_POST['position'],
				'active' 		=> $_POST['active'],
			);
						
            // If all fields are ok
            if (empty($error)) {
			
				// Edit array
				$data = array(
					'title_bg' 		=> $post_data['title_bg'],
					'content_bg' 	=> $post_data['content_bg'],
					'position'		=> $post_data['position'],
					'active' 		=> $post_data['active'],
				);

                // Update database
                $this->db->update('faq', $data, "idfaq = '" . $faq['idfaq'] . "'", false);

                // Get faq data
                $faq = reset(Faq::getFaq(array('idfaq' => $faq['idfaq'])));

                // Assign success message
                $this->smarty->assign('success', $this->lang['changes_saved_successfully']);
            }

            // If we have errors
            else {

                // If posting merge posted data with predefined one
                if (!empty($post_data)) $faq = array_merge($faq, $post_data);

                // Assign error message
                $this->smarty->assign('error', $error);
            }
        }
	
        // Assign values and display template
        $this->smarty->assign('faq', $faq);
        $this->smarty->display('faq/edit.tpl.html');
    }
	
	// Add faq
	public function addAction(){
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}
	
		// Load needed classes
        Loader::loadClass('Faq');
        Loader::loadClass('Common');
        Loader::loadClass('Filter');
        
        // If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
				
			$error = '';
			
			// Post data
			$post_data = array(
				'title_bg' 		=> Filter::sanitize($_POST['title_bg']),
				'content_bg' 	=> Filter::sanitize($_POST['content_bg']),
				'position'		=> $_POST['position'],
				'active' 		=> $_POST['active'],
			);
			
            // If all fields are ok
            if (empty($error)) {
			
				// Edit array
				$data = array(
					'title_bg' 		=> $post_data['title_bg'],
					'content_bg' 	=> $post_data['content_bg'],
					'position'		=> $post_data['position'],
					'active' 		=> $post_data['active'],
					'date_added'	=> date('Y-m-d H:i:s'),
				);

                // Update database
                $this->db->insert('faq', $data);

                // Assign success message
				$_SESSION['success'] = $this->lang['changes_saved_successfully'];
				
				// Redirect back
				System::redirect($this->config['site_url'] . "faq/edit/idfaq/" . $this->db->lastInsertId());
            }

            // If we have errors
            else {

                // If posting merge posted data with predefined one
                if (!empty($post_data)) $faq = array_merge($faq, $post_data);

                // Assign error message
                $this->smarty->assign('error', $error);
            }
        }
	
		// Assign values and display template
        $this->smarty->assign('faq', $faq);
        $this->smarty->display('faq/add.tpl.html');
	}
	
    // Worker
    public function workerAction() {
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}

        // Get params
        $type = $this->getParam('type');
        $idtarget = (int) $this->getParam('idtarget');

        // If no params
        if (empty($type) || empty($idtarget)) {

            // Assign error message
            $_SESSION['error'] = 'Parameters missing!';

            // Redirect back
            System::redirect((!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $this->config['site_url']);
        }

        // Switch between object types
        switch ($type) {

            // Delete faq
            case 'delete-faq':

                // Get faq id
                $idfaq = $idtarget;

                // Delete faq
                if ($this->db->delete('faq', "idfaq = '" . $idfaq . "'")) {
                    
					// Assign success message
                    $_SESSION['success'] = "Данните са изтрити успешно!";

                    // Redirect back
                    System::redirect((!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $this->config['site_url']);
                }

                // If not deleted
                else {

                    // Assign error message
                    $_SESSION['error'] = "Данните немогат да бъдат изтрити";

                    // Redirect to index page
                    System::redirect($this->config['site_url']);
                }
				
            break;
        }
    }
	
}
?>