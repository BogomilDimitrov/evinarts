<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved
 *
 * Carts controller
 *
 * @file            cartsController.php
 * @category        Application
 * @author          Elenko Ivanow
 *
 */
Class CartsController extends Controller {

    // Database object
    private $db = null;
    // Config array
    private $config = null;
    // Smarty object
    private $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;

    // If actions is not found
    public function _call() {
        System::redirect($this->config['site_url']);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // If no admin logged in
        if (empty($_SESSION['admin'])) {

            // Redirect to login page
            System::redirect($this->config['site_url']);
        }

        // Get variables
        $this->db = Registry::get('db');
        $this->config = Registry::get('config');
        $this->smarty = Registry::get('smarty');
        $this->lang = Registry::get('lang');
        $this->cache = Registry::get('cache');
        $this->router = Registry::get("router");

        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);

        // If not logged - redirect to login page
        if (empty($_SESSION['admin'])) {
            System::redirect($this->config['site_url'] . "index/login");
        }

        // Check for messages or any post data
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
        if (!empty($_SESSION['post'])) {
            $this->smarty->assign("post", $_SESSION['post']);
            unset($_SESSION['post']);
        }
    }
	
    // Browse Carts
    public function browseAction() {
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}

        // Load needed classes
		Loader::loadClass("Carts");
        Loader::loadClass("Pager");

        // Parameters for pagination
        $page = (int) $this->getParam("page");
        $page = (!empty($page)) ? $this->getParam("page") : 1;
        $this->smarty->assign("page", $page);
        $show = (int) $this->getParam("show", 20);

        // Set pagination url
        $url = $this->config['site_url'] . "carts/browse/page/[page]/";
        

        // If posting - set page to first one
        if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
            $page = 1;
        }

        $pager = new Pager();
        $pager->replace = "[page]";
        $pager->page = $page;
        $pager->rows_per_page = $show;
        $pager->link = $url;

		// Count results
		$count = Carts::countCarts();

		// Get pager data
		$return = $pager->paginate($count);

		// Get Carts
		$listCarts = Carts::getCarts( array(), array('by' => 'p.id_carts', 'type' => 'DESC'), array('from' => $pager->limit, 'count' => $pager->rows_per_page) );

        // Construct parse data
        $data = array(
			'page'			=> 'carts all',
			'page_title'	=> 'Всички поръчки',
            'listCarts' 	=> $listCarts,
            'pager' 		=> $return[0]
        );

        // Assign values and display template
        $this->smarty->assign("data", $data);
        $this->smarty->display("carts/browse.tpl.html");
    }

	// Approved carts
    public function approvedAction() {
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}

        // Load needed classes
		Loader::loadClass("Carts");
        Loader::loadClass("Pager");

        // Parameters for pagination
        $page = (int) $this->getParam("page");
        $page = (!empty($page)) ? $this->getParam("page") : 1;
        $this->smarty->assign("page", $page);
        $show = (int) $this->getParam("show", 20);

        // Set pagination url
        $url = $this->config['site_url'] . "carts/approved/page/[page]/";
        

        // If posting - set page to first one
        if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
            $page = 1;
        }

        $pager = new Pager();
        $pager->replace = "[page]";
        $pager->page = $page;
        $pager->rows_per_page = $show;
        $pager->link = $url;

		// Count results
		$count = Carts::countCarts(array('status' => 3));

		// Get pager data
		$return = $pager->paginate($count);

		// Get Carts
		$listCarts = Carts::getCarts( array('p.status' => 3), array('by' => 'p.id_carts', 'type' => 'DESC'), array('from' => $pager->limit, 'count' => $pager->rows_per_page) );

        // Construct parse data
        $data = array(
			'page'			=> 'carts approved',
			'page_title'	=> 'Потвърдени поръчки',
            'listCarts' 	=> $listCarts,
            'pager' 		=> $return[0]
        );

        // Assign values and display template
        $this->smarty->assign("data", $data);
        $this->smarty->display("carts/browse.tpl.html");
    }

    // Edit cart
    public function editAction() {

        // Get idcart
        $idcart = $this->getParam('idcart');
		
		// If posting details form
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
						
			// Edit array
			$send_data = array(
				'status'		=> (int)$_POST['status'],
				'description'	=> Filter::sanitize($_POST['description']),
			);

			// Update database
			$this->db->update('carts', $send_data, "id_carts = " . $idcart, false);
        }
		
        // Load needed classes
        Loader::loadClass('Carts');
		
		// Get Cart Info
		$cartInfo = reset(Carts::getCarts(array('p.id_carts' => $idcart)));
		
		// Preview Order
		$productInCart = Carts::listProductCart($cartInfo['id_carts']);
	
		if($productInCart){
			$totalPrice = 0;
			foreach($productInCart as $k=>$v){
				$price 				= $v['price_1'];
				$col_price 			= $v['col']*$price;
				$productInCart[$k]['price_col']	= $col_price;
				
				// Calculate max price
				$totalPrice +=	$col_price;
			}			
		}
		
		$data = array(
			'page_title'	=> 'Преглед на поръчката',
			'cartInfo'		=> $cartInfo,
			'productInCart'	=> $productInCart,
			'totalPrice'	=> $totalPrice,
		);
		
        // Assign values and display template
        $this->smarty->assign($data);
        $this->smarty->display('carts/edit.tpl.html');
    }
	
    // Worker
    public function workerAction() {
	
		// If no permissions
		if ($_SESSION['admin']['type'] != 'admin') {

			// Redirect to dashboard
			System::redirect($this->config['site_url']);
		}

        // Get params
        $type 		= $this->getParam('type');
        $idtarget	= (int) $this->getParam('idtarget');

        // If no params
        if (empty($type) || empty($idtarget)) {

            // Assign error message
            $_SESSION['error'] = 'Parameters missing!';

            // Redirect back
            System::redirect((!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $this->config['site_url']);
        }

        // Switch between object types
        switch ($type) {

            // Delete cart
            case 'delete-cart':

                // Get cart id
                $idcart = $idtarget;

                // Delete cart
                if ($this->db->delete('carts', "id_carts ='" . $idcart . "'")) {
					
					// Delete products in cart
					$this->db->delete('carts_products', 'id_cart =' . $idcart);
										
					// Assign success message
                    $_SESSION['success'] = "Данните са изтрити успешно!";
		
                    // Redirect back
                    System::redirect((!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $this->config['site_url']);
                }

                // If not deleted
                else {

                    // Assign error message
                    $_SESSION['error'] = "Данните немогат да бъдат изтрити";

                    // Redirect to index page
                    System::redirect($this->config['site_url'] . 'carts/browse');
                }
				
            break;
			
			case 'delete-product':
				
				// Get cart id
                $idcart = $idtarget;

                // Delete products in cart
                if ($this->db->delete('carts_products', 'id = ' . $idcart)) {
					
					// Assign success message
                    $_SESSION['success'] = "Данните са изтрити успешно!";
		
                    // Redirect back
                    System::redirect((!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $this->config['site_url']);
                }
				
			break;			
			
        }
    }
}
?>