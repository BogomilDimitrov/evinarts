<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2012 All Rights Reserved
 *
 * Ajax controller
 *
 * @file            AjaxController.php
 * @category        Application
 * @author          Elenko Ivanov
 *
 */
Class AjaxController extends Controller {

    // Database object
    private $db = null;
    // Config array
    private $config = null;
    // Smarty object
    private $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;

    // If actions is not found
    public function _call() {
        System::redirect($this->config['site_url']);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // Get variables
        $this->db = Registry::get('db');
        $this->config = Registry::get('config');
        $this->smarty = Registry::get('smarty');
        $this->lang = Registry::get('lang');
        $this->cache = Registry::get('cache');
        $this->router = Registry::get("router");

        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);
        
        // If not logged - redirect to login page
        if (empty($_SESSION['admin'])) {
            System::redirect($this->config['site_url'] . "index/login");
        }
        
        // Check for messages or any post data
        if(!empty($_SESSION['error'])){
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if(!empty($_SESSION['success'])){
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
        if(!empty($_SESSION['post'])){
            $this->smarty->assign("post", $_SESSION['post']);
            unset($_SESSION['post']);
        }
        
    }
	
	public function updateTopPictureAction(){
		
		$table = $_POST['t'];
		
		if ($table == 'news')
			$this->db->query("UPDATE `news` SET `picture` = '" . $_POST['imgName'] . "' WHERE `idnews`=" . (int)$_POST['id_p']);
		else if($table == 'products')
			$this->db->query("UPDATE `products` SET `picture` = '" . $_POST['imgName'] . "' WHERE `idproduct`=" . (int)$_POST['id_p']);
			
		echo 1;
		
		exit();
	}
	
}
?>