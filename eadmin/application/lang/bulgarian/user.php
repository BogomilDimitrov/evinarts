<?php

	// Language name
	$lang['language'] = "Bg";
	$lang['direction'] = "ltr";

    // Pager
    $lang['first'] 	= "Първа";
    $lang['before'] = "Предишна";
    $lang['next'] 	= "Следваща";
    $lang['last'] 	= "Последна";
	$lang['name'] 	= "Име";
	$lang['value'] 	= "Стойност";
	$lang['statistics'] 	= "Статистика";
	$lang['logout'] = "Изход";
	$lang['search'] = "Търси";
	$lang['no_data_found'] = "Няма намерени данни!";
	$lang['date_added'] = "Дата";
	$lang['state'] 		= "Статус";
	$lang['options'] 	= "Опции";
	$lang['edit']		= "Редактирай";
	$lang['save']		= "Запиши";
	$lang['delete']		= "Изтрии";
	$lang['active'] 	= "Активен";
	$lang['inactive']	= "Неактивен";
	$lang['changes_saved_successfully'] = "Данните са записани  успешно";
	$lang['view_website'] = "Към сайта";
	$lang['picture']	  = "Снимка";
	$lang['add']		  = "Добави";
	$lang['position']	  = "Позиция";
	
	
	// Section Menu
	$lang['menu_home'] 		= "Начало";
	$lang['menu_users'] 	= "Потребители";
	$lang['menu_browse'] 	= "Покажи всички";
	$lang['menu_in_active'] = "Неактивни";
	$lang['menu_new'] 		= "Нови";
	$lang['menu_orders'] 	= "Поръчки";
	$lang['menu_products'] 	= "Продукти";
	$lang['menu_promo'] 	= "В промоция";
	$lang['menu_category'] 	= "Категории";
	$lang['menu_faq']		= "Полезно";
	$lang['menu_brand'] 	= "Марки";
	$lang['menu_carts']		= "Поръчки";
	$lang['menu_denied'] 	= "Отказани";
	$lang['menu_approved']  = "Потвърдени";
	
	
	// Section Users
	$lang['users_all_users'] 			= "Всички потребители";
	$lang['users_search_field'] 		= "Търсете потребител по име, username или email";
	$lang['users_username'] 			= "Потребителско име";
	$lang['users_delete_user'] 			= "Изтриване на потребител";
	$lang['users_confirm_delete'] 		= "Сигурни ли сте, че искате да изтриете потребителя ";
	$lang['users_new_user'] 			= "Нов потребител, не удобрен";
	$lang['users_delete_successfully'] 	= "Потребителя е изтрит успешно!";
	$lang['users_cannot_delete'] 		= "Потребителя не може да бъде изтрит";
	$lang['username_cannot_be_empty'] 	= "Потребителското име не може да бъде празно.";
	$lang['username_too_short'] 		= "Потребителското име е твърде късо.";
	$lang['username_too_long'] 			= "Потребителското име е над 128 символа.";
	$lang['username_illegal_chars'] 	= "Потребителското име е невалидно.";
	$lang['username_already_taken'] 	= "Потребителското име е заето.";
	$lang['username_forbidden'] 		= "Потребителското име е блокирано. Моля избере друго!";
	$lang['password_cannot_be_empty']	= "Паролата не може да бъде празна.";
	$lang['password_too_short'] 		= "Паролата трябва да е повече от 6 символа.";
	$lang['password_too_long'] 			= "Паролата не трябва да е повече от 128 символа.";
	$lang['passwords_dont_match'] 		= "Паролата не съвпада.";
	$lang['email_cannot_be_empty'] 		= "Email адреса не може да бъде празен.";
	$lang['email_already_taken'] 		= "Този email вече се използва.";
	$lang['email_not_valid'] 			= "Email адреса е невалиден.";
	$lang['users_edit_user'] 			= "Промяна на потребител";
	$lang['users_general_information'] 	= "Основна информация";
	$lang['users_edit_user_profile'] 	= "Редактиране на профила";
	$lang['users_address']				= "Адрес";
	$lang['users_password']				= "Парола (при промяна)";
	$lang['users_fullname']				= "Име";
	$lang['users_phone']				= "Телефон";
	$lang['users_city']					= "Град";
	$lang['users_type']					= "Тип";
	$lang['users_all_ban']				= "Неактивни потребители";
	$lang['users_all_new']				= "Нови потребители";
	
	
	// Section Brands
	$lang['brand_search_field'] 		= "Търсене по име на марка";
	$lang['brands_all_brands'] 			= "Всички марки";
	$lang['brand_title']				= "Име марка";
	$lang['brand_edit_title']			= "Редактиране на марка";
	$lang['brand_description']			= "Описание";
	$lang['photo_supported_formats'] 	= "Снимката трябва да е във формат JPG, PNG or GIF format.";
	$lang['photo_too_big'] 				= "Снимката нетрябва да е по-голяма от 2 MB.";
	$lang['photo_too_small'] 			= "Снимката трябва да е с размери по-големи от 80x80 пиксела.";
	
	
	// Section category
	$lang['category_title']			= "Име категория";
	$lang['category_description'] 	= "Описание";
	$lang['category_edit_title']	= "Редактиране на категория";
	$lang['category_search_field']  = "Търси по име на категория";
	
	
	// Section faq
	$lang['faq_title'] = "Заглавие";
	$lang['faq_description'] = "Описание";
	
	// Section products
	$lang['products_search_field'] = "Търсене по име на продукт";
	
?>