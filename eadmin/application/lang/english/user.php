<?php
	// Language name
	$lang['language'] = "English";
	$lang['direction'] = "ltr";
	
    // Pager
    $lang['first'] = "First";
    $lang['before'] = "Prev";
    $lang['next'] = "Next";
    $lang['last'] = "Last";
	
	// Social networks content publishing
	$lang['facebook_content_publishing_title'] = "I have just shared a blog on Iranian.com";
	$lang['twitter_content_publish_content'] = "I just shared a new post titled %s on Iranian.com. Check it out!";
	
	// Login Messages
	$lang['login_wrong_user_or_pass'] = "Wrong Username/Email or Password.";
	
	// Fullname Messages
	$lang['fullname_cannot_be_empty'] = "The full name cannot be empty.";
	$lang['fullname_too_short'] = "The full name must be longer than 4 characters.";
	$lang['fullname_too_long'] = "The full name must be shorter than 128 characters.";
	
	// Username Messages
	$lang['username_cannot_be_empty'] = "The username cannot be empty.";
	$lang['username_too_short'] = "The username must be longer than 2 characters.";
	$lang['username_too_long'] = "The username must be shorter than 128 characters.";
	$lang['username_illegal_chars'] = "The username containes illegal characters.";
	$lang['username_already_taken'] = "This username has already been taken.";
	$lang['username_fb_already_taken'] = "The username you're trying to login with is aready taken.";
	$lang['username_tw_already_taken'] = "The username you're trying to login with is aready taken.";
	$lang['username_forbidden'] = "The username is forbidden. Please choose another one.";
	
	// Password Messages
	$lang['password_cannot_be_empty'] = "Password cannot be empty.";
	$lang['password_too_short'] = "The password must be longer than 6 characters.";
	$lang['password_too_long'] = "The password must be shorter than 128 characters.";
	$lang['passwords_dont_match'] = "Passwords don't match.";
	$lang['new_password_sent'] = "Your new password has been sent to your email.";
	
	// Email Messages
	$lang['email_cannot_be_empty'] = "The email cannot be empty.";
	$lang['email_already_taken'] = "The email address has already being used by somebody else.";
	$lang['email_fb_already_taken'] = "The email address you're trying to login with is aready taken.";
	$lang['email_not_valid'] = "The email address is not valid.";
	$lang['email_not_exist'] = "This email does not exist in our database.";
	
	// Account Activation Messages
	$lang['account_doesnt_exist'] = "An account associated with that confirmation code doesn't seem to exist in our system.";
	$lang['account_activated'] = "You have successfully activated your account. You can now login.";
	$lang['no_code_supplied'] = "You have not presented a confirmation code.";
	$lang['no_iduser_supplied'] = "You have not presented a user ID.";
	$lang['account_activation_required'] = "You have successfully registered! In order to activate your account please follow the confirmation link that we have sent to your email account.";
	$lang['account_not_activated_yet'] = "This account has not yet been activated.";
	$lang['account_suspended'] = "Your account has been suspended.";
	$lang['confirmation_code_resent'] = "Your confirmation code has been resent.";
	$lang['confirmation_email_resend'] = "Click here to resend the confirmation email.";
	$lang['new_password_email_subject'] = "Your new Iranian.com password";
	$lang['new_password_email_content'] = "Greetings!<br/><br/>Your new password for accessing your account is %s<br/><br/>Regards,<br/>The Iranian.com Team";
	$lang['account_activation_email_subject'] = "Iranian.com confirmation code";
	$lang['account_activation_email_content'] = "Greetings!<br/><br/>Activate your account by clicking on the link below<br/><br/>%s<br/><br/>Regards,<br/>The Iranian.com Team";
	$lang['login_with_fb'] = "The profile associated with this email was created through Facebook. Please sign in with Facebook.";
	$lang['login_with_tw'] = "The profile associated with this email was created through Twitter. Please sign in with Twitter.";
	
	// Location Messages
	$lang['location_too_short'] = "The location must be longer than 4 characters.";
	$lang['location_too_long'] = "The location must be shorter than 25 characters.";
	
	// Biography Messages
	$lang['biography_too_long'] = "The biography must be shorter than 250 characters.";
	
	// Gender Messages
	$lang['gender_empty'] = "Please, select gender.";

	// Birthday Messages
	$lang['birthday_not_full'] = "Please, enter your full birth date.";
	
	// Photo Messages
	$lang['photo_cannot_be_empty'] = "You must upload or submit a photo.";
	$lang['photo_supported_formats'] = "The photo must be in JPG, PNG or GIF format.";
	$lang['photo_too_big'] = "The size of the photo shouldn't exceed 2 MB.";
	$lang['photo_too_small'] = "The photo must be larger than 80x80 pixels.";
	$lang['photo_too_small_2'] = "The photo must be larger than 250x250 pixels.";
	$lang['photo_not_uploaded'] = "One or more of your photos didn't meet the requirements and wasn't uploaded.";
	$lang['photos'] = "photos";
	
	// Audio Messages
	$lang['audio_cannot_be_empty'] = "You must upload an audio file or submit an embed code.";
	$lang['audio_supported_formats'] = "The audio file must be in MP3 format.";
	$lang['audio_too_big'] = "The size of the audio file shouldn't exceed 10 MB.";
	$lang['audio_file_added_successfully'] = "Audio file added successfully.";
	
	// Quote Messages
	$lang['quote_cannot_be_empty'] = "Quote cannot be empty.";
	$lang['quote_too_short'] = "Quote must be longer than 4 characters.";
	$lang['quote_too_long'] = "Quote must be shorter than 300 characters.";
	$lang['quote_added_successfully'] = "Quote added successfully.";
	
	// Tag Messages
	$lang['tags_cannot_be_empty'] = "Tags cannot be empty.";
	$lang['tags_illegal_chars'] = "Tags contain illegal characters.";
	
	// Title Messages
	$lang['title_cannot_be_empty'] = "Title cannot be empty.";
	$lang['title_too_short'] = "Title must be longer than 4 characters.";
	$lang['title_too_long'] = "Title must be shorter than 140 characters.";
	$lang['title_too_long_news'] = "Title must be shorter than 160 characters.";
	
	// Post Messages
	$lang['post_cannot_be_empty'] = "Post cannot be empty.";
	$lang['post_too_short'] = "Post must be longer than 5 characters.";
	$lang['post_added_successfully'] = "Post added successfully.";
	
	// Category Messages
	$lang['category_is_mandatory'] = "You must select a category.";
	
	// Miscellaneous
	$lang['changes_saved_successfully'] = "Your changes have been saved successfully.";
	$lang['delete_confirmation'] = "Are you sure you want to delete this post?";
	
	$lang['self_no_followers'] = "You do not yet have any followers.";
	$lang['self_no_following'] = "You are not following anyone";
	$lang['self_no_toops'] = "You have not Toop’d anything";
	$lang['self_no_comments'] = "You have not commented anything";
	$lang['self_no_received_comments'] = "You have not received any comments";
	
	$lang['user_no_followers'] = "User hasn't got any followers";
	$lang['user_no_following'] = "User is not following anyone";
	$lang['user_no_toops'] = "User has not Toop’d anything";
	$lang['user_no_comments'] = "User has not commented anything";
	$lang['user_no_received_comments'] = "User has not received any comments";
	
	$lang['deactivate_confirmation'] = "Are you sure you want to deactivate your account?";
	
	$lang['hour'] = 'hour';
	$lang['hours'] = 'hours';
	
	// Comments Messages
	$lang['comment_deleted'] = "This comment was deleted by";	
	$lang['comment_deleted_user_blocked'] = "This comment was deleted by [user] and the user has been blocked";
	
	// Video Messages
	$lang['video_embed_cannot_be_empty'] = "Video embed code field cannot be empty.";
	$lang['video_post_added_successfully'] = "Video post added successfully.";
	
	// Contact Us Messages
	$lang['name_is_required'] = "Fill in your name, please.";
	$lang['email_is_required'] = "Fill in your email, please.";
	$lang['message_cannot_be_empty'] = "Message cannot be empty.";
	$lang['message_sent_successfully'] = "Your message has been sent successfully.<br/>We will get back to you shortly.";
	
	// URL Messages
	$lang['url_cannot_be_empty'] = "URL cannot be empty.";
	$lang['url_too_short'] = "URL must be longer than 10 characters.";
	$lang['url_too_long'] = "URL must be shorter than 300 characters.";
	$lang['url_not_valid'] = "URL is not valid.";
	
	// Interface
	$lang['interface_site_name'] = "Iranian.com";
	$lang['interface_site_slogan'] = "Nothing is sacred";
	
	$lang['interface_january'] = "January";
	$lang['interface_february'] = "February";
	$lang['interface_march'] = "March";
	$lang['interface_april'] = "April";
	$lang['interface_may'] = "May";
	$lang['interface_june'] = "June";
	$lang['interface_july'] = "July";
	$lang['interface_august'] = "August";
	$lang['interface_september'] = "September";
	$lang['interface_october'] = "October";
	$lang['interface_november'] = "November";
	$lang['interface_december'] = "December";	

	$lang['interface_header_home'] = "Home";
	$lang['interface_header_activity'] = "Activity";
	$lang['interface_header_blog'] = "Blog";
	$lang['interface_header_account'] = "Account";
	$lang['interface_header_settings'] = "Settings";
	$lang['interface_header_edit_profile'] = "Edit Profile";
	$lang['interface_header_view_as_visitor'] = "View Blog as Visitor";
	$lang['interface_header_logout'] = "Log Out";
	$lang['interface_header_signin'] = "Sign In";
	$lang['interface_header_joinus'] = "Join Us";
	$lang['interface_header_search_placeholder'] = "Search for Posts and @Users";
	
	$lang['interface_footer_copyright'] = "© Copyright 1995 - 2012, Iranian LLC.";
	$lang['interface_footer_user_agreement'] = "User Agreement and Privacy Policy";
	$lang['interface_footer_rights_permissions'] = "Rights and Permissions";
	$lang['interface_footer_aboutus'] = "About Us";
	$lang['interface_footer_contactus'] = "Contact Us";
	
	$lang['interface_footer_advertise_with_us'] = "Advertise With Us";
	
	$lang['interface_inline_scroll_to_top'] = "Scroll to Top";
	$lang['interface_inline_continue_session_message'] = "Due to inactivity you will be logged out in 5 minutes.<br/>Would you like to continue your session?";
	$lang['interface_inline_continue_session'] = "Continue session";
	$lang['interface_inline_advertise'] = "Advertise";
	$lang['interface_inline_terms'] = "Terms";
	
	$lang['interface_elements_cancel'] = "Cancel";
	$lang['interface_elements_or'] = "or";
	
	$lang['interface_post_type_text'] = "Text";
	$lang['interface_post_type_news'] = "News";
	$lang['interface_post_type_blogs'] = "Blogs";
	$lang['interface_post_type_video'] = "Video";
	$lang['interface_post_type_audio'] = "Audio";
	$lang['interface_post_type_photo'] = "Photo";
	$lang['interface_post_type_quote'] = "Quote";
	
	$lang['interface_everyone'] = "Everyone";
	$lang['interface_following'] = "Following";
	
	$lang['interface_sort_content'] = "Sort content";
	$lang['interface_all_content'] = "All Content";
	$lang['interface_text_only'] = "Text only";
	$lang['interface_photos_only'] = "Photos only";
	$lang['interface_videos_only'] = "Videos only";
	$lang['interface_audio_only'] = "Audio only";
	$lang['interface_quotes_only'] = "Quotes only";
	
	$lang['interface_last_24hours'] = "Last 24 Hours";
	$lang['interface_last_7days'] = "Last 7 Days";
	$lang['interface_last_30days'] = "Last 30 Days";
	
	$lang['interface_rss'] = "RSS";
	
	$lang['interface_received'] = "Received";
	$lang['interface_shared'] = "Shared";
	$lang['interface_everything'] = "Everything";
	$lang['interface_show'] = "Show";
	
	$lang['interface_only_toops'] = "Only toops";
	$lang['interface_only_follows'] = "Only follows";
	$lang['interface_only_mentions'] = "Only mentions";
	$lang['interface_only_comments'] = "Only comments";
	$lang['interface_only_comment_likes'] = "Only comment likes";
	$lang['interface_only_gifts'] = "Only gifts";
	$lang['interface_all_feedback'] = "All Feedback";
	
	$lang['interface_activity_you'] = "You";
	$lang['interface_activity_you_sc'] = "you";
	$lang['interface_activity_your_sc'] = "your";
	$lang['interface_activity_toopd'] = "Toop'd";
	$lang['interface_activity_posted_comment'] = "posted a comment";
	$lang['interface_activity_liked'] = "liked";
	$lang['interface_activity_replied_to'] = "replied to";
	$lang['interface_activity_post_sc'] = "post";
	$lang['interface_activity_on_sc'] = "on";
	$lang['interface_activity_are_sc'] = "are";
	$lang['interface_activity_is_sc'] = "is";
	$lang['interface_activity_comment_sc'] = "comment";
	$lang['interface_activity_mentioned'] = "mentioned";
	$lang['interface_activity_in_comment'] = "in comment";
	$lang['interface_activity_in_post'] = "in post";
	$lang['interface_activity_now_following'] = "now following";
	
	$lang['interface_post_untitled'] = "Untitled";
	$lang['interface_show_more'] = "Show more";
	
	$lang['interface_signin_title'] = "Sign into Iranian.com";
	$lang['interface_signin_slogan'] = "Tap into the pulse of Iranians, worldwide.";
	$lang['interface_signin_username_email'] = "Username or Email";
	$lang['interface_signin_password'] = "Password";
	$lang['interface_signin_remember_me'] = "Remember me";
	$lang['interface_signin_forgot_password'] = "Forgot password";
	$lang['interface_signin_login_fb'] = "Login with Facebook";
	$lang['interface_signin_or'] = "or";
	$lang['interface_signin_login_tw'] = "Login with Twitter";
	$lang['interface_signin_signin'] = "Sign in";
	
	$lang['interface_joinus_title'] = "Join Iranian.com";
	$lang['interface_joinus_slogan'] = "Tap into the pulse of Iranians, worldwide.";
	$lang['interface_joinus_fullname'] = "Full name";
	$lang['interface_joinus_fullname_desc'] = "Must be longer than 4 characters";
	$lang['interface_joinus_email'] = "Email";
	$lang['interface_joinus_email_desc'] = "A valid one, please";
	$lang['interface_joinus_password'] = "Password";
	$lang['interface_joinus_password_desc'] = "Must be 6 characters or longer";
	$lang['interface_joinus_password_again'] = "Password again";
	$lang['interface_joinus_password_again_desc'] = "Must be the same as the above one";
	$lang['interface_joinus_signup'] = "Sign up";
	$lang['interface_joinus_login_fb'] = "Sign up with Facebook";
	$lang['interface_joinus_or'] = "or";
	$lang['interface_joinus_login_tw'] = "Sign up with Twitter";
	
	$lang['interface_fpass_title'] = "Forgot password";
	$lang['interface_fpass_slogan'] = "Enter the email address that you registered your account with. We will send you your new password.";
	$lang['interface_fpass_email'] = "Email";
	$lang['interface_fpass_send_pass'] = "Send password";

	$lang['interface_blog_biography'] = "Biography";
	
	$lang['interface_edit'] = "Edit";
	$lang['interface_delete'] = "Delete";
	$lang['interface_all_posts'] = "All Posts";
	$lang['interface_sponsored_ads'] = "Sponsored Ads";
	
	$lang['interface_post_comments'] = "Comments";
	$lang['interface_post_sort_comments'] = "Sort comments";
	$lang['interface_post_most_recent'] = "Most Recent";
	$lang['interface_post_most_popular'] = "Most Popular";
	$lang['interface_post_leave_comment'] = "Leave a comment";
	$lang['interface_post_you_said'] = "You said";
	$lang['interface_post_post_comment'] = "Post Comment";
	
	$lang['interface_thanks_title'] = "A few interesting people you may want to follow";
	$lang['interface_thanks_fullname'] = "Full Name";
	$lang['interface_thanks_biography'] = "Bio";
	$lang['interface_thanks_proceed'] = "Onward!";
	
	$lang['interface_profile_title'] = "Tell us about yourself";
	$lang['interface_profile_slogan'] = "Introduce yourself to Iranians worldwide";
	$lang['interface_profile_your_name'] = "Your name";
	$lang['interface_profile_your_name_desc'] = "Must be longer than 4 characters";
	$lang['interface_profile_username'] = "Username";
	$lang['interface_profile_username_desc'] = "Must be longer than 2 and shorter than 64 characters.<br/>Can consist of letters, numbers, dots and underscores";
	$lang['interface_profile_new_password'] = "New Password";
	$lang['interface_profile_new_password_desc'] = "Must be longer than 6 characters (private)";
	$lang['interface_profile_password_again'] = "Password again";
	$lang['interface_profile_password_again_desc'] = "Must be the same as the above one (private)";
	$lang['interface_profile_email'] = "Email";
	$lang['interface_profile_email_desc'] = "A valid one, please (private)";
	$lang['interface_profile_biography'] = "Bio";
	$lang['interface_profile_biography_desc'] = "Maximum length is 250 characters.";
	$lang['interface_profile_location'] = "Location";
	$lang['interface_profile_location_desc'] = "Must be longer than 2 and shorter than 25 characters.";
	$lang['interface_profile_gender'] = "Gender";
	$lang['interface_profile_safe'] = "Safe";
	$lang['interface_profile_gender_male'] = "Male";
	$lang['interface_profile_gender_female'] = "Female";
	$lang['interface_profile_yes'] = "Yes";
	$lang['interface_profile_no'] = "No";
	$lang['interface_profile_birthday'] = "Birthday";
	$lang['interface_profile_birthday_month'] = "Month";
	$lang['interface_profile_birthday_day'] = "Day";
	$lang['interface_profile_birthday_year'] = "Year";
	$lang['interface_profile_photo'] = "Photo";
	$lang['interface_profile_photo_desc'] = "Maximum size: 2MB<br/>Minimum dimensions: 80 x 80 pixels<br/>Supported formats: JPG, PNG & Static GIF";
	$lang['interface_profile_social_title'] = "Display your social media identities on your blog";
	$lang['interface_profile_social_slogan'] = "Let other members know more about you by showing sites you like to spend time on. When you add a site, a badge for that social media site will appear on your blog.";
	$lang['interface_profile_add_url'] = "Add a URL";
	$lang['interface_profile_social_add_service'] = "Add service";
	$lang['interface_profile_social_fb_connect'] = "Connect with Facebook";
	$lang['interface_profile_agreement'] = "By clicking Finish, you agree to the %s Terms & Conditions %s";
	$lang['interface_profile_finish'] = "Finish";
	$lang['interface_profile_save'] = "Save";

	$lang['interface_settings_title'] = "Edit your account settings";
	$lang['interface_settings_slogan'] = "Click here to %s edit your profile %s";
	$lang['interface_settings_email_me'] = "Email me when";
	$lang['interface_settings_email_on_toop'] = "My content is Toop’d";
	$lang['interface_settings_email_on_content_like'] = "My content receives a comment or is liked";
	$lang['interface_settings_email_on_mention'] = "My name is mentioned";
	$lang['interface_settings_email_on_follow'] = "I’m followed by someone new";
	$lang['interface_settings_timezone'] = "Time Zone";
	$lang['interface_settings_language'] = "Language";
	$lang['interface_settings_socialization'] = "Socialization";
	$lang['interface_settings_share_posts_facebook'] = "Share posts on your Facebook Timeline";
	$lang['interface_settings_share_posts_twitter'] = "Share posts on your Twitter profile";
	$lang['interface_settings_socialization_desc'] = "You can toggle these options when posting.";
	$lang['interface_settings_manage_blocked_members'] = "Manage blocked members";
	$lang['interface_settings_manage_blocked_members_desc'] = 'Select the users you want to unblock and click "Save changes"';
	$lang['interface_settings_manage_account'] = "Manage account";
	$lang['interface_settings_deactivate_account'] = "Deactivate my account";
	$lang['interface_settings_deactivate_account_desc'] = "You can reactivate it whenever you want by simply logging in.";
	$lang['interface_settings_save_changes'] = "Save changes";
	
	$lang['interface_members_followers'] = "Followers";
	$lang['interface_members_following'] = "Following";
	$lang['interface_members_sort_by_name'] = "Sort by name";
	$lang['interface_members_sort_by_popularity'] = "Sort by popularity";
	$lang['interface_members_title_following'] = "%s is following %d people";
	$lang['interface_members_title_followers'] = "%s is being followed by %d people";
	$lang['interface_members_fullname'] = "Full Name";
	$lang['interface_members_location'] = "Location";
	
	$lang['interface_category_browse_title'] = "News Category";
	$lang['interface_category_browse_total_posts'] = "%d posts";
	
	$lang['interface_category_list_title'] = "Categories";
	$lang['interface_category_list_category'] = "Category";
	$lang['interface_category_list_num_entries'] = "Number of Entries";
	$lang['interface_category_list_popualr_authors'] = "Popular Authors";
	
	$lang['interface_contactus_title'] = "Contact Iranian.com";
	$lang['interface_contactus_slogan'] = "Share with us your thoughts, suggestions and critics";
	$lang['interface_contactus_name'] = "Your name";
	$lang['interface_contactus_email'] = "Your email";
	$lang['interface_contactus_email_desc'] = "A valid one, please. We will send you our answer over there.";
	$lang['interface_contactus_message'] = "Message";
	$lang['interface_contactus_send'] = "Send";
	
	$lang['interface_latest_blogs_sort_content'] = "Sort content";
	$lang['interface_latest_blogs_all_blogs'] = "All Blogs";
	$lang['interface_latest_blogs_text_only'] = "Text only";
	$lang['interface_latest_blogs_photos_only'] = "Photos only";
	$lang['interface_latest_blogs_videos_only'] = "Videos only";
	$lang['interface_latest_blogs_audio_only'] = "Audio only";
	$lang['interface_latest_blogs_quotes_only'] = "Quotes only";
	$lang['interface_latest_blogs_latest_blogs'] = "Latest blogs";
	$lang['interface_latest_blogs_author'] = "Author";
	$lang['interface_latest_blogs_blog_title'] = "Blog title";
	$lang['interface_latest_blogs_comments'] = "Comments";
	$lang['interface_latest_blogs_toops'] = "Toops";
	
	$lang['interface_most_discussed_title'] = "Most discussed";
	$lang['interface_most_discussed_news_title'] = "Title";
	$lang['interface_most_discussed_author'] = "Author";
	$lang['interface_most_discussed_category'] = "Type";
	$lang['interface_most_discussed_comments'] = "Comments";
	
	$lang['interface_post_preview_toop'] = "Toop";
	$lang['interface_post_preview_views'] = "Views";
	$lang['interface_post_preview_comments'] = "Comments";
	$lang['interface_post_preview_publish'] = "Publish";
	$lang['interface_post_preview_save'] = "Save";
	$lang['interface_post_preview_go_back'] = "Go Back";
	
	$lang['interface_search_match'] = "Your search criteria matched a total of <strong>%d</strong> result(s).";
	$lang['interface_search_no_match'] = "Your search criteria did not match any results";
	$lang['interface_search_fullname'] = "Full Name";
	$lang['interface_search_bio'] = "Bio";
	$lang['interface_search_placeholder'] = "Type in your search";
	$lang['interface_search_search'] = "Search";
	
	$lang['interface_elements_latest_blogs'] = "Latest Blogs";
	$lang['interface_elements_view_more'] = "View more";
	$lang['interface_elements_total_comments'] = "%d comments";
	
	$lang['interface_elements_most_discussed'] = "Most Discussed";
	$lang['interface_elements_news_categories'] = "News Categories";
	$lang['interface_elements_popular_tags'] = "Popular Blog Tags";
	$lang['interface_elements_received_comments'] = "Received Comments";
	$lang['interface_elements_read_more'] = "Read more";
	$lang['interface_elements_views'] = "Views";
	$lang['interface_elements_comments'] = "Comments";
	$lang['interface_elements_toop'] = "Toop";
	$lang['interface_elements_latest_activity'] = "Latest Activity";
	$lang['interface_elements_toops'] = "Toops";
	$lang['interface_elements_followers'] = "Followers";
	$lang['interface_elements_following'] = "Following";
	$lang['interface_elements_members'] = "Members";
	
	$lang['interface_'] = "";
	
	$lang['interface_report_comment_0'] = "Choose one";
	$lang['interface_report_comment_1'] = "Personal abuse";
	$lang['interface_report_comment_2'] = "Off topic";
	$lang['interface_report_comment_3'] = "Legal issue";
	$lang['interface_report_comment_4'] = "Trolling";
	$lang['interface_report_comment_5'] = "Hate speech";
	$lang['interface_report_comment_6'] = "Offensive/Threatening language";
	$lang['interface_report_comment_7'] = "Copyright";
	$lang['interface_report_comment_8'] = "Spam";
	$lang['interface_report_comment_9'] = "Other";
	
	// country 
	$lang['af'] = "Afghanistan";
	$lang['al'] = "Albania";
	$lang['dz'] = "Algeria";
	$lang['as'] = "American Samoa";
	$lang['ad'] = "Andorra";
	$lang['ao'] = "Angola";
	$lang['ai'] = "Anguilla";
	$lang['aq'] = "Antarctica";
	$lang['ag'] = "Antigua And Barbuda";
	$lang['ar'] = "Argentina";
	$lang['am'] = "Armenia";
	$lang['aw'] = "Aruba";
	
?>