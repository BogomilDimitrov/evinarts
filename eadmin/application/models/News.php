<?php

/**
 *
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved.
 *
 * News model
 *
 * @file			News.php
 * @category		Application
 * @author			Elenko Ivanov
 *
 */
class News {

    // Generate search string from search array
    public static function whereStringFromArray($array = "") {

        // If we don't have search conditions
        if (!is_array($array) OR empty($array))
            return;

        // Form search array
        foreach ($array AS $k => $v) {
            if (strstr($v, "%")) {
                $input[] = $k . " LIKE '" . $v . "'";
            } else if (strstr($v, "!")) {
                $input[] = $k . " != '" . str_replace('!', '', trim($v)) . "'";
            } else if (strstr($v, "<")) {
                $input[] = $k . " < " . str_replace('<', '', trim($v));
            } else if (strstr($v, ">")) {
                $input[] = $k . " > " . str_replace('>', '', trim($v));
            } else if (strstr($v, "<=")) {
                $input[] = $k . " <= " . str_replace('<=', '', trim($v));
            } else if (strstr($v, ">=")) {
                $input[] = $k . " >= " . str_replace('>=', '', trim($v));
            } else if (strstr($v, "in[")) {
                $input[] = $k . " in (" . str_replace('in[','', $v) . ")";
			} else {
                $input[] = $k . " = '" . $v . "'";
            }
        }

        // Form search string
        $where = " WHERE " . implode(" AND ", $input);

        // Return search string
        return $where;
    }
    
    // Generate order string from array
    public static function orderStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " ORDER BY " . $array['by'] . " " . $array['type'];
        }
    }

    // Generate limit string from array
    public static function limitStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " LIMIT " . $array['from'] . "," . $array['count'];
        }
    }

    // Get News
    public static function getNews($where = "", $order = "", $limit = "", $group = "") {
        $db = Registry::get("db");

        // Generate where clause
        if(is_array($where)) $where = self::whereStringFromArray($where);
        
        // Generate order clause
		if(is_array($order)) $order = self::orderStringFromArray($order);

        // Generate limit clause
        $limit = self::limitStringFromArray($limit);
				
        // Get News
        return $db->fetchAll("SELECT p.*
                                FROM `news` AS p
                                $where $group $order $limit");
    }
	
    // Count News
    public static function countNews($where = "") {
        $db = Registry::get("db");

		// Generate where clause
        $where = self::whereStringFromArray($where);
		
		
        // Count products
        return $db->fetchOne("SELECT COUNT(*)
								FROM `news` as p
								$where");
    }
}
?>