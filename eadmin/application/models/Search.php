<?php

/**
 *
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved.
 *
 * Search model
 *
 * @file			Search.php
 * @category		Application
 * @author			Elenko Ivanov
 *
 */
class Search {

    // Generate search string from search array
    public static function whereStringFromArray($array = "") {

        // If we don't have search conditions
        if (!is_array($array) OR empty($array))
            return;

        // Form search array
        foreach ($array AS $k => $v) {
            if (strstr($v, "%")) {
                $input[] = $k . " LIKE '" . $v . "'";
            } else if (strstr($v, "!")) {
                $input[] = $k . " != '" . str_replace('!', '', trim($v)) . "'";
            } else if (strstr($v, "<")) {
                $input[] = $k . " < " . str_replace('<', '', trim($v));
            } else if (strstr($v, ">")) {
                $input[] = $k . " > " . str_replace('>', '', trim($v));
            } else if (strstr($v, "<=")) {
                $input[] = $k . " <= " . str_replace('<=', '', trim($v));
            } else if (strstr($v, ">=")) {
                $input[] = $k . " >= " . str_replace('>=', '', trim($v));
            } else {
                $input[] = $k . " = '" . $v . "'";
            }
        }

        // Form search string
        $where = " WHERE " . implode(" AND ", $input);

        // Return search string
        return $where;
    }
    
    // Generate order string from array
    public function orderStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " ORDER BY " . $array['by'] . " " . $array['type'];
        }
    }

    // Generate limit string from array
    public function limitStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " LIMIT " . $array['from'] . "," . $array['count'];
        }
    }

    // Search product
    public static function searchProducts($query, $order = "", $limit = "", $group = "", $approved = "") {
        $db = Registry::get("db");

        // Generate order clause
        $order = self::orderStringFromArray($order);
        
        // Generate limit clause
        $limit = self::limitStringFromArray($limit);
		
		// Replace chars
		$query = trim(str_replace(array('-','$','(',')','%','#','$','^','+','|','!','&'), ' ', $query));
		
		// Get Products
		return $db->fetchAll("SELECT p.*, c.idparrent, c.title_bg as category_title
								FROM `products` AS p
								LEFT JOIN `category` c ON c.idcategory = p.idcategory
								WHERE p.title_bg  LIKE '%" . $query . "%'
								$approved
								$group $order $limit");
		
        // Get data 
        return $db->fetchAll("SELECT p.*, u.username, 
								(MATCH (p.title) AGAINST ('*$query*' IN BOOLEAN MODE) 
								OR MATCH (t.name) AGAINST ('*$query*' IN BOOLEAN MODE)) AS score
								FROM `posts` AS p
								LEFT JOIN `posts_tags` AS t ON t.idpost = p.idpost
								LEFT JOIN `users` AS u ON u.iduser = p.iduser
								WHERE (MATCH (p.title) AGAINST ('*$query*' IN BOOLEAN MODE) 
								OR MATCH (t.name) AGAINST ('*$query*' IN BOOLEAN MODE))
								AND p.status = 'published' AND u.state = 'active'
                                $approved
                                $group $order $limit");
   }

    // Search user
    public static function searchUser($query, $order = "", $limit = "", $group = "") {
        $db = Registry::get("db");

        // Generate order clause
        $order = self::orderStringFromArray($order);
        
        // Generate limit clause
        $limit = self::limitStringFromArray($limit);
		
		// Replace chars
		$query = trim(str_replace(array('-','$','(',')','%','#','$','^','+','|','!','&'), ' ', $query));

        // Get data 
        return $db->fetchAll("SELECT u.*, MATCH (u.username, u.fullname) AGAINST ('" . $query . "*' IN BOOLEAN MODE) AS score, 
								LEFT JOIN `users` AS u
                                WHERE MATCH (u.username, u.fullname) AGAINST ('" . $query . "*' IN BOOLEAN MODE) 
								AND u.state = 'active'
                                $group $order $limit");
    }
}

?>