<?php

/**
 *
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved.
 *
 * Products model
 *
 * @file			Products.php
 * @category		Application
 * @author			Elenko Ivanov
 *
 */
class Products {

    // Generate search string from search array
    public static function whereStringFromArray($array = "") {

        // If we don't have search conditions
        if (!is_array($array) OR empty($array))
            return;

        // Form search array
        foreach ($array AS $k => $v) {
            if (strstr($v, "%")) {
                $input[] = $k . " LIKE '" . $v . "'";
            } else if (strstr($v, "!")) {
                $input[] = $k . " != '" . str_replace('!', '', trim($v)) . "'";
            } else if (strstr($v, "<")) {
                $input[] = $k . " < " . str_replace('<', '', trim($v));
            } else if (strstr($v, ">")) {
                $input[] = $k . " > " . str_replace('>', '', trim($v));
            } else if (strstr($v, "<=")) {
                $input[] = $k . " <= " . str_replace('<=', '', trim($v));
            } else if (strstr($v, ">=")) {
                $input[] = $k . " >= " . str_replace('>=', '', trim($v));
            } else if (strstr($v, "in[")) {
                $input[] = $k . " in (" . str_replace('in[','', $v) . ")";
			} else {
                $input[] = $k . " = '" . $v . "'";
            }
        }

        // Form search string
        $where = " WHERE " . implode(" AND ", $input);

        // Return search string
        return $where;
    }
    
    // Generate order string from array
    public static function orderStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " ORDER BY " . $array['by'] . " " . $array['type'];
        }
    }

    // Generate limit string from array
    public static function limitStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " LIMIT " . $array['from'] . "," . $array['count'];
        }
    }

    // Get products
    public static function getProducts($where = "", $order = "", $limit = "", $group = "") {
        $db = Registry::get("db");

        // Generate where clause
        if(is_array($where)) $where = self::whereStringFromArray($where);
        
        // Generate order clause
		if(is_array($order)) $order = self::orderStringFromArray($order);

        // Generate limit clause
        $limit = self::limitStringFromArray($limit);
		
        // Get products
        return $db->fetchAll("SELECT p.*, c.idparrent, c.title_bg as category_title 
                                FROM `products` AS p
								LEFT JOIN `category` c ON c.idcategory = p.idcategory
                                $where $group $order $limit");
    }
	
    // Count posts
    public static function countProducts($where = "") {
        $db = Registry::get("db");

		// Generate where clause
        $where = self::whereStringFromArray($where);
		
		
        // Count products
        return $db->fetchOne("SELECT COUNT(*)
								FROM `products` as p
								$where");
    }
}
?>