<?php

/**
 *
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved.
 *
 * Carts model
 *
 * @file			Carts.php
 * @category		Application
 * @author			Elenko Ivanov
 *
 */
class Carts {

    // Generate search string from search array
    public static function whereStringFromArray($array = "") {

        // If we don't have search conditions
        if (!is_array($array) OR empty($array))
            return;

        // Form search array
        foreach ($array AS $k => $v) {
            if (strstr($v, "%")) {
                $input[] = $k . " LIKE '" . $v . "'";
            } else if (strstr($v, "!")) {
                $input[] = $k . " != '" . str_replace('!', '', trim($v)) . "'";
            } else if (strstr($v, "<")) {
                $input[] = $k . " < " . str_replace('<', '', trim($v));
            } else if (strstr($v, ">")) {
                $input[] = $k . " > " . str_replace('>', '', trim($v));
            } else if (strstr($v, "<=")) {
                $input[] = $k . " <= " . str_replace('<=', '', trim($v));
            } else if (strstr($v, ">=")) {
                $input[] = $k . " >= " . str_replace('>=', '', trim($v));
            } else {
                $input[] = $k . " = '" . $v . "'";
            }
        }

        // Form search string
        $where = " WHERE " . implode(" AND ", $input);

        // Return search string
        return $where;
    }
    
    // Generate order string from array
    public static function orderStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " ORDER BY " . $array['by'] . " " . $array['type'];
        }
    }

    // Generate limit string from array
    public static function limitStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " LIMIT " . $array['from'] . "," . $array['count'];
        }
    }

    // Get carts
    public static function getCarts($where = "", $order = "", $limit = "", $group = "") {
        $db = Registry::get("db");

        // Generate where clause
        if(is_array($where)) $where = self::whereStringFromArray($where);
        
        // Generate order clause
		if(is_array($order)) $order = self::orderStringFromArray($order);

        // Generate limit clause
        $limit = self::limitStringFromArray($limit);
		
        // Get carts
        return $db->fetchAll("SELECT p.*
                                FROM `carts` AS p
                                $where $group $order $limit");
    }
	
	// List products in cart
    public static function listProductCart($idcart){
		
		$db = Registry::get("db");
		
		return $db->fetchAll("SELECT cp.*, p.`title_bg` as title, p.`price_1`, p.`status`, p.`picture`, p.`alias_bg` as alias, p.idproduct, 
								c.title_bg as category_title, c.slug_bg as category_alias
								FROM `carts_products` as cp
								LEFT JOIN `products` p ON p.idproduct = cp.id_product
								LEFT JOIN `category` c ON p.`idcategory` = c.`idcategory`
								WHERE cp.`id_cart`=" . $idcart . " 
								ORDER BY cp.`id` ASC");
	}
	
    // Count carts
    public static function countCarts($where = "") {
        $db = Registry::get("db");

		// Generate where clause
        $where = self::whereStringFromArray($where);
		
        // Count products
        return $db->fetchOne("SELECT COUNT(*)
								FROM `carts`
								$where");
    }	
}
?>