<?php
/**
* X3M 1.0
*    
* This file may not be redistributed in whole or significant part.
* --------------- X3M IS NOT FREE SOFTWARE ----------------
* 
* Copyright 2008-2009 All Rights Reserved.
* 
* Loader class - Load the models, pachages and etc.
* 
* @file			Loader.class.php
* @subpackage	Main
* @category		Core
* @author		Jordan Trifonov
*   	
*/
class Loader{

	// Find and load class
	public static function loadClass($class){
		
		// Posible files path
		$files_path = array(
			"library/packages/".$class."/",
			"application/models/"
		);
		
		// Check all paths
		foreach($files_path AS $path){

			// Form the class file
			$file_path = $path.$class.".php";
			
			// Check if the class file exists
			if(is_file($file_path)){
				
				// If the class or the interface are not there return
				if (class_exists($class, false) || interface_exists($class, false)) {
					return;
				}
				
				// If everything is ok include the class file
				return include_once $file_path;
			}
		}
	}
	
	// Checks if a controller exists in the user area
	public static function controllerExists($controller){
	
		$config = Registry::get("config");
		
		$file_path = $config['userarea_dir'] . "application/controllers/" . ucfirst(strtolower($controller)) . "Controller.php";
		
		// Check if the controller file exists
		if(is_file($file_path)){
		
			// Controller exists - return true
			return true;
		}else{
		
			// Controller does not exist - return false
			return false;
		}
	}
}

?>