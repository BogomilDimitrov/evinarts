<?php

/**
 * Balkanec.bg 1.1
 *    
 * This file may not be redistributed in whole or significant part.
 * --------------- NOT FREE SOFTWARE ----------------
 * 
 * Copyright 2013 All Rights Reserved.
 * 
 * Database class - Executing DB queries
 * 
 * @file			Database.class.php
 * @subpackage	Main
 * @category	Core
 * @author		Elenko Ivanov
 *   	
 */
class Database {

    // PDO connection
    public $_connection = '';
    // Debugger on or off
    private $debugger = NULL;
    // Queries counter
    public $debug_counter = 1;
    // Debugger data
    public $debugger_data = array();

    // Connect to DB
    public function __construct($config) {

        // Check the existing connection
        if ($this->_connection)
            return;

        // Try to connect to the db
        try {
            $db = new PDO("mysql:host=" . $config['host'] . ";dbname=" . $config['database'], $config['username'], $config['password']);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        // Set the connection
        $this->_connection = $db;

        // Set encoding
        $this->_connection->query("SET NAMES '" . $config['charset'] . "'");

        // Check to start or not the debugger
        ($config['debugger']) ? $this->debugger = true : $this->debugger = false;
    }

    // Get all found rows
    public function fetchAll($query) {

        // Get start time
        $start = microtime(true);

        // Execure the query
        $sth = $this->_connection->prepare($query);
        $sth->execute();

        // Fetch data
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
		
		// Unsanitize data
		$result = Filter::unsanitize($result);

        // Get end time
        $end = microtime(true);

        // If the debugger is on - add the time and the query
        if ($this->debugger)
            $this->debugger_data[] = array("time" => round($end - $start, 4), "query" => $query);

        // Return fetched data
        return $result;
    }
	
    // Get all rows within a column
    public function fetchColumn($query) {

        // Get start time
        $start = microtime(true);

        // Execure the query
        $sth = $this->_connection->prepare($query);
        $sth->execute();

        // Fetch data
        $result = $sth->fetchAll(PDO::FETCH_COLUMN);
		
		// Unsanitize data
		$result = Filter::unsanitize($result);

        // Get end time
        $end = microtime(true);

        // If the debugger is on - add the time and the query
        if ($this->debugger)
            $this->debugger_data[] = array("time" => round($end - $start, 4), "query" => $query);

        // Return fetched data
        return $result;
    }

    // Get a row
    public function fetchRow($query) {

        // Get start time
        $start = microtime(true);

        // Execure the query
        $sth = $this->_connection->prepare($query);
        $sth->execute();

        // Fetch data
        $result = $sth->fetch(PDO::FETCH_ASSOC);
		
		// Unsanitize data
		$result = Filter::unsanitize($result);

        // Get end time
        $end = microtime(true);

        // If the debugger is on - add the time and the query
        if ($this->debugger)
            $this->debugger_data[] = array("time" => round($end - $start, 4), "query" => $query);

        // Return fetched data
        return $result;
    }

    // Get one field
    public function fetchOne($query) {

        // Get start time
        $start = microtime(true);

        // Execure the query
        $sth = $this->_connection->prepare($query);
        $sth->execute();

        // Fetch data
        $result = $sth->fetch(PDO::FETCH_NUM);
		
		// Unsanitize data
		$result = Filter::unsanitize($result);

        // Get end time
        $end = microtime(true);

        // If the debugger is on - add the time and the query
        if ($this->debugger)
            $this->debugger_data[] = array("time" => round($end - $start, 4), "query" => $query);

        // Return fetched data
        return $result[0];
    }

    // Get the last insert id
    public function lastInsertId() {

        // Return the last insert id
        return $this->_connection->lastInsertId();
    }

    // Insert record
    public function insert($table, $data, $secure = true) {

        // If the insert data is not an array
        if (!is_array($data))
            return;

        // Filter the input data
        foreach ($data as $col => $val) {
            $cols[] = "`".$col."`";

            if ($secure == true) {
                if (is_float($val)) {
                    $vals[] = htmlspecialchars(addslashes(trim($val)), ENT_QUOTES, 'UTF-8', false);
                } else {
                    $vals[] = "'" . htmlspecialchars(addslashes(trim($val)), ENT_QUOTES, 'UTF-8', false) . "'";
                }
            } else {
                if (is_float($val)) {
                    $vals[] = $val;
                } else {
                    $vals[] = "'" . $val . "'";
                }
            }
        }

        // Form the SQL query
        $query = "INSERT INTO " . $table . ' (' . implode(', ', $cols) . ') '
                . 'VALUES (' . implode(', ', $vals) . ')';
				
        // Get start time
        $start = microtime(true);

        // Execure the query		
        $sth = $this->_connection->prepare($query);
        $result = $sth->execute();

        // Get end time
        $end = microtime(true);

        // If the debugger is on - add the time and the query
        if ($this->debugger)
            $this->debugger_data[] = array("time" => round($end - $start, 4), "query" => $query);

        // If the query is not executed properly
        if (!$result)
            return false;
        else
            return true;
    }

    // Delete record
    public function delete($table, $where, $limit='') {

        // Check the data
        if (!isset($table) || !is_string($table))
            return false;
        if (!isset($where) || !is_string($where))
            return false;
        if (!empty($limit) AND !is_int($limit))
            return false;

        // Set some limit
        if (!empty($limit))
            $limit = " LIMIT " . $limit;

        // Clear the data
        $table = htmlspecialchars(addslashes(trim($table)));
        $where = htmlspecialchars(trim($where));

        // Form the SQL query
        $query = "DELETE from " . $table . " WHERE " . $where . $limit;
        unset($table, $where, $limit);

        // Get start time
        $start = microtime(true);

        // Execure the query	
        $sth = $this->_connection->prepare($query);
        $result = $sth->execute();

        // Get end time
        $end = microtime(true);

        // If the debugger is on - add the time and the query
        if ($this->debugger)
            $this->debugger_data[] = array("time" => round($end - $start, 4), "query" => $query);

        // Check if the query run ok
        if (!$result)
            return false; else
            return true;
    }

    // Update record
    public function update($table, array $update, $where, $filter = true) {

        // Clear the data and form the update string
        foreach ($update as $key => $val) {
            if ($filter == true) {
                if (!is_float($val)) {
                    $val = htmlspecialchars(addslashes(trim($val)), ENT_QUOTES, 'UTF-8', false);
                } else {
                    
                }
            }

            if (!is_float($val)) {
                $update_string .= $key . "='" . $val . "'";
                $update_array[] = $key . "='" . $val . "'";
            } else {
                $update_string .= $key . "='" . $val . "'";
                $update_array[] = $key . "='" . $val . "'";
            }

            if ($count > $i)
                $update_string .= ', ';
            $i++;

            $update_string = implode(", ", $update_array);
        }

        // Form the SQL query
        $query = "UPDATE " . $table . " SET " . $update_string . " WHERE " . $where;

        // Unset variables
        unset($table, $update_string, $where);

        // Get start time
        $start = microtime(true);

        // Execure the query
        $sth = $this->_connection->prepare($query);
        $result = $sth->execute();

        // Get end time
        $end = microtime(true);

        // If the debugger is on - add the time and the query
        if ($this->debugger)
            $this->debugger_data[] = array("time" => round($end - $start, 4), "query" => $query);

        // Check if the query run ok
        if (!$result)
            return false; else
            return true;
    }

    // Execute a query
    public function query($query) {

        // Get start time
        $start = microtime(true);

        // Execure the query
        $sth = $this->_connection->prepare($query);
        $result = $sth->execute();

        // Get end time
        $end = microtime(true);

        // If the debugger is on - add the time and the query
        if ($this->debugger)
            $this->debugger_data[] = array("time" => round($end - $start, 4), "query" => $query);

        // Check if the query run ok
        if (!$result)
            return false; else
            return true;
    }

    // MySQL debugger - show queryies in the footer
    public function showSQLDebugger() {

        // Check if the debugger is on
        if ($this->debugger) {

            // Form the main table
            $return = '';

            $return .= '
				<table border="1" cellpadding="2" cellspacing="2" width="100%" style="font-size: 11px; color: black;">
					<tr>
						<td align="center" colspan="3"><b>MySQL Debugger Results</b></td>
					</tr>
					<tr>
						<td align="center" width="5%"><i>Count</i></td>
						<td align="left" width="75%"><i>SQL query</i></td>
						<td align="center"><i>Time to execute</i></td>
					</tr>
			';

            // Store all queries time
            $time = 0;

            // Add queries data
            foreach ($this->debugger_data as $key => $value) {
                $return .= '
					<tr>
						<td align="center"><b>' . $key . '</b></td>
						<td align="left"><b>' . $value['query'] . '</b></td>
						<td align="center"><b>' . $value['time'] . '</b></td>
					</tr>
				';
                $time += $value['time'];
            }

            $return .= '
					<tr>
						<td align="center" style="color: red"> - - </td>
						<td align="left" style="color: red"><b>All Queries Time</b></td>
						<td align="center" style="color: red"><b>' . $time . '</b></td>
					</tr>
				';

            // Show the SQL debugger
            echo $return . "</table><br /><br />";
        }
    }

}

?>