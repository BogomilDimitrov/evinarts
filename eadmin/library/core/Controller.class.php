<?php

/**
 * X3M 1.0
 *
 * This file may not be redistributed in whole or significant part.
 * --------------- X3M IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2008-2009 All Rights Reserved.
 *
 * Main controller class - Part of the MVC model
 *
 * @file			Controller.class.php
 * @subpackage	Main
 * @category		Core
 * @author		Jordan Trifonov
 *
 */
class Controller {

    // Set default controller path
    protected $_defaultPath = "application/controllers/";
    // Default controller
    protected $_defaultController = "Index";
    // Default action
    protected $_defaultAction = "index";
    // Controller
    protected $_controller = NULL;
    // Action
    protected $_action = NULL;
    // URL Array
    protected $url_array = array();

    // Set controller
    private function setController($controller) {
        $this->_controller = $controller;
    }

    // Get controller
    private function getController() {
        return ucfirst($this->_controller);
    }

    // Set action
    private function setAction($action) {
        $this->_action = $action;
    }

    // Get action
    private function getAction() {
        return $this->_action;
    }

    // Check if file exists
    private function checkFile($file_path) {
        return file_exists($file_path);
    }

    // Check if the method exists
    private function checkMethod($object, $method) {
        return method_exists($object, $method);
    }

    // Filter for the url
    private function filter($str = "") {
        return preg_replace('/[^a-z0-9]+/i', "", $str);
    }

    // Main controller and action management
    public function Controller() {

        // Get controller and action from the url
        $url = $_SERVER['REQUEST_URI'];
        $url_array = explode("/", $url);

        // Store the url params
        $this->url_array = $url_array;

        // Shit the url
        array_shift($url_array);

        // Check if trying to access admin controllers - search in admin folder
        $controller = $this->filter($url_array[0]);
        $action = $this->filter($url_array[1]);

        // Set the controller
        (empty($controller)) ? $this->setController($this->_defaultController) : $this->setController(ucfirst(strtolower($controller)));

        // Set the action
        (empty($action)) ? $this->setAction($this->_defaultAction) : $this->setAction(strtolower($action));
    }

    // Start X3M
    public function Start($router = "") {

        // Check if we have router
        if (is_array($router)) {
            if ($router['controller'])
                $this->setController($router['controller']);
            if ($router['action'])
                $this->setAction($router['action']);
        }

        // Form the controller path
        $controller_path = $this->_defaultPath . $this->getController() . 'Controller.php';

        // Check if the current file exists
        if (!$this->checkFile($controller_path)) {
            $controller_path = $this->_defaultPath . $this->_defaultController . 'Controller.php';
            $this->setAction("_call");
            $this->setController($this->_defaultController);
        }

        // Load the controller file
        require_once $controller_path;

        // Get controller class name
        $controller_class = $this->getController() . 'Controller';

        // Create object
        $controller = new $controller_class;

        // Check if init method exists
        if ($this->checkMethod($controller, 'init')) {
            $controller->init();
        }

        // Check action - if no action then use the call method
        $action_name = $this->getAction() . "Action";

        if (!$this->checkMethod($controller_class, $action_name)) {
            $action_name = "_call";
        }

        // Run controller action
        $controller->$action_name();
    }

    // Get URL parameters
    public function getParam($variable, $default = NULL) {

        // Get the key of the param
        $key = array_search($variable, $this->url_array);

        // If the parameter is found get the value
        if ($key)
            return $this->url_array[$key + 1];
        else
            return $default;
    }

}

?>