<?php
/**
* X3M 1.0
*    
* This file may not be redistributed in whole or significant part.
* --------------- X3M IS NOT FREE SOFTWARE ----------------
* 
* Copyright 2008-2009 All Rights Reserved.
* 
* Registry class - transport the variables
* 
* @file			Controller.class.php
* @subpackage	Main
* @category		Core
* @author		Jordan Trifonov
*   	
*/
class Registry{

	// Storage
	public static $register = array();


	// Set value
	public static function set($var, $name = ""){		
		self::$register[$name] = $var;
	}


	// Get value
	public static function get($name){
		return self::$register[$name];
	}


	// Check value
	public static function isRegister($name){
		return array_key_exists($name, self::$register);
	}

}

?>
