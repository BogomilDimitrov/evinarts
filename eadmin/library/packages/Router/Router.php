<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- X3M IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2008-2009 All Rights Reserved.
 *
 * Router class
 *
 * @file             System.class.php
 * @subpackage       Main
 * @category         Router
 * @author           Jordan Trifonov
 *
 */
class Router {

    // Check if we have a rewrite rule for the current url
    public static function checkURL() {

        // Get the current url
        $url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $url = urldecode($url);
		$rq  = urldecode($_SERVER['REQUEST_URI']);
		
		// Get only first parameter
		$params = explode('/', substr($rq, 1));
		
		// Posts router
		if($params[0] == 'posts' && ($params[1] == 'edit')){
		
            // Set controller and action
            $rewrite['controller'] = 'posts';
            $rewrite['action'] = $params[1] . '_' . $params[2];
        }

        // Return rewrite array
        return $rewrite;
    }

}

?>
