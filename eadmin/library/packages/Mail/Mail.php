<?php
/**
* X3M 1.0
*    
* This file may not be redistributed in whole or significant part.
* --------------- X3M IS NOT FREE SOFTWARE ----------------
* 
* Copyright 2008-2009 All Rights Reserved.
* 
* Emails
* 
* @file			Mail.php
* @subpackage	Mail
* @category		Core
* @author		Jordan Trifonov
*   	
*/

class Mail {


	/**
	 * ����� �������
	 *  	 
	 * @date		3.11.2009     
     * @author		Jordan Trifonov
	 * @return		bool
	 */	
	static public function sendEmail($from, $to, $title, $msg){
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$headers .= "From: $from". "\r\n";
		$headers .= "Reply-To: $to" .  "\r\n";
		
 
		$message = html_entity_decode($msg);  
        if(mail($to, '=?UTF-8?B?'.base64_encode($title).'?=', nl2br($message), $headers)){
			return true;
		} else {
			return false;
		}
	}

	 
}