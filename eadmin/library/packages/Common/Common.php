<?php

/**
 * X3M 1.0
 *    
 * This file may not be redistributed in whole or significant part.
 * --------------- X3M IS NOT FREE SOFTWARE ----------------
 * 
 * Copyright 2008-2009 All Rights Reserved.
 * 
 * Common
 * 
 * @file			Common.php
 * @subpackage	Common
 * @category		Packages
 * @author		Martin Tsvetanov
 *   	
 */
class Common {

    // Get page referer
    public function getReferer() {

        if (stripos($_SERVER['HTTP_REFERER'], $this->config['site_url']) !== false
                AND stripos($_SERVER['HTTP_REFERER'], 'posts/create') === false
                AND stripos($_SERVER['HTTP_REFERER'], 'posts/edit') === false
                AND stripos($_SERVER['HTTP_REFERER'], 'posts/preview') === false) {

            // Set proper referer
            $cancel = $_SERVER['HTTP_REFERER'];
            $_SESSION['referer'] = $_SERVER['HTTP_REFERER'];
        }

        // If referer is empty or invalid
        else if (!empty($_SESSION['referer'])) {

            // Get last valid referer from session
            $cancel = $_SESSION['referer'];
        }

        // If no referer set
        else {

            // Redirect to blog
            $cancel = $this->config['site_url'] . $_SESSION['user']['username'];
        }

        // Return referer
        return $cancel;
    }

    // Get remote file via cURL
    public function curlGetFile($url) {
	
		// Parse URL
		$parsed_url = parse_url($url);
		
		// Build referer
		$referer = $parsed_url['scheme'] . '://' . $parsed_url['host'];

        // Get remote file content
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 Windows NT 5.1 AppleWebKit/536.5 KHTML, like Gecko Chrome/19.0.1084.56 Safari/536.5"); // Set user agent
        curl_setopt($ch, CURLOPT_HEADER, 0); // Do not return headers
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Call to curl_exec returns the HTML from the web page as a variable instead of echoing it out to standard output.
		curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10); // Set timeout
        curl_setopt($ch, CURLOPT_REFERER, $referer); // Set refferer
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_URL, $url); // Set target URL
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // Follow redirects
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Do not verify encrypted connections
        $content = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        return array(
            'content' => $content,
            'info' => $info
        );
    }

    // Generate thumbnail
    public static function generateThumbnail($width, $height, $file_path, $thumb_path, $resize = true) {

        // Load needed classes
        Loader::loadClass('Image');

        // Load thumbs class
        $image = new Image();

        // Generate thumb
        copy($file_path, $thumb_path);
        $image->start($file_path);

        // If resizing
        if ($resize == true) {

            // Crop from center
            $image->resizeWithCropFromCenter($width, $height);
        }

        // Save image
        $image->save($thumb_path);

        // CHMOD
        chmod($thumb_path, 0777);

        // Unset the image object
        unset($image);
    }

    // Make links clickable
    public function makeLinksClickable($text, $new_window = true) {
        if ($new_window == true)
            $target = ' target="blank"';
        preg_match_all('!(((ftp://|http://|https://|www.))[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', strip_tags($text), $matches);
        $matches[1] = array_unique($matches[1]);
        foreach ($matches[1] as $match) {
            $url = (strtolower(substr($match, 0, 7)) != 'http://' && strtolower(substr($match, 0, 8)) != 'https://' && strtolower(substr($match, 0, 6)) != 'ftp://') ? 'http://' . $match : $match;
            $text = str_replace($match, '<a href="' . $url . '"' . $target . '>' . $match . '</a>', $text);
        }
        return $text;
    }

    // Generate random string
    public function generateRandomString($length) {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $string = '';

        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters))];
        }

        return $string;
    }

    // Search array recursively
    public function find($haystack, $needle, $strict = false, $path = array()) {

        if (!is_array($haystack)) {
            return false;
        }

        foreach ($haystack as $key => $val) {
            if (is_array($val) && $subPath = self::find($val, $needle, $strict, $path)) {
                $path = array_merge($path, array($key), $subPath);
                return $path;
            } elseif ((!$strict && $val == $needle) || ($strict && $val === $needle)) {
                $path[] = $key;
                return $path;
            }
        }
        return false;
    }

    // Char replacer
    public function charReplacer($string) {

        if ($_SESSION['language'] == 'farsi') {

            $replace = array(
                '1' => '١',
                '2' => '۲',
                '3' => '۳',
                '4' => '۴',
                '5' => '۵',
                '6' => '۶',
                '7' => '۷',
                '8' => '۸',
                '9' => '۹',
                '0' => '۰'
            );

            foreach ($replace AS $k => $v) {
                $string = str_replace($k, $v, $string);
            }
        }

        return $string;
    }

    // Sort by user toops
    public function sortMultidimensional($x, $y) {

        if ($x == $y)
            return 0;
        else if ($x < $y)
            return 1;
        else
            return -1;
    }

    // Timespan convertion
    public function timeAgo($date) {

        $lang = Registry::get('lang');

        // for using it with preceding 'vor'            index 
        $timeStrings = array($lang['timespan_just_now'], // 0       <- now or future posts :-) 
            $lang['timespan_second_ago'], $lang['timespan_seconds_ago'], // 1,1 
            $lang['timespan_minute_ago'], $lang['timespan_minutes_ago'], // 3,3 
            $lang['timespan_hour_ago'], $lang['timespan_hours_ago'], // 5,5 
            $lang['timespan_day_ago'], $lang['timespan_days_ago'], // 7,7 
            $lang['timespan_week_ago'], $lang['timespan_weeks_ago'], // 9,9 
            $lang['timespan_month_ago'], $lang['timespan_months_ago'], // 11,12 
            $lang['timespan_year_ago'], $lang['timespan_years_ago']);      // 13,14 
        $debug = false;
        $sec = time() - (( strtotime($date)) ? strtotime($date) : $date);

        if ($sec <= 0)
            return $timeStrings[0];

        if ($sec < 2)
            return $sec . " " . $timeStrings[1];
        if ($sec < 60)
            return $sec . " " . $timeStrings[2];

        $min = $sec / 60;
        if (floor($min + 0.5) < 2)
            return floor($min + 0.5) . " " . $timeStrings[3];
        if ($min < 60)
            return floor($min + 0.5) . " " . $timeStrings[4];

        $hrs = $min / 60;
        echo ($debug == true) ? $lang['timespan_hours'] . ": " . floor($hrs + 0.5) . "<br />" : '';
        if (floor($hrs + 0.5) < 2)
            return floor($hrs + 0.5) . " " . $timeStrings[5];
        if ($hrs < 24)
            return floor($hrs + 0.5) . " " . $timeStrings[6];

        $days = $hrs / 24;
        echo ($debug == true) ? $lang['timespan_days'] . ": " . floor($days + 0.5) . "<br />" : '';
        if (floor($days + 0.5) < 2)
            return floor($days + 0.5) . " " . $timeStrings[7];
        if ($days < 7)
            return floor($days + 0.5) . " " . $timeStrings[8];

        $weeks = $days / 7;
        echo ($debug == true) ? $lang['timespan_weeks'] . ": " . floor($weeks + 0.5) . "<br />" : '';
        if (floor($weeks + 0.5) < 2)
            return floor($weeks + 0.5) . " " . $timeStrings[9];
        if ($weeks < 4)
            return floor($weeks + 0.5) . " " . $timeStrings[10];

        $months = $weeks / 4;
        if (floor($months + 0.5) < 2)
            return floor($months + 0.5) . " " . $timeStrings[11];
        if ($months < 12)
            return floor($months + 0.5) . " " . $timeStrings[12];

        $years = $weeks / 51;
        if (floor($years + 0.5) < 2)
            return floor($years + 0.5) . " " . $timeStrings[13];
        return floor($years + 0.5) . " " . $timeStrings[14];
    }

    // Fixes uploaded files array - thanks to php.net for that one
    public function fixFilesArray($files) {
        $ret = array();

        if (isset($files['tmp_name'])) {
            if (is_array($files['tmp_name'])) {
                foreach ($files['name'] as $idx => $name) {
                    $ret[$idx] = array(
                        'name' => $name,
                        'tmp_name' => $files['tmp_name'][$idx],
                        'size' => $files['size'][$idx],
                        'type' => $files['type'][$idx],
                        'error' => $files['error'][$idx]
                    );
                }
            } else {
                $ret = $files;
            }
        } else {
            foreach ($files as $key => $value) {
                $ret[$key] = self::fixFilesArray($value);
            }
        }

        return $ret;
    }
	
	// Create valid URL
	function createURLstring($string, $lang = "latin") {

		$cyr = array('а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ь', 'ю', 'я', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ь', 'Ю', 'Я');

		$lat = array('a', 'b', 'v', 'g', 'd', 'e', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u',
			'f', 'h', 'ts', 'ch', 'sh', 'sht', 'a', 'y', 'yu', 'ya', 'A', 'B', 'V', 'G', 'D', 'E', 'Zh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U',
			'F', 'H', 'Ts', 'Ch', 'Sh', 'Sht', 'A', 'Y', 'Yu', 'Ya');

		if ($lang == 'latin') {
			$string = str_replace($cyr, $lat, $string);
			// Remove any unwanted chars in the url
			$string = preg_replace("/[^a-zA-Z0-9\/_| -]/", '', $string);

		} else if ($lang == 'cyrilic') {
			$string = str_replace($lat, $cyr, $string);
		}

		$string = strtolower($string);
		$string = str_replace(" ", "-", $string);
		$string = str_replace("[", "", $string);
		$string = str_replace("]", "", $string);
		$string = str_replace("#", "", $string);
		$string = str_replace("/", "-", $string);

		// Replace more than one - with nothing
		$dashes = array(
			"--",
			"---",
			"----",
			"-----",
			"------",
			"-------",
			"--------",
			"---------",
			"----------",
		);

		return str_replace($dashes, "", $string);
	}
	
	// Slugify
	public static function slugify($text, $limit = 64){

		// Replace all non letters or digits with -
		$text = preg_replace('/\W+/', '-', $text);

		// Trim and lowercase
		$text = strtolower(trim($text, '-'));

		// Limit text
		$text = mb_substr($text, 0, $limit);

		// Return result
		return $text;
	}
}
?>