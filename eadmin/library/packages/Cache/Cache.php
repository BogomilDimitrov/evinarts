<?php
/**
* X3M 1.0
*    
* This file may not be redistributed in whole or significant part.
* --------------- X3M IS NOT FREE SOFTWARE ----------------
* 
* Copyright 2008-2009 All Rights Reserved.
* 
* Cache
* 
* @file			Cache.php
* @subpackage	Cache
* @category		Packages
* @author		Jordan Trifonov
*   	
*/
class Cache {
	
	// Cache dir
	public $dir = NULL;
	
	// Time the cache is active
	public $lifetime = NULL;

	// Cache files suffix
	public $suffix = NULL;
	
	// Construct method - set the main params
	public function __construct($cache){
		$this->dir = $cache['dir'];
		$this->lifetime = $cache['lifetime'];
		$this->suffix = $cache['suffix'];
	}
	
	
	// Create a cache
	public function set($name, $data){
	
		// Check if the data is empty
		if(empty($name) OR empty($data)) return;
		
		// Get the cache file path
		$cache_path = $this->dir . $name . $this->suffix;
		
		// Serialize the data to save it
		$data = serialize($data);
		
		// Save the data
		file_put_contents($cache_path, $data);
		
		// Change the cache file permision
		@chmod($cache_path, 0777);
		
	}
	
	
	// Get a cache
	public function get($name, $lifetime = NULL){
	
		// Check if no name is set
		if(empty($name)) return;
		
		// Check if no lifetime is set - get the default one
		if(!$lifetime) $lifetime = $this->lifetime;
		
		// Get the cache file path
		$cache_path = $this->dir . $name . $this->suffix;
		
		// Check if file exists
		if (file_exists($cache_path)) {
			
			// Get file time
			$file_time = filemtime($cache_path);
			
			// If the cache is still active
			if($file_time > (time() - $lifetime)){
				
				$data = file_get_contents($cache_path);
				
				return unserialize($data);
				
			// If the file is not active
			} else {
				
				// Remove the cache
				unlink($cache_path);
				
				// Return false
				return false;
			}
			
		} else return false;
	}
	
	
	// Delete a cache
	public function delete($name){
	
		// Check if no name is set
		if(empty($name)) return;
		
		// Get the cache file path
		$cache_path = $this->dir . $name . $this->suffix;
		
		// Unlink the cache file
		@unlink($cache_path);
	}
}
?>