<?php

/**
 * X3M 1.0
 *
 * This file may not be redistributed in whole or significant part.
 * --------------- X3M IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2008-2009 All Rights Reserved.
 *
 * Pager - Pagination for the data
 *
 * @file            Pager.php
 * @subpackage      Pager
 * @category        Packages
 * @author          Jordan Trifonov
 *
 */
class Pager {

    public $link;
    public $replace;
    public $page = 0;
    public $rows_per_page = 15;
    public $max_links_shown = 7;
    public $limit;
    public $lang;

    // Paginate
    public function paginate($all) {
        $this->lang = Registry::get('lang');
        $return = "";
        $this->limit = ($this->page * $this->rows_per_page) - $this->rows_per_page;

        $totalPage = ceil($all / $this->rows_per_page);
        if ($totalPage > 1) {

            if ($this->max_links_shown >= $totalPage) {
                for ($i = 1; $i <= $totalPage; $i++) {
                    if ($i == $this->page) {
                        $return .= '&nbsp;<a href="#" class="current">' . $i . '</a>&nbsp;';
                    } else {
                        if (strpos($this->link, $this->replace)) {
                            $return .= '&nbsp;<a href="' . str_replace($this->replace, $i, $this->link) . '" >' . $i . '</a>&nbsp;';
                        } else
                            $return .= '&nbsp;<a href="' . $this->link . $i . '">' . $i . '</a>&nbsp;';
                    }
                }
            }
            else {
                if (($this->max_links_shown % 2) != 0) {
                    $first_page_link = $this->page - (($this->max_links_shown - 1) / 2);
                    $last_page_link = $this->page + (($this->max_links_shown - 1) / 2);
                } else {
                    $first_page_link = $this->page - ($this->max_links_shown / 2) + 1;
                    $last_page_link = $this->page + ($this->max_links_shown / 2);
                }

                if ($first_page_link < 1) {
                    for ($i = $first_page_link; $i < 1; $i++) {
                        $last_page_link++;
                    }
                    $first_page_link = 1;
                }

                if ($last_page_link > $totalPage) {
                    $last_page_link = $totalPage;
                }

                if ($first_page_link != 1) {
                    if (strpos($this->link, $this->replace)) {
                        $return .= '&nbsp;<a href="' . str_replace($this->replace, 1, $this->link) . '" >' . $this->lang['first'] . '</a>&nbsp;';
                    } else
                        $return .= '&nbsp;<a href="' . $this->link . '1">' . $this->lang['first'] . '</a>&nbsp;';
                }

                if ($this->page != 1) {
                    if (strpos($this->link, $this->replace)) {
                        $return .= '&nbsp;<a href="' . str_replace($this->replace, ($this->page - 1), $this->link) . '" >' . $this->lang['before'] . '</a>&nbsp;';
                    } else
                        $return .= '&nbsp;<a href="' . $this->link . ($this->page - 1) . '">' . $this->lang['before'] . '</a>&nbsp;';
                }

                for ($i = $first_page_link; $i <= $last_page_link; $i++) {
                    if ($i == $this->page) {
                        $return .= '<a href="#" class="current">' . $i . '</a>';
                    } else {

                        if (strpos($this->link, $this->replace)) {
                            $return .= '&nbsp;<a href="' . str_replace($this->replace, $i, $this->link) . '" >' . $i . '</a>&nbsp;';
                        } else
                            $return .= '&nbsp;<a href="' . $this->link . $i . '" >' . $i . '</a>&nbsp;';
                    }
                }

                if ($this->page != $totalPage) {
                    if (strpos($this->link, $this->replace)) {
                        $return .= '&nbsp;<a href="' . str_replace($this->replace, ($this->page + 1), $this->link) . '" >' . $this->lang['next'] . '</a>&nbsp;';
                    } else
                        $return .= '&nbsp;<a href="' . $this->link . ($this->page + 1) . '">' . $this->lang['next'] . '</a>&nbsp;';
                }

                if ($last_page_link != $totalPage) {
                    if (strpos($this->link, $this->replace)) {
                        $return .= '&nbsp;<a href="' . str_replace($this->replace, $totalPage, $this->link) . '" >' . $this->lang['last'] . '</a>&nbsp;';
                    } else
                        $return .= '&nbsp;<a href="' . $this->link . $totalPage . '">' . $this->lang['last'] . '</a>&nbsp;';
                }
            }
        }

        return array($return, $totalPage);
    }

}

?>
