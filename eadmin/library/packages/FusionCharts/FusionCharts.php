<?php
/**
* X3M 1.0
*    
* This file may not be redistributed in whole or significant part.
* --------------- X3M IS NOT FREE SOFTWARE ----------------
* 
* Copyright 2008-2009 All Rights Reserved.
* 
* FusionCharts
* 
* @file			FusionCharts.php
* @subpackage	FusionCharts
* @category		Packages
* @author		Jordan Trifonov
*   	
*/
class FusionCharts{
	
	// Config data
	public $config = NULL;
	
	
	// Get config array
	function __construct(){
		$this->config 			= Registry::get('config');
	}
	
	
	// Chart renderer
	public function renderChart($chartSWF, $strURL, $strXML, $chartId, $chartWidth, $chartHeight, $debugMode, $registerWithJS) {
	
		if ($strXML=="")
        	$tempData = "//Set the dataURL of the chart\n\t\tchart_$chartId.setDataURL(\"$strURL\")";
    	else
        	$tempData = "//Provide entire XML data using dataXML method\n\t\tchart_$chartId.setDataXML(\"$strXML\")";

    	// Set up necessary variables for the RENDERCAHRT
    	$chartIdDiv = $chartId . "Div";
    	$ndebugMode = $this->boolToNum($debugMode);
    	$nregisterWithJS = $this->boolToNum($registerWithJS);

    	// create a string for outputting by the caller
$render_chart = <<<RENDERCHART
	<!-- START Script Block for Chart $chartId -->
	<div id="$chartIdDiv" align="center">
		Chart.
	</div>
	<script type="text/javascript">	
		var chart_$chartId = new FusionCharts("$chartSWF", "$chartId", "$chartWidth", "$chartHeight", "$ndebugMode", "$nregisterWithJS");
		$tempData
		//chart_$chartId.setTransparent();
		chart_$chartId.addParam('wmode', 'opaque');
		chart_$chartId.render("$chartIdDiv");
	</script>	
	<!-- END Script Block for Chart $chartId -->
RENDERCHART;

  		return $render_chart;
	}
	
	
	// Bool to num method
	static private function boolToNum($value) {
    	return (($value==true) ? 1 : 0);
	}
	
	
	// Test graphic
    public function testGraphic(){
    
		// Get config array
        $config = Registry::get('config');
		
		// Set some  test data
        $arrData[0][1] = "Product A";
   		$arrData[1][1] = "Product B";
   		$arrData[2][1] = "Product C";
   		$arrData[3][1] = "Product D";
   		$arrData[4][1] = "Product E";
   		$arrData[5][1] = "Product F";
		
   		// Store test sales data
   		$arrData[0][2] = 567500;
   		$arrData[1][2] = 815300;
   		$arrData[2][2] = 556800;
   		$arrData[3][2] = 734500;
   		$arrData[4][2] = 676800;
   		$arrData[5][2] = 648500;

   		// Initialize <chart> element
   		$strXML = "<chart caption='Sales by Product' numberPrefix='$' formatNumberScale='0'>";
		
   		// Convert data to XML and append
   		foreach ($arrData as $arSubData) {
      		$strXML .= "<set label='" . $arSubData[1] . "' value='" . $arSubData[2] . "' />";
   		}
   			
   		// Close <chart> element
   		$strXML .= "</chart>";

		// Some graph options
   		$strXML = "<chart palette='2' caption='Sales by Product' subCaption='March 2006' showValues='0' divLineDecimalPrecision='1' limitsDecimalPrecision='1' PYAxisName='Amount' SYAxisName='Quantity' numberPrefix='$' formatNumberScale='0'><categories><category label='A' /><category label='B' /><category label='C' /><category label='D' /><category label='E' /><category label='F' /><category label='G' /><category label='03/03/2007' /><category label='I' /><category label='J' /></categories><dataset seriesName='Revenue'><set value='5854' /><set value='4171' /><set value='1375' /><set value='1875' /><set value='2246' /><set value='2696' /><set value='1287' /><set value='2140' /><set value='1603' /><set value='1628' /></dataset><dataset seriesName='Profit' renderAs='Area' parentYAxis='P'><set value='3242' /><set value='3171' /><set value='700' /><set value='1287' /><set value='1856' /><set value='1126' /><set value='987' /><set value='1610' /><set value='903' /><set value='928' /></dataset><dataset lineThickness='3' seriesName='Quantity' parentYAxis='S'><set value='174' /><set value='197' /><set value='155' /><set value='15' /><set value='66' /><set value='85' /><set value='37' /><set value='10' /><set value='44' /><set value='322' /></dataset></chart>";

   		// Create the chart
   		return $this->renderChart($this->config['site_swf']."MSCombiDY2D.swf", "", $strXML, "productSales", 600, 300, false, false);
	}
	
}
?>