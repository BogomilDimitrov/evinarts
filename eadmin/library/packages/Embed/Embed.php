<?php
/**
* X3M 1.0
*    
* This file may not be redistributed in whole or significant part.
* --------------- X3M IS NOT FREE SOFTWARE ----------------
* 
* Copyright 2008-2009 All Rights Reserved.
* 
* Embed
* 
* @file			Embed.php
* @subpackage	Embed
* @category		Packages
* @author		Martin Tsvetanoff
*   	
*/
class Embed {

	// Detect video
	public function embedVideo($source, $getDetails = false){
	
		// YouTube
		if(strpos(strtolower($source), 'youtube.com') !== false || strpos(strtolower($source), 'youtu.be') !== false){
			
			// Get video ID
			$video_id = self::getYouTubeVideoID($source);
			
			if(!empty($video_id)){
			
				// Construct result
				$result = array(
					'source'	=> 'youtube',
					'video' 	=> '<iframe width="300" height="200" src="http://www.youtube.com/embed/' . $video_id . '?wmode=transparent&amp;autohide=1&amp;egm=0&amp;hd=1&amp;iv_load_policy=3&amp;modestbranding=1&amp;rel=0&amp;showinfo=0&amp;showsearch=0&amp;autoplay=0" frameborder="0" allowfullscreen=""></iframe>',
					'thumb'		=> 'http://img.youtube.com/vi/' . $video_id . '/0.jpg'
				);
				
				// Get title and description if set
				if($getDetails == true){ 
					
					// Get video metadata
					$metadata = self::getYouTubeVideoDetails($video_id);
					
					// If OK Merge arrays
					if(!empty($metadata)) $result = array_merge($result, $metadata);
					
					// If error
					else $result = 'Error occured while trying to fetch video metadata';
				}
			}
			
			// Invalid video ID
			else $result = 'Invalid video ID';
		}

		// Vimeo
		else if(strpos(strtolower($source), 'vimeo.com') !== false){
		
			// cURL data fetching
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
			curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com/?hl=en');
			curl_setopt($ch, CURLOPT_URL, $source);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$data = curl_exec($ch);
			$info = curl_getinfo($ch);
			curl_close($ch);
		
			// Predefine source url
			$source = $info['url'];

			// Get video ID
			$video_id = self::getVimeoVideoID($source);
			
			if(!empty($video_id)){
			
				// Construct result
				$result = array(
					'source'	=> 'vimeo',
					'video' 	=> '<iframe src="http://player.vimeo.com/video/' . $video_id . '?title=0&amp;byline=0&amp;portrait=0" width="500" height="330" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>'
				);
				
				// Get title and description if set
				if($getDetails == true){ 
					
					// Get video metadata
					$metadata = self::getVimeoVideoDetails($video_id);
					
					// If OK Merge arrays
					if(!empty($metadata)) $result = array_merge($result, $metadata);
					
					// If error
					else $result = 'Error occured while trying to fetch video metadata';
				}
			}
			
			// Invalid video ID
			else $result = 'Invalid video ID';
		}

		// Unknown video source
		else $result = 'Unsupported video source';
		
		// Return result
		return $result;
	}

	// Get YouTube video ID
	private function getYouTubeVideoID($source) {

		preg_match('/https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/ytscreeningroom\?v=|\/feeds\/api\/videos\/|\/user\S*[^\w\-\s]|\S*[^\w\-\s]))([\w\-]{11})[?=&+%\w-]*/i', $source, $match);
		return $match[1];
	}
	
	// Get YouTube video details
	private function getYouTubeVideoDetails($video_id){

		// Get metadata
		$temp['video_raw_details'] = file_get_contents('http://gdata.youtube.com/feeds/api/videos/' . $video_id);
		
		// Check for errors
		if(strpos($temp['video_raw_details'], 'Invalid id')) return false;
		
		// Video title
		if(preg_match("/<media:title type=\'plain\'>(.*?)<\/media:title>/", $temp['video_raw_details'], $matches) != 0) $video_details['title'] = $matches[1];
		
		// Video description
		if(preg_match("/<media:description type=\'plain\'>(.*?)<\/media:description>/", $temp['video_raw_details'], $matches) != 0) $video_details['description'] = $matches[1];

		// Return response
		return $video_details;		
	}

	// Get Vimeo video ID
	private function getVimeoVideoID($source) {

		preg_match("/vimeo.com\/([^(\&|$)]*)/", $source, $match);
		return $match[1];
	}
	
	// Get Vimeo video details
	private function getVimeoVideoDetails($video_id){

		// Get metadata
		$temp['video_raw_details'] = file_get_contents('http://vimeo.com/api/v2/video/' . $video_id . '.json');
		
		// Check for errors
		if(strpos($temp['video_raw_details'], 'not found')) return false;
		
		// Decode video data
		else $temp['video_details'] = (array) reset(json_decode($temp['video_raw_details']));

		// Video title
		if(!empty($temp['video_details']['title'])) $video_details['title'] = $temp['video_details']['title'];
		
		// Video description
		if(!empty($temp['video_details']['description'])) $video_details['description'] = $temp['video_details']['description'];

		// Return response
		return $video_details;		
	}
}

?>