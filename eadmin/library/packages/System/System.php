<?php

/**
 * X3M 1.0
 *
 * This file may not be redistributed in whole or significant part.
 * --------------- X3M IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2008-2009 All Rights Reserved.
 *
 * System class
 *
 * @file			System.class.php
 * @subpackage	Main
 * @category		Core
 * @author		Jordan Trifonov
 *
 */
class System {

    // Redirecting method
    public static function redirect($url) {

        // Chech if some headers are sent
        if (!headers_sent()) {
            header('Location: ' . $url);

            // If the headers are sent - redirect via JavaScript
        } else {
            echo '<script type="text/javascript">';
            echo 'window.location.href="' . $url . '";';
            echo '</script>';
            echo '<noscript>';
            echo '<meta http-equiv="refresh" content="0;url=' . $url . '" />';
            echo '</noscript>';
        }

        // Terminate
        exit();
    }

    // Send email
    static public function sendEmail($from, $to, $title, $msg) {
	
		// Get SMTP config file
		$config = Registry::get('config');
		
		// Include the class
		require('library/packages/Swift/swift.php');
		
		// If using SMTP
		if($config['use_smtp'] == true){
		
			// Create the Transport
			$transport = Swift_SmtpTransport::newInstance($config['smtp_host'], $config['smtp_port'])
				->setUsername($config['smtp_user'])
				->setPassword($config['smtp_pass'])
			;
		}
		
		// Use native mail function
		else {
		
			// Create the Transport
			$transport = Swift_MailTransport::newInstance();
		}
		
		// Create the Mailer using created transport
		$mailer = Swift_Mailer::newInstance($transport);

		// Create a message
		$mail = Swift_Message::newInstance($title)
			->setFrom($from)
			->setTo($to)
			->setBody($msg, 'text/html')
		;

		// Send the mail
		$response = $mailer->send($mail);

		// Return response
		return $response;
    }
	
	// Create valid URL
    public static function createURLstring($string) {
	
        $string = str_replace(" ", "-", $string);
		$string = str_replace("[", "", $string);
		$string = str_replace("]", "", $string);
		$string = str_replace("#", "", $string);
		$string = str_replace("/", "-", $string);

		// Replace more than one - with nothing
		$dashes = array(
			"--",
			"---",
			"----",
			"-----",
			"------",
			"-------",
			"--------",
			"---------",
			"----------",
		);

        return str_replace($dashes, "", $string);
    }
	
    // Detect user OS
    function detectOS($userAgent) {

        // Create list of operating systems with operating system name as array key
        $oses = array (
            'iPhone'            =>  '(iPhone)',
            'Windows 3.11'      =>  'Win16',
            'Windows 95'        =>  '(Windows 95)|(Win95)|(Windows_95)',
            'Windows 98'        =>  '(Windows 98)|(Win98)',
            'Windows 2000'      =>  '(Windows NT 5.0)|(Windows 2000)',
            'Windows XP'        =>  '(Windows NT 5.1)|(Windows XP)',
            'Windows 2003'      =>  '(Windows NT 5.2)',
            'Windows Vista'     =>  '(Windows NT 6.0)|(Windows Vista)',
            'Windows 7'         =>  '(Windows NT 6.1)|(Windows 7)',
            'Windows NT 4.0'    =>  '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
            'Windows ME'        =>  'Windows ME',
            'Open BSD'          =>  'OpenBSD',
            'Sun OS'            =>  'SunOS',
            'Linux'             =>  '(Linux)|(X11)',
            'Safari'            =>  '(Safari)',
            'Macintosh'         =>  '(Mac_PowerPC)|(Macintosh)',
            'QNX'               =>  'QNX',
            'BeOS'              =>  'BeOS',
            'OS/2'              =>  'OS/2',
            'Search Bot'        =>  '(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp/cat)|(msnbot)|(ia_archiver)'
        );

        // For each OS
        foreach($oses AS $os => $pattern){

            // Use regular expressions to check operating system type
            if(eregi($pattern, $userAgent)) {
                return $os;
            }
        }

        // If we can't find the OS
        return 'Unknown';
    }


}

?>
