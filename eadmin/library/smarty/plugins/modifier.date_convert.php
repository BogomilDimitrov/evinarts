<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */
function smarty_modifier_date_convert($date, $time = false, $time_separator = " - ")
{
   
    // Get first part - only date
    $date_parts = reset(explode(" ", $date));
    $date_parts = explode("-", $date_parts);
    
    // Check month
    switch($date_parts[1]){
        case "01":
            $month = "January";
            break;
         case "02":
            $month = "February";
            break;
        case "03":
            $month = "March";
            break;
        case "04":
            $month = "April";
            break;
        case "05":
            $month = "May";
            break;
        case "06":
            $month = "June";
            break;
        case "07":
            $month = "July";
            break;
        case "08":
            $month = "August";
            break;
        case "09":
            $month = "September";
            break;
        case "10":
            $month = "October";
            break;
        case "11":
            $month = "November";
            break;
        case "12":
            $month = "December";
            break;
    }
    
    // Return date with time
	if($time){

		return $date_parts['2']." ".$month." ".$date_parts[0]. $time_separator . date("H:m", $date);
		
	// Return date withour time
	} else {
	
		return $date_parts['2']." ".$month." ".$date_parts[0];
		
	}
}

/* vim: set expandtab: */

?>
