tinyMCEPopup.requireLangPack();

var VimeoDialog = {
    init: function () {
        var f = document.forms[0];

        // Get the selected contents as text and place it in the input
        f.vimeoURL.value = tinyMCEPopup.editor.selection.getContent({ format: 'text' });
    },

    insert: function () {
        // Insert the contents from the input into the document
        var url = document.forms[0].vimeoURL.value;
        if (url === null) { tinyMCEPopup.close(); return; }

        var code, regexRes;
		var regexRes = url.match(/http:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/);
		
        code = (regexRes === null) ? url : regexRes[2];
        if (code === "") { tinyMCEPopup.close(); return; }

        tinyMCEPopup.editor.execCommand('mceInsertContent', false, '<iframe src="http://player.vimeo.com/video/'  + code + '?title=0&amp;byline=0&amp;portrait=0" width="500" height="330" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>');
		tinyMCEPopup.close();
    }
};

tinyMCEPopup.onInit.add(VimeoDialog.init, VimeoDialog);
