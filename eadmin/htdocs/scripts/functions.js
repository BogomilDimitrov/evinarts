var Admin = {

	init: function(){
	
		// Get page name
		var page = $('body').attr('data-page');
	
		// Load default
		Admin.Tipsy();
		
		// Switchable fields
		if ($('.switchable').length) {
			Admin.switchForm();
		}

		// Duplicate fields
		if ($('.form-duplicate').length && $('.form-duplicate-action').length) {
			Admin.duplicateForm();
		}
		
		// Delete photo
		if ($('.delete-photo').length) {
			Admin.deletePhoto();
		}
		
		// Draggable images
		if ($('ul.draggable-gallery').length) {
			Admin.triggerDraggable('ul.draggable-gallery');
		}

		// Edit post
		if(page == 'edit text' || page == 'edit photo' || page == 'edit video' || page == 'edit audio' || page == 'edit poetry' || page == 'edit news'){
			Admin.editPost(page);
		}

		// Datepicker
		if ($('.datepicker').length) {
			//var date_added = $('form#post').attr('data-dateadded');
			$('.datepicker').datetimepicker({
				/* 'minDate': date_added, */
				'dateFormat': 'yy-mm-dd',
				'timeFormat': 'HH:mm:ss',
			});
		}

		// WYSIWYG Editor
		if ($('textarea.wysiwyg').length) {

			// TinyMCE Config
			var tinymce_config = {
			
				// Location of TinyMCE script
				script_url : '/htdocs/scripts/tiny_mce/tiny_mce.js',

				// General options
				theme : "advanced",
				plugins : "youtube,vimeo,jbimages,autocomplete,inlinepopups,contextmenu,noneditable,paste,table",
				
				width: 700,
				height: 350,

				// Theme options
				theme_advanced_buttons1 : "bold,italic,underline,|,fontsizeselect,fontselect,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent,|,link,unlink,image",
				theme_advanced_buttons2 : "jbimages,youtube,vimeo,|,tablecontrols,|,code",
				theme_advanced_font_sizes: "14px (default),16px,18px,20px,22px,24px,28px",
				font_size_style_values: "14px,16px,18px,20px",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_resizing : false,
				theme_advanced_path : false,
				paste_auto_cleanup_on_paste : true,
				paste_remove_styles: true,
				paste_remove_styles_if_webkit: true,
				paste_strip_class_attributes: true,
				formats : {
					blockquote : {block : 'blockquote', wrapper : 1, remove : 'all'}
				},
				
				setup : function(ed) {
					
				},

				force_br_newlines : false,
				force_p_newlines : true,
				forced_root_block : '',
				noneditable_leave_contenteditable : true,

				// Autocomplete
				autocomplete_options: {
					"options": []
				},
				
				// Formatting
				formats : {
					bold : {inline : 'span', 'classes' : 'bold'},
					italic : {inline : 'span', 'classes' : 'italic'},
					underline : {inline : 'span', 'classes' : 'underline', exact : true},
				},
				
				// Path
				relative_urls : false,
				convert_urls : false,
				remove_script_host : true,
			}

			// Remove plugins
			if ($('textarea.wysiwyg').attr('data-tinymce-plugins') == 'false') {
				//tinymce_config.height = 140;
				tinymce_config.theme_advanced_buttons1 = null; 
				tinymce_config.plugins = 'autocomplete';
			}
			
			// Set editor height
			if ($('textarea.wysiwyg').attr('data-tinymce-height')) {
				tinymce_config.height = $('textarea.wysiwyg').attr('data-tinymce-height');
			}
			
			// If editing the super featured post
			if(page == 'super_featured'){
				tinymce_config.theme_advanced_font_sizes = "14px (default),16px,18px,20px,22px,24px,28px,32px,36px,42px";
				tinymce_config.theme_advanced_buttons1 = "bold,italic,underline,|,fontsizeselect,|,addquote,removequote,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent,|,link,unlink,image,jbimages,youtube,vimeo";
				tinymce_config.theme_advanced_buttons2 = "";
			}
			
			// Init TinyMCE
			$('textarea.wysiwyg').tinymce(tinymce_config);
		}
	},
	
	Tipsy: function() {
		$('a.userpic, a.fn').tipsy({
			trigger: 'hover',
			live: true,
			opacity: 1,
			gravity: 's'
		});	
	},
	
	triggerDraggable: function(target_field) {

		$(target_field).dragsort({ dragSelector: 'img', dragEnd: function() {
			var data = $(target_field).find('li').map(function() { return $(this).data('iditem'); }).get();
			$('input[name="order"]').val(JSON.stringify(data));
		}});
	},
	
	removeMessage: function(){
		$('.message').fadeOut(300);
	},
	
	editPost: function(page){

		// Post edit
		if($('select[name="approved"]').val() == 'yes'){
			$('li#featured').show();
		}else{
			$('select[name="featured"]').val('0');
			$('input[name="featured_date"]').val('0000-00-00 00:00:00');
			$('li#featured, li#featured-date').hide();
		}
		
		if($('select[name="featured"]').val() == '1'){
			$('li#featured-date').show();
		}else{
			$('input[name="featured_date"]').val('0000-00-00 00:00:00');
			$('li#featured-date').hide();
		}
		
		// Approved
		$(document).on('change', 'select[name="approved"]', function(){
			if($(this).val() == 'yes'){
				$('li#featured').show();
			}else{
				$('select[name="featured"]').val('0');
				$('input[name="featured_date"]').val('0000-00-00 00:00:00');
				$('li#featured, li#featured-date').hide();
			}
		});
		
		// Featured
		$(document).on('change', 'select[name="featured"]', function(){
			if($(this).val() == '1'){
				var today = new Date();
				if(page == 'edit news'){
					date = today.getFullYear() + '-' + ('0' + (today.getMonth()+1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2) + ' ' + ('0' + today.getHours()).slice(-2) + ':' + ('0' + today.getMinutes()).slice(-2) + ':' + ('0' + today.getSeconds()).slice(-2);
					$('input[name="featured_date"]').val(date);
				}else{
					today.setDate(today.getDate() + 3);
					date = today.getFullYear() + '-' + ('0' + (today.getMonth()+1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2) + ' ' + ('0' + today.getHours()).slice(-2) + ':' + ('0' + today.getMinutes()).slice(-2) + ':' + ('0' + today.getSeconds()).slice(-2);
					$('input[name="featured_date"]').val(date);
				}
				$('li#featured-date').show();
			}else{
				$('input[name="featured_date"]').val('0000-00-00 00:00:00');
				$('li#featured-date').hide();
			}
		});
	},
	
	switchForm: function() {
		$(document).on('click', '.switchable a.action', function(e){
			e.preventDefault();
			$(this).parents('.switchable').addClass('hide').siblings('.switchable').removeClass('hide');
		});
	},

	duplicateForm: function() {
		var n = 1;
		var limit = 10;
		
		$(document).on('click', '.form-duplicate-action a.action', function(e){
			e.preventDefault();
			
			var clone = $('.form-duplicate:last').clone();
			$(clone).find('[type=text]').val('');
			$(clone).find('[type=file]').val('');
			
			if (n < limit) {
				n++;
				$(this).parents('.form-duplicate-action').before(clone);
				Admin.switchForm();
			}
			if (n == limit) {
				$(this).parents('.form-duplicate-action').hide();
			}
		});
	},
	
	deletePhoto: function() {
	
		var photo_li;
	
		$(document).on('click', 'a.delete-photo', function(){
			
			if($('ul.draggable-gallery').find('li').length > 1){
				photo_li = $(this).parents('li');
				$('#modal #heading').html('Confirm photo deletion');
				$('#modal #content p').html('Do you really want to delete this photo?');
				$('#modal #content #buttons').html('<a href="javascript:;" class="button green confirm">Yes</a><a href="javascript:;" id="closeCbox" class="button red deny">No</a>');
				$.colorbox({fixed:true, transition:"none", scrolling:false, width:520, overlayClose:true, inline:true, href:"#modal"});
			} else {
				$('#modal #heading').html('Photo deletion denied');
				$('#modal #content p').html('Photo blogs must contain atleast one image.');
				$('#modal #content #buttons').html('<a href="javascript:;" id="closeCbox" class="button red deny">Cancel</a>');
				$.colorbox({fixed:true, transition:"none", scrolling:false, width:520, overlayClose:true, inline:true, href:"#modal"});
			}
		});
		
		$(document).on('click', 'a.confirm', function(){
			$(photo_li).remove();
			var data = $('ul.draggable-gallery').find('li').map(function() { return $(this).data('iditem'); }).get();
			$('input[name="order"]').val(JSON.stringify(data));
			$.colorbox.close();
		});
		
		$(document).on('click', 'a.deny', function(){
			$.colorbox.close();
		});
	},
}

// Upload picture without flash
$( document ).ready(function() {

	if($('#file_upload_new').length) {
		$('#file_upload_new').on('change', function() {
			
			var product_upload = ($('#product_id').length)? 1 : 2;
			var mode = ($('#min_width').val())? 'upload_picture' : 'upload_picture_product';
			var upload_url = 'upload.php';
			var prod_id = $('#prod_id').val();
			
			var file_data = $('#file_upload_new').prop('files')[0];   
			var form_data = new FormData();
			
			form_data.append('Filedata', file_data);
			form_data.append('mode', mode);
			if (prod_id) {
				form_data.append('id', prod_id);
			}
			
			//alert(form_data);                             					
			$.ajax({
				url: '/' + upload_url,
				dataType: 'text',
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,                         
				type: 'post',
				success: function(response) {
					
					if (mode == 'upload_picture_product') {
						location.reload();
					} else {
						var img_path = $('#img_path').val();
						
						var width_x = $('#min_width').val();
						var width_y = $('#min_height').val();
						var ration  = width_x/width_y;
						
						$('#preview_img').css("width", width_x);
						$('#preview_img').css("height", width_y);
						
						$('#width_x').val(width_x);
						$('#width_y').val(width_y);
						
						var s = response.split(",");
						
						$("#ww").attr("value", s[0]);
						$("#hh").attr("value", s[1]);

						var img1 = '<img src="' + img_path + 'temp/'+s[2]+'" id="cropbox" />';
						var img2 = '<img src="' + img_path + 'temp/'+s[2]+'" id="preview" />';

						$("#crop_img").html(img1);
						$("#preview_img").html(img2);
						$("#image_path").attr("value", s[2]);
						$("#loadCrop").fadeIn("slow");
						
						LoadCrop(width_x, width_y, ration);
					}	
				}
			});
		});
	}

	function LoadCrop(width_x, width_y, ration) {
		$('#cropbox').Jcrop({
			onSelect:    showPreview,
			onChange: 	 showPreview,
			minSize: 	 [width_x, width_y],
			setSelect:   [ 30,30,80,80 ],
			aspectRatio: ration / 1
		});
	}

	function showPreview(coords) {
		var width_x = $('#min_width').val();
		var width_y = $('#min_height').val();
					
		if (parseInt(coords.w) > 0) {
			var rx = width_x / coords.w;
			var ry = width_y / coords.h;

			var ww =  $('#ww').val();
			var hh =  $('#hh').val();

			$('#preview').css({
				width: Math.round(rx * ww) + 'px',
				height: Math.round(ry * hh) + 'px',
				marginLeft: '-' + Math.round(rx * coords.x) + 'px',
				marginTop: '-' + Math.round(ry * coords.y) + 'px'
			});
			updateCoords(coords);
		}
	}

	function updateCoords(coords) {
		$('#x').val(coords.x);
		$('#y').val(coords.y);
		$('#w').val(coords.w);
		$('#h').val(coords.h);
	};

	function checkCoords() {
		if (parseInt($('#w').val())) return true;
		alert('Error');
		return false;
	};

});

// Show hide with slide animation
function showHide(id, speed){
    if($("#"+id).is(":visible")){
        $("#"+id).slideUp(speed);
    } else {
        $("#"+id).slideDown(speed);
    }
}

// Set filter dates
function setFilterDates(date_from, date_to){
	$("#date_from").val(date_from);
	$("#date_to").val(date_to);
	$("#filter_form").submit();
}

// Set top image
function makeTop(img, id, img_id, table){
	$.post( '/ajax/updateTopPicture', {ajax: 1, mode: 'makeTopImg', imgName: img, id_p: id, t: table}, function(data){
			if(data == 1) {
				$("a[id*='topim_']").each(function(){
						var a = $(this).attr('id');
						$("#"+a).removeClass("icon-3 info-tooltip selected");
						$("#"+a).addClass("icon-3 info-tooltip");
				});
				$("#topim_"+img_id).addClass("icon-3 info-tooltip selected");
			}
			else {
				alert('Error');
			}
	});
}

$(document).ready(Admin.init);