<?php /* Smarty version Smarty-3.0.8, created on 2015-03-07 10:26:41
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/faq/edit.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:177053932654fab641960d56-00361280%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ebc417ffab4b269d838f92fac0e02fba883d5f77' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/faq/edit.tpl.html',
      1 => 1425716799,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '177053932654fab641960d56-00361280',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/home/balkanec/public_html/evinarts.com/eadmin/library/smarty/plugins/modifier.escape.php';
?><?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
	<div class="path clear">
		<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <a href="/faq/browse"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_faq'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
		<strong>
			Редактиране на полезно
		</strong>
	</div><!--path-->

	<div class="page_title fleft clear bg_light round3">
		<h1 class="fleft">
			<?php echo $_smarty_tpl->getVariable('faq')->value['title_bg'];?>

		</h1>
	</div>

	<?php $_template = new Smarty_Internal_Template("message.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
	
	<div class="sidebar fleft">

		<div class="box box_side_tabs">
			<ul class="side_tabs">
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
faq/edit/idfaq/<?php echo $_smarty_tpl->getVariable('faq')->value['idfaq'];?>
" class="current"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 BG:</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
faq/editlang/idfaq/<?php echo $_smarty_tpl->getVariable('faq')->value['idfaq'];?>
/langsel/en"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 EN:</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
faq/editlang/idfaq/<?php echo $_smarty_tpl->getVariable('faq')->value['idfaq'];?>
/langsel/fr"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 FR:</a></li>
			</ul>
		</div><!--box-->
	
	</div><!--sidebar-->

	<div class="main frigt">

		<div class="box box_form">
			<div class="box_head move">
				<h3 class="box_title">Редактиране на полезно</h3>
			</div><!--box head-->
			
			<div class="box_conent">
				<form method="post" action="" enctype="multipart/form-data" class="form_3w">
				
					<ul class="form_list">
						<li>
							<label><?php echo $_smarty_tpl->getVariable('lang')->value['faq_title'];?>
 BG:</label>
							<input type="text" name="title_bg" class="input size_xl" value="<?php echo $_smarty_tpl->getVariable('faq')->value['title_bg'];?>
" />
						</li>
						
						<li>
							<label><?php echo $_smarty_tpl->getVariable('lang')->value['faq_description'];?>
 BG:</label>
							<textarea name="content_bg" class="t_editor size_xl fleft noclear marginleft10"><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('faq')->value['content_bg']);?>
</textarea>
						</li>
						
						<li>
							<label><?php echo $_smarty_tpl->getVariable('lang')->value['position'];?>
</label>
							<input type="text" name="position" class="input size_s" value="<?php echo $_smarty_tpl->getVariable('faq')->value['position'];?>
" />
						</li>
						
						<li>
							<label><?php echo $_smarty_tpl->getVariable('lang')->value['state'];?>
</label>
							<select name="active" class="select size_s">
								<option value="1" <?php if (($_smarty_tpl->getVariable('faq')->value['active']=='1')){?> selected="selected"<?php }?>>Активнa</option>
								<option value="0" <?php if (($_smarty_tpl->getVariable('faq')->value['active']=='0')){?> selected="selected"<?php }?>>Неактивна</option>
							</select>
						</li>
					</ul>
									
					<div class="button_set">
						<input type="submit" name="submit" class="submit button bg_blue bg_blue_hover" value="<?php echo $_smarty_tpl->getVariable('lang')->value['save'];?>
" />
					</div><!--button set-->

				</form>
				
			</div><!--box content-->
		</div><!--box-->
	</div><!--main-->
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>