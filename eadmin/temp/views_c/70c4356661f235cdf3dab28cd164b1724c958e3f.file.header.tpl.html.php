<?php /* Smarty version Smarty-3.0.8, created on 2019-08-19 06:58:10
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/header.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:13159234135d5a1e526ab6b9-39078622%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '70c4356661f235cdf3dab28cd164b1724c958e3f' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/header.tpl.html',
      1 => 1566186578,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13159234135d5a1e526ab6b9-39078622',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Admin panel - <?php echo $_smarty_tpl->getVariable('config')->value['site_name'];?>
</title>
        <link type="text/css" rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_css'];?>
admin.css?timestamp=<?php echo time();?>
" />
        <link type="text/css" rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_css'];?>
tipsy.css" />
		<link type="text/css" rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_css'];?>
colorbox.css" />
		<link type="text/css" rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_css'];?>
modal.css" />
        <link type="text/css" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_css'];?>
flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
        <script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
jquery-ui-1.8.18.custom.min.js"></script>
        <script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
jquery.tipsy.js"></script>
        <script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
jquery.dragsort-0.5.1.min.js"></script>
        <script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
tiny_mce/jquery.tinymce.js"></script>
		
		<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
Jcrop/css/jquery.Jcrop.css" type="text/css" media="screen" />
		<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
Jcrop/js/jquery.Jcrop.js"></script>
		
		<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
jquery.colorbox-min.js"></script>
        <script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
functions.js?timestamp=<?php echo time();?>
"></script>
        <script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('config')->value['site_js'];?>
jquery-ui-timepicker-addon.js"></script>

        <script>
            document.createElement('header');
            document.createElement('nav');
            document.createElement('section');
            document.createElement('footer');
            document.createElement('article');
            document.createElement('aside');
            document.createElement('details');
            document.createElement('figcaption');
            document.createElement('figure');
            document.createElement('hgroup');
            document.createElement('menu');
        </script>
    </head>

    <body data-page="<?php echo $_smarty_tpl->getVariable('page')->value;?>
">
        <div class="content">
            <header>
                <a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
" class="logo"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
EvinArts.jpg" /></a>
                <div class="user">
                    <a href="#" class="avatar fleft round5" ><?php $_template = new Smarty_Internal_Template("elements/self_avatar.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
$_template->assign('size',56); echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?></a>
                    <div class="fleft">
                        <p class="clear">&nbsp;&nbsp;Здравей, <a href="javascript:void(0);" class="userpic" title="@<?php echo $_smarty_tpl->getVariable('admin_data')->value['username'];?>
"><?php echo $_smarty_tpl->getVariable('admin_data')->value['fullName'];?>
</a></p>
                        <ul>
                            <li class="inline"><a href="<?php echo $_smarty_tpl->getVariable('config')->value['userarea_url'];?>
" class="fleft round3" target="_blank"><?php echo $_smarty_tpl->getVariable('lang')->value['view_website'];?>
</a></li>
                            <li class="inline"><a href="/index/logout" class="fleft round3"><?php echo $_smarty_tpl->getVariable('lang')->value['logout'];?>
</a></li>
                        </ul>
                    </div>
                </div><!--user-->
            </header>

            <?php if (($_smarty_tpl->getVariable('admin_data')->value['type']=='admin')){?>
				<nav>
					<ul class="menu">
						<li class="btn">
							<a class="btna" href="/"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
						</li>
						<li class="btn_border"></li>
						<li class="btn">
							<a class="btna" href="/pages/browse">Страници</a>
						</li>
						
						<li class="btn_border"></li>
						<li class="btn">
							<a class="btna" href="/products/browse">Картини</a>
							<ul class="round3 shadow">
								<li><a href="/products/browse"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/blog.png" alt="" />Всички картини</a></li>
								<li><a href="/products/add"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/plus_16.png" alt="" /><?php echo $_smarty_tpl->getVariable('lang')->value['add'];?>
 картина</a></li>
							</ul>
						</li>
												
						<li class="btn_border"></li>
						<li class="btn">
							<a class="btna" href="/category/browse"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_category'];?>
</a>
							<ul>
								<li><a href="/category/browse"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/blog.png" alt="" /><?php echo $_smarty_tpl->getVariable('lang')->value['menu_category'];?>
</a></li>
								<li><a href="/category/add"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/plus_16.png" alt="" /><?php echo $_smarty_tpl->getVariable('lang')->value['add'];?>
</a></li>
							</ul>
						</li>
						
						<li class="btn_border"></li>
						<li class="btn">
							<a class="btna" href="/news/browse">Новини</a>
							<ul>
								<li><a href="/news/browse"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/blog.png" alt="" />Всички новини</a></li>
								<li><a href="/news/add"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/plus_16.png" alt="" />Добави новина</a></li>
							</ul>
						</li>
						
						<li class="btn_border"></li>
						<li class="btn">
							<a class="btna" href="/faq/browse"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_faq'];?>
</a>
							<ul>
								<li><a href="/faq/browse"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/blog.png" alt="" /><?php echo $_smarty_tpl->getVariable('lang')->value['menu_faq'];?>
</a></li>
								<li><a href="/faq/add"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/plus_16.png" alt="" /><?php echo $_smarty_tpl->getVariable('lang')->value['add'];?>
</a></li>
							</ul>
						</li>
						
						<li class="btn_border"></li>
						<li class="btn">
							<a class="btna" href="/carts/browse"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_carts'];?>
</a>
							<ul class="round3 shadow">
								<li><a href="/carts/browse"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/blog.png" alt="" /><?php echo $_smarty_tpl->getVariable('lang')->value['menu_browse'];?>
</a></li>
								<li><a href="/carts/new"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/new_inline_16.png" alt="" /><?php echo $_smarty_tpl->getVariable('lang')->value['menu_new'];?>
</a></li>
							</ul>
						</li>
						<li class="btn_border"></li>
					</ul>
				</nav>
            <?php }?>