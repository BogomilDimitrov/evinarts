<?php /* Smarty version Smarty-3.0.8, created on 2015-03-05 17:00:39
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/pages/edit.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:47064365454f86f97da0ab5-23019311%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ab1b93f83043af73b19dd54a5bf0266b4f80bc37' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/pages/edit.tpl.html',
      1 => 1425567616,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '47064365454f86f97da0ab5-23019311',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/home/balkanec/public_html/evinarts.com/eadmin/library/smarty/plugins/modifier.escape.php';
?><?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
	<div class="path clear">
		<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <a href="/pages/browse">Страници</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
		<strong>
			Редактиране на статични страници
		</strong>
	</div><!--path-->

	<div class="page_title fleft clear bg_light round3">
		<h1 class="fleft">
			<?php echo $_smarty_tpl->getVariable('pageInfo')->value['title_bg'];?>

		</h1>
	</div>

	<?php $_template = new Smarty_Internal_Template("message.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
	
	<div class="sidebar fleft">

		<div class="box box_side_tabs">
			<ul class="side_tabs">
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
pages/edit/id/<?php echo $_smarty_tpl->getVariable('pageInfo')->value['id'];?>
" class="current"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 BG:</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
pages/editlang/id/<?php echo $_smarty_tpl->getVariable('pageInfo')->value['id'];?>
/langsel/en"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 EN:</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
pages/editlang/id/<?php echo $_smarty_tpl->getVariable('pageInfo')->value['id'];?>
/langsel/fr"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 FR:</a></li>
			</ul>
		</div><!--box-->
	
	</div><!--sidebar-->

	<div class="main frigt">

		<div class="box box_form">
			<div class="box_head move">
				<h3 class="box_title">Редактиране - <?php echo $_smarty_tpl->getVariable('pageInfo')->value['title_bg'];?>
</h3>
			</div><!--box head-->
			
			<div class="box_conent">
				<form method="post" action="" enctype="multipart/form-data" class="form_3w">
				
					<ul class="form_list">
						<li>
							<label>Заглавие BG:</label>
							<input type="text" name="title_bg" class="input size_xl" value="<?php echo smarty_modifier_escape($_smarty_tpl->getVariable('pageInfo')->value['title_bg']);?>
" />
						</li>
					</ul>
					<div>	
						<b><?php echo $_smarty_tpl->getVariable('lang')->value['faq_description'];?>
 BG:</b><br />
						<textarea name="content_bg" class="wysiwyg t_editor size_xl fleft noclear marginleft10"><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('pageInfo')->value['content_bg']);?>
</textarea>
					</div>
					<ul class="form_list">
						<li>
							<label>Мета заглавие BG:</label>
							<input type="text" name="meta_title_bg" class="input size_xl" value="<?php echo $_smarty_tpl->getVariable('pageInfo')->value['meta_title_bg'];?>
" />
						</li>
						
						<li>
							<label>Мета описание BG:</label>
							<textarea name="meta_description_bg" class="t_editor size_xl fleft noclear marginleft10"><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('pageInfo')->value['meta_description_bg']);?>
</textarea>
						</li>
						
						<li>
							<label>Мета ключови думи BG:</label>
							<textarea name="meta_keywords_bg" class="t_editor size_xl fleft noclear marginleft10"><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('pageInfo')->value['meta_keywords_bg']);?>
</textarea>
						</li>
					</ul>
									
					<div class="button_set">
						<input type="submit" name="submit" class="submit button bg_blue bg_blue_hover" value="<?php echo $_smarty_tpl->getVariable('lang')->value['save'];?>
" />
					</div><!--button set-->

				</form>
				
			</div><!--box content-->
		</div><!--box-->
	</div><!--main-->
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>