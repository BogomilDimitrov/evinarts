<?php /* Smarty version Smarty-3.0.8, created on 2019-11-18 07:34:15
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/news/edit.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:1202326615dd22d5718d2d8-49707421%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '011023c937d521bfc04031c54a4175648505679c' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/news/edit.tpl.html',
      1 => 1574055213,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1202326615dd22d5718d2d8-49707421',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/home/balkanec/public_html/evinarts.com/eadmin/library/smarty/plugins/modifier.escape.php';
?><?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
	<div class="path clear">
		<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <a href="/news/browse">Новини</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
		<strong><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</strong>
	</div><!--path-->

	<div class="page_title fleft clear bg_light round3">
		<h1 class="fleft"><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</h1>
	</div>

	<?php $_template = new Smarty_Internal_Template("message.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
	
	<div class="sidebar fleft">

		<div class="box box_side_tabs">
			<ul class="side_tabs">
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
news/edit/idnews/<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
" class="current"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 BG</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
news/editlang/idnews/<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
/langsel/en"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 EN:</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
news/editlang/idnews/<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
/langsel/fr"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 FR:</a></li>
				
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
news/editPicture/idnews/<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
">Снимки</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
news/editMeta/idnews/<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
">Мета данни</a></li>
			</ul>
		</div><!--box-->
	
	</div><!--sidebar-->

	<div class="main frigt">

		<div class="box box_form">
			<div class="box_head move">
				<h3 class="box_title"><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</h3>
			</div><!--box head-->
			
			<div class="box_conent">
				<form method="post" action="" enctype="multipart/form-data" class="form_3w">
					<ul class="form_list">
						
						<li>
							<label>Заглавие BG</label>
							<input type="text" name="title_bg" class="input size_xl" value="<?php echo smarty_modifier_escape($_smarty_tpl->getVariable('news')->value['title_bg']);?>
" />
						</li>
					</ul>
					
						<div>
							<b>Описание BG:</b><br />
							<textarea name="content_bg" class="wysiwyg t_editor size_xl fleft noclear marginleft10"><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('news')->value['content_bg']);?>
</textarea>
						</div>
					
					<ul class="form_list">	
						
						<li>
							<label>Дата</label>
							<input type="text" name="date_added" class="input size_s datepicker" value="<?php echo $_smarty_tpl->getVariable('news')->value['date_added'];?>
" />
						</li>
						
						
						<li>
							<label>Предстоящо</label>
							<select name="upcoming" class="select size_l">
								<option value="1" <?php if ($_smarty_tpl->getVariable('news')->value['upcoming']=='1'){?>selected<?php }?>>Не</option>
								<option value="2" <?php if ($_smarty_tpl->getVariable('news')->value['upcoming']=='2'){?>selected<?php }?>>Да</option>
							</select>
						</li>
						
						<li>
							<label><?php echo $_smarty_tpl->getVariable('lang')->value['state'];?>
</label>
							<select name="status" class="select size_l">
								<option value="active" <?php if ($_smarty_tpl->getVariable('news')->value['status']=='active'){?>selected<?php }?>>Активна</option>
								<option value="inactive" <?php if ($_smarty_tpl->getVariable('news')->value['status']=='inactive'){?>selected<?php }?>>Не активна</option>
							</select>
						</li>
					</ul>
									
					<div class="button_set">
						<input type="submit" name="submit" class="submit button bg_blue bg_blue_hover" value="<?php echo $_smarty_tpl->getVariable('lang')->value['save'];?>
" />
					</div><!--button set-->

				</form>
				
			</div><!--box content-->
		</div><!--box-->
	</div><!--main-->
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>