<?php /* Smarty version Smarty-3.0.8, created on 2015-03-07 12:00:22
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/products/browse.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:135319286354facc36200ae5-28625603%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '010d144cbc8f817e448d664aea0e6aec8c21ea45' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/products/browse.tpl.html',
      1 => 1425722418,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '135319286354facc36200ae5-28625603',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
    <div class="path clear">
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <a href="/products/browse">Картини</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <strong>
            <?php echo $_smarty_tpl->getVariable('data')->value['page_title'];?>

        </strong>
    </div><!--path-->

    <?php $_template = new Smarty_Internal_Template("message.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

    <div class="box box_table full_box">
        <div class="box_head">
            <h3 class="box_title">
                <?php echo $_smarty_tpl->getVariable('data')->value['page_title'];?>

            </h3>
        </div><!--box head-->

        <div class="box_conent">

            <form action="/products/browse" method="post" class="table_form inline">

                <div class="filter" style="width:968px;">
                    <div>
                        <input type="text" value="<?php echo $_smarty_tpl->getVariable('data')->value['search'];?>
" placeholder="Търсене по име" name="search" class="input" style="width:830px" />
                        <input type="submit" class="submit bg_grey bg_grey_hover round3 frigt" value="<?php echo $_smarty_tpl->getVariable('lang')->value['search'];?>
" />
                    </div>
                </div><!--filter-->	

                <?php if (($_smarty_tpl->getVariable('data')->value['products'])){?>

                <table cellspacing="1" cellpadding="0" border="0" width="960px" class="data-table">
                    <thead>
                        <tr class="dt_titles bg_black"> 
                            <td align="center" width="30px">ID</td>
							<td width="80px"><?php echo $_smarty_tpl->getVariable('lang')->value['picture'];?>
</td>
							<td>Име продукт</td>
							<td><?php echo $_smarty_tpl->getVariable('lang')->value['category_title'];?>
</td>
                            <td align="center" width="80"><?php echo $_smarty_tpl->getVariable('lang')->value['state'];?>
</td>
                            <td align="center" width="80"><?php echo $_smarty_tpl->getVariable('lang')->value['options'];?>
</td>
                        </tr>
                    </thead>					

                    <tbody>
                        <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data')->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['product']->key;
?>
							<tr class="<?php if (($_smarty_tpl->tpl_vars['k']->value%2==0)){?>even<?php }else{ ?>odd<?php }?>">
								<td align="center"><?php echo $_smarty_tpl->tpl_vars['product']->value['idproduct'];?>
</td>
								<td><?php if ($_smarty_tpl->tpl_vars['product']->value['picture']){?><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
thumb/<?php echo $_smarty_tpl->tpl_vars['product']->value['picture'];?>
" width="80" /><?php }?></td>
								<td><a href="/products/edit/idproduct/<?php echo $_smarty_tpl->tpl_vars['product']->value['idproduct'];?>
/"><?php echo $_smarty_tpl->tpl_vars['product']->value['title_bg'];?>
</a></td>
								<td><?php echo $_smarty_tpl->tpl_vars['product']->value['category_title'];?>
</td>
								<td align="center">
									<?php if ($_smarty_tpl->tpl_vars['product']->value['active']==2||$_smarty_tpl->tpl_vars['product']->value['active']==3){?>
										<a href="#" class="fn size-24" title="В наличност"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/cart.png"></a>
									<?php }elseif($_smarty_tpl->tpl_vars['product']->value['active']==1){?>
										<a href="#" class="fn size-24" title="Няма наличност"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/add-cart.png"></a>
									<?php }else{ ?>
										<a href="#" class="fn size-24" title="Скрит продукт"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/no-cart.png"></a>
									<?php }?>
									
									&nbsp;
									<?php if ($_smarty_tpl->tpl_vars['product']->value['status']=='new'){?>
										<a href="javascript:void(0)" class="fn size-24" title="Нов продукт"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/new-yellow-24.png"></a>
									<?php }elseif($_smarty_tpl->tpl_vars['product']->value['status']=='promo'||$_smarty_tpl->tpl_vars['product']->value['status']=='20'){?>
										<a href="javascript:void(0)" class="fn size-24" title="В промоция"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/megaphone_24.png"></a>
									<?php }?>
								</td>
								<td align="center">
									<a class="fn display-block size-16" title="<?php echo $_smarty_tpl->getVariable('lang')->value['edit'];?>
" href="/products/edit/idproduct/<?php echo $_smarty_tpl->tpl_vars['product']->value['idproduct'];?>
/">
										<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/pencil_16.png">
									</a>
									<a class="fn display-block size-16" title="<?php echo $_smarty_tpl->getVariable('lang')->value['delete'];?>
" href="javascript:;" onClick="confirmDeleteItem(<?php echo $_smarty_tpl->tpl_vars['product']->value['idproduct'];?>
, '<?php echo $_smarty_tpl->tpl_vars['product']->value['title'];?>
');">
										<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/delete_16.png">
									</a>									
								</td>
							</tr>							
                        <?php }} ?>
                    </tbody>
                </table>

                <?php if (($_smarty_tpl->getVariable('data')->value['pager'])){?>
					<div class="pager"><?php echo $_smarty_tpl->getVariable('data')->value['pager'];?>
</div>
                <?php }?>

                <?php }else{ ?>
					<div id="nodata">
						<?php echo $_smarty_tpl->getVariable('lang')->value['no_data_found'];?>

					</div>
                <?php }?>

            </form><!--table form-->

        </div><!--box content-->                
    </div>
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<!-- Delete user -->
<div id="dialog-confirm" title="Изтриване на продукт" style="padding-top: 20px; display: none;">
    <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
    <b>Сигурни ли сте, че искате да изтриете <span id="remove_item_id" style="color:red;"></span>?
</div>

<script>
    // Remove item popup
    function confirmDeleteItem(idproduct, username){
        $("#remove_item_id").html(username);

        $( "#dialog-confirm" ).dialog({
            resizable: false,
            width:500,
            modal: true,
            buttons: {
                "Confirm": function() {
                    window.location.href = "/products/worker/type/delete-products/idtarget/"+idproduct+"/";
                },
                "Cancel": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        $('.ui-dialog :button').blur();
    }
</script>