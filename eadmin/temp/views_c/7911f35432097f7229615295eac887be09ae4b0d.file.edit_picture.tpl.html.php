<?php /* Smarty version Smarty-3.0.8, created on 2019-08-19 07:08:07
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/products/edit_picture.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:2338293085d5a20a756a0d8-01279606%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7911f35432097f7229615295eac887be09ae4b0d' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/products/edit_picture.tpl.html',
      1 => 1566187640,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2338293085d5a20a756a0d8-01279606',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
	<div class="path clear">
		<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <a href="/products/browse"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_products'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
		<strong><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</strong>
	</div><!--path-->

	<div class="page_title fleft clear bg_light round3">
		<h1 class="fleft"><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</h1>
	</div>

	<?php $_template = new Smarty_Internal_Template("message.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
	
	<div class="sidebar fleft">

		<div class="box box_side_tabs">
			<ul class="side_tabs">
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
products/edit/idproduct/<?php echo $_smarty_tpl->getVariable('product')->value['idproduct'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 BG:</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
products/editlang/idproduct/<?php echo $_smarty_tpl->getVariable('product')->value['idproduct'];?>
/langsel/en"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 EN:</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
products/editlang/idproduct/<?php echo $_smarty_tpl->getVariable('product')->value['idproduct'];?>
/langsel/fr"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 FR:</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
products/editPicture/idproduct/<?php echo $_smarty_tpl->getVariable('product')->value['idproduct'];?>
" class="current">Снимки</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
products/editMeta/idproduct/<?php echo $_smarty_tpl->getVariable('product')->value['idproduct'];?>
">Мета данни</a></li>
			</ul>
		</div><!--box-->
	
	</div><!--sidebar-->

	<div class="main frigt">

		<div class="box box_form">
			<div class="box_head move">
				<h3 class="box_title"><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</h3>
			</div><!--box head-->
			
			<div class="box_conent">
					
				<form name="add_picture" method="post" enctype="multipart/form-data">
					<br /><br />
					<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
						<tr>
							<th>Избери снимка: &nbsp;&nbsp;</th>
							<td>
								<input id="file_upload_new" name="file_upload" type="file" />
								<input type="hidden" id="img_path" value="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
" />
								
								<input type="hidden" id="min_width" value="" />
								<input type="hidden" id="min_height" value="" />
								<input type="hidden" id="prod_id" value="<?php echo $_smarty_tpl->getVariable('product')->value['idproduct'];?>
" />
							</td>
							<td></td>
						</tr>
					</table>
				</form>
				<br /><br />
				<div>
					<?php  $_smarty_tpl->tpl_vars['picture'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('pictures')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['picture']->key => $_smarty_tpl->tpl_vars['picture']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['picture']->key;
?>
						<div style="float:left; text-align:center; margin-right:10px;">
							<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
thumb/<?php echo $_smarty_tpl->tpl_vars['picture']->value['picture'];?>
" alt="" border="0" /><br />
							<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
products/worker/type/delete-picture/idtarget/<?php echo $_smarty_tpl->getVariable('product')->value['idproduct'];?>
/idpicture/<?php echo $_smarty_tpl->tpl_vars['picture']->value['idpicture'];?>
" onclick="return confirm('Сигурни ли сте, че искате да изтриете?')" title="Изтриване" class="icon-2 info-tooltip"></a>
							<a href="javascript:void(0);" onclick="return makeTop('<?php echo $_smarty_tpl->tpl_vars['picture']->value['picture'];?>
', <?php echo $_smarty_tpl->getVariable('product')->value['idproduct'];?>
, <?php echo $_smarty_tpl->tpl_vars['k']->value;?>
, 'products');" title="Главна Снимка" class="icon-3 info-tooltip <?php if ($_smarty_tpl->getVariable('product')->value['picture']==$_smarty_tpl->tpl_vars['picture']->value['picture']){?>selected<?php }?>" id="topim_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"></a>
						</div>
					<?php }} ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>