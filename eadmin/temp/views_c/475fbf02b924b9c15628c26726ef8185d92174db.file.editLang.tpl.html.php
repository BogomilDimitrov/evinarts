<?php /* Smarty version Smarty-3.0.8, created on 2015-03-07 10:19:25
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/news/editLang.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:2645592854fab48d955941-88393797%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '475fbf02b924b9c15628c26726ef8185d92174db' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/news/editLang.tpl.html',
      1 => 1425716360,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2645592854fab48d955941-88393797',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/home/balkanec/public_html/evinarts.com/eadmin/library/smarty/plugins/modifier.escape.php';
?><?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
	<div class="path clear">
		<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <a href="/news/browse">Новини</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
		<strong><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</strong>
	</div><!--path-->

	<div class="page_title fleft clear bg_light round3">
		<h1 class="fleft">
			<?php echo $_smarty_tpl->getVariable('news')->value['title_bg'];?>

		</h1>
	</div>

	<?php $_template = new Smarty_Internal_Template("message.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
	
	<div class="sidebar fleft">

		<div class="box box_side_tabs">
			<ul class="side_tabs">
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
news/edit/idnews/<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 BG</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
news/editlang/idnews/<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
/langsel/en" <?php if ($_smarty_tpl->getVariable('langsel')->value=='en'){?>class="current"<?php }?>><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 EN:</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
news/editlang/idnews/<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
/langsel/fr" <?php if ($_smarty_tpl->getVariable('langsel')->value=='fr'){?>class="current"<?php }?>><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 FR:</a></li>
				
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
news/editPicture/idnews/<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
">Снимки</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
news/editMeta/idnews/<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
">Мета данни</a></li>
			</ul>
		</div><!--box-->
	
	</div><!--sidebar-->

	<div class="main frigt">

		<div class="box box_form">
			<div class="box_head move">
				<h3 class="box_title">Редактиране - <?php echo $_smarty_tpl->getVariable('news')->value['title_bg'];?>
</h3>
			</div><!--box head-->
			
			<div class="box_conent">
				<form method="post" action="" enctype="multipart/form-data" class="form_3w">
				
					<ul class="form_list">
						<li>
							<label>Заглавие <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('langsel')->value, 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('langsel')->value,SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('langsel')->value));?>
:</label>
							<input type="text" name="title_<?php echo $_smarty_tpl->getVariable('langsel')->value;?>
" class="input size_xl" value="<?php echo smarty_modifier_escape($_smarty_tpl->getVariable('news')->value['title']);?>
" />
						</li>
					</ul>
					
						<div>
							<b>Описание <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('langsel')->value, 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('langsel')->value,SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('langsel')->value));?>
:</b><br />
							<textarea name="content_<?php echo $_smarty_tpl->getVariable('langsel')->value;?>
" class="wysiwyg t_editor size_xl fleft noclear marginleft10"><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('news')->value['content']);?>
</textarea>
						</div>
					
					<ul class="form_list">
						<li>
							<label>Мета заглавие <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('langsel')->value, 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('langsel')->value,SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('langsel')->value));?>
:</label>
							<input type="text" name="meta_title_<?php echo $_smarty_tpl->getVariable('langsel')->value;?>
" class="input size_xl" value="<?php echo $_smarty_tpl->getVariable('news')->value['meta_title'];?>
" />
						</li>
						
						<li>
							<label>Мета описание <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('langsel')->value, 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('langsel')->value,SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('langsel')->value));?>
:</label>
							<textarea name="meta_description_<?php echo $_smarty_tpl->getVariable('langsel')->value;?>
" class="t_editor size_xl fleft noclear marginleft10"><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('news')->value['meta_description']);?>
</textarea>
						</li>
						
						<li>
							<label>Мета ключови думи <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('langsel')->value, 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('langsel')->value,SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('langsel')->value));?>
:</label>
							<textarea name="meta_keywords_<?php echo $_smarty_tpl->getVariable('langsel')->value;?>
" class="t_editor size_xl fleft noclear marginleft10"><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('news')->value['meta_keywords']);?>
</textarea>
						</li>
					</ul>
									
					<div class="button_set">
						<input type="submit" name="submit" class="submit button bg_blue bg_blue_hover" value="<?php echo $_smarty_tpl->getVariable('lang')->value['save'];?>
" />
					</div><!--button set-->

				</form>
				
			</div><!--box content-->
		</div><!--box-->
	</div><!--main-->
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>