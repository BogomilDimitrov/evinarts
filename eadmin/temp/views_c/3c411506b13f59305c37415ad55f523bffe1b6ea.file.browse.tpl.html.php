<?php /* Smarty version Smarty-3.0.8, created on 2015-03-05 18:45:03
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/category/browse.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:184914964954f8880f375898-21047023%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3c411506b13f59305c37415ad55f523bffe1b6ea' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/category/browse.tpl.html',
      1 => 1425573900,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '184914964954f8880f375898-21047023',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_truncate')) include '/home/balkanec/public_html/evinarts.com/eadmin/library/smarty/plugins/modifier.truncate.php';
?><?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
    <div class="path clear">
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <a href="/category/browse"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_category'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <strong>
            <?php echo $_smarty_tpl->getVariable('data')->value['page_title'];?>

        </strong>
    </div><!--path-->

    <?php $_template = new Smarty_Internal_Template("message.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

    <div class="box box_table full_box">
        <div class="box_head">
            <h3 class="box_title">
                <?php echo $_smarty_tpl->getVariable('data')->value['page_title'];?>

            </h3>
        </div><!--box head-->

        <div class="box_conent">

            <form action="" method="post" class="table_form inline">

                <div class="filter" style="width:968px;">
                    <div>
                        <input type="text" value="<?php echo $_smarty_tpl->getVariable('data')->value['search'];?>
" placeholder="<?php echo $_smarty_tpl->getVariable('lang')->value['category_search_field'];?>
" name="search" class="input" style="width:830px" />
                        <input type="submit" class="submit bg_grey bg_grey_hover round3 frigt" value="<?php echo $_smarty_tpl->getVariable('lang')->value['search'];?>
" />
                    </div>
                </div><!--filter-->	

                <?php if (($_smarty_tpl->getVariable('data')->value['category'])){?>

                <table cellspacing="1" cellpadding="0" border="0" width="960px" class="data-table">
                    <thead>
                        <tr class="dt_titles bg_black"> 
                            <td align="center" width="30px">ID</td>
							<td><?php echo $_smarty_tpl->getVariable('lang')->value['category_title'];?>
</td>
							<td><?php echo $_smarty_tpl->getVariable('lang')->value['position'];?>
</td>
                            <td align="center"><?php echo $_smarty_tpl->getVariable('lang')->value['state'];?>
</td>
                            <td align="center" width="80px"><?php echo $_smarty_tpl->getVariable('lang')->value['options'];?>
</td>
                        </tr>
                    </thead>					

                    <tbody>
                        <?php  $_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data')->value['category']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cat']->key => $_smarty_tpl->tpl_vars['cat']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['cat']->key;
?>
							<tr class="<?php if (($_smarty_tpl->tpl_vars['k']->value%2==0)){?>even<?php }else{ ?>odd<?php }?>">
								<td align="center"><?php echo $_smarty_tpl->tpl_vars['cat']->value['idcategory'];?>
</td>
								<td><a href="/category/edit/idcategory/<?php echo $_smarty_tpl->tpl_vars['cat']->value['idcategory'];?>
/"><?php echo $_smarty_tpl->tpl_vars['cat']->value['title_bg'];?>
</a></td>
								<td><?php echo $_smarty_tpl->tpl_vars['cat']->value['position'];?>
</td>
								<td align="center">
									<?php if ($_smarty_tpl->tpl_vars['cat']->value['active']==1){?>
										<a href="#" class="fn" title="<?php echo $_smarty_tpl->getVariable('lang')->value['active'];?>
"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/tick_16.png"></a>
									<?php }else{ ?>
										<a href="#" class="fn" title="<?php echo $_smarty_tpl->getVariable('lang')->value['inactive'];?>
"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/red-mark.png" /></a>
									<?php }?>
								</td>
								<td align="center">
									<a class="fn display-block" title="<?php echo $_smarty_tpl->getVariable('lang')->value['edit'];?>
" href="/category/edit/idcategory/<?php echo $_smarty_tpl->tpl_vars['cat']->value['idcategory'];?>
/">
										<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/pencil_16.png">
									</a>
									<a class="fn display-block" title="<?php echo $_smarty_tpl->getVariable('lang')->value['delete'];?>
" href="javascript:;" onClick="confirmDeleteItem(<?php echo $_smarty_tpl->tpl_vars['cat']->value['idcategory'];?>
, '<?php echo $_smarty_tpl->tpl_vars['cat']->value['title'];?>
');">
										<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/delete_16.png">
									</a>									
								</td>
							</tr>
							<?php if ($_smarty_tpl->tpl_vars['cat']->value['sub_category']){?>
								<?php  $_smarty_tpl->tpl_vars['subcategory'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['y'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cat']->value['sub_category']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['subcategory']->key => $_smarty_tpl->tpl_vars['subcategory']->value){
 $_smarty_tpl->tpl_vars['y']->value = $_smarty_tpl->tpl_vars['subcategory']->key;
?>
									<tr class="<?php if (($_smarty_tpl->tpl_vars['y']->value%2==0)){?>even<?php }else{ ?>odd<?php }?>">
										<td align="center"><?php echo $_smarty_tpl->tpl_vars['subcategory']->value['idcategory'];?>
</td>
										<td style="text-indent: 10px;"><a href="/category/edit/idcategory/<?php echo $_smarty_tpl->tpl_vars['subcategory']->value['idcategory'];?>
/"> &rdsh; <?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['subcategory']->value['title_bg'],25,'...','UTF-8');?>
</a></td>
										<td><?php echo $_smarty_tpl->tpl_vars['subcategory']->value['position'];?>
</td>
										<td align="center">
											<?php if ($_smarty_tpl->tpl_vars['subcategory']->value['active']==1){?>
												<a href="#" class="fn" title="<?php echo $_smarty_tpl->getVariable('lang')->value['active'];?>
"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/tick_16.png"></a>
											<?php }else{ ?>
												<a href="#" class="fn" title="<?php echo $_smarty_tpl->getVariable('lang')->value['inactive'];?>
"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/red-mark.png" /></a>
											<?php }?>
										</td>
										<td align="center">
											<a class="fn display-block" title="<?php echo $_smarty_tpl->getVariable('lang')->value['edit'];?>
" href="/category/edit/idcategory/<?php echo $_smarty_tpl->tpl_vars['subcategory']->value['idcategory'];?>
/">
												<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/pencil_16.png">
											</a>
											<a class="fn display-block" title="<?php echo $_smarty_tpl->getVariable('lang')->value['delete'];?>
" href="javascript:void(0);" onClick="confirmDeleteItem(<?php echo $_smarty_tpl->tpl_vars['subcategory']->value['idcategory'];?>
, '<?php echo $_smarty_tpl->tpl_vars['subcategory']->value['title'];?>
');">
												<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/delete_16.png">
											</a>									
										</td>
									</tr>
								<?php }} ?>
							<?php }?>
							
                        <?php }} ?>
                    </tbody>
                </table>

                <?php if (($_smarty_tpl->getVariable('data')->value['pager'])){?>
					<div class="pager"><?php echo $_smarty_tpl->getVariable('data')->value['pager'];?>
</div>
                <?php }?>

                <?php }else{ ?>
					<div id="nodata">
						<?php echo $_smarty_tpl->getVariable('lang')->value['no_data_found'];?>

					</div>
                <?php }?>

            </form><!--table form-->

        </div><!--box content-->                
    </div>
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<!-- Delete user -->
<div id="dialog-confirm" title="Изтриване на категория" style="padding-top: 20px; display: none;">
    <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
    <b>Сигурни ли сте, че искате да изтриете <span id="remove_item_id" style="color:red;"></span>?
</div>

<script>
    // Remove item popup
    function confirmDeleteItem(idcategory, username){
        $("#remove_item_id").html(username);

        $( "#dialog-confirm" ).dialog({
            resizable: false,
            width:500,
            modal: true,
            buttons: {
                "Confirm": function() {
                    window.location.href = "/category/worker/type/delete-category/idtarget/"+idcategory+"/";
                },
                "Cancel": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        $('.ui-dialog :button').blur();
    }
</script>