<?php /* Smarty version Smarty-3.0.8, created on 2015-03-07 10:24:38
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/faq/add.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:164995433654fab5c6dae0d7-52074664%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6bc2bf5da19da96f7c8345398a8b4f5a3983dae8' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/faq/add.tpl.html',
      1 => 1425716664,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '164995433654fab5c6dae0d7-52074664',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
	<div class="path clear">
		<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <a href="/faq/browse"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_faq'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
		<strong>
			Добавяне на полезно
		</strong>
	</div><!--path-->

	<div class="page_title fleft clear bg_light round3">
		<h1 class="fleft">
			Добавяне на полезно
		</h1>
	</div>

	<?php $_template = new Smarty_Internal_Template("message.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
	
	<div class="sidebar fleft">

		<div class="box box_side_tabs">
			<ul class="side_tabs">
				<li><a href="javascript:void(0)" class="current"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
</a></li>
			</ul>
		</div><!--box-->
	
	</div><!--sidebar-->

	<div class="main frigt">

		<div class="box box_form">
			<div class="box_head move">
				<h3 class="box_title">Добавяне</h3>
			</div><!--box head-->
			
			<div class="box_conent">
				<form method="post" action="" enctype="multipart/form-data" class="form_3w">
				
					
					<ul class="form_list">
						<li>
							<label><?php echo $_smarty_tpl->getVariable('lang')->value['faq_title'];?>
 BG:</label>
							<input type="text" name="title_bg" class="input size_xl" value="" />
						</li>
						
						<li>
							<label><?php echo $_smarty_tpl->getVariable('lang')->value['faq_description'];?>
 BG:</label>
							<textarea name="content_bg" class="t_editor size_xl fleft noclear marginleft10"></textarea>
						</li>
						
						<li>
							<label><?php echo $_smarty_tpl->getVariable('lang')->value['position'];?>
</label>
							<input type="text" name="position" class="input size_s" value="" />
						</li>
						
						<li>
							<label><?php echo $_smarty_tpl->getVariable('lang')->value['state'];?>
</label>
							<select name="active" class="select size_s">
								<option value="1">Активнa</option>
								<option value="0">Неактивна</option>
							</select>
						</li>
					</ul>
									
					<div class="button_set">
						<input type="submit" name="submit" class="submit button bg_blue bg_blue_hover" value="Добави" />
					</div><!--button set-->

				</form>
				
			</div><!--box content-->
		</div><!--box-->
	</div><!--main-->
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>