<?php /* Smarty version Smarty-3.0.8, created on 2015-06-30 14:15:46
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/carts/edit.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:161457370655927a62d14351-06012996%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '044f8cd2bc15b89d10e014457f2615947091e6e8' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/carts/edit.tpl.html',
      1 => 1435662943,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '161457370655927a62d14351-06012996',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/home/balkanec/public_html/evinarts.com/eadmin/library/smarty/plugins/modifier.escape.php';
?><?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
	<div class="path clear">
		<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <a href="/carts/browse"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_carts'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
		<strong><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</strong>
	</div><!--path-->

	<div class="page_title fleft clear bg_light round3">
		<h1 class="fleft"><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</h1>
	</div>

	<?php $_template = new Smarty_Internal_Template("message.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

	<div class="box box_table full_box">

		<div class="box_head move">
			<h3 class="box_title"><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
 - #<?php echo $_smarty_tpl->getVariable('cartInfo')->value['id_carts'];?>
</h3>
		</div><!--box head-->
		
		<div class="box_conent">
			<table cellspacing="1" cellpadding="0" border="0" width="960px" class="data-table">
				<thead>
					<tr class="dt_titles bg_black"> 
						<td align="center">Номер</td>
						<td>Артикул</td>
						<td>Количество</td>
						<td>Ед. цена</td>
						<td>Общо</td>
						<td>Дата</td>
					</tr>
				</thead>			

				<tbody>
					<?php  $_smarty_tpl->tpl_vars['cart'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('productInCart')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cart']->key => $_smarty_tpl->tpl_vars['cart']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['cart']->key;
?>
						<tr class="<?php if (($_smarty_tpl->tpl_vars['k']->value%2==0)){?>even<?php }else{ ?>odd<?php }?>">
							<td align="center"><?php echo $_smarty_tpl->tpl_vars['cart']->value['id'];?>
</td>
							<td><a href="<?php echo $_smarty_tpl->getVariable('config')->value['userarea_url'];?>
bg/<?php echo $_smarty_tpl->tpl_vars['cart']->value['alias'];?>
-<?php echo $_smarty_tpl->tpl_vars['cart']->value['idproduct'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['cart']->value['title'];?>
</a></td>
							<td><?php echo $_smarty_tpl->tpl_vars['cart']->value['col'];?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['cart']->value['price_1'];?>
 лв</td>
							<td><?php echo $_smarty_tpl->tpl_vars['cart']->value['price_col'];?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['cart']->value['date_added'];?>
</td>
						</tr>							
					<?php }} ?>
						<tr class="even">
							<td colspan="3"></td>
							
							<td><b>Общо:</b></td>
							<td>
								<b><?php echo $_smarty_tpl->getVariable('totalPrice')->value;?>
</b>лв
							</td>
							<td colspan="2"></td>
						</tr>
				</tbody>
			</table>
		</div><!--box content-->
		
		<div class="box_head move">
			<h3 class="box_title">Данни за клиента</h3>
		</div><!--box head-->
		
		<div class="box_conent">
			<form method="post" action="" enctype="multipart/form-data" class="form_3w">
					
				<table cellspacing="1" cellpadding="0" border="0" width="960px" class="data-table">
					<tr class="even">
						<td>Име:</td>
						<td><?php echo $_smarty_tpl->getVariable('cartInfo')->value['fullName'];?>
</td>
					</tr>
					<tr class="odd">
						<td>Email:</td>
						<td><?php echo $_smarty_tpl->getVariable('cartInfo')->value['email'];?>
</td>
					</tr>
					<tr class="even">
						<td>Град:</td>
						<td><?php echo $_smarty_tpl->getVariable('cartInfo')->value['city'];?>
</td>
					</tr>
					<tr class="odd">
						<td>Адрес:</td>
						<td><?php echo $_smarty_tpl->getVariable('cartInfo')->value['address'];?>
</td>
					</tr>
					<tr class="even">
						<td>Телефон:</td>
						<td><?php echo $_smarty_tpl->getVariable('cartInfo')->value['phone'];?>
</td>
					</tr>
					<tr class="odd">
						<td>Пощенски код:</td>
						<td><?php echo $_smarty_tpl->getVariable('cartInfo')->value['post_code'];?>
</td>
					</tr>
					<tr class="even">
						<td>Забележка:</td>
						<td>
							<textarea name="description" class="t_editor size_xl fleft noclear" style="height:50px;"><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('cartInfo')->value['description']);?>
</textarea>
						</td>
					</tr>
					<tr class="odd">
						<td>Статус</td>
						<td>
							<select name="status" class="select size_m">
								<option value="1" <?php if ($_smarty_tpl->getVariable('cartInfo')->value['status']==1){?>selected<?php }?>>В процес на поръчка</option>
								<option value="2" <?php if ($_smarty_tpl->getVariable('cartInfo')->value['status']==2){?>selected<?php }?>>Поръчана</option>
								<option value="3" <?php if ($_smarty_tpl->getVariable('cartInfo')->value['status']==3){?>selected<?php }?>>Потвърдена</option>
								<option value="4" <?php if ($_smarty_tpl->getVariable('cartInfo')->value['status']==4){?>selected<?php }?>>Отказана</option>
							</select>
						</td>
					</tr>
				</table>
				<div class="button_set">
					<input type="submit" name="submit" class="submit button bg_blue bg_blue_hover" value="<?php echo $_smarty_tpl->getVariable('lang')->value['save'];?>
" />
				</div><!--button set-->
			</form>
		</div>
		
		
	</div><!--main-->
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>