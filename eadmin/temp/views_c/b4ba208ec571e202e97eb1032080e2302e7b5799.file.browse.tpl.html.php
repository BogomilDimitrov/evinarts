<?php /* Smarty version Smarty-3.0.8, created on 2015-03-05 19:06:10
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/news/browse.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:75876530354f88d020afcf5-52701563%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b4ba208ec571e202e97eb1032080e2302e7b5799' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/news/browse.tpl.html',
      1 => 1425575167,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '75876530354f88d020afcf5-52701563',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
    <div class="path clear">
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <a href="/news/browse">Новини</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <strong>
            <?php echo $_smarty_tpl->getVariable('page_title')->value;?>

        </strong>
    </div><!--path-->

    <?php $_template = new Smarty_Internal_Template("message.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

    <div class="box box_table full_box">
        <div class="box_head">
            <h3 class="box_title">
                <?php echo $_smarty_tpl->getVariable('page_title')->value;?>

            </h3>
        </div><!--box head-->

        <div class="box_conent">

            <form action="/news/browse" method="post" class="table_form inline">

                <div class="filter" style="width:968px;">
                    <div>
                        <input type="text" value="<?php echo $_smarty_tpl->getVariable('search')->value;?>
" placeholder="Търсете по име" name="search" class="input" style="width:830px" />
                        <input type="submit" class="submit bg_grey bg_grey_hover round3 frigt" value="<?php echo $_smarty_tpl->getVariable('lang')->value['search'];?>
" />
                    </div>
                </div><!--filter-->	

                <?php if (($_smarty_tpl->getVariable('news')->value)){?>

                <table cellspacing="1" cellpadding="0" border="0" width="960px" class="data-table">
                    <thead>
                        <tr class="dt_titles bg_black"> 
                            <td align="center" width="30">ID</td>
							<td width="80"><?php echo $_smarty_tpl->getVariable('lang')->value['picture'];?>
</td>
							<td>Заглавие</td>
							<td width="130">Дата</td>
                            <td align="center" width="100"><?php echo $_smarty_tpl->getVariable('lang')->value['options'];?>
</td>
                        </tr>
                    </thead>					

                    <tbody>
                        <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('news')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['product']->key;
?>
							<tr class="<?php if (($_smarty_tpl->tpl_vars['k']->value%2==0)){?>even<?php }else{ ?>odd<?php }?>">
								<td align="center"><?php echo $_smarty_tpl->tpl_vars['product']->value['idnews'];?>
</td>
								<td><?php if ($_smarty_tpl->tpl_vars['product']->value['picture']){?><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
news/thumb/<?php echo $_smarty_tpl->tpl_vars['product']->value['picture'];?>
" width="80" /><?php }?></td>
								<td><a href="/news/edit/type/<?php echo $_smarty_tpl->getVariable('typePage')->value;?>
/idnews/<?php echo $_smarty_tpl->tpl_vars['product']->value['idnews'];?>
/"><?php echo $_smarty_tpl->tpl_vars['product']->value['title_bg'];?>
</a></td>
								<td><?php echo $_smarty_tpl->tpl_vars['product']->value['date_added'];?>
</td>
								<td align="center">
									<?php if ($_smarty_tpl->tpl_vars['product']->value['status']=='active'){?>
										<a class="fn display-block" title="Активна" href="javascript:void(0)"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/tick_16.png" /></a>
									<?php }else{ ?>
										<a class="fn display-block" title="Неактивна" href="javascript:void(0)"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/red-mark.png" /></a>
									<?php }?>
									
									<?php if ($_smarty_tpl->tpl_vars['product']->value['topNews']=='yes'){?>
										<a class="fn display-block" title="Топ новина" href="javascript:void(0)"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/topicon.png" /></a>
									<?php }?>
								
									<a class="fn display-block size-16" title="<?php echo $_smarty_tpl->getVariable('lang')->value['edit'];?>
" href="/news/edit/idnews/<?php echo $_smarty_tpl->tpl_vars['product']->value['idnews'];?>
/">
										<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/pencil_16.png">
									</a>
									<a class="fn display-block size-16" title="<?php echo $_smarty_tpl->getVariable('lang')->value['delete'];?>
" href="javascript:;" onClick="confirmDeleteItem(<?php echo $_smarty_tpl->tpl_vars['product']->value['idnews'];?>
, '<?php echo $_smarty_tpl->tpl_vars['product']->value['title_bg'];?>
');">
										<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/delete_16.png">
									</a>								
								</td>
							</tr>							
                        <?php }} ?>
                    </tbody>
                </table>

                <?php if (($_smarty_tpl->getVariable('pager')->value)){?>
					<div class="pager"><?php echo $_smarty_tpl->getVariable('pager')->value;?>
</div>
                <?php }?>

                <?php }else{ ?>
					<div id="nodata">
						<?php echo $_smarty_tpl->getVariable('lang')->value['no_data_found'];?>

					</div>
                <?php }?>

            </form><!--table form-->

        </div><!--box content-->                
    </div>
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<!-- Delete user -->
<div id="dialog-confirm" title="Изтриване на продукт" style="padding-top: 20px; display: none;">
    <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
    <b>Сигурни ли сте, че искате да изтриете <span id="remove_item_id" style="color:red;"></span>?
</div>

<script>
    // Remove item popup
    function confirmDeleteItem(idnews, username){
        $("#remove_item_id").html(username);

        $( "#dialog-confirm" ).dialog({
            resizable: false,
            width:500,
            modal: true,
            buttons: {
                "Confirm": function() {
                    window.location.href = "/news/worker/type/delete-news/idtarget/"+idnews+"/";
                },
                "Cancel": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        $('.ui-dialog :button').blur();
    }
</script>