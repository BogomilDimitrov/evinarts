<?php /* Smarty version Smarty-3.0.8, created on 2015-03-05 18:32:16
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/category/edit.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:152288689154f8851068bc43-18046945%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5170648bc83466338742110b16cfc85c66661e46' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/category/edit.tpl.html',
      1 => 1425572899,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '152288689154f8851068bc43-18046945',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/home/balkanec/public_html/evinarts.com/eadmin/library/smarty/plugins/modifier.escape.php';
?><?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
	<div class="path clear">
		<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <a href="/category/browse"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_category'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
		<strong>
			<?php echo $_smarty_tpl->getVariable('lang')->value['category_edit_title'];?>

		</strong>
	</div><!--path-->

	<div class="page_title fleft clear bg_light round3">
		<h1 class="fleft">
			<?php echo $_smarty_tpl->getVariable('category')->value['title_bg'];?>

		</h1>
	</div>

	<?php $_template = new Smarty_Internal_Template("message.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
	
	<div class="sidebar fleft">

		<div class="box box_side_tabs">
			<ul class="side_tabs">
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
category/edit/idcategory/<?php echo $_smarty_tpl->getVariable('category')->value['idcategory'];?>
" class="current"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 BG:</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
category/editlang/idcategory/<?php echo $_smarty_tpl->getVariable('category')->value['idcategory'];?>
/langsel/en"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 EN:</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
category/editlang/idcategory/<?php echo $_smarty_tpl->getVariable('category')->value['idcategory'];?>
/langsel/fr"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 FR:</a></li>
			</ul>
		</div><!--box-->
	
	</div><!--sidebar-->

	<div class="main frigt">

		<div class="box box_form">
			<div class="box_head move">
				<h3 class="box_title"><?php echo $_smarty_tpl->getVariable('lang')->value['category_edit_title'];?>
</h3>
			</div><!--box head-->
			
			<div class="box_conent">
				<form method="post" action="" enctype="multipart/form-data" class="form_3w">
					<ul class="form_list">
						<li>
							<label>Избери категория</label>
							<select name="idparrent" class="select size_s">
								<option value="0">Главна категория</option>
								<?php  $_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('category_list')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cat']->key => $_smarty_tpl->tpl_vars['cat']->value){
?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['cat']->value['idcategory'];?>
" <?php if ($_smarty_tpl->tpl_vars['cat']->value['idcategory']==$_smarty_tpl->getVariable('category')->value['idparrent']){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['cat']->value['title_bg'];?>
</option>
								<?php }} ?>
							</select>
						</li>
					
						<li>
							<label><?php echo $_smarty_tpl->getVariable('lang')->value['category_title'];?>
 BG:</label>
							<input type="text" name="title_bg" class="input size_xl" value="<?php echo smarty_modifier_escape($_smarty_tpl->getVariable('category')->value['title_bg']);?>
" />
						</li>
						<li>
							<label><?php echo $_smarty_tpl->getVariable('lang')->value['category_description'];?>
 BG:</label>
							<textarea name="content_bg" class="t_editor size_xl fleft noclear marginleft10"><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('category')->value['content_bg']);?>
</textarea>
						</li>
						<li>
							<label><?php echo $_smarty_tpl->getVariable('lang')->value['position'];?>
</label>
							<input type="text" name="position" class="input size_s" value="<?php echo $_smarty_tpl->getVariable('category')->value['position'];?>
" />
						</li>
						
						<li>
							<label><?php echo $_smarty_tpl->getVariable('lang')->value['state'];?>
</label>
							<select name="active" class="select size_s">
								<option value="1" <?php if ($_smarty_tpl->getVariable('category')->value['active']==1){?>selected<?php }?>>Активнa</option>
								<option value="0" <?php if ($_smarty_tpl->getVariable('category')->value['active']==0){?>selected<?php }?>>Неактивна</option>
							</select>
						</li>
						
						<li>
							<label>Мета заглавие BG:</label>
							<input type="text" name="meta_title_bg" class="input size_xl" value="<?php echo smarty_modifier_escape($_smarty_tpl->getVariable('category')->value['meta_title_bg']);?>
" />
						</li>
						
						<li>
							<label>Мета описание BG:</label>
							<textarea name="meta_description_bg" class="t_editor size_xl fleft noclear marginleft10"><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('category')->value['meta_description_bg']);?>
</textarea>
						</li>
						
						<li>
							<label>Мета ключови думи BG:</label>
							<textarea name="meta_keywords_bg" class="t_editor size_xl fleft noclear marginleft10"><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('category')->value['meta_keywords_bg']);?>
</textarea>
						</li>
					</ul>
									
					<div class="button_set">
						<input type="submit" name="submit" class="submit button bg_blue bg_blue_hover" value="<?php echo $_smarty_tpl->getVariable('lang')->value['save'];?>
" />
					</div><!--button set-->

				</form>
				
			</div><!--box content-->
		</div><!--box-->
	</div><!--main-->
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>