<?php /* Smarty version Smarty-3.0.8, created on 2015-03-05 16:45:55
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/message.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:128103406754f86c2342aa49-01926250%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c762e18abda80099d48143d5b4c1e6d204b0c054' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/message.tpl.html',
      1 => 1425559129,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '128103406754f86c2342aa49-01926250',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (($_smarty_tpl->getVariable('success')->value)){?>
    <div class="message message_ok">
        <div>
            <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons32x32/tick_32.png" alt="Success" />
            <strong><?php echo $_smarty_tpl->getVariable('success')->value;?>
</strong>
            <a href="javascript:;" onClick="Admin.removeMessage();" title="Close">
                <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
close.gif" class="close">
            </a>
        </div>
    </div><!--eror_message-->
<?php }elseif(($_smarty_tpl->getVariable('error')->value)){?>
    <div class="message message_error">
        <div>
            <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons32x32/delete_32.png" alt="Error" />
            <strong><?php echo $_smarty_tpl->getVariable('error')->value;?>
</strong>
            <a href="javascript:;" onClick="Admin.removeMessage();" title="Close">
                <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
close.gif" class="close">
            </a>
        </div>
    </div><!--eror_message-->
<?php }?>