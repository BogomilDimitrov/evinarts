<?php /* Smarty version Smarty-3.0.8, created on 2015-03-07 11:54:20
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/products/add.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:160321011254facacc3352b7-66501967%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3df1ef7aad2fbacc6691e8bdbd8b6b1fb8af114d' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/products/add.tpl.html',
      1 => 1425721692,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '160321011254facacc3352b7-66501967',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
	<div class="path clear">
		<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <a href="/products/browse"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_brand'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
		<strong>
			<?php echo $_smarty_tpl->getVariable('page_title')->value;?>

		</strong>
	</div><!--path-->

	<div class="page_title fleft clear bg_light round3">
		<h1 class="fleft">
			<?php echo $_smarty_tpl->getVariable('page_title')->value;?>

		</h1>
	</div>

	<?php $_template = new Smarty_Internal_Template("message.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
	
	<div class="sidebar fleft">

		<div class="box box_side_tabs">
			<ul class="side_tabs">
				<li><a href="javascript:void(0)" class="current"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
</a></li>
			</ul>
		</div><!--box-->
	
	</div><!--sidebar-->

	<div class="main frigt">

		<div class="box box_form">
			<div class="box_head move">
				<h3 class="box_title"><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</h3>
			</div><!--box head-->
			
			<div class="box_conent">
				<form method="post" action="" enctype="multipart/form-data" class="form_3w">
				
					<ul class="form_list">
						<li>
							<label>Избери категория</label>
							<select name="idcategory" class="select size_l">
								<option value=""> - Избери категория - </option>
								<?php  $_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('category_list')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cat']->key => $_smarty_tpl->tpl_vars['cat']->value){
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['cat']->value['idcategory'];?>
"><?php echo $_smarty_tpl->tpl_vars['cat']->value['title_bg'];?>
</option>
										<?php if ($_smarty_tpl->tpl_vars['cat']->value['sub_category']){?>
											<?php  $_smarty_tpl->tpl_vars['sub_cat'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cat']->value['sub_category']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['sub_cat']->key => $_smarty_tpl->tpl_vars['sub_cat']->value){
?>
												<option value="<?php echo $_smarty_tpl->tpl_vars['sub_cat']->value['idcategory'];?>
">&nbsp;&nbsp;- <?php echo $_smarty_tpl->tpl_vars['sub_cat']->value['title_bg'];?>
</option>
											<?php }} ?>
										<?php }?>
								<?php }} ?>
							</select>
						</li>
													
						<li>
							<label>Име BG:</label>
							<input type="text" name="title_bg" class="input size_xl" value="" />
						</li>
						
					</ul>
						<div>	
							<b>Кратко описание BG:</b><br />
							<textarea name="content_bg" class="wysiwyg t_editor size_xl fleft noclear marginleft10"></textarea>
						</div>
					<ul class="form_list">
						<li>
							<label>Цена</label>
							<input type="text" name="price_1" class="input size_s" value="" />
						</li>
						
						<li>
							<label>Стара цена</label>
							<input type="text" name="price_2" class="input size_s" value="" /> * (ако е намалена)
						</li>
						
						<li>
							<label>Размери</label>
							<input type="text" name="razmeri" class="input size_s" value="" />
						</li>
						
						<li>
							<label>Опции</label>
							<select name="status" class="select size_l">
								<option value="normal">Нормален</option>
								<option value="promo">В промоция</option>
								<option value="new">Нов продукт</option>
							</select>
						</li>
						
						<li>
							<label><?php echo $_smarty_tpl->getVariable('lang')->value['state'];?>
</label>
							<select name="active" class="select size_l">
								<option value="2">В наличност</option>
								<option value="3">В наличност, препоръчан</option>
								<option value="1">Няма го в наличност</option>
								<option value="4">Неактивен</option>
							</select>
						</li>
					</ul>
									
					<div class="button_set">
						<input type="submit" name="submit" class="submit button bg_blue bg_blue_hover" value="Добави" />
					</div><!--button set-->

				</form>
				
			</div><!--box content-->
		</div><!--box-->
	</div><!--main-->
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>