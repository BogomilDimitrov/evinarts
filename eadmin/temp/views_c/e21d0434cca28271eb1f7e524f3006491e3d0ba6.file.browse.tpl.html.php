<?php /* Smarty version Smarty-3.0.8, created on 2015-03-07 13:11:13
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/carts/browse.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:169352902654fadcd18e8508-95251238%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e21d0434cca28271eb1f7e524f3006491e3d0ba6' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/carts/browse.tpl.html',
      1 => 1425559130,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '169352902654fadcd18e8508-95251238',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
    <div class="path clear">
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <a href="/carts/browse"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_carts'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <strong>
            <?php echo $_smarty_tpl->getVariable('data')->value['page_title'];?>

        </strong>
    </div><!--path-->

    <?php $_template = new Smarty_Internal_Template("message.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

    <div class="box box_table full_box">
        <div class="box_head">
            <h3 class="box_title">
                <?php echo $_smarty_tpl->getVariable('data')->value['page_title'];?>

            </h3>
        </div><!--box head-->

        <div class="box_conent">

            <form action="" method="post" class="table_form inline">

                <?php if (($_smarty_tpl->getVariable('data')->value['listCarts'])){?>

                <table cellspacing="1" cellpadding="0" border="0" width="960px" class="data-table">
                    <thead>
                        <tr class="dt_titles bg_black"> 
                            <td align="center">Номер на поръчката</td>
							<td>Дата</td>
                            <td align="center" width="100px"><?php echo $_smarty_tpl->getVariable('lang')->value['state'];?>
</td>
                            <td align="center" width="80px"><?php echo $_smarty_tpl->getVariable('lang')->value['options'];?>
</td>
                        </tr>
                    </thead>					

                    <tbody>
                        <?php  $_smarty_tpl->tpl_vars['cart'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data')->value['listCarts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cart']->key => $_smarty_tpl->tpl_vars['cart']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['cart']->key;
?>
							<tr class="<?php if (($_smarty_tpl->tpl_vars['k']->value%2==0)){?>even<?php }else{ ?>odd<?php }?>">
								<td align="center"><?php echo $_smarty_tpl->tpl_vars['cart']->value['id_carts'];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['cart']->value['date_added'];?>
</td>
								<td align="center">
									<?php if ($_smarty_tpl->tpl_vars['cart']->value['status']==1){?>
										<a href="#" class="fn size-24" title="В процес на поръчка"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/clock-new.png"></a>
									<?php }elseif($_smarty_tpl->tpl_vars['cart']->value['status']==2){?>
										<a href="#" class="fn size-24" title="Поръчано"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/refresh-new.png"></a>
									<?php }elseif($_smarty_tpl->tpl_vars['cart']->value['status']==3){?>
										<a href="#" class="fn size-24" title="Потвърдена"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/accept.png"></a>
									<?php }else{ ?>
										<a href="#" class="fn size-24" title="Отказана"><img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/block.png"></a>
									<?php }?>
								</td>
								<td align="center">
									<a class="fn display-block size-16" title="<?php echo $_smarty_tpl->getVariable('lang')->value['edit'];?>
" href="/carts/edit/idcart/<?php echo $_smarty_tpl->tpl_vars['cart']->value['id_carts'];?>
/">
										<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/pencil_16.png">
									</a>
									<a class="fn display-block size-16" title="<?php echo $_smarty_tpl->getVariable('lang')->value['delete'];?>
" href="javascript:;" onClick="confirmDeleteItem(<?php echo $_smarty_tpl->tpl_vars['cart']->value['id_carts'];?>
, 'поръчката');">
										<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/delete_16.png">
									</a>									
								</td>
							</tr>							
                        <?php }} ?>
                    </tbody>
                </table>

                <?php if (($_smarty_tpl->getVariable('data')->value['pager'])){?>
					<div class="pager"><?php echo $_smarty_tpl->getVariable('data')->value['pager'];?>
</div>
                <?php }?>

                <?php }else{ ?>
					<div id="nodata">
						<?php echo $_smarty_tpl->getVariable('lang')->value['no_data_found'];?>

					</div>
                <?php }?>

            </form><!--table form-->

        </div><!--box content-->                
    </div>
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<!-- Delete user -->
<div id="dialog-confirm" title="Изтриване на поръчка" style="padding-top: 20px; display: none;">
    <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
    <b>Сигурни ли сте, че искате да изтриете <span id="remove_item_id" style="color:red;"></span>?
</div>

<script>
    // Remove item popup
    function confirmDeleteItem(idproduct, username){
        $("#remove_item_id").html(username);

        $( "#dialog-confirm" ).dialog({
            resizable: false,
            width:500,
            modal: true,
            buttons: {
                "Confirm": function() {
                    window.location.href = "/carts/worker/type/delete-cart/idtarget/"+idproduct+"/";
                },
                "Cancel": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        $('.ui-dialog :button').blur();
    }
</script>