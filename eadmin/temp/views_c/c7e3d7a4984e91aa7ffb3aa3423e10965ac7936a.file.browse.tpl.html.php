<?php /* Smarty version Smarty-3.0.8, created on 2015-03-05 16:45:55
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/pages/browse.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:177695361054f86c230cafb8-69746037%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c7e3d7a4984e91aa7ffb3aa3423e10965ac7936a' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/pages/browse.tpl.html',
      1 => 1425559132,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '177695361054f86c230cafb8-69746037',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
    <div class="path clear">
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <a href="/pages/browse">Страници</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <strong>
            <?php echo $_smarty_tpl->getVariable('data')->value['page_title'];?>

        </strong>
    </div><!--path-->

    <?php $_template = new Smarty_Internal_Template("message.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

    <div class="box box_table full_box">
        <div class="box_head">
            <h3 class="box_title">
                <?php echo $_smarty_tpl->getVariable('data')->value['page_title'];?>

            </h3>
        </div><!--box head-->

        <div class="box_conent">

            <form action="" method="post" class="table_form inline">
                <?php if (($_smarty_tpl->getVariable('data')->value['pagesList'])){?>

                <table cellspacing="1" cellpadding="0" border="0" width="960px" class="data-table">
                    <thead>
                        <tr class="dt_titles bg_black"> 
                            <td align="center" width="30px">ID</td>
							<td>Име страница</td>
                            <td align="center" width="80px"><?php echo $_smarty_tpl->getVariable('lang')->value['options'];?>
</td>
                        </tr>
                    </thead>					

                    <tbody>
                        <?php  $_smarty_tpl->tpl_vars['pinfo'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data')->value['pagesList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['pinfo']->key => $_smarty_tpl->tpl_vars['pinfo']->value){
?>
							<tr class="<?php if (($_smarty_tpl->getVariable('k')->value%2==0)){?>even<?php }else{ ?>odd<?php }?>">
								<td align="center"><?php echo $_smarty_tpl->tpl_vars['pinfo']->value['id'];?>
</td>
								<td><a href="/pages/edit/id/<?php echo $_smarty_tpl->tpl_vars['pinfo']->value['id'];?>
/"><?php echo $_smarty_tpl->tpl_vars['pinfo']->value['title_bg'];?>
</a></td>
								<td align="center">
									<a class="fn display-block" title="<?php echo $_smarty_tpl->getVariable('lang')->value['edit'];?>
" href="/pages/edit/id/<?php echo $_smarty_tpl->tpl_vars['pinfo']->value['id'];?>
/">
										<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
icons16x16/pencil_16.png">
									</a>
								</td>
							</tr>
                        <?php }} ?>
                    </tbody>
                </table>

                <?php if (($_smarty_tpl->getVariable('data')->value['pager'])){?>
					<div class="pager"><?php echo $_smarty_tpl->getVariable('data')->value['pager'];?>
</div>
                <?php }?>

                <?php }else{ ?>
					<div id="nodata">
						<?php echo $_smarty_tpl->getVariable('lang')->value['no_data_found'];?>

					</div>
                <?php }?>

            </form><!--table form-->

        </div><!--box content-->                
    </div>
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>