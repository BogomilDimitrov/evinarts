<?php /* Smarty version Smarty-3.0.8, created on 2015-03-07 14:21:37
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/products/edit_meta.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:70573081654faed5151f630-36070725%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8fe4d3ab6d8f0ea48e24a75c43a94f1aaa01ec68' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/products/edit_meta.tpl.html',
      1 => 1425724478,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '70573081654faed5151f630-36070725',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/home/balkanec/public_html/evinarts.com/eadmin/library/smarty/plugins/modifier.escape.php';
?><?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
	<div class="path clear">
		<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <a href="/products/browse"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_products'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
		<strong><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</strong>
	</div><!--path-->

	<div class="page_title fleft clear bg_light round3">
		<h1 class="fleft"><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</h1>
	</div>

	<?php $_template = new Smarty_Internal_Template("message.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
	
	<div class="sidebar fleft">

		<div class="box box_side_tabs">
			<ul class="side_tabs">
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
products/edit/idproduct/<?php echo $_smarty_tpl->getVariable('product')->value['idproduct'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 BG</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
products/editlang/idproduct/<?php echo $_smarty_tpl->getVariable('product')->value['idproduct'];?>
/langsel/en"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 EN:</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
products/editlang/idproduct/<?php echo $_smarty_tpl->getVariable('product')->value['idproduct'];?>
/langsel/fr"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 FR:</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
products/editPicture/idproduct/<?php echo $_smarty_tpl->getVariable('product')->value['idproduct'];?>
">Снимки</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
products/editMeta/idproduct/<?php echo $_smarty_tpl->getVariable('product')->value['idproduct'];?>
" class="current">Мета данни</a></li>
			</ul>
		</div><!--box-->
	
	</div><!--sidebar-->

	<div class="main frigt">

		<div class="box box_form">
			<div class="box_head move">
				<h3 class="box_title"><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</h3>
			</div><!--box head-->
			
			<div class="box_conent">
				<form method="post" action="" enctype="multipart/form-data" class="form_3w">
					<ul class="form_list">
											
						<li>
							<label>Мета заглавие BG:</label>
							<input type="text" name="meta_title_bg" class="input size_xl" value="<?php echo smarty_modifier_escape($_smarty_tpl->getVariable('product')->value['meta_title_bg']);?>
" />
						</li>

						<li>
							<label>Мета описание BG:</label>
							<textarea name="meta_description_bg" class="t_editor size_xl fleft noclear marginleft10"><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('product')->value['meta_description_bg']);?>
</textarea>
						</li>

						<li>
							<label>Ключови думи BG:</label>
							<textarea name="meta_keywords_bg" class="t_editor size_xl fleft noclear marginleft10"><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('product')->value['meta_keywords_bg']);?>
</textarea>
						</li>
					</ul>
									
					<div class="button_set">
						<input type="submit" name="submit" class="submit button bg_blue bg_blue_hover" value="<?php echo $_smarty_tpl->getVariable('lang')->value['save'];?>
" />
					</div><!--button set-->

				</form>
				
			</div><!--box content-->
		</div><!--box-->
	</div><!--main-->
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>