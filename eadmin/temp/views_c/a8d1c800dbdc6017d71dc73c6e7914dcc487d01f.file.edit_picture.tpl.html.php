<?php /* Smarty version Smarty-3.0.8, created on 2019-08-19 07:10:04
         compiled from "/home/balkanec/public_html/evinarts.com/eadmin/application/views/news/edit_picture.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:10153848605d5a211cb38d45-27127422%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a8d1c800dbdc6017d71dc73c6e7914dcc487d01f' => 
    array (
      0 => '/home/balkanec/public_html/evinarts.com/eadmin/application/views/news/edit_picture.tpl.html',
      1 => 1566187801,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10153848605d5a211cb38d45-27127422',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("header.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<section>
	<div class="path clear">
		<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_home'];?>
</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
        <a href="/news/browse">Новини</a>
        <img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_img'];?>
path_step.png">
		<strong><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</strong>
	</div><!--path-->

	<div class="page_title fleft clear bg_light round3">
		<h1 class="fleft"><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</h1>
	</div>

	<?php $_template = new Smarty_Internal_Template("message.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
	
	<div class="sidebar fleft">

		<div class="box box_side_tabs">
			<ul class="side_tabs">			
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
news/edit/idnews/<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
" ><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 BG</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
news/editlang/idnews/<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
/langsel/en"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 EN:</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
news/editlang/idnews/<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
/langsel/fr"><?php echo $_smarty_tpl->getVariable('lang')->value['users_general_information'];?>
 FR:</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
news/editPicture/idnews/<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
" class="current">Снимки</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
news/editMeta/idnews/<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
">Мета данни</a></li>
			</ul>
		</div><!--box-->
	
	</div><!--sidebar-->

	<div class="main frigt">

		<div class="box box_form">
			<div class="box_head move">
				<h3 class="box_title"><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</h3>
			</div><!--box head-->
			
			<div class="box_conent">
							
					<form name="add_picture" method="post" enctype="multipart/form-data">
						<br /><br />
						<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
							<tr>
								<th>Избери снимка: &nbsp;&nbsp;</th>
								<td>
									<input id="file_upload_new" name="file_upload" type="file" />
									<input type="hidden" id="img_path" value="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
" />
									
									<input type="hidden" id="min_width" value="350" />
									<input type="hidden" id="min_height" value="216" />
								</td>
								<td></td>
							</tr>
						</table>
					</form>
					<div id="loadCrop" style="display:none;">
						<div class="cropScreen">
							<h3 style="text-align:center; margin-bottom:0;">Изрязване</h3>
							<div style="float:left; margin-top:10px;" id="crop_img"></div>
							<div style="width:380px; float:left; margin:10px 0 0 20px;">
								<span style="width:30px; margin:0; padding:0;">Визуализация:</span><br />
								<div style="width:350px; height:216px; overflow:hidden;" id="preview_img"></div><br />
								<input type="button" name="crop" onClick="document.cropimg.submit();" value="Crop" />
							</div>
							<br style="clear:both;"/>
							<form name="cropimg" method="post" action="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
upload.php" enctype="multipart/form-data" onsubmit="return checkCoords();">
								<input type="hidden" id="x" name="x" />
								<input type="hidden" id="y" name="y" />
								<input type="hidden" id="w" name="w" />
								<input type="hidden" id="h" name="h" />
								<input type="hidden" id="ww" name="ww" />
								<input type="hidden" id="hh" name="hh" />
								<input type="hidden" name="img_path" id="image_path" value="" />
								<input type="hidden" name="news_id" id="news_id" value="<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
" />
								<input type="hidden" name="crop" value="9" />
							</form>
						</div>
					</div>
					
					<br /><br />
					<div>
						<?php  $_smarty_tpl->tpl_vars['picture'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('pictures')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['picture']->key => $_smarty_tpl->tpl_vars['picture']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['picture']->key;
?>
							<div style="float:left; text-align:center; margin-right:10px;">
								<img src="<?php echo $_smarty_tpl->getVariable('config')->value['site_data'];?>
news/thumb/<?php echo $_smarty_tpl->tpl_vars['picture']->value['picture'];?>
" width="150" alt="" border="0" /><br />
								<a href="<?php echo $_smarty_tpl->getVariable('config')->value['site_url'];?>
news/worker/type/delete-picture/idtarget/<?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
/idpicture/<?php echo $_smarty_tpl->tpl_vars['picture']->value['idpicture'];?>
" onclick="return confirm('Сигурни ли сте, че искате да изтриете?')" title="Изтриване" class="icon-2 info-tooltip"></a>
								<a href="javascript:void(0);" onclick="return makeTop('<?php echo $_smarty_tpl->tpl_vars['picture']->value['picture'];?>
', <?php echo $_smarty_tpl->getVariable('news')->value['idnews'];?>
, <?php echo $_smarty_tpl->tpl_vars['k']->value;?>
, 'news');" title="Главна Снимка" class="icon-3 info-tooltip <?php if ($_smarty_tpl->getVariable('news')->value['picture']==$_smarty_tpl->tpl_vars['picture']->value['picture']){?>selected<?php }?>" id="topim_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"></a>
							</div>
						<?php }} ?>
					</div>
			</div><!--box content-->
		</div><!--box-->
	</div><!--main-->
</section>

<?php $_template = new Smarty_Internal_Template("footer.tpl.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>