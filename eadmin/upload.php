<?php
	session_start();
	require_once "application/config.php";
	//require_once "../classes/sql.class.php";
	require_once "library/packages/UploadClass/upload.class.php";
	require_once "library/core/Filter.class.php";
	require_once "library/core/Database.class.php";

	// Initialize db object for the queries
	$db = new Database($db_config);

	define("FILE_PATH" , dirname(__FILE__)."/../");
	$types = array(
	            IMAGETYPE_GIF  => 'gif',
	            IMAGETYPE_JPEG => 'jpg',
	            IMAGETYPE_PNG  => 'png',
	    );

	$types1 = array(
	            IMAGETYPE_GIF  => 'gif',
	            IMAGETYPE_JPEG => 'jpeg',
	            IMAGETYPE_PNG  => 'png',
	    );

	if($_POST['mode']=='upload_picture') {


		$tempFile   = $_FILES['Filedata']['tmp_name'];
		$pic_name   = time();

	    $infoImg   = @getimagesize($tempFile);
	    $extension = $types[$infoImg[2]];

	    if($extension == null) $extension = 'jpg';

	    $picture    = $pic_name.".".$extension;

		// 600x400
		if($infoImg[0] > 800){ $resize = 1; $x=800; $y=0; }
		elseif($infoImg[1]>600){ $resize = 1; $x=0; $y=600; }
		else { $x=0; $y=0; $resize = 0; }

		$_FILES['Filedata']['type'] = $infoImg['mime'];

		$upPic = new Upload($_FILES['Filedata']);

	    if ($upPic->uploaded) {
              $upPic->file_new_name_body = $pic_name;
              $upPic->image_resize       = $resize;
              if($x!=0)$upPic->image_x   = $x;
			  if($y!=0)$upPic->image_y   = $y;

			  if($x==0)$upPic->image_ratio_x 	 = true;
			  if($y==0)$upPic->image_ratio_y 	 = true;

			  //$upPic->image_watermark       	= $config['userarea_dir'] . 'data/big-logo.png';
			  //$upPic->image_watermark_position 	= '';

			  $upPic->file_overwrite     = true;
              $upPic->file_auto_rename   = false;
              $upPic->process(FILE_PATH.'data/temp/');
        }

		$new_file = FILE_PATH . 'data/temp/' . $picture;

		$Img   = @getimagesize($new_file);

		echo $Img[0].",".$Img[1].",".$picture.$thumb;

		exit();
	}
	else if($_POST['mode'] == 'upload_picture_product'){

		$idproduct  = $_POST['id'];

		$tempFile   = $_FILES['Filedata']['tmp_name'];
		$pic_name   = time();

	    $infoImg   = @getimagesize($tempFile);
	    $extension = $types[$infoImg[2]];

	    if($extension == null) $extension = 'jpg';

	    $picture    = $pic_name.".".$extension;

		// 600x400
		if($infoImg[0] > 1200){ $resize = true; $x=1200; $y=0; }
		elseif($infoImg[1]>800){ $resize = true; $x=0; $y=800; }
		else { $x=0; $y=0; $resize = false; }

		$_FILES['Filedata']['type'] = $infoImg['mime'];

		$upPic = new Upload($_FILES['Filedata']);

	    if ($upPic->uploaded) {
              $upPic->file_new_name_body = $pic_name;
              $upPic->image_resize       = $resize;
              if($x!=0)$upPic->image_x   = $x;
			  if($y!=0)$upPic->image_y   = $y;

			  if($x==0)$upPic->image_ratio_x 	 = true;
			  if($y==0)$upPic->image_ratio_y 	 = true;

			  $upPic->file_overwrite     = true;
              $upPic->file_auto_rename   = false;
              $upPic->process(FILE_PATH . 'data/big/');

        }

		// Малка снимка
		$resize = 1; $x=300; $y=0;

		if ($upPic->uploaded) {
              $upPic->file_new_name_body = $pic_name;
              $upPic->image_resize       = $resize;
              if($x!=0)$upPic->image_x   = $x;
			  if($y!=0)$upPic->image_y   = $y;

			  if($x==0)$upPic->image_ratio_x 	 = true;
			  if($y==0)$upPic->image_ratio_y 	 = true;

			  $upPic->file_overwrite     = true;
              $upPic->file_auto_rename   = false;
              $upPic->process(FILE_PATH . 'data/thumb/');
        }

		addImgToProduct($idproduct, $picture, 'products', 'product');

		echo $pic_name;

		exit();
	}

	elseif($_POST['crop'] == 1){ // products

		$id_f 		= (int)$_POST['product_id'];
		$id_image 	= crop_image_normal('product', $id_f, 240, 150,  $_POST);

		checkTopImgExists('products', 'idproduct', $id_f, $id_image);

		header("Location: " . $config['site_url'] . "products/editPicture/idproduct/" . $id_f);

		exit();
	}
	elseif($_POST['crop'] == 9){ // news

		$id_f 		= (int)$_POST['news_id'];
		$id_image 	= crop_image_normal('news', $id_f, 350, 216,  $_POST);

		checkTopImgExists('news', 'idnews', $id_f, $id_image);

		header("Location: " . $config['site_url'] . "news/editPicture/idnews/" . $id_f);

		exit();
	}

//////////////////////////////////////////////////////
	function crop_image_normal($name, $id_f, $size_w, $size_h, $data){
		global $types1, $types, $SQL;

		$pic_name = time();

		$targ_w = $size_w;
		$targ_h = $size_h;
		$jpeg_quality = 90;

		$src   = FILE_PATH.'data/temp/'.$data['img_path'];

		// Original image's details
		list($originalWidth, $originalHeight, $originalType) = getimagesize($src);

		// Original image's resource
		$image = call_user_func('imagecreatefrom'.$types1[$originalType], $src);

		// Crop image
		if (function_exists('imagecreatetruecolor') && ($temp = imagecreatetruecolor($targ_w, $targ_h) )){
		    imagecopyresampled($temp, $image, 0, 0, $data['x'] ,$data['y'], $targ_w, $targ_h, $data['w'], $data['h']);
		}
		else {
		    $temp = imagecreate($targ_w, $targ_h);
		    imagecopyresized($temp, $image, 0, 0, $data['x'] ,$data['y'], $targ_w, $targ_h, $data['w'], $data['h']);
		}

		if( !is_dir(FILE_PATH.'data/thumb/') ){

			@mkdir(FILE_PATH.'data/' , 0777, true);
			@mkdir(FILE_PATH.'data/thumb/' , 0777, true);
			@mkdir(FILE_PATH.'data/big/', 0777, true);
		}

		if($name == 'news') $spec_path = 'news/';

		$img_name = $pic_name.".".$types[$originalType];
		if($types[$originalType] == 'png')
		    call_user_func('image' . $types1[$originalType], $temp, FILE_PATH.'data/'.$spec_path.'thumb/'.$img_name);
		else
		    call_user_func('image' . $types1[$originalType], $temp, FILE_PATH.'data/'.$spec_path.'thumb/'.$img_name,  $jpeg_quality);

		//// move big image
		copy($src, FILE_PATH.'data/'.$spec_path.'big/'.$img_name);

		//// delete temp
		sleep(2);
		@unlink($src);

		if($name == 'product') $id_im = addImgToProduct($id_f, $img_name, 'products', 'product');
		if($name == 'news') $id_im = addImgToProduct($id_f, $img_name, 'news', 'news');

		return $img_name;
	}

	function checkTopImgExists($tablename, $id_field_name, $id, $img_name){
		global $db;

		$pic = $db->fetchOne("SELECT `picture` FROM `".$tablename."` WHERE `".$id_field_name."`=".$id);
		if($pic) return;

		$db->query("UPDATE `".$tablename."` SET `picture`='".$img_name."' WHERE `".$id_field_name."`=".$id);
	}

	function addImgToProduct($id_news, $img_name, $tname, $fname=''){
		global $db;

		$fname = ($fname)? $fname : $tname;

		$fields['id'.$fname] 	= $id_news;
		$fields['picture'] 		= $img_name;

		$db->insert($tname.'_picture', $fields);

		return $db->lastInsertId();
	}

	function crop_image_special($name, $id_f, $size_w, $size_h, $data){
		global $types1, $types;

		$pic_name = time();

		$targ_w = $size_w;
		$targ_h = $size_h;
		$jpeg_quality = 90;

		$src   = FILE_PATH.'pictures/temp/'.$data['img_path'];

		// Original image's details
		list($originalWidth, $originalHeight, $originalType) = getimagesize($src);

		// Original image's resource
		$image = call_user_func('imagecreatefrom'.$types1[$originalType], $src);

		// Crop image
		if (function_exists('imagecreatetruecolor') && ($temp = imagecreatetruecolor($targ_w, $targ_h) )){
		    imagecopyresampled($temp, $image, 0, 0, $data['x'] ,$data['y'], $targ_w, $targ_h, $data['w'], $data['h']);
		}
		else {
		    $temp = imagecreate($targ_w, $targ_h);
		    imagecopyresized($temp, $image, 0, 0, $data['x'] ,$data['y'], $targ_w, $targ_h, $data['w'], $data['h']);
		}

		if( !is_dir(FILE_PATH.'pictures/'.$name.'/'.$id_f.'/small/') ){

			@mkdir(FILE_PATH.'pictures/'.$name.'/'.$id_f.'/' , 0777, true);
			@mkdir(FILE_PATH.'pictures/'.$name.'/'.$id_f.'/small/' , 0777, true);
			@mkdir(FILE_PATH.'pictures/'.$name.'/'.$id_f.'/big/', 0777, true);
		}

		$img_name = $pic_name.".".$types[$originalType];
		if($types[$originalType] == 'png')
		    call_user_func('image' . $types1[$originalType], $temp, FILE_PATH.'pictures/'.$name.'/'.$id_f.'/'.$img_name);
		else
		    call_user_func('image' . $types1[$originalType], $temp, FILE_PATH.'pictures/'.$name.'/'.$id_f.'/'.$img_name,  $jpeg_quality);

		//// delete temp
		sleep(2);
		@unlink($src);

		return $img_name;
	}
?>