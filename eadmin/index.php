<?php
		
	// Configuration
	ini_set('memory_limit', '1164M');
	ini_set("zend.ze1_compatibility_mode", "off");
	ini_set('display_errors', 0);

	// Start the sessions
	if (!session_id()) {
		session_start();
	}

	// Define root path
	define('ROOT_PATH', dirname(__FILE__) . "/");

	// Load main framework classes
	require_once(ROOT_PATH . "library/core/Controller.class.php");
	require_once(ROOT_PATH . "library/core/Filter.class.php");
	require_once(ROOT_PATH . "library/core/Database.class.php");
	require_once(ROOT_PATH . "library/core/Registry.class.php");
	require_once(ROOT_PATH . "library/core/Language.class.php");
	require_once(ROOT_PATH . "library/smarty/Smarty.class.php");

	// Load config, satellites and language files
	require_once(ROOT_PATH . "application/config.php");

	// Start the template system
	$smarty = new Smarty();
	$smarty->template_dir = ROOT_PATH . 'application/views/';
	$smarty->compile_dir = ROOT_PATH . 'temp/views_c/';
	$smarty->config_dir = ROOT_PATH . 'temp/views_c/';
	$smarty->cache_dir = ROOT_PATH . 'temp/cache/';
	$smarty->debugging = false;
	$smarty->caching = false;
	$smarty->compile_check = true;
	$smarty->debugging = false;
	$smarty->cache_lifetime = 3600;

	// Initialize db object for the queries
	$db = new Database($db_config);

	// Start the registry
	Registry::set($db, "db");
	Registry::set($smarty, "smarty");
	Registry::set($config, "config");
	Registry::set(Language::load("bulgarian"), "lang");	
	
	// Load system class
	require_once(ROOT_PATH . "library/core/Loader.class.php");
	Loader::loadClass("System");

	// User session check
	if ($_SESSION['admin']) {
		$smarty->assign("admin_data", $_SESSION['admin']);
	}

	// Start the controllers management
	Loader::loadClass("Router");
	$controller = new Controller();
	$controller->start(Router::checkURL());

	// Show debugger
	$developers_ips = array("84.22.27.137");
	if (in_array($_SERVER['REMOTE_ADDR'], $developers_ips)) {
		$db->showSQLDebugger();
	}
	
?>