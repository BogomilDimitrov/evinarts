<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved
 *
 * Pages controller
 *
 * @file            PagesController.php
 * @category        Application
 * @author          Elenko Ivanov
 *
 */
Class PagesController extends Controller {

    // Database object
    public $db = null;
    // Config array
    public $config = null;
    // Memcache object
    public $memcache = null;
    // Smarty object
    private $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;
	
	// Flashdata
	private $flashdata = null;

    // If actions is not found
    public function _call() {
		
		// Load needed classes
		Loader::loadClass('Common');
	
		// Show error page
		Common::showPage(404);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // Get variables
        $this->db = Registry::get('db');
        $this->config = Registry::get('config');
		$this->memcache = Registry::get('memcache');
        $this->smarty = Registry::get('smarty');
        $this->lang = Registry::get('lang');
        $this->cache = Registry::get('cache');
        $this->router = Registry::get("router");
		$this->lg = Registry::get("lg");

        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);

        // Check for messages or any flashdata
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
        if (!empty($_SESSION['flashdata'])) {
			$this->flashdata = $_SESSION['flashdata'];
            unset($_SESSION['flashdata']);
        }
    }
	
	public function promotionAction(){
		
		$listSliders = $this->db->fetchAll("SELECT `title_" . $this->lg . "` as title, `url_" . $this->lg . "` as url, picture, file
									 FROM slider WHERE typeBanner=6 and status=1 ORDER BY position ASC");
		
		// Define meta tags
		$meta['title'] 		= 'Промоции';
		$meta['keywords'] 	= '';
		
		// Parse data to template
		$data = array(
			'listSliders'	=> $listSliders,
			'meta'			=> $meta,
		);
		
		// Assign values and display template
		$this->smarty->assign('page', 'promotion');
		$this->smarty->assign($data);
		$this->smarty->display("pages/promotion.tpl.html");
		
	}
	
	// Catalogues action
	public function cataloguesAction(){
				
		// Get all Catalogs
		$listCatalogs = $this->db->fetchAll("SELECT `title_" . $this->lg . "` as title, `file_" . $this->lg . "` as file, picture FROM `catalogs` WHERE `status` = '1' ORDER BY `position` ASC");
				
		// Define meta tags
		$meta['title'] 		= 'Каталози';
		$meta['keywords'] 	= '';
		
		// Parse data to template
		$data = array(
			'listCatalogs'	=> $listCatalogs,
			'meta'			=> $meta,
		);
		
		// Assign values and display template
		$this->smarty->assign('page', 'catalogues');
		$this->smarty->assign($data);
		$this->smarty->display("pages/catalogues.tpl.html");
	}
	
	// Faq action
    public function faqAction() {
		
		// Get all Faq
		$listFaq = $this->db->fetchAll("SELECT `alias_" . $this->lg . "` as alias, `title_" . $this->lg . "` as title, `content_" . $this->lg . "` as content, picture FROM `faq` WHERE `active` = '1' and `typeData` = 1 ORDER BY `position` ASC");
				
		// Define meta tags
		$meta['title'] 		= 'Често задавани въпроси';
		$meta['keywords'] 	= '';
		
		// Todo get news
		
		// Parse data to template
		$data = array(
			'listFaq'		=> $listFaq,
			'meta'			=> $meta,
		);
		
		// Assign values and display template
		$this->smarty->assign('page', 'faq');
		$this->smarty->assign($data);
		$this->smarty->display("pages/faq.tpl.html");
	}
	
	// References
	public function referencesAction(){
		
		Loader::loadClass('Pager');
				
		// Parameters for pagination
		$pageNum 	= ($this->getParam('page')) ? $this->getParam('page') : 1;
		$url 		= $this->config['site_url'] . $this->lg . '/references/page/[page]/';
		
		$rows_per_page = 5;
		
		// Construct limit
		$limit = (($pageNum-1) * $rows_per_page);
		
		// Create pagination
		$pager = new Pager();
		$pager->replace = '[page]';
		$pager->page = $pageNum;
		$pager->rows_per_page = $rows_per_page;
		$pager->link = $url;	
			
		// Count references
		$countReferences = $this->db->fetchOne("SELECT count(*) FROM `review` WHERE `status` = 'active'");
							
		// Get pager data
		$return = $pager->paginate($countReferences);		
		
		// Get all references
		$listRef = $this->db->fetchAll("SELECT `title_" . $this->lg . "` as title, `profession_" . $this->lg . "` as profesion, 
											`firma_" . $this->lg . "` as firma, `content_" . $this->lg . "` as content,	picture, picture_reference 
										FROM `review` 
										WHERE `status` = 'active' 
										ORDER BY `position` ASC
										LIMIT " . $limit . "," . $rows_per_page );
				
		// Define meta tags
		$meta['title'] 		= 'Референции';
		$meta['keywords'] 	= '';
				
		// Parse data to template
		$data = array(
			'listRef'		=> $listRef,
			'paginate'		=> $return[0],
			'meta'			=> $meta,
		);
		
		// Assign values and display template
		$this->smarty->assign('page', 'references');
		$this->smarty->assign($data);
		$this->smarty->display("pages/references.tpl.html");
	}
	
	// Advice action
    public function adviceAction() {
		
		// Load needed classes
		Loader::loadClass('Common');
		Loader::loadClass('Category');
		
		$url_par = $this->getParam('advice');
	
		if($url_par){
			
			// Separate dashes
			$dashSeparated = explode('-', $url_par);

			// Get faq id
			$idfaq = end($dashSeparated);

			// Remove news id from string
			array_pop($dashSeparated);

			// Get faq slug
			$slug = implode('-', $dashSeparated);
		}
		
		if($idfaq){
			$faqInfo = $this->db->fetchRow("SELECT `alias_" . $this->lg . "` as alias, `title_" . $this->lg . "` as title, `content_" . $this->lg . "` as content, picture FROM `faq` WHERE idfaq = " . $idfaq . " and `active` = '1' and `typeData` = 2");
			
			// Define meta tags
			$meta['title'] 			= $faqInfo['title'];
			$meta['description'] 	= Common::str_cut($faqInfo['content'], 300);
		}
		else{
						
			// Get all advice
			$listFaq = $this->db->fetchAll("SELECT `alias_" . $this->lg . "` as alias, `title_" . $this->lg . "` as title, `content_" . $this->lg . "` as content, picture FROM `faq` WHERE `active` = '1' and `typeData` = 2 ORDER BY `position` ASC");
			
			// Define meta tags
			$meta['title'] 		= 'Relevant Information';
			$meta['keywords'] 	= '';
		}
		
		// Right Banners
		$rightBanners = $this->db->fetchAll("SELECT idslide, title_" . $this->lg . " as title, picture, url_" . $this->lg . " as url FROM `slider` WHERE `active`='yes' and `type_banner` = 'small' ORDER BY `position` ASC LIMIT 6");
		
		// Left Banners
		$leftBanners = $this->db->fetchAll("SELECT idslide, title_" . $this->lg . " as title, picture, url_" . $this->lg . " as url FROM `slider` WHERE `active`='yes' and `type_banner` = 'small_left' ORDER BY `position` ASC LIMIT 2");
	
		// Generate left categories
		$leftCategories = Category::getCategory(array('active' => 1, 'left_menu' => 'yes'), array('by' => 'position', 'type' => 'ASC'));
		
		// Parse data to template
		$data = array(
			'listFaq'			=> $listFaq,
			'rightBanners'		=> $rightBanners,
			'leftBanners'		=> $leftBanners,
			'leftCategories' 	=> $leftCategories,
			'faqInfo'			=> $faqInfo,
			'meta'				=> $meta,
		);
		
		// Assign values and display template
		$this->smarty->assign('page', 'advice');
		$this->smarty->assign($data);
		$this->smarty->display("pages/advice.tpl.html");
	}
	
    // Page action
    public function viewAction() {
	
		// Load needed classes
		Loader::loadClass('Common');
			
		// Get page name
		$page_name = $this->router['idpage'];
				
		// Get page data
		$pageInfo = $this->db->fetchRow("SELECT id, page_name, title_" . $this->lg . " as title, content_" . $this->lg . " as content, 
													meta_title_" . $this->lg . " as meta_title, meta_description_" . $this->lg . " as meta_description, meta_keywords_" . $this->lg . " as meta_keywords
											FROM `pages` 
											WHERE `page_name` = '" . $page_name . "'");
		
		// Show error if page doesn't exist
		if(empty($pageInfo)) {
		
			// Show error page
			Common::showPage(404);
		}
		
		// Define meta tags
		$meta['title'] 			= $pageInfo['meta_title'];
		$meta['description'] 	= $pageInfo['meta_description'];
		$meta['keywords'] 		= $pageInfo['meta_keywords'];
		
		// Parse data to template
		$data = array(
			'pageInfo'	=> $pageInfo,
			'meta'		=> $meta,
		);
		
		// Assign values and display template
		$this->smarty->assign('page', $page_name);
		$this->smarty->assign($data);
		$this->smarty->display("pages/page.tpl.html");
    }
	
	
	// Contact Us
    public function contactsAction() {
	
		// Load needed classes
		Loader::loadClass('Common');
		
		// If posting
		if(strtolower($_SERVER['REQUEST_METHOD']) == 'post'){
		
			// Get post data
			$post_data = array(
				'name'			=> Filter::sanitize($_POST['name']),
				'email'			=> $_POST['email'],
				'phone'			=> Filter::sanitize($_POST['phone']),
				'message'		=> Filter::sanitize($_POST['comment']),
			);
			
			if($_SESSION['security_code'] != $_POST['security_code']) {
				$error = "Кода за сигурност е невалиден!";
			}
						
			// Check name
			else if(empty($post_data['name'])){
				$error = "Въведете вашето име!";
				$mark = 'name';
			}	
			
			// Check email
			else if(empty($post_data['email'])){
				$error = 'Email адреса е задължителен!';
				$mark = 'email';
			} else if(!Filter::checkEmail($post_data['email'])){
				$error = 'Email адреса е невалиден!';
				$mark = 'email';
			}
							
			// Check message
			else if(empty($post_data['message'])){
				$error = 'Съобщението не може да бъде празно!';
				$mark = 'message';
			}
			
			// If no errors
			if(empty($error)){
						
				$msg = 'Име: ' . $post_data['name'] . '<br />';
				$msg .= 'Email: ' . $post_data['email'] . '<br />';
				$msg .= 'Телефон: ' . $post_data['phone'] . '<br />';
				$msg .= 'Запитване: ' . $post_data['message'] . '<br />';
				
				// Send to Силвия
				System::sendEmail(
					array($this->config['site_email'] => $this->config['site_name']), 
					'evin@abv.bg', 
					'[Evinarts.com] Ново съобщение!', 
					$msg
				);
				
				// Assign success message
				$this->smarty->assign('success', 'Съобщението е изпратено успешно!');
			}
			
			// If we have an error
			else {
			
				// Assign error message and posted data
				$this->smarty->assign('error', $error);
				$this->smarty->assign('mark', $mark);
			}
		}

		// Contacts information
		$homeInfo = $this->db->fetchRow("SELECT id, page_name, title_" . $this->lg . " as title, content_" . $this->lg . " as content, 
													meta_title_" . $this->lg . " as meta_title, meta_description_" . $this->lg . " as meta_description, meta_keywords_" . $this->lg . " as meta_keywords 
													FROM `pages` 
													WHERE `page_name` = 'contacts'");
		
		// Define meta tags
		$meta['title'] 			= $homeInfo['meta_title'];
		$meta['description'] 	= $homeInfo['meta_description'];
		$meta['keywords'] 		= $homeInfo['meta_keywords'];
				
		
		$data = array(
			'meta' 		=> $meta,
			'homeInfo'	=> $homeInfo,
		);
		
		// Assign variables and display template
		$this->smarty->assign('page', 'contacts');
		$this->smarty->assign($data);
        $this->smarty->display('pages/contactus.tpl.html');
	}
	
	// Sitemap
	public function sitemapAction() {
				
		// Load needed classes
		Loader::loadClass('Gallery');
		Loader::loadClass('News');
		
		$htmlSiteMap = '<ul class="sitemap_list">
			<li><a href="'. $this->config['site_url'] . $this->lg .'">' . $this->lang['home'] . ' </a></li>
			<li><a href="' . $this->config['site_url'] . $this->lg .'/about">' . $this->lang['za_nas'] . '</a></li>
			<li><a href="' . $this->config['site_url'] . $this->lg .'/archive">' . $this->lang['archive'] . '</a></li>
			<li><a href="' . $this->config['site_url'] . $this->lg .'/cards">' . $this->lang['header_cards'] . '</a></li>
			<li><a href="' . $this->config['site_url'] . $this->lg .'/decorations">' . $this->lang['header_dekoracii'] . '</a></li>
			<li><a href="' . $this->config['site_url'] . $this->lg .'/gallery">' . $this->lang['gallery'] . '</a><ul>';
			
		$htmlSiteMap .= "\n";
		
		$prodList = Gallery::getGallery(array('active' => '!4', 'title_' . $this->lg => '! '));
		
		foreach($prodList as $k=>$prod){
			$htmlSiteMap .= '<li><a href="'. $this->config['site_url'] . $this->lg . '/' . $prod['alias'] .'-'. $prod['idproduct'] .'">' . $prod['title'] . '</a></li>';
			$htmlSiteMap .= "\n";
		}
		
		$htmlSiteMap .= '</ul></li>';
							
		$htmlSiteMap .= '<li><a href="' . $this->config['site_url'] . $this->lg . '/news">' . $this->lang['news'] . '</a><ul>';
		$newsList = News::getNews(array('status' => 'active'), array('by' => 'date_added', 'type' => 'DESC') );
		
		foreach($newsList as $k=>$n){
			$htmlSiteMap .= '<li><a href="'. $this->config['site_url'] . $this->lg . '/news/' . $n['alias'] .'-'. $n['idnews'] .'">' . $n['title'] . '</a></li>';
			$htmlSiteMap .= "\n";
		}
		
		$htmlSiteMap .= '</ul></li>
			<li><a href="' . $this->config['site_url'] . $this->lg . '/contacts">' . $this->lang['kontakti'] . '</a></li>
		</ul>';
		
		// Define meta tags
		$meta['title'] 		= 'Sitemap';
		$meta['keywords'] 	= 'sitemap';
				
		// Parse data to template
		$data = array(
			'page'			=> 'sitemap',
						
			'htmlSiteMap' 	=> $htmlSiteMap,
			'meta'			=> $meta,
		);
		
		$this->smarty->assign('page', 'sitemap');
		$this->smarty->assign($data);
        $this->smarty->display('pages/sitemap.tpl.html');
	}
	
}

?>