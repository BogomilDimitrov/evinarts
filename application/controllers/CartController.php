<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved
 *
 * Cart controller
 *
 * @file            CartController.php
 * @category        Application
 * @author          Elenko Ivanov
 * @email			elenko.ivanov@gmail.com
 *
 */
Class CartController extends Controller {

    // Database object
    public $db = null;
    // Config array
    public $config = null;
    // Memcache object
    public $memcache = null;	
    // Smarty object
    public $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;

    // If actions is not found
    public function _call() {
	
		// Load needed classes
		Loader::loadClass('Common');
	
		// Show error page
		Common::showPage(404);
    }

    // Initialization method - executed with every controller method
    public function init() {
	
        // Get variables
        $this->db = Registry::get('db');
        $this->config = Registry::get('config');
		$this->memcache = Registry::get('memcache');
        $this->smarty = Registry::get('smarty');
        $this->lang = Registry::get('lang');
        $this->cache = Registry::get('cache');
        $this->router = Registry::get("router");
		$this->lg	= Registry::get("lg");

        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);
		
		// If refreshing the page is needed
		$refresh = $this->getParam("refresh");
		if(!empty($refresh)) $this->smarty->assign("refresh_page", $this->config['site_url'] . str_replace('/refresh/page', '', substr($_SERVER["REQUEST_URI"], 1)));

        // Check for messages
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
    }

    // Index action - display the index page
    public function indexAction() {
		
		// Load needed classes
		Loader::loadClass('Cart');
		Loader::loadClass('Common');
		
		$cartInfo = $this->db->fetchrow("SELECT * FROM `carts` WHERE `phpsession`='" . session_id() . "' AND `status`=1");
		
		// Get all products in cart
		if($cartInfo){ 
			
			// Взимем всички продукти в количката
			$productInCart = Cart::listProductCart($cartInfo['id_carts']);
			
			if($productInCart){
				$totalPrice = 0;
				foreach($productInCart as $k=>$v){
					
					// Дефинираме цената
					$price = $v['price'];
					
					$price = Common::calculatePrice($price);
					
					$productInCart[$k]['price'] = $price;
										
					$col_price = $v['col']*$price;
					$productInCart[$k]['price_col']	= $col_price;
											
					// Calculate max price
					$totalPrice +=	$col_price;
					
					// Модифицираме цената, за да и добавим .00 ако вече няма стойностти
					$productInCart[$k]['price'] 		= Common::roundPreDi($productInCart[$k]['price']);
					$productInCart[$k]['price_col'] 	= Common::roundPreDi($productInCart[$k]['price_col']);
				}
			}
			
			$totalPrice = round($totalPrice, 2);
		}
		
		// TODO da se opravi valutata
		
        // Define meta tags
		$meta['title'] 		= 'Final order';
		$meta['keywords'] 	= '';
		
		// Parse data to template
		$data = array(
			'title'				=> 'Order',
			'meta'				=> $meta,
			'productInCart'		=> $productInCart,
			'totalPrice'		=> Common::roundPreDi($totalPrice),
		);
		
		// Assign values and display template
		$this->smarty->assign('page', 'cart');
		$this->smarty->assign($data);
		$this->smarty->display("cart/cart.tpl.html");
    }
	
	// Checkout page
    public function checkoutAction() {
		
		// Load classes
		Loader::loadClass('Cart');
		
		$cartInfo = $this->db->fetchrow("SELECT * FROM `carts` WHERE `phpsession`='" . session_id() . "' AND `status`=1");
		
		if(!$cartInfo){
			System::redirect( $this->config['site_url'] . 'cart' );
		}
		
		// Взимаме всички секции и ги организирам в тип масив idsection => section_name
		$sectionNameList = $this->db->fetchAll("SELECT idsection, `section_name_" . $this->lg . "` as section_name FROM size_section WHERE status='active'");
		foreach($sectionNameList as $k=>$v){
			$sectionNames[$v['idsection']] = $v['section_name'];
		}
		
		// Get all products in cart
		if($cartInfo){ 
			
			// Взимаме всички параметри и ги организирам по секции от тип idsection => array( idsize => title)
			$listParameters = Common::listParameters();
			
			$productInCart = Cart::listProductCart($cartInfo['id_carts']);
			
			if($productInCart){
				$totalPrice = 0;
				foreach($productInCart as $k=>$v){
					
					// Проверяваме дали има допълнителни параметри към продукта
					if($v['variant_code']){
						$productInCart[$k]['parametersInfo'] = $this->db->fetchAll("SELECT s.`title_" . $this->lg . "` as title, s.section FROM size as s WHERE s.idsize in (" . $v['variant_code'] . ")");
					}
					
					// Конвертиране на валутата
					$price = Common::calculatePrice($v['price_1']); // Main price
					$productInCart[$k]['price_1'] = $price;
					$productInCart[$k]['price_2'] = Common::calculatePrice($v['price_2']);
					
					$col_price = $v['col']*$price;
					$productInCart[$k]['price_col']	= $col_price;
					
					if($v['status'] == 1){
						$price_2 = $productInCart[$k]['price_2'];
						$productInCart[$k]['price_discount'] = $price_2 - $price;
					}
					
					// Calculate max price
					$totalPrice +=	$col_price;
					
					// Модифицираме цената, за да и добавим .00 ако вече няма стойностти
					$productInCart[$k]['price_1'] = Common::roundPreDi($productInCart[$k]['price_1']);
					$productInCart[$k]['price_2'] = Common::roundPreDi($productInCart[$k]['price_2']);
					
					$productInCart[$k]['price_col'] = Common::roundPreDi($productInCart[$k]['price_col']);
				}
			}
					
			// На всички регистрирани получават % отстъпка, който е дефинирана константа в config.php
			if($percentPromo = $this->config['discounts'][$_SESSION['user']['discount']]){
				$totalPricePercent = $totalPrice; 
				$totalPrice = $totalPrice - ($totalPrice*($percentPromo/100));
			}
			
			$totalPrice = round($totalPrice, 2);
		}
		
		// Define meta tags
		$meta['title'] 		= 'Финализиране на поръчката - данни за доставка';
		$meta['keywords'] 	= '';
		
		// Parse data to template
		$data = array(
			'title'				=> 'Финализиране на поръчката',
			'meta'				=> $meta,
			'sectionNames'		=> $sectionNames,
			'productInCart'		=> $productInCart,
			'totalPrice'		=> Common::roundPreDi($totalPrice),
			'percent'			=> $percent,
			'totalPricePercent' => ($totalPricePercent)? Common::roundPreDi($totalPricePercent) : 0,
			'percentPromo'		=> $percentPromo,
		);
		
		// Assign values and display template
		$this->smarty->assign('page', 'cart');
		$this->smarty->assign($data);
		$this->smarty->display("cart/checkout.tpl.html");
	}
	
	// Order page
    public function orderAction() {
		
		// Load classes
		Loader::loadClass('Cart');
		Loader::loadClass('Common');
		
		// If posting
		if(strtolower($_SERVER['REQUEST_METHOD']) == 'post'){
			
			// Get Cart information
			$cartInfo = $this->db->fetchrow("SELECT * FROM `carts` WHERE `phpsession`='" . session_id() . "' AND `status`=1");
						
			if($cartInfo){
				
				$post_data = array(
					'email'			=> $_POST['customer']['email'],
								
					'fullName'		=> Filter::sanitize($_POST['customer']['fullName']),
					'phone'			=> Filter::sanitize($_POST['customer']['phone']),
					'city'			=> Filter::sanitize($_POST['customer']['city']),
					'address'		=> Filter::sanitize($_POST['customer']['address']),
					'post_code'		=> Filter::sanitize($_POST['customer']['post_code']),
					'description'	=> Filter::sanitize($_POST['customer']['description']),
					'status'		=> 2,
					'iduser'		=> ($_SESSION['user']['iduser'])? $_SESSION['user']['iduser'] : 0,
				);
											
				Cart::updateCart($post_data, $cartInfo['id_carts']);
								
				// Preview Order
				$productInCart = Cart::listProductCart($cartInfo['id_carts']);

				if($productInCart) {
					$totalPrice = 0;
					$i = 0;
					foreach($productInCart as $k=>$v) {
						$price = $v['price']; // Main price
						
						$price = Common::calculatePrice($price);
						
						$col_price 						= $v['col']*$price;
						$productInCart[$k]['price_col']	= $col_price;
						
						// Calculate max price
						$totalPrice +=	$col_price;
						
						// Генерираме продуктите за да ги добавим в майл-а за поръчката
						$i=$k+1;

						$productEmailStore[$i] = array(
													'productName' => $v['title'],
													'quantity'	  => $v['col'],
													'price'		  => $price,
													'totalPrice'  => $col_price
												);
					}
										
					$totalPrice = round($totalPrice, 2);
				}
				
				// Взимаме обновената информация от базата
				$cartInfo = reset(Cart::getCartInfo(array('id_carts' => $cartInfo['id_carts'])));
				
				// Тодо да опражя цената в майл-а
				
				// Генерираме параметрите в майл-а
				$dataEmail = array(
					'idorder' 		=> $cartInfo['id_carts'],
					'date_create' 	=> date('Y-m-d H:i:s', strtotime($cartInfo['date_added'])),
					'orderTotal' 	=> $totalPrice,
					'currency'		=>  $_SESSION['currency'],
					'orderDiscount' 	=> '',
					'orderPercentPromo' => '',
					'orderDeliveryPay'	=> '',
					'orderDelivery' 	=> '',
					'orderPay' 			=> '',
					'description'		=> $cartInfo['description'],
					
					'shipping' => array(
							'name' 			=> $cartInfo['fullName'],
							'email' 		=> $cartInfo['email'],
							'address' 		=> $cartInfo['address'],
							'city' 			=> $cartInfo['city'],
							'post_code' 	=> $cartInfo['post_code'],
							'phone' 		=> $cartInfo['phone'],
						),
					'products' => $productEmailStore,	
				);
				
				// Взимаме темплейта
				$msgUser = Common::getEmailTemplate('order-complete', $dataEmail);
				
				// Send to User who order
				if($cartInfo['email']){
					System::sendEmail(
						array($this->config['site_email'] => $this->config['site_name']), 
						$cartInfo['email'], 
						'[evinarts.com] Нова поръчка!', 
						$msgUser
					);
				}

				// Send to Operator
				System::sendEmail(
					array($this->config['site_email'] => $this->config['site_name']), 
						'evin@abv.bg', 
						'[evinarts.com] Нова поръчка!', 
					$msgUser
				);
			}
			else {
				// Redirect
				System::redirect( $this->config['site_url'] . $this->lg . '/cart' );
			}
		}	
		else {
			// Redirect
			System::redirect( $this->config['site_url'] . $this->lg . '/cart' );
		}
		
		// Define meta tags
		$meta['title'] 		= 'Край на поръчката';
		$meta['keywords'] 	= '';
		
		// Parse data to template
		$data = array(
			'title'				=> $meta['title'],
			'idorder'			=> $cartInfo['id_carts'],
			'date_order'		=> date('Y-m-d, H:i'),
			
			'orderInfo'         => $post_data,
			'productInCart'		=> $productInCart,
			
			'totalPrice'		=> $totalPrice,
			'totalPricePercent' => $totalPricePercent,
			
			'meta'				=> $meta,
		);
		
		// Assign values and display template
		$this->smarty->assign('page', 'order');
		$this->smarty->assign($data);
		$this->smarty->display("cart/order.tpl.html");
	}
	
}

?>