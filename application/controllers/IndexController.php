<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved
 *
 * Index controller
 *
 * @file            IndexController.php
 * @category        Application
 * @author          Elenko Ivanov
 *
 */
Class IndexController extends Controller {

    // Database object
    public $db = null;
    // Config array
    public $config = null;
    // Memcache object
    public $memcache = null;
    // Smarty object
    private $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;

	// Flashdata
	private $flashdata = null;

    // If actions is not found
    public function _call() {

		// Load needed classes
		Loader::loadClass('Common');

		// Show error page
		Common::showPage(404);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // Get variables
        $this->db = Registry::get('db');
        $this->config = Registry::get('config');
		$this->memcache = Registry::get('memcache');
        $this->smarty = Registry::get('smarty');
        $this->lang = Registry::get('lang');
        $this->cache = Registry::get('cache');
        $this->router = Registry::get("router");
		$this->lg	= Registry::get("lg");

        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);

        // Check for messages or any flashdata
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
        if (!empty($_SESSION['flashdata'])) {
			$this->flashdata = $_SESSION['flashdata'];
            unset($_SESSION['flashdata']);
        }
    }

    // Homepage
    public function indexAction() {

		// Load needed classes
		Loader::loadClass('News');
		Loader::loadClass('Gallery');

		// Home information
		$homeInfo = $this->db->fetchRow("SELECT id, page_name, title_" . $this->lg . " as title, content_" . $this->lg . " as content, 
													meta_title_" . $this->lg . " as meta_title, meta_description_" . $this->lg . " as meta_description, 
													meta_keywords_" . $this->lg . " as meta_keywords 
													FROM `pages` 
													WHERE id=13");


		// Взимаме id на категориите картини - 11
		$categoryList = Gallery::getCategory(array('active' => 1, 'idparrent' => 11), array('by' => 'position', 'type' => 'ASC'));

		// Главната категория
		$ids_array[] = 11;



		// Взимаме само id-тата, за да ги използвам в where clause
		if($categoryList) {
			foreach($categoryList as $k=>$v){
                $ids_array[] = $v['idcategory'];
                $categoryNames[] = $v;
            }
		}

		$_SESSION['galleryCategories'] = $categoryNames;

		// Генерирам Where, в която е само картини от избрани категории
		$where_clause = " and idcategory in (" . implode(',', $ids_array) . ")";

		// Слайдер информация
        //TODO: change flexslider to show only chosen pictures from admin panel.
		$listPicture = Gallery::getGallery(array('status' => 'new`'), array('by' => 'idproduct'), array('from' => 0 , 'count' => 5), $where_clause);

		// Последните 3 новини
		$listNews = News::getNews(array('status' =>'active'), array('by' => 'idnews', 'type' => 'DESC'), array('from' =>0, 'count' => 1) );

		// Категория Картички
		$categoryList = Gallery::getCategory(array('active' => 1, 'idparrent' => 28), array('by' => 'position', 'type' => 'ASC'));

		$ids_array = array();
		// Главната категория
		$ids_array[] = 28;

		// Взимаме само id-тата, за да ги използвам в where clause
		if($categoryList) {
			foreach($categoryList as $k=>$v){
				$ids_array[] = $v['idcategory'];
			}
		}

		// Генерирам Where, в която е само картини от избрани категории
		$where_clause = " and idcategory in (" . implode(',', $ids_array) . ")";

		$listKartichki = reset(Gallery::getGallery(array('active' => '!4'), array('by' => 'RAND'), array('from' => 0 , 'count' => 1), $where_clause));

		// Define meta tags
		$meta['title'] 			= $homeInfo['meta_title'];
		$meta['description'] 	= $homeInfo['meta_description'];
		$meta['keywords'] 		= $homeInfo['meta_keywords'];

		// Parse data to template
		$data = array(
			'page'			=> 'home',
			'homeInfo'		=> $homeInfo,

			'listNews'			=> $listNews,
			'listPicture'		=> $listPicture,
			'listKartichki'		=> $listKartichki,

			'meta'			=> $meta,
		);

		// Assign values and display template
		$this->smarty->assign($data);
		$this->smarty->display('index.tpl.html');
    }

	// Login page
    public function loginAction() {

		// If the user is registered - redirect to index page
        if (!empty($_SESSION['user'])) {

			// Redirect
			System::redirect( $this->config['site_url'] );
        }

		// Load needed classes
		Loader::loadClass('Users');
		Loader::loadClass('Crypt');
		Loader::loadClass('Common');

		// If login come from cart
		$redirect = ($this->getParam('redirect'))? $this->lg . '/cart/checkout' : '';

		// If posting registration form
        if ( strtolower($_SERVER['REQUEST_METHOD']) == 'post' AND !empty($_POST['login']) AND !empty($_POST['password']) ) {

			// Login with email
			if(strstr($_POST['login'], '@')) {

				// Try to get the user by email
				$data = reset(Users::getUsers(array('u.email' => $_POST['login'], 'u.password' => Crypt::encrypt($_POST['password']), 'u.active' => '!0')));
			}

            // If no error
            if(!empty($data)) {

				// Insert user data into session
				$_SESSION['user'] = $data;

				if(!empty($_POST['remember'])){

					// Set the remember me cookie
					setcookie('rememberd', Crypt::X3MEncrypt($data['iduser']), time() + 2592000, '/');
				}

				// Record login
				Common::insertLoginAttempt($data['iduser'], session_id(), 'dive', 'successfull');

				// Redirect
				System::redirect((!empty($redirect)) ? $this->config['site_url'] . $redirect : $this->config['site_url']);

			}

            // If error
            else {

				// Record login
				Common::insertLoginAttempt($_POST['login'], null, 'dive', 'unsuccessfull');

				// If redirect is set
				if(!empty($redirect)){

					// Assign redirect url
					$this->smarty->assign('redirect', $this->config['site_url'] . 'cart');
				}

				// Assign error message
                $this->smarty->assign('error', $this->lang['error_user_password']);
            }
        }

		// Define meta tags
		$meta['title'] 		= 'Login';
		$meta['keywords'] 	= 'Login';

		// Parse data to template
		$data = array(
			'page'			=> 'login',
			'meta'			=> $meta,
		);

		// Assign values and display template
		$this->smarty->assign($data);
		$this->smarty->display('users/login.tpl.html');
	}

	// Register page
    public function registerAction() {

		// If the user is registered - redirect to index page
        if (!empty($_SESSION['user'])) {

			// Redirect
			System::redirect( $this->config['site_url'] . $this->lg );
        }

		// Load needed classes
		Loader::loadClass('Users');
		Loader::loadClass('Crypt');
		Loader::loadClass('Common');

		// If posting registration form
        if ( strtolower($_SERVER['REQUEST_METHOD']) == 'post' AND !empty($_POST['email']) AND !empty($_POST['password']) ) {

			// Put data in array
			$post_data = array(
				'email'			=> Filter::sanitize($_POST['email']),
				'password'		=> Filter::sanitize($_POST['password']),
				'first_name'	=> Filter::sanitize($_POST['first_name']),
				'second_name'	=> Filter::sanitize($_POST['second_name']),
				'phone'			=> Filter::sanitize($_POST['phone']),
				'firma'			=> Filter::sanitize($_POST['firma']),
				'firma_dds'		=> Filter::sanitize($_POST['firma_dds']),
				'city'			=> Filter::sanitize($_POST['city']),
				'country'		=> Filter::sanitize($_POST['country']),
				'address'		=> Filter::sanitize($_POST['address']),
				'post_code'		=> Filter::sanitize($_POST['post_code']),
				'bulletin'		=> $_POST['bulletin']? 1:2,
			);

			// Check if the email address hasn't being used by somebody else
			$email_check = reset(Users::getUsers(array('u.email' => $post_data['email'])));

			if($_SESSION['security_code'] != $_POST['security_code']) {
				$error = $this->lang['return_error_1'];
			}

			// Check passwords
			if (empty($post_data['password'])) {
				$error = $this->lang['register_errorpass_1'];
			}else if (mb_strlen($post_data['password'], 'UTF-8') < 4) {
				$error = $this->lang['register_errorpass_2'];
			}else if (mb_strlen($post_data['password'], 'UTF-8') > 20) {
				$error = $this->lang['register_errorpass_3'];
			}else if ($_POST['password'] != $_POST['password_repeat']) {
				$error = $this->lang['register_errorpass_4'];
			}

			// Check email
			if (empty($post_data['email'])) {
				$error = $this->lang['register_erroremail_1'];
			}else if ($email_check) {
				$error = $this->lang['register_erroremail_2'];
			}

			if( empty($post_data['first_name']) || empty($post_data['second_name']) || empty($post_data['phone']) || empty($post_data['city']) || empty($post_data['address']) ){
				$error = $this->lang['register_erroremail_3'];
			}

			if(!$error){
				$post_data['date_added'] = date("Y-m-d H:i:s");
				$post_data['ip_added'] 	 = $_SERVER['REMOTE_ADDR'];
				$post_data['password']   = Crypt::encrypt($post_data['password']);
				$post_data['active']	 = 1; // Нов потребител, неудобрен
				$post_data['discount']	 = 2; // 10% отстъпка

				// Insert new user
				$this->db->insert('users', $post_data, false);

				// Get user id
				$data['iduser'] = $this->db->lastInsertId();

				if($data['iduser']){
					// Auto login
					$_SESSION['user'] = reset(Users::getUsers(array('u.iduser' => $data['iduser'])));

					if($_SESSION['user']){
						// Redirect
						System::redirect( $this->config['site_url'] . $this->lg);
					}
				}
				else {
					$error = $this->lang['register_errorfalse_1'];
				}
			}

			$this->smarty->assign("error", $error);
        }

		// Define meta tags
		$meta['title'] 		= 'Become a Member';
		$meta['keywords'] 	= 'Become a Member';

		// Parse data to template
		$data = array(
			'page'			=> 'register',
			'meta'			=> $meta,
			'topProduct'	=> $topProduct,
			'post_data'		=> $post_data,
		);

		// Assign values and display template
		$this->smarty->assign($data);
		$this->smarty->display('users/register.tpl.html');
	}

	// Profile page
    public function profileAction() {

		// If the user is not login - redirect to index page
        if (empty($_SESSION['user'])) {

			// Redirect
			System::redirect( $this->config['site_url'] . $this->lg . '/index/login' );
        }

		// Load needed classes
		Loader::loadClass('Users');
		Loader::loadClass('Crypt');
		Loader::loadClass('Common');

		// If posting registration form
        if ( strtolower($_SERVER['REQUEST_METHOD']) == 'post' ) {

			// Put data in array
			$post_data = array(
				'phone'			=> Filter::sanitize($_POST['phone']),
				'city'			=> Filter::sanitize($_POST['city']),
				'address'		=> Filter::sanitize($_POST['address']),
				'post_code'		=> Filter::sanitize($_POST['post_code']),
				'first_name'	=> Filter::sanitize($_POST['first_name']),
				'second_name'	=> Filter::sanitize($_POST['second_name']),
				'firma'			=> Filter::sanitize($_POST['firma']),
				'firma_dds'		=> Filter::sanitize($_POST['firma_dds']),
				'country'		=> $_POST['country'],
				'bulletin'		=> $_POST['bulletin']? 1:2,
			);


			if( empty($post_data['first_name']) || empty($post_data['second_name']) || empty($post_data['phone']) || empty($post_data['city']) || empty($post_data['address']) ){
				$error = $this->lang['register_erroremail_3'];
			}

			if(!$error){

				// Change password
				if($_POST['password']) $post_data['password'] = Crypt::encrypt($_POST['password']);

				// Update user
				$this->db->update('users', $post_data, ' iduser = ' . $_SESSION['user']['iduser']);

				// Get user data
				$_SESSION['user'] = reset(Users::getUsers(array('u.iduser' => $_SESSION['user']['iduser'])));

				$success = $this->lang['profile_update_success'];
				$this->smarty->assign('success', $success);
			}

			$this->smarty->assign("error", $error);

        }

		// If come from register by facebook
		// $registerFacebook = $this->getParam('loginfrom');

		// Define meta tags
		$meta['title'] 		= 'Profile edit';
		$meta['keywords'] 	= 'profile edit';

		// Parse data to template
		$data = array(
			'page'			=> 'profile',
			'meta'			=> $meta,
			// 'registerFacebook'	=> $registerFacebook,
		);

		// Assign values and display template
		$this->smarty->assign($data);
		$this->smarty->display('users/profile.tpl.html');
	}

	// Forgot password page
    public function forgotpassAction() {

		// If the user is login - redirect to index page
        if (!empty($_SESSION['user'])) {

			// Redirect
			System::redirect( $this->config['site_url'] . $this->lg );
        }

		// If posting registration form
        if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {

			// Put data in array
			$post_data = array(
				"email"		=> $_POST['email']
			);

			// Check email
			if (empty($post_data['email'])) {
				$error = $this->lang['register_erroremail_1'];
			}else if (!Filter::checkEmail($post_data['email'])) {
				$error = $this->lang['register_erroremail_4'];
			}

			// If no errors
			if(empty($error)){

				// Load needed classes
				Loader::loadClass("Users");
				Loader::loadClass("Crypt");

				// Get user data
				$user = reset(Users::getUsers(array('u.email' => $post_data['email'])));

				// If user exists
				if(!empty($user)){

					// User has registered through Diveinstinct.com

					// Generate new password
					$new_password = rand(1000, 10000);

					// Update database and set new password
					$this->db->update('users', array('password' => Crypt::encrypt($new_password)), "iduser = '" . $user['iduser'] . "'");

					// Set email content
					$email = array(
						'title' 	=> $this->lang['forgot_password_subject_email'],
						'slogan'	=> '',
						'content'	=> $this->lang['forgot_password_text_first'] . ' (User: @' . $user['email'] . ').
										<br>
										<br>Your new password is <span style="font-weight:bold;">' . $new_password . '</span>. If you want you can change your password
										<br>Following the link below: <a href="' . $this->config['site_url'] . 'index/profile" style="color:#0000FF;">Profile</a>.
										<br>
										<br><span style="font-style:italic;">Diveinstinct.com</span>
										<br>
										<br>
										<br><span>If you did not request this password reset, please let us know.</span>
										<br><span>Best Regards,</span>
										<br><span>Customer Support</span>'
					);

					// Fetch email design from template
					$this->smarty->assign('email', $email);
					$this->smarty->assign('config', $this->config);
					$email = $this->smarty->fetch('pages/email.tpl.html');

					// Send html email
					System::sendEmail(array($this->config['site_email'] => $this->config['site_name']), $user['email'], 'New password Diveinstinct.com', $email);

					// Assign success message
					$this->smarty->assign("success", $this->lang['forgot_password_error']);
				}

				// If error
				else {

					// Assign error message
					$error = $this->lang['forgot_error_wrong_email'];
				}
			}

			if($error)
				$this->smarty->assign("error", $error);
		}

		// Define meta tags
		$meta['title'] 		= 'Forgot password';
		$meta['keywords'] 	= 'Forgot password';

		// Parse data to template
		$data = array(
			'page'			=> 'forgotpass',
			'meta'			=> $meta,
		);

		// Assign values and display template
		$this->smarty->assign($data);
		$this->smarty->display('users/password.tpl.html');
	}

	public function testEmailAction(){






		return;

		$dataEmail = array(
			'name' 			=> 'Еленко Иванов',
			'phone'			=> '7879 897 89',
			'idorder' 		=> 879654,
			'date_create' 	=> date('Y-m-d H:i:s'),
			'email' 		=> 'elenko@test.net',
			'orderTotal' 	=> 313,
			'orderDeliveryPay'	=> 12,
			'orderDelivery' 	=> 'TNT',
			'orderPay' 			=> 'paypal',
			'shipping' => array(
					'name' 		=> 'Elenko Ivanov',
					'email' 	=> 'e@test.bg',
					'address' 	=> 'Trudovec, vitosha 7',
					'city' 		=> 'Sofia',
					'country' 	=> 'Bulgaria',
					'zip_code' 	=> '1325',
					'phone' 	=> '088 88 088 888',
				),
			'products' => array(
				1 => array(
					'productName' => 'Тест наме',
					'code'		  => '46764131',
					'quantity'	  => 2,
					'price'		  => 45,
					'totalPrice'  => 90,
				),
				2 => array(
					'productName' => 'das das das',
					'code'		  => 'das45',
					'quantity'	  => 1,
					'price'		  => 30,
					'totalPrice'  => 30,
				),
			)
		);

		$msgUser = Common::getEmailTemplate('order-complete', $dataEmail);


		// Send to Elenko
		$das = System::sendEmail(
			array($this->config['site_email'] => $this->config['site_name']),
			'elenko.ivanov@gmail.com',
			'[Diveinstinct.com] New order!',
			$msgUser
		);

		var_dump($das); die();
	}

	// Logout action
    public function logoutAction($redirect = true) {

        // Destroy all sessions
        session_destroy();

        // Unset the sessions
        unset($_SESSION['user']);

		// Remove cookies
        setcookie('rememberd', '', time() - 3600, '/');

		// If redirect is on
		if($redirect == true){

			// Redirect back to the main page
			System::redirect($this->config['site_url'] . $this->lg );
		}
    }

	// Change interface language
	public function changeLanguageAction() {

		// Get the new language
		$_SESSION['language'] = $this->getParam('lg');

		// If logged in
		if ($_SESSION['user']) {

			// Set the new language in user's profile
			$this->db->update('users', array('language' => $_SESSION['language']), "iduser = '" . $_SESSION['user']['iduser'] . "'");
		}

		// Set language cookie
		setcookie('language', $_SESSION['language'], time() + 2592000, '/');

		// Redirect
		(!empty($_SERVER['HTTP_REFERER'])) ? System::redirect($_SERVER['HTTP_REFERER']) : System::redirect($this->config['site_url']);
	}


	// Премахваме изличните продукти
	public function syncProdAction() {

		Loader::loadClass('Category');
		Loader::loadClass('Products');


		$idcat = array(131, 65, 86, 63, 87, 70);

		$whereCondition = array('p.active' => '!20');

		$idsProduct = Products::getProductsIds($whereCondition, " AND pc.idcategory in (" . implode(",", $idcat) . ")");

		//echo $idsProduct; die();

	}

}
?>