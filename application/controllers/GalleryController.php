<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2015 All Rights Reserved
 *
 * Gallery controller
 *
 * @file            GalleryController.php
 * @category        Application
 * @author          Elenko Ivanov
 *
 */
Class GalleryController extends Controller {

    // Database object
    public $db = null;
    // Config array
    public $config = null;
    // Memcache object
    public $memcache = null;
    // Smarty object
    private $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;
	
	// Flashdata
	private $flashdata = null;

    // If actions is not found
    public function _call() {
	
		// Load needed classes
		Loader::loadClass('Common');
	
		// Show error page
		Common::showPage(404);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // Get variables
        $this->db 		= Registry::get('db');
        $this->config 	= Registry::get('config');
		$this->memcache = Registry::get('memcache');
        $this->smarty 	= Registry::get('smarty');
        $this->lang 	= Registry::get('lang');
        $this->cache 	= Registry::get('cache');
        $this->router 	= Registry::get("router");
		$this->lg		= Registry::get("lg");
				
        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);
		
        // Check for messages or any flashdata
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
        if (!empty($_SESSION['flashdata'])) {
			$this->flashdata = $_SESSION['flashdata'];
            unset($_SESSION['flashdata']);
        }
		
		$this->menuselect = array(
			11 => array( 'page' => 'gallery', 'title' => $this->lang['gallery']),
			24 => array( 'page' => 'archive', 'title' => $this->lang['archive']),
			28 => array( 'page' => 'cards', 'title' => $this->lang['header_cards']),
			21 => array( 'page' => 'decorations', 'title' => $this->lang['header_dekoracii']),
		);
		
		$specialCat = $this->db->fetchRow("SELECT idcategory, `title_" . $this->lg . "` as title, `slug_" . $this->lg . "` as alias FROM category WHERE idcategory=27 and `title_" . $this->lg . "` != ' '");
		$this->smarty->assign("specialCat", $specialCat);
    }
	
	// Index action
    public function indexAction() {
		
		// Load needed classes
		Loader::loadClass('Common');
		Loader::loadClass('Gallery');
		Loader::loadClass('Pager');
		
		$idmaincat = 11; // Картини
		
		// Различни секции
		if($this->router['idcat']) $idmaincat = $this->router['idcat'];
		
		// Parameters for pagination
		$pageNum 	= ($this->getParam('page')) ? $this->getParam('page') : 1;
		$url 		= $this->config['site_url'] . $this->lg . '/' . $this->menuselect[$idmaincat]['page'] . '/index/page/[page]/';
		
		$rows_per_page = 10;
		
		// Construct limit
		$limit = (($pageNum-1) * $rows_per_page);
		
		// Create pagination
		$pager = new Pager();
		$pager->replace = '[page]';
		$pager->page = $pageNum;
		$pager->rows_per_page = $rows_per_page;
		$pager->link = $url;
		
		// Взимаме id на категориите картини. Главната категория е 11, това е и лявото меню
		$categoryList = Gallery::getCategory(array('active' => 1, 'idparrent' => $idmaincat), array('by' => 'position', 'type' => 'ASC'));
		
		// Главната категория
		$ids_array[] = $idmaincat;
		
		// Взимаме само id-тата, за да ги използвам в where clause
		if($categoryList) {
			foreach($categoryList as $k=>$v){
				$ids_array[] = $v['idcategory'];
			}
		}
		
		// Генерирам Where, в която е само картини от избрани категории
		$where_clause = " and idcategory in (" . implode(',', $ids_array) . ")";
		
		// Get Gallery
		$countGallery = Gallery::countGallery(array('active' => '!4'), $where_clause);
							
		// Get pager data
		$return = $pager->paginate($countGallery);
		
		$listGallery = Gallery::getGallery(array('active' => '!4', 'title_' . $this->lg => '! '), array('by' => 'active DESC, idproduct', 'type' => 'DESC'), array('from' => $limit , 'count' => $rows_per_page), $where_clause);
		
		// Генерираме цената, ако страницата се разглежда във версия различна от BG
		if($listGallery && $this->lg != 'bg'){
			foreach($listGallery as $k=>$product){
				// Build
				$listGallery[$k] = Common::buildProduct($product);
			}
		}
		
		// Define meta tags
		$meta['title'] 			= $this->menuselect[$idmaincat]['title'];
		$meta['description'] 	= '';
		$meta['keywords'] 		= '';
		
		// Parse data to template
		$data = array(

			'page'			=> $this->menuselect[$idmaincat]['page'],
			'sectionTitle'	=> $this->menuselect[$idmaincat]['title'],
			
			'listGallery'	=> $listGallery,
			'categoryList'	=> $categoryList,
			
			'paginate'		=> $return[0],
			'maxPage'		=> $return[1],
			'currentPage'	=> $pageNum,
			'prev_link'		=> $return[2],
			'next_link'		=> $return[3],
			
			'meta'			=> $meta,
		);
		
		// Assign values and display template
		$this->smarty->assign($data);
		$this->smarty->display("gallery/browse.tpl.html");
	}
	
	// Разглеждаме картините по категории
	public function categoryAction(){
		
		// Load needed classes
		Loader::loadClass('Common');
		Loader::loadClass('Gallery');
		
		// Взимаме текущата категория
		$categoryInfo = reset(Gallery::getCategory(array('active' => '1', 
											'idcategory' => (int)$this->router['idcategory'], 
											'slug_' . $this->lg => $this->router['slug'])));
		if(!$categoryInfo){
			// Show error page
			Common::showPage(404);
			exit();
		}
		
		Loader::loadClass('Pager');
				
		$idmaincat = ($categoryInfo['idparrent']==0)? $categoryInfo['idcategory'] : $categoryInfo['idparrent'];
		
		$page =	$this->menuselect[$idmaincat]['page'];
		
		// Parameters for pagination
		$pageNum 	= ($this->getParam('page')) ? $this->getParam('page') : 1;
		$url 		= $this->config['site_url'] . $this->lg . '/' . $page . '/' . $categoryInfo['alias'] . '-'. $categoryInfo['idcategory'] . '/page/[page]/';
		
		$rows_per_page = 10;
		
		// Construct limit
		$limit = (($pageNum-1) * $rows_per_page);
		
		// Create pagination
		$pager = new Pager();
		$pager->replace = '[page]';
		$pager->page = $pageNum;
		$pager->rows_per_page = $rows_per_page;
		$pager->link = $url;
		
		// Генерирам Where, в която е само картини от избрани категории
		$where_clause = " and idcategory in (" . $categoryInfo['idcategory'] . ")";
		
		// Get Gallery
		$countGallery = Gallery::countGallery(array('active' => '!4'), $where_clause);
							
		// Get pager data
		$return = $pager->paginate($countGallery);
		
		$listGallery = Gallery::getGallery(array('active' => '!4', 'title_' . $this->lg => '! '), array('by' => 'active DESC, idproduct', 'type' => 'DESC'), array('from' => $limit , 'count' => $rows_per_page), $where_clause);
		
		// Генерираме цената, ако страницата се разглежда във версия различна от BG
		if($listGallery && $this->lg != 'bg'){
			foreach($listGallery as $k=>$product){
				// Build
				$listGallery[$k] = Common::buildProduct($product);
			}
		}
		
		// Взимаме id на категориите картини. Главната категория 
		$categoryList = Gallery::getCategory(array('active' => 1, 'idparrent' => $idmaincat), array('by' => 'position', 'type' => 'ASC'));
		
		// Define meta tags
		$meta['title'] 			= ($categoryInfo['meta_title'])? $categoryInfo['meta_title'] : $categoryInfo['title'];
		$meta['keywords'] 		= ($categoryInfo['meta_keywords'])? $categoryInfo['meta_keywords'] : $categoryInfo['title'];
		$meta['description']	= ($categoryInfo['meta_description'])? $categoryInfo['meta_description'] : Common::str_cut($categoryInfo['content'], 300);
		
		// Parse data to template
		$data = array(
	
			'page'			=> $this->menuselect[$idmaincat]['page'],
			'sectionTitle'	=> $this->menuselect[$idmaincat]['title'],
			'idmaincat'		=> $idmaincat,
			
			
			'listGallery'	=> $listGallery,
			'categoryList'	=> $categoryList,
			
			'categoryInfo'  => $categoryInfo,
			'idcategory'	=> $categoryInfo['idcategory'], 
			
			'paginate'		=> $return[0],
			'maxPage'		=> $return[1],
			'currentPage'	=> $pageNum,
			'prev_link'		=> $return[2],
			'next_link'		=> $return[3],
			
			'meta'			=> $meta,
		);
		
		// Assign values and display template
		$this->smarty->assign($data);
		$this->smarty->display("gallery/browse.tpl.html");
	}
	
	// View products
    public function viewAction() {
	
		// Load needed classes
		Loader::loadClass('Common');
		Loader::loadClass('Gallery');
		
		// Get products
		$productInfo = reset(Gallery::getGallery(array('active' => '!4', 'idproduct' => (int)$this->router['idproduct'], 'alias_' . $this->lg => $this->router['slug'] )));	
		
		if(!$productInfo){
			// Show error page
			Common::showPage(404);
			exit();
		}
		
		// Build product data
		$productInfo = Common::buildProduct($productInfo, true);
				
		// Взимаме текущата категория
		$categoryInfo = reset(Gallery::getCategory(array('active' => '1', 'idcategory' => $productInfo['idcategory'])));
		
		$idmaincat = ($categoryInfo['idparrent']==0)? $categoryInfo['idcategory'] : $categoryInfo['idparrent'];
				
		// Взимаме id на категориите картини. Главната категория е 11, това е и лявото меню
		$categoryList = Gallery::getCategory(array('active' => 1, 'idparrent' => $idmaincat), array('by' => 'position', 'type' => 'ASC'));
		
		// Подобни продукти от тази категория, на случаен принцип
		$sameProduct = $this->db->fetchAll("SELECT `idproduct`, `title_" . $this->lg . "` as title, `alias_" . $this->lg . "` as alias, 
													`content_" . $this->lg . "` as content, `picture`, price_1
												FROM `products` 
												WHERE active !=4 and idcategory=" . $productInfo['idcategory'] . " and idproduct != " . $productInfo['idproduct'] . "
												ORDER BY RAND() LIMIT 3");
		if($sameProduct) {
			foreach($sameProduct as $k=>$v){
				$sameProduct[$k] = Common::buildProduct($v);
			}
		}
				
		// Define meta tags
		$meta['title'] 			= ($productInfo['meta_title'])? $productInfo['meta_title'] : $productInfo['title'];
		$meta['keywords'] 		= ($productInfo['meta_keywords'])? $productInfo['meta_keywords'] : $productInfo['title'] . ',' . $categoryInfo['title'];
		$meta['description']	= ($productInfo['meta_description'])? $productInfo['meta_description'] : Common::str_cut($productInfo['content'], 300);
		
		$meta['title'] .= ' | ' . $categoryInfo['title'];
		
		// Facebook information
		$facebook['name'] = $meta['title'];
		$facebook['link'] = $productInfo['alias'] . '-' . $productInfo['idproduct'];
		$facebook['picture'] = 'big/' . $productInfo['picture'];
				
		// Parse data to template
		$data = array(
			'page'			=> $this->menuselect[$idmaincat]['page'],
			'sectionTitle'	=> $this->menuselect[$idmaincat]['title'],
			'idmaincat'		=> $idmaincat,
			
			'productInfo'	=> $productInfo,
			'categoryList'	=> $categoryList,
			'sameProduct'	=> $sameProduct,
			
			'categoryInfo'  => $categoryInfo,
			'idcategory'	=> $categoryInfo['idcategory'],
			
			'facebook'		=> $facebook,
			'meta'			=> $meta,
		);
		
		// Assign values and display template
		$this->smarty->assign($data);
		$this->smarty->display("gallery/view.tpl.html");
		
	}
}

?>