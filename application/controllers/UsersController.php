<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved
 *
 * Users controller
 *
 * @file            UsersController.php
 * @category        Application
 * @author          Elenko Ivano
 * @email			elenko.ivanov@gmail.com
 *
 */
Class UsersController extends Controller {

    // Database object
    public $db = null;
    // Config array
    public $config = null;
    // Memcache object
    public $memcache = null;	
    // Smarty object
    public $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;

    // If actions is not found
    public function _call() {
	
		// Load needed classes
		Loader::loadClass('Common');
	
		// Show error page
		Common::showPage(404);
    }

    // Initialization method - executed with every controller method
    public function init() {
	
        // Get variables
        $this->db = Registry::get('db');
        $this->config = Registry::get('config');
		$this->memcache = Registry::get('memcache');
        $this->smarty = Registry::get('smarty');
        $this->lang = Registry::get('lang');
        $this->cache = Registry::get('cache');
        $this->router = Registry::get("router");

        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);
		
		// If refreshing the page is needed
		$refresh = $this->getParam("refresh");
		if(!empty($refresh)) $this->smarty->assign("refresh_page", $this->config['site_url'] . str_replace('/refresh/page', '', substr($_SERVER["REQUEST_URI"], 1)));

        // Check for messages
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
    }

    // Index action - display the index page
    public function indexAction() {

        // Redirect to index page
        System::redirect($this->config['site_url']);
    }

}

?>