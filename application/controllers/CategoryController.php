<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved
 *
 * Category controller
 *
 * @file            CategoryController.php
 * @category        Application
 * @author          Elenko Ivanov
 *
 */
Class CategoryController extends Controller {

    // Database object
    public $db = null;
    // Config array
    public $config = null;
    // Memcache object
    public $memcache = null;
    // Smarty object
    private $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;
	
	// Flashdata
	private $flashdata = null;

    // If actions is not found
    public function _call() {
	
		// Load needed classes
		Loader::loadClass('Common');
	
		// Show error page
		Common::showPage(404);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // Get variables
        $this->db 		= Registry::get('db');
        $this->config 	= Registry::get('config');
		$this->memcache = Registry::get('memcache');
        $this->smarty 	= Registry::get('smarty');
        $this->lang 	= Registry::get('lang');
        $this->cache 	= Registry::get('cache');
        $this->router 	= Registry::get("router");
		$this->lg		= Registry::get("lg");

        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);

        // Check for messages or any flashdata
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
        if (!empty($_SESSION['flashdata'])) {
			$this->flashdata = $_SESSION['flashdata'];
            unset($_SESSION['flashdata']);
        }
    }
	
    // Browse category
    public function browseAction() {
	
		// Load needed classes
		Loader::loadClass('Common');
		Loader::loadClass('Category');
		Loader::loadClass('Products');
		Loader::loadClass('Pager');
	
		// Get category name
		$categorySlug = $this->router['slug'];
		$icat 		  = $this->router['idcategory'];
		// $idbrand	  = $this->router['idbrand'];	
		
		if($_GET){
			$idsubCategory = (int)$_GET['category'];
			$idbrand 	   = (int)$_GET['manufacturer'];
			$priceFilter   = strip_tags($_GET['price']);
		}
				
		// Взимаме информацията за категория (главна или не), според урл-а
		$categoryInfo = reset(Category::getCategory( array('slug_'. $this->lg => $categorySlug, 'idcategory' =>$icat, 'active' => 1)));
		
		// If no category
		if(empty($categoryInfo)){
					
			// Show error page
			Common::showPage(404);
		}
		
		// Сортиране на резултатите
		if ( strtolower($_SERVER['REQUEST_METHOD']) == 'post' ){
			$limitShow  = (int)$_POST['pageView'];
			$orderPrice = (int)$_POST['productList'];
		}
		
		// Condition array()
		$whereCondition = array('p.active' => '!4');
		
		// Само продуктите от определена марка
		if($idbrand){
			$whereCondition['p.idbrand'] = $idbrand;
		}
		
		// Само продуктите в диапазон от до
		if($priceFilter){
			$priceInfo = explode("-", $priceFilter);
			if($priceInfo){
				$priceInfo[0] = (int)$priceInfo[0];
				$priceInfo[1] = (int)$priceInfo[1];
				
				// Трансформираме цената в евро, ако не е, за да е правилно търсенето
				if($_SESSION['currency'] == 'leva'){ 
					$peuro_1 = round($priceInfo[0]/1.9558);
					$peuro_2 = round($priceInfo[1]/1.9558);
				}
				else {
					// Ако е цената в евро, не се трансофмира цената
					$peuro_1 = $priceInfo[0];
					$peuro_2 = $priceInfo[1];
				}
				
				$whereCondition['p.price_1'] = 'BETWEEN ' . $peuro_1 . ' AND ' . $peuro_2; 
				$priceInfo['url'] = $priceInfo[0] . '-' . $priceInfo[1];
				
				// Добавяме 00
				$priceInfo['value_1'] = Common::roundPreDi($priceInfo[0]);
				$priceInfo['value_2'] = Common::roundPreDi($priceInfo[1]);
			}
		}
				
		// Check if this category is main
		if($categoryInfo['idparrent'] == 0 && !$idsubCategory){
			
			// Get sub category
			$ids = Category::getCategoryIds($categoryInfo['idcategory']);
			
			// Id main category
			$idMainCategory = $categoryInfo['idcategory'];
			
// TODO когато има избран бранд, да листва категориите правилно, в момента показва всички категории
			
			// Взимаме под категориите
			$listCategoryMenu = Category::getCategory( array('idparrent' => $categoryInfo['idcategory'], 'active' => 1), array('by' => 'position', 'type' => 'ASC'));
		}
		
		// Влиза тук, когато главния урл е на главна категория, но има избрана под категория
		else if($idsubCategory){
			//$subCategoryInfo = reset(Category::getCategory( array('idparrent' => $categoryInfo['idcategory'], 'idcategory' =>$idsubCategory, 'active' => 1)));
			$subCategoryInfo = reset(Category::getCategory( array( 'idcategory' =>$idsubCategory, 'active' => 1)));
			
			// Опит за хак, да се достигне суб категория, но не от тази главна категория
			if(empty($subCategoryInfo)){
				// Show error page
				Common::showPage(404);
			}
			
			// Get sub category
			$ids_sub = Category::getCategoryIds($subCategoryInfo['idcategory']);
			
			// Взимаме под категориите
			$listCategoryMenu = Category::getCategory( array('idparrent' => $subCategoryInfo['idcategory'], 'active' => 1), array('by' => 'position', 'type' => 'ASC'));
		}
		
		// Влиза тук, когато урл-а е директно на под категория
		else {
			
			// Get main category info
			$categoryParrent = reset(Category::getCategory( array('idcategory' => $categoryInfo['idparrent'], 'active' => 1)));
			
			// Id main category
			$idMainCategory = $categoryParrent['idcategory'];
					
			// Get sub category
			$ids_sub_direct = Category::getCategoryIds($categoryInfo['idcategory']);
		}
		
		// Добавяне избраната категория към id-tata
		if($ids) $ids .= ',' . $categoryInfo['idcategory'];
		else if($ids_sub_direct) $ids = $ids_sub_direct . ',' . $categoryInfo['idcategory'];
		else if($ids_sub) $ids = $ids_sub . ',' . $subCategoryInfo['idcategory'];
		else if ($idsubCategory) $ids = $idsubCategory;
		else $ids = $categoryInfo['idcategory'];


		// Parameters for pagination
		$pageNum 	= ($this->router['page_num']) ? $this->router['page_num'] : 1;
		$url 		= $this->config['site_url'] . $this->lg . '/' . $categoryInfo['slug'] . '-' . $categoryInfo['idcategory'] . '/page/[page]?category=' . $idsubCategory . '&manufacturer=' . $idbrand . '&price=' . $priceFilter;
		
		// Rows per page
		Common::numShowProd('category', $limitShow);
		$rows_per_page = $_SESSION['category']['show'];
		
		// Order by price
		Common::numPriceProd('category', $orderPrice);
		
		// Construct limit
		$limit = (($pageNum-1) * $rows_per_page);
		
		// Create pagination
		$pager = new Pager();
		$pager->replace = '[page]';
		$pager->page = $pageNum;
		$pager->rows_per_page = $rows_per_page;
		$pager->link = $url;
		
		// Count all products
		$count = Products::countProducts($whereCondition, " AND pc.idcategory in (" . $ids . ")");
				
		// Get pager data
		$return = $pager->paginate($count);
		
		// Define order array
		if($_SESSION['category']['price']){
			$conditionOrder = array('by' => 'p.status, p.price_1', 'type' => $_SESSION['category']['price']);
			$priceOrder = $_SESSION['category']['price'];
		}
		else {
			$conditionOrder = array('by' => 'p.status, p.idbrand, p.price_1', 'type' => 'ASC');
		}
			
		// Get all ids products from category
		$idsProduct = Products::getProductsIds($whereCondition, " AND pc.idcategory in (" . $ids . ")");
		
		if($idsProduct){
		
			// Get products
			$listProducts = Products::getProducts($whereCondition, $conditionOrder , array('from' => $limit , 'count' => $rows_per_page), " AND p.idproduct in (" . $idsProduct . ")");
			
			// Взимаме максималната цена, за да генерираме филтъра по цена
			$maxAmount = Products::getMaxPriceProducts($whereCondition, " AND p.idproduct in (" . $idsProduct . ")");
			
			// Трансформираме цената в лв.
			$maxAmount = Common::calculatePrice($maxAmount);
			
			// Взимаме минималната цена, за да генерираме филтъра по цена
			$minAmount = Products::getMinPriceProducts($whereCondition, " AND p.idproduct in (" . $idsProduct . ")");
			
			// Трансформираме цената в лв.
			$minAmount = Common::calculatePrice($minAmount);
			
			// Biuld product info
			if($listProducts){
				foreach($listProducts as $k=>$product){
					
					// Build
					$listProducts[$k] = Common::buildProduct($product);
				}
			}
			
			if(!$idbrand){
				// List brads and count products
				$listAllBrands = Category::getBrands(array('active' => 1), array('by' => 'position', 'type' => 'ASC'));
				
				if($listAllBrands){
					foreach($listAllBrands as $k=>$v){
						$cproduct  = Products::countProductsBrand(array('p.active' => '!4', 'p.idbrand' => $v['idbrand']), " AND p.idproduct in (" . $idsProduct . ")");
						if($cproduct == 0) continue;
						
						$listBrands[$k] = $v;
						$listBrands[$k]['countProd'] = $cproduct;
					}
				}
			}
			else {
				$brandInfo = $this->db->fetchRow("SELECT `title_" . $this->lg . "` as title, `slug_" . $this->lg . "` as slug FROM brands WHERE idbrand = " . $idbrand);
			}
		}
		
		// Define meta tags
		$meta['title'] 		= ($categoryInfo['meta_title'])? $categoryInfo['meta_title'] : $categoryInfo['title'];
		$meta['description']= $categoryInfo['meta_description'];
		$meta['keywords'] 	= ($categoryInfo['meta_keywords'])? $categoryInfo['meta_keywords'] : $categoryInfo['title'];		
		
		// Parse data to template
		$data = array(
			'data'				=> $data,
			'categoryInfo'  	=> $categoryInfo,
			'categoryParrent'	=> $categoryParrent,
			'listProducts'		=> $listProducts,
			'paginate'			=> $return[0],
			'idMainCategory'	=> $idMainCategory,
			'rows_per_page'		=> $rows_per_page,
			
			'pageView'			=> $_SESSION['category']['show'],
			'productList'		=> $_SESSION['category']['priceOrder'],
			'filterUrlPrice'	=> $this->config['site_url'] . $this->lg . '/' . $categoryInfo['slug'] . '-' . $categoryInfo['idcategory'] . '/page/1?category=' . $subCategoryInfo['idcategory'] . '&manufacturer=' . $idbrand,
			'filterUrl'			=> $this->lg . '/' . $categoryInfo['slug'] . '-' . $categoryInfo['idcategory'] . '/page/1?category=' . $subCategoryInfo['idcategory'] . '&manufacturer=' . $idbrand . '&price=' . $priceInfo['url'],

			'maxAmount'			=> ($maxAmount)? $maxAmount : 0,
			'minAmount'			=> ($minAmount)? $minAmount : 0,
			'subCategoryInfo'	=> $subCategoryInfo,
			'idbrand'			=> $idbrand,
			'brandInfo'			=> $brandInfo,
			'priceInfo'			=> $priceInfo,
			
			'listBrands'		=> $listBrands,
			'listCategoryMenu'	=> $listCategoryMenu,
			'priceOrder'		=> $priceOrder,
			'total_results'		=> $count,
			'num_page_count'	=> $return[1],
			'sel_page'			=> $pageNum,
			'meta'				=> $meta,
			'remarketing'		=> 1,
		);
		
		// Assign values and display template
		$this->smarty->assign('page', 'category');
		$this->smarty->assign($data);
		$this->smarty->display("products/category.tpl.html");
    }	
}

?>