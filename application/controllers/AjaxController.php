<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved
 *
 * Ajax controller
 *
 * @file            AjaxController.php
 * @category        Application
 * @author          Elenko Ivanov
 * @email			elenko.ivanov@gmail.com
 *
 */
 
Class AjaxController extends Controller {

    // Database object
    public $db = null;
    // Config array
    public $config = null;
    // Memcache object
    public $memcache = null;
    // Smarty object
    public $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;
	
	// Flashdata
	private $flashdata = null;

    // If actions is not found
    public function _call() {
	
		// Load needed classes
		Loader::loadClass('Common');
	
		// Show error page
		Common::showPage(404);
    }

    // Initialization method - executed with every controller method
    public function init() {
	
        // Get variables
        $this->db = Registry::get('db');
        $this->config = Registry::get('config');
		$this->memcache = Registry::get('memcache');
        $this->smarty = Registry::get('smarty');
        $this->lang = Registry::get('lang');
        $this->cache = Registry::get('cache');
        $this->router = Registry::get("router");
		
        if (!empty($_SESSION['flashdata'])) {
			$this->flashdata = $_SESSION['flashdata'];
            unset($_SESSION['flashdata']);
        }
    }
	
	// Change currency
	public function changeCurrencyAction(){
		
		$currency = $_POST['currency'];
		
		if(!$this->config['currency'][$currency])
			$_SESSION['currency'] = 'eur';
		else
			$_SESSION['currency'] = $currency;
		
		exit(json_encode(array('ok' => 1)));
	}
	
	// Изчисляваме цената на модела
	public function modelPriceAction(){
		
		Loader::loadClass('Common');
		
		$idmodel 	= (int)$_POST['idmodel'];
		$idproduct 	= (int)$_POST['idproduct'];
		
		$priceInfo = $this->db->fetchRow("SELECT price, active FROM products_models WHERE idmodel= " . $idmodel . " and idproduct=" . $idproduct ." and active !=4 LIMIT 1");
		
		if($priceInfo) {
			
			if($priceInfo['active'] == 2){
				$nalichnost = '<b style="color:green;">В наличност</b>';
			}
			else if ($priceInfo['active'] == 3 || $priceInfo['active'] == 1)
				$nalichnost = '<b style="color:red;">По заявка</b>';
			
			$res = array(
				'priceModel' => Common::roundPreDi($priceInfo['price']) . " ",
				'nalichnost' => $nalichnost,
				'active' 	 => $priceInfo['active'],
				);
			
			exit(json_encode($res));
		}
		else {
			header('HTTP/1.1 406 Not Acceptable');
			exit('Price is not available!');
		}		
	}	
	
	// Изчисляваме доставката
	public function deliveryPriceAction(){
			
		// Взимаме параметрите от POST
		$idpost 	= (int)$_POST['id_post_code']; // Пощенски код
		$idcountry 	= strip_tags(trim($_POST['id_country'])); // Кода на държавата
		$iddelivery = (int)$_POST['iddelivery']; // Кода на избраната компания
		
		$result = array();
		
		// Зареждаме необходимите класове
		Loader::loadClass('Cart');
		Loader::loadClass('Common');
		
		// Взимаме параметрите на държавата според кода
		$infoCountry = $this->db->fetchRow("SELECT * FROM `country` WHERE code = '" . $idcountry . "'");
		
		// Изчисляваме теглото
		$cartInfo = reset(Cart::getCartInfo(array( 'phpsession' => session_id(),  'status' => 1 )));
		
		if($cartInfo){
			
			$productInCart = Cart::listProductCart($cartInfo['id_carts']);
			
			if($productInCart){
				
				$totalPrice = 0;
				$totalTeglo = 0;
				foreach($productInCart as $k=>$v){
					
					// Конвертиране на валутата
					$price = Common::calculatePrice($v['price_1']); // Main price
					
					$col_price = $v['col']*$price;
					$col_teglo  = $v['col']*$v['teglo'];
					
					// Calculate max price
					$totalPrice += $col_price;
					$totalTeglo += $col_teglo;
				}
			}
					
			// На всички регистрирани получават % отстъпка, който е дефинирана константа в config.php
			if($percentPromo = $this->config['discounts'][$_SESSION['user']['discount']]){
				$totalPricePercent = $totalPrice; 
				$totalPrice = $totalPrice - ($totalPrice*($percentPromo/100));
			}
			
			$totalPrice = round($totalPrice, 2);
			
		}
		
		// Общото тегло
		$result['totalTeglo'] = $totalTeglo;
		
		// Калкулираме доставката според компанията 
		if($iddelivery == 5){
			// Цената се връща е евро!
			$priceDelivery  = Cart::priceRegularPost($totalTeglo, $infoCountry['eu'], $infoCountry['zone']);
			
			// Трансформираме цената, защото се връща в евро
			$result['priceDelivery'] = Common::calculatePrice($priceDelivery);
		}
		else if ($iddellivery == 2) {
			
			
		}
		
		// Записваме в таблица carts цената за доставка и коя компания избира клиента
		if($result['priceDelivery']){
			
			// Добавяне данните в таблицата с поръчката
			$dbUpdate['delivery_pay'] 		= $priceDelivery;
			$dbUpdate['delivery_method'] 	= $iddelivery;
			
			$this->db->update('carts', $dbUpdate, ' id_carts = ' . $cartInfo['id_carts']);
			
			// Генерирарме данните за масива
			$result['totalPrice']     = $totalPrice + $result['priceDelivery'];
			$result['currency']		  = $this->config['currency'][$_SESSION['currency']];
			
			// Форматираме правилно цената
			$result['priceDelivery']  = Common::roundPreDi($result['priceDelivery']);
			$result['totalPrice']	  = Common::roundPreDi($result['totalPrice']);
		}
		
		exit(json_encode($result));
	}
	
	// Add to cart
	public function addToCartAction(){
		
		// Get Mode
		$mode = $_POST['mode'];
		
		// Load classes
		Loader::loadClass('Cart');
		Loader::loadClass('Common');
		
		if($mode =='addToCart'){
			
			$data['id_product']  = (int)$_POST['idp'];
						
			// Взимаме информацията за продукта
			$pInfo = $this->db->fetchRow("SELECT idproduct, price_1 FROM products WHERE idproduct=" . $data['id_product'] . " and active!=4");
			
			$data['price'] 	= $pInfo['price_1'];
			
			// Create new cart
			$idcart = Cart::createCart(session_id());
			
			// Add product to cart
			Cart::insertProdInCart($idcart, $data);

			// Get cart information
			$cartInfo = Cart::getColPrice(session_id());
			
			// Set response
			$result['col'] 		= $cartInfo['col'];
			$result['price'] 	= $cartInfo['price'];
			
			// Return response
			exit(json_encode($result));
		}
		
	}
	
	// Remove from cart 
	public function removeFromCartAction(){
	
		// Load classes
		Loader::loadClass('Cart');
		Loader::loadClass('Common');
		
		$idcart = (int)$_POST['idcart'];
		
		$cardInfoId = $this->db->fetchOne("SELECT `id_carts` FROM `carts` WHERE `phpsession`='". session_id() ."' and `status`=1 LIMIT 1");
		
		if(!$cardInfoId){
		
			// Set content-type
			header('HTTP/1.1 406 Not Acceptable');
			exit('This product can not remove!');
		}
		
		Cart::deleteProdFromCart($cardInfoId, $idcart);
		
		// Get cart information
		$cartInfo = Cart::getColPrice(session_id());
		
		$totalPricePercent = 0;
		
		// На всички регистрирани получават % отстъпка, който е дефинирана константа в config.php
		if($percentPromo = $this->config['discounts'][$_SESSION['user']['discount']]){
			$totalPricePercent = $cartInfo['price']; 
			$cartInfo['price'] = $cartInfo['price'] - ($cartInfo['price']*($percentPromo/100));
		}
		
		// Set response
		$result['col'] 		= $cartInfo['col'];
		$result['price'] 	= Common::roundPreDi($cartInfo['price']);
		$result['totalPricePercent'] = Common::roundPreDi(round($totalPricePercent, 2));
		
		// Return response
		exit(json_encode($result));
		
	}
	
	// Update col in cart
	public function updateProductCartAction(){
		
		// Load classes
		Loader::loadClass('Cart');
		Loader::loadClass('Common');
		
		$idorder = (int)$_POST['idop'];
		$pcount	 = (int)$_POST['count'];
		
		if($pcount <1) $pcount = 1;
		
		// Update cart
		$this->db->query("UPDATE `carts_products` SET `col`='" . $pcount . "' WHERE id=" . $idorder . " LIMIT 1");
		
		// Get cart information
		$cartInfo = Cart::getColPrice(session_id());
		
		// Get updated product
		$prodUpdate = $this->db->fetchRow("SELECT cp.*, p.idproduct, p.active
										FROM `carts_products` as cp
										LEFT JOIN `products` p ON p.idproduct=cp.id_product
										WHERE cp.`id`=" . $idorder);
		$price = $prodUpdate['price'];
		
		$prodPrice 	= $prodUpdate['col']*$price;
		
		$totalPricePercent = 0;
		
		// Check if this cart have promo code
		/*
		$promo = $this->db->fetchRow('SELECT pc.* FROM `carts_products` as cp
										LEFT JOIN `carts` c ON c.id_carts = cp.id_cart
										LEFT JOIN `promoCode` pc ON pc.id = c.idpromo
										WHERE cp.`id`=' . $idorder);
		
		if($promo){
			$totalPricePercent = $cartInfo['price']; 
			$cartInfo['price'] = $cartInfo['price'] - ($cartInfo['price']*($promo['percent']/100));
			$result['promoInfo'] = $promo;
		
		}
		
		
		// На всички регистрирани получават % отстъпка, който е дефинирана константа в config.php
		if($percentPromo = $this->config['discounts'][$_SESSION['user']['discount']]){
			$totalPricePercent = $cartInfo['price']; 
			$cartInfo['price'] = $cartInfo['price'] - ($cartInfo['price']*($percentPromo/100));
		}
		*/
		
		// Set response
		$result['col'] 					= $cartInfo['col'];
		$result['price'] 				= Common::roundPreDi(round($cartInfo['price'], 2));
		$result['prodPrice'] 			= Common::roundPreDi($prodPrice);
		//$result['totalPricePercent'] 	= Common::roundPreDi(round($totalPricePercent, 2));
		
		// Return response
		exit(json_encode($result));
	}
	
	// Зареждаме параметрите ако има такива
	public function loadVariantiAction(){
	
		$idproduct  = (int)$_POST['idproduct'];
		$parameters = $_POST['parameters'];
		
		Loader::loadClass('Common');
		
		if(count($_POST['parameters']) > 1) {
			$parameters = $_POST['parameters'];
			
			// Махаме елементите без стойност
			foreach($parameters as $k=>$v) {
				if(!$v) unset($parameters[$k]);
			}

			$variant_code = implode(",", $parameters);
		}
		
		if($variant_code){
			// Проверяваме дали за тази комбинация от кодове има вариант и дали самата секция е във варианти добавена
			$result = $this->db->fetchRow("SELECT * FROM `products_price` WHERE idproduct=" . $idproduct . " and variant_code='" . $variant_code . "'");
			if($result){
				$price_1 = Common::calculatePrice($result['price_1']);
				$price_1 = Common::roundPreDi($price_1);
				
				$result['price_value'] = $this->config['currency'][$_SESSION['currency']] . "" . $price_1;
				
				if($result['price_2']){
					$price_2 = Common::calculatePrice($result['price_2']);
					$price_2 = Common::roundPreDi($price_2);			
					$result['price_promo'] = $this->config['currency'][$_SESSION['currency']] . "" . $price_2;
				}
			}
			else {
				header('HTTP/1.1 406 Not Acceptable');
				exit('End page');
			}
		}
		else {
			header('HTTP/1.1 406 Not Acceptable');
			exit('End page');
		}
				
		// Return response
		exit(json_encode($result));
		
		return;
		
		$idproduct  = (int)$_POST['idproduct'];
		$idsize 	= (int)$_POST['idsize'];
		$idcolor	= (int)$_POST['idcolor'];
		
		Loader::loadClass('Common');
		
		if($idsize >0){
			$result = $this->db->fetchRow("SELECT `code`, `price` FROM `products_price` WHERE idproduct = " . $idproduct . " and idsize=" . $idsize);
			$result['price_value'] = $this->config['currency'][$_SESSION['currency']] . "" . Common::calculatePrice($result['price']);
		}
		else if($idcolor>0){
			$result = $this->db->fetchRow("SELECT `code`, `price` FROM `products_price` WHERE idproduct = " . $idproduct . " and idcolor=" . $idcolor);
			$result['price_value'] = $this->config['currency'][$_SESSION['currency']] . "" . Common::calculatePrice($result['price']);
		}
		else {
			header('HTTP/1.1 406 Not Acceptable');
			exit('End page');
		}
	
		// Return response
		exit(json_encode($result));
	}
	
	
	// Add promo code
	public function updatePromoCodeAction(){
		
		// Load classes
		Loader::loadClass('Cart');
			
		$promoCode = $_POST['promoCode'];
		
		// Set content-type
		if(!$promoCode){
			$this->db->update('carts', array('idpromo' => 0), " phpsession = '" .  session_id() . "'");
			
			// Get cart information
			$cartInfo = Cart::getColPrice(session_id());
			
			// Set response
			$result['col'] 			= $cartInfo['col'];
			$result['price'] 		= $cartInfo['price'];
			$result['prodPrice'] 	= ''; //$prodPrice;
			
			// Return response
			exit(json_encode($result));
		}
		
		$promo = $this->db->fetchRow("SELECT * FROM `promoCode` WHERE `code`='" . strip_tags(trim($promoCode)) . "'");
		
		if(!$promo){
			$this->db->update('carts', array('idpromo' => 0), " phpsession = '" .  session_id() . "'");
			
			// Get cart information
			$cartInfo = Cart::getColPrice(session_id());
			
			// Set response
			$result['col'] 			= $cartInfo['col'];
			$result['price'] 		= $cartInfo['price'];
			$result['prodPrice'] 	= ''; //$prodPrice;
			
			// Return response
			exit(json_encode($result));
		}
		
		$this->db->update('carts', array('idpromo' => $promo['id']), " phpsession = '" .  session_id() . "'");
		
		// Get cart information
		$cartInfo = Cart::getColPrice(session_id());
		
		$totalPricePercent = $cartInfo['price']; 
		$cartInfo['price'] = $cartInfo['price'] - ($cartInfo['price']*($promo['percent']/100));
		$result['promoInfo'] = $promo;
		
		// Set response
		$result['col'] 			= $cartInfo['col'];
		$result['price'] 		= $cartInfo['price'];
		$result['prodPrice'] 	= ''; //$prodPrice;
		$result['totalPricePercent'] = $totalPricePercent;
		
		// Return response
		exit(json_encode($result));
	}
	
	// Vote up
	public function voteUpAction(){
		
		// If user is not logged in
		if(empty($_SESSION['user'])){

			// Set content-type
			header('HTTP/1.1 406 Not Acceptable');
			exit('You must be logged in.');
		}
		
		// Return result
		exit(json_encode($result));
	}
}

?>