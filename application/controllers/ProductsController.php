<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved
 *
 * Products controller
 *
 * @file            ProductsController.php
 * @products        Application
 * @author          Elenko Ivanov
 *
 */
Class ProductsController extends Controller {

    // Database object
    public $db = null;
    // Config array
    public $config = null;
    // Memcache object
    public $memcache = null;
    // Smarty object
    private $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;
	
	// Flashdata
	private $flashdata = null;

    // If actions is not found
    public function _call() {
	
		// Load needed classes
		Loader::loadClass('Common');
	
		// Show error page
		Common::showPage(404);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // Get variables
        $this->db = Registry::get('db');
        $this->config = Registry::get('config');
		$this->memcache = Registry::get('memcache');
        $this->smarty = Registry::get('smarty');
        $this->lang = Registry::get('lang');
        $this->cache = Registry::get('cache');
        $this->router = Registry::get("router");
		$this->lg = Registry::get("lg");

        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);

        // Check for messages or any flashdata
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
        if (!empty($_SESSION['flashdata'])) {
			$this->flashdata = $_SESSION['flashdata'];
            unset($_SESSION['flashdata']);
        }
    }
	
		
	// View products
    public function viewAction() {
	
		// Load needed classes
		Loader::loadClass('Common');
		Loader::loadClass('Filter');
		Loader::loadClass('Products');
		Loader::loadClass('Category');
		
		// Get products
		$productInfo = reset(Products::getProducts(array('p.active' => '!4', 'p.idproduct' => (int)$this->router['idproduct'], 'p.alias_' . $this->lg => $this->router['slug'] )));	
		
		if(!$productInfo){
			// Show error page
			Common::showPage(404);
			exit();
		}
		
		// If posting
		if(strtolower($_SERVER['REQUEST_METHOD']) == 'post'){
		
			// Get post data
			$post_data = array(
				'name'			=> Filter::sanitize($_POST['name']),
				'email'			=> $_POST['email'],
				'phone'			=> Filter::sanitize($_POST['phone']),
				'message'		=> Filter::sanitize($_POST['message']),
			);
			
			/*
			if($_SESSION['security_code'] != $_POST['security_code']) {
				$error = "Security code is invalid!";
			} */
						
			// Check name
			if(empty($post_data['name'])){
				$error = "Не е попълнено коректно полето ИМЕ!";
			}	
			
			// Check email
			else if(empty($post_data['email'])){
				$error = 'Email адреса е невалиден!';
			} else if(!Filter::checkEmail($post_data['email'])){
				$error = 'Email адреса е невалиден!';
			}
							
			// Check message
			else if(empty($post_data['message'])){
				$error = 'Съобщението не може да бъде празно!';
			}
			
			// If no errors
			if(empty($error)){
						
				$msg  = 'Име: ' . $post_data['name'] . '<br />';
				$msg .= 'Email: ' . $post_data['email'] . '<br />';
				$msg .= 'Телефон: ' . $post_data['phone'] . '<br />';
				$msg .= 'Запитване: ' . $post_data['message'] . '<br />';
				$msg .= 'Продукт: ' . $productInfo['title'] . ' - <a href="' . $this->config['site_url'] . $this->lg . '/' . $productInfo['alias'] . '-' . $productInfo['idproduct'] . '">виж</a><br />';
				
				// Send to Mitev
				System::sendEmail(
					array($this->config['site_email'] => $this->config['site_name']), 
					'shop@vento-k.com', 
					'[vento-k.com] Новa заявка!', 
					$msg
				);
				
				// Send to Elenko
				System::sendEmail(
					array($this->config['site_email'] => $this->config['site_name']), 
					'elenko.ivanov@gmail.com', 
					'[vento-k.com] Новa заявка!', 
					$msg
				);
				
				// Assign success message
				$this->smarty->assign('success', 'Заявката е направена успешно!');
			}
			
			// If we have an error
			else {
			
				// Assign error message and posted data
				$this->smarty->assign('error', $error);
				$this->smarty->assign('mark', $mark);
			}
		}
			
		// Взимаме всички категории, ако ги няма в сесия
		if(!$_SESSION['rightMenu']){
			$listCategoryM = Category::getCategory(array('status' => '1', 'idparrent' => 0), array('by' => 'position', 'type' => 'ASC'));
			
			foreach($listCategoryM as $k=>$v) {
				$listCategoryMain[$v['idcategory']] = $v;
				$listCategoryMainSub = Category::getCategory(array('status' => '1', 'idparrent' => $v['idcategory']), array('by' => 'position', 'type' => 'ASC'));
				
				if($listCategoryMainSub) {
					
					foreach($listCategoryMainSub as $kk=>$vv) {
							
						$listCategoryMain[$v['idcategory']]['subCat'][$vv['idcategory']] = $vv;
						
						$listCategoryMainSubSub = Category::getCategory(array('status' => '1', 'idparrent' => $vv['idcategory']), array('by' => 'position', 'type' => 'ASC'));
						if($listCategoryMainSubSub){
							foreach($listCategoryMainSubSub as $kkk=>$vvv){
								$listCategoryMain[$v['idcategory']]['subCat'][$vv['idcategory']]['sub'][$vvv['idcategory']] = $vvv;
							}
						}				
					}
				}
			}

			$_SESSION['rightMenu'] = $listCategoryMain;
		}
		else {
			$listCategoryMain = $_SESSION['rightMenu'];
		}
		
		// Намираме дървото на категорията
		$categoryInfo = reset(Category::getCategory(array('status' => '1', 'idcategory' => $productInfo['idcategory'])));
		
		if($categoryInfo['idparrent'] == 0){
			$idmaincategory = $productInfo['idcategory'];	
		}
		else {
			$categoryInfoSub = reset(Category::getCategory(array('status' => '1', 'idcategory' => $categoryInfo['idparrent'])));
						
			if($categoryInfoSub['idparrent'] == 0){
				$idmaincategory = $categoryInfoSub['idcategory'];	
				$idsubcategory  = $categoryInfo['idcategory'];
			}
			else {
				
				$idmaincategory = $categoryInfoSub['idparrent'];
				$idsubcategory  = $categoryInfoSub['idcategory'];				
			}	
		}
		
		$idcategory = $productInfo['idcategory']; 
				
		// Build product data
		$productInfo = Common::buildProduct($productInfo, true);
			
		// Define meta tags
		$meta['title'] 			= ($productInfo['meta_title'])? $productInfo['meta_title'] : $productInfo['title'];
		$meta['keywords'] 		= ($productInfo['meta_keywords'])? $productInfo['meta_keywords'] : $productInfo['title'] . ',' . $categoryInfo['title'];
		$meta['description']	= ($productInfo['meta_description'])? $productInfo['meta_description'] : Common::str_cut($productInfo['short_info'], 300);
		
		$meta['title'] .= ' | ' . $categoryInfo['title'];
		
		// Facebook information
		$facebook['name'] = $meta['title'];
		$facebook['link'] = $productInfo['alias'] . '-' . $productInfo['idproduct'];
		$facebook['picture'] = 'big/' . $productInfo['picture'];
		
		// Parse data to template
		$data = array(
			'page'					=> 'products',
			'title'					=> $productInfo['title'],
			'productInfo'			=> $productInfo,
			
			'listCategoryMain'		=> $listCategoryMain,
			
			
			'idmaincategory' 		=> $idmaincategory,
			'idsubcategory'			=> $idsubcategory,
			'idcategory'			=> $categoryInfo['idcategory'],		
			
			'facebook'			=> $facebook,
			'meta'				=> $meta,
		);
		
		// Assign values and display template
		$this->smarty->assign($data);
		$this->smarty->display("products/view.tpl.html");
    }
	
	// All products
    public function indexAction() {
	
		// Load needed classes
		Loader::loadClass('Common');
		Loader::loadClass('Category');
		Loader::loadClass('Products');
			
		// Взимаме всички категории
		if(!$_SESSION['rightMenu']){
			$listCategoryM = Category::getCategory(array('status' => '1', 'idparrent' => 0), array('by' => 'position', 'type' => 'ASC'));
			
			foreach($listCategoryM as $k=>$v) {
				$listCategoryMain[$v['idcategory']] = $v;
				$listCategoryMainSub = Category::getCategory(array('status' => '1', 'idparrent' => $v['idcategory']), array('by' => 'position', 'type' => 'ASC'));
				
				if($listCategoryMainSub) {
					
					foreach($listCategoryMainSub as $kk=>$vv) {
							
						$listCategoryMain[$v['idcategory']]['subCat'][$vv['idcategory']] = $vv;
						
						$listCategoryMainSubSub = Category::getCategory(array('status' => '1', 'idparrent' => $vv['idcategory']), array('by' => 'position', 'type' => 'ASC'));
						if($listCategoryMainSubSub){
							foreach($listCategoryMainSubSub as $kkk=>$vvv){
								$listCategoryMain[$v['idcategory']]['subCat'][$vv['idcategory']]['sub'][$vvv['idcategory']] = $vvv;
							}
						}				
					}
				}
			}

			$_SESSION['rightMenu'] = $listCategoryMain;
		}
		else {
			$listCategoryMain = $_SESSION['rightMenu'];
		}
		
		$idcategory = (int)$this->router['idcategory'];
		$slug		= $this->router['slug'];
		
		// Define meta tags
		$meta['title'] 			= 'Продукти - ';
		$meta['keywords'] 		= '';
		$meta['description']  	= '';	
		
		// Списъка от категории който се показват в центъра
		$listCategory = $listCategoryMain;
		
		//print_r($listCategoryMain); die();
		
		// Ако има избрана категория
		if($idcategory){
			
			// Взимаме информацията за текущата категория
			$currentCategoryInfo = reset(Category::getCategory(array('status' => '1', 'idcategory' => $idcategory, 'alias_' . $this->lang['lg'] => $slug)));	
			
			if($currentCategoryInfo){
					
				if($currentCategoryInfo['idparrent'] == 0) {
					// Категория ниво 1
					$idmaincategory = $idcategory;
					$listCategory   = $listCategoryMain[$idmaincategory]['subCat'];
					
					// Ако няма под категории, директо продуктите
					if(!$listCategory) $listProducts = Products::getProducts(array('idcategory' => $idcategory, 'active' => '!4'), array('by' => 'position', 'type' => 'ASC'));
				}
				else {
				
					$currentCategoryInfoSub = reset(Category::getCategory(array('status' => '1', 'idcategory' => $currentCategoryInfo['idparrent'])));
					
					if($currentCategoryInfoSub['idparrent'] == 0) {
						// Категория ниво 2
						$idmaincategory = $currentCategoryInfoSub['idcategory'];
						
						$idsubcategory  = $idcategory; 
												
						$listCategory   = $listCategoryMain[$idmaincategory]['subCat'][$idcategory]['sub'];
											
						// Ако няма под категории, директо продуктите
						if(!$listCategory) $listProducts = Products::getProducts(array('idcategory' => $idcategory, 'active' => '!4'), array('by' => 'position', 'type' => 'ASC'));
					}
					else {
						// Последна категори ниво 3
						$currentCategoryInfoSubSub = reset(Category::getCategory(array('status' => '1', 'idcategory' => $currentCategoryInfoSub['idparrent'])));
						
						$idmaincategory = $currentCategoryInfoSubSub['idcategory'];
					
						$idsubcategory  = $currentCategoryInfoSub['idcategory'];
						
						// Взимаме продуктите		
						$listProducts = Products::getProducts(array('idcategory' => $idcategory, 'active' => '!4'), array('by' => 'position', 'type' => 'ASC'));
					}
				}			
				
				// Define meta tags
				$meta['title'] 			= $currentCategoryInfo['meta_title'];
				$meta['keywords'] 		= $currentCategoryInfo['meta_keywords'];
				$meta['description']  	= $currentCategoryInfo['meta_description'];
			}
			else {
				// Show error page if url is wrong
				Common::showPage(404);
				exit();
			}
		}

		// Parse data to template
		$data = array(
			'title'				=> $this->lang['products'],
			'page'				=> 'products',
			
			'listCategoryMain'		=> $listCategoryMain,
			'listCategory'			=> $listCategory,
			
			'listProducts'			=> $listProducts,
			'currentCategoryInfo' => $currentCategoryInfo,
			
			'idcategory'		=> $idcategory,
			'idmaincategory'	=> $idmaincategory,
			'idsubcategory'		=> $idsubcategory,
			
			'meta'				=> $meta,
		);
		
			
		// Assign values and display template
		$this->smarty->assign($data);
		$this->smarty->display("products/browse.tpl.html");
    }
		
}
?>