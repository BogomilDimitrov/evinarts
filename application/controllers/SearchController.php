<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2014 All Rights Reserved
 *
 * Search controller
 *
 * @file            SearchController.php
 * @category        Application
 * @author          Elenko Ivanov
 * @email			elenko.ivanov@gmail.com
 *
 */
Class SearchController extends Controller {

    // Database object
    public $db = null;
    // Config array
    public $config = null;
    // Memcache object
    public $memcache = null;	
    // Smarty object
    private $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;
	
	// Flashdata
	private $flashdata = null;

    // If actions is not found
    public function _call() {
	
		// Load needed classes
		Loader::loadClass('Common');
	
		// Show error page
		Common::showPage(404);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // Get variables
        $this->db = Registry::get('db');
        $this->config = Registry::get('config');
		$this->memcache = Registry::get('memcache');
        $this->smarty = Registry::get('smarty');
        $this->lang = Registry::get('lang');
        $this->cache = Registry::get('cache');
        $this->router = Registry::get("router");
		$this->lg = Registry::get("lg");

        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);

        // Check for messages or any flashdata
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
        if (!empty($_SESSION['flashdata'])) {
			$this->flashdata = $_SESSION['flashdata'];
            unset($_SESSION['flashdata']);
        }
    }
	
	// Search for posts and users
	public function indexAction(){
	
		// Get search query
		$query = (!empty($_POST['search'])) ? Filter::sanitize($_POST['search']) : Filter::sanitize(urldecode($this->getParam("index")));
				
		// If search query is too short
		if(mb_strlen($query, 'UTF-8') < 2){
		
			// Dispaly error
			$error = 'Заявката за търсене е твърде кратка!';
		}
		
		// If no error
		if(empty($error)){
			
			// Load needed classes
			Loader::loadClass('Search');
			Loader::loadClass('Common');
			Loader::loadClass('Pager');
			Loader::loadClass('Products');
			
			// Parameters for pagination
			$page = ($this->getParam("page")) ? $this->getParam("page") : 1;
						
			// Rows per page
			$show = 15;
			
			// Set pagination url
			$url = $this->config['site_url'] . $this->lg . "/search/index/" . urlencode($query) . "/page/[page]/";
			
			// Construct limit
			$limit = (($page-1) * $show);
			
			$pager = new Pager();
			$pager->replace = "[page]";
			$pager->page = $page;
			$pager->rows_per_page = $show;
			$pager->link = $url;
						
			// Count matching products
			$count = count(Search::searchProduct($query, null, null, 'GROUP BY p.idproduct', $condition_where));
			
			// Get pager data
			$return = $pager->paginate($count);
			
			$conditionOrder = array('by' => 'p.idproduct', 'type' => 'DESC');
						
			// Get products
			$products = Search::searchProduct($query, $conditionOrder, array('from' => $limit , 'count' => $show), 'GROUP BY p.idproduct', $condition_where);
			
			// Build products
			foreach($products as $i => $product){
			
				// Build product
				$products[$i] 	= Common::buildProduct($product);
			}
						
			// Parse data to template
			$data = array(
				'listProducts'		=> $products,
								
				'total_results'		=> $count,
				'num_page_count'	=> $return[1],
				'sel_page'			=> $page,
				'paginate'			=> $return[0],
				'priceOrder'		=> $priceOrder,
				
			); 
		}
		
		// If error
		else {		
			// Assign error message
			$this->smarty->assign('error', $error);
		}
		
		// Define meta tags
		$meta['title'] 		= 'Търсене - ' . $query;
		$meta['keywords'] 	= '' . $query;
		
		// Parse data to template
		$data['rows_per_page']	= $show;
			
		$data['meta']			= $meta;
				
		// Assign values and display template
		$this->smarty->assign('page', 'search');
		$this->smarty->assign('query', $query);
		$this->smarty->assign($data);
		$this->smarty->display('search/search.tpl.html');
	}	
}
?>