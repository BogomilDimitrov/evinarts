<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved
 *
 * News controller
 *
 * @file            NewsController.php
 * @category        Application
 * @author          Elenko Ivanov
 *
 */
Class NewsController extends Controller {

    // Database object
    public $db = null;
    // Config array
    public $config = null;
    // Memcache object
    public $memcache = null;
    // Smarty object
    private $smarty = null;
    // Lang array
    private $lang = null;
    // Router params
    private $router = null;
	
	// Flashdata
	private $flashdata = null;

    // If actions is not found
    public function _call() {
	
		// Load needed classes
		Loader::loadClass('Common');
	
		// Show error page
		Common::showPage(404);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // Get variables
        $this->db 		= Registry::get('db');
        $this->config 	= Registry::get('config');
		$this->memcache = Registry::get('memcache');
        $this->smarty 	= Registry::get('smarty');
        $this->lang 	= Registry::get('lang');
        $this->cache 	= Registry::get('cache');
        $this->router 	= Registry::get("router");
		$this->lg		= Registry::get("lg");
				
        // Assign values and display template
        $this->smarty->assign("lang", $this->lang);
        $this->smarty->assign("config", $this->config);
		$this->smarty->assign("hideFooterInfo", 1);
		
        // Check for messages or any flashdata
        if (!empty($_SESSION['error'])) {
            $this->smarty->assign("error", $_SESSION['error']);
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->smarty->assign("success", $_SESSION['success']);
            unset($_SESSION['success']);
        }
        if (!empty($_SESSION['flashdata'])) {
			$this->flashdata = $_SESSION['flashdata'];
            unset($_SESSION['flashdata']);
        }
    }
	
	// Index action
    public function indexAction() {
		
		// Load needed classes
		Loader::loadClass('Common');
		Loader::loadClass('News');
		Loader::loadClass('Pager');
				
		// Parameters for pagination
		$pageNum 	= ($this->getParam('page')) ? $this->getParam('page') : 1;
		$url 		= $this->config['site_url'] . $this->lg . '/news/index/page/[page]/';
		$upcoming   = ($this->getParam('up')) ? $this->getParam('up') : '';
		if($upcoming) {
			$url .= 'up/' . $upcoming;
		}
		
		$rows_per_page = 10;
		
		// Construct limit
		$limit = (($pageNum-1) * $rows_per_page);
		
		// Create pagination
		$pager = new Pager();
		$pager->replace = '[page]';
		$pager->page = $pageNum;
		$pager->rows_per_page = $rows_per_page;
		$pager->link = $url;
		
		
		$where = array('status' => 'active', 'title_' . $this->lg => '! ');
		if($upcoming) {
			$where['upcoming'] = $upcoming;
		}
		
		// Get News
		$countNews = News::countNews($where);
							
		// Get pager data
		$return = $pager->paginate($countNews);
		
		// Get News
		$listNews = News::getNews($where, array('by' => 'date_added', 'type' => 'DESC'), array('from' => $limit , 'count' => $rows_per_page) );
		
		// Define meta tags
		$meta['title'] 			= $this->lang['news'];
		$meta['description'] 	= '';
		$meta['keywords'] 		= '';
		
		// Parse data to template
		$data = array(
			'listNews'		=> $listNews,
			'pageTitle'		=> $this->lang['news'] . ' ',
			
			'paginate'		=> $return[0],
			'maxPage'		=> $return[1],
			'currentPage'	=> $pageNum,
			'prev_link'		=> $return[2],
			'next_link'		=> $return[3],
			
			'upcoming' => $upcoming,
			
			'meta'			=> $meta,
		);
		
		// Assign values and display template
		$this->smarty->assign('page', 'news');
		$this->smarty->assign($data);
		$this->smarty->display("news/browse.tpl.html");
	}

	// View action
    public function viewAction() {
		
		// Load needed classes
		Loader::loadClass('Common');
		Loader::loadClass('News');
		
		// Get id news
		$idnews = (int)$this->router['idnews'];
		
		// Get news name
		$alias = strip_tags(trim($this->router['slug']));
	
		$infoNews = reset(News::getNews(array('idnews' => $idnews, 'alias_' . $this->lg => $alias)));
		
		// Show error if page doesn't exist
		if(empty($infoNews)) {
		
			// Show error page
			Common::showPage(404);
		}
		
		$infoNews['images'] = $this->db->fetchAll("SELECT * FROM news_picture WHERE idnews=" . $idnews . " and picture != '" . $infoNews['picture'] . "'");
	
		// Define meta tags
		$meta['title'] 			= ($infoNews['meta_title'])? $infoNews['meta_title'] : $infoNews['title'];
		$meta['keywords'] 		= ($infoNews['meta_keywords'])? $infoNews['meta_keywords'] : $infoNews['title'];
		$meta['description']	= ($infoNews['meta_description'])? $infoNews['meta_description'] : Common::str_cut($infoNews['content'], 300);
		
		// Facebook information
		$facebook['name'] = $meta['title'];
		$facebook['link'] = 'news/' . $infoNews['alias'] . '-' . $infoNews['idnews'];
		$facebook['picture'] = 'news/big/' . $infoNews['picture'];
		
		// Parse data to template
		$data = array(
			'infoNews'		=> $infoNews,
			'facebook'		=> $facebook,
			'meta'			=> $meta,
		);
		
		// Assign values and display template
		$this->smarty->assign('page', 'news');
		$this->smarty->assign($data);
		$this->smarty->display("news/view.tpl.html");
	}
}

?>