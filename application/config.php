<?php

// Live server
$db_config = array(
    'host'        	=> 'localhost',
    'username'      => 'balkanec_evinart',
    'password'      => '123evin321',
    'database'      => 'balkanec_evinnew',
    'server_type'   => 'MySQL',
    'charset'       => 'utf8',
    'debugger'      => false
);

// Site paths configuration array
$config = array(
    'site_url'      => 'https://evinarts.com/',
    'site_img'      => 'https://evinarts.com/htdocs/images/',
    'site_css'      => 'https://evinarts.com/htdocs/styles/',
    'site_js'       => 'https://evinarts.com/htdocs/scripts/',
    'site_swf'      => 'https://evinarts.com/htdocs/swf/',
	'site_data'     => 'https://evinarts.com/data/',
	'site_dir'      => '/home/balkanec/public_html/evinarts.com/',
	'site_email'	=> 'no-reply@evinarts.com',
	'site_name'		=> 'evinarts.com',
	'admin_url'		=> 'https://eadmin.evinarts.com/',
	'use_smtp'		=> false,
	'smtp_host'		=> 'vita.superhosting.bg',
	'smtp_port'		=> 25,
	'smtp_user'		=> '',
	'smtp_pass'		=> '',
	'memcache_port'	=> 5959
);
 
// Site cache configuration array
$cache = array(
    'dir'           => '/home/balkanec/public_html/evinarts.com/temp/cache/',
    'lifetime'      => 600,
    'suffix'        => '_cache.txt'
);

$config['status_information'] = array(
	2 => "На склад",
	3 => "По заявка",
	1 => "Изчерпано",
	4 => "Неактивен - скрит",
);

$config['status_promo'] = array( 1 => 'Промоция', 2 => 'Нов продукт', 3 => 'Нормален');

$config['currency'] = array(
	'leva'	=> 'лв.',
	'eur' 	=> '€',
	'gbp'	=> '&pound;',
	'usd'	=> '$',
);

// Settings
mb_internal_encoding('UTF-8');
?>