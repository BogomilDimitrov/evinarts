<?php 

	// Language name                       
	$lang['language'] = "Bulgarian";     
	$lang['lg']		  = "bg";

	// Pager
    $lang['first'] = "Първа";
    $lang['before'] = "Предишна";
    $lang['next'] = "Следваща";
    $lang['last'] = "Последна";
	
	// Timespan
	$lang['posted_on'] = "Submitted on ";
	$lang['posted'] = "Submitted ";
	$lang['timespan_just_now'] = "just now";
	$lang['timespan_second_ago'] = "second ago";
	$lang['timespan_seconds_ago'] = "seconds ago";
	$lang['timespan_minute_ago'] = "minute ago";
	$lang['timespan_minutes_ago'] = "minutes ago";
	$lang['timespan_hour_ago'] = "hour ago";
	$lang['timespan_hours_ago'] = "hours ago";
	$lang['timespan_day_ago'] = "day ago";
	$lang['timespan_days_ago'] = "days ago";
	$lang['timespan_week_ago'] = "week ago";
	$lang['timespan_weeks_ago'] = "weeks ago";
	$lang['timespan_month_ago'] = "month ago";
	$lang['timespan_months_ago'] = "months ago";
	$lang['timespan_year_ago'] = "year ago";
	$lang['timespan_years_ago'] = "years ago";
	$lang['timespan_hours'] = "hours";
	$lang['timespan_days'] = "days";
	$lang['timespan_weeks'] = "weeks";
	
	// Мета
	$lang['meta_head_title'] = ' ';
	
	// Menu
	$lang['za_nas'] 		= 'ЗА АВТОРА';
	$lang['kontakti'] 		= 'КОНТАКТИ';
	$lang['home'] 			= 'НАЧАЛО';
	$lang['price']	 		= 'Цена';
	$lang['gallery']		= 'ГАЛЕРИЯ';
	$lang['news']			= 'Новини';
	$lang['sitemap']		= 'Карта на сайта';
	$lang['map']			= 'Карта';
	$lang['archive']		= 'АРХИВ';
	
	$lang['url_product_menu']	= '';
	
	$lang['header_cart']			= 'КОШНИЦА';
	$lang['header_cards']			= 'КАРТИЧКИ';
	$lang['header_dekoracii']		= 'ДЕКОРАЦИИ';
	$lang['header_']		= 'ДЕКОРАЦИИ';
	$lang['header_events']			= 'СЪБИТИЯ';
	
	$lang['cart_city']				= 'Град';
	$lang['index_history'] 			= 'История....';
	$lang['index_za_men']			= 'ЗА МЕН';
	$lang['index_posledni_izqvi'] 	= 'ПОСЛЕДНИ ИЗЯВИ';
	$lang['index_tehniki']			= 'ИЗПОЛЗВАНИ ТЕХНИКИ И МАТЕРИАЛИ';
	$lang['index_molid']			= 'Въглен/молив';
	$lang['index_boi']				= 'Маслени бои';
	$lang['index_igla']				= 'С игла и конец';
	$lang['index_kviling'] 			= 'Квилинг';
	$lang['index_drugi']			= 'Други';
	$lang['index_pishete_mi']		= 'ПИШЕТЕ МИ';
	$lang['index_facebook']			= 'ВИЖТЕ ВЪВ ФЕЙСБУК';
	
	$lang['breadcrumb_home'] 		= 'Начало';
	$lang['page_number']			= 'Страница';
	$lang['page_ot']				= 'от';
	$lang['page_next_page']			= 'Следваща страница';
	$lang['page_prev_page']			= 'Предишна страница';
	
	$lang['view_sm']				= 'см';
	$lang['view_zaqvka']			= 'Продадено';
	$lang['view_same']				= 'Подобни картини';
	$lang['view_order']				= 'Поръчай';
	
	
	// Errors
	$lang['error_interface_404_title'] 		= "Страницата не е намерена";
	$lang['error_interface_404_message'] 	= "Oops ...SORRY! The page you are looking for does not exist; It may have been moved, or removed altogether.";
	$lang['interface_return_to_homepage'] 	= "Върни се на началната страница на evinarts.com";
	
	$lang['vsichki_prava_zapazeni']	= 'Всички права запазени.';
	$lang['index_home']			= 'Добре дошли!';
	$lang['view_all']			= 'виж всички';
	$lang['nazad'] 				= 'Back';
	$lang['more']				= 'още';
	$lang['order_product']		= 'Поръчай';
	$lang['order_title']		= 'Поръчка';
	
	$lang['page_share']			= 'Сподели:';
	$lang['page_back']			= 'Назад';
	
	// Contacts
	$lang['contact_title'] 	= 'Форма за обратна връзка';
	$lang['contact_forma'] 		= 'Запитване';
	$lang['contact_fields'] 	= '<span class="required">*</span> задължително поле.';
	$lang['contact_name'] 		= 'Име';
	$lang['contact_phone'] 		= 'Телефон';
	$lang['contact_comentar'] 	= 'Запитване';
	$lang['contact_security'] 	= 'Код';
	$lang['contact_send'] 		= 'Изпрати';
	$lang['contact_men']		= 'Връзка с мен';
	$lang['contact_address_info']= 'ул. Алеко Константинов 35; район Оборище; гр. София';
	
	// Final page
	$lang['forder_title'] 		= 'Поръчката е направена успешно!';
	$lang['forder_porychka'] 	= 'Поръчка';
	$lang['forder_final'] 		= 'Детайли';
	$lang['forder_prod']		= 'Картина';
	$lang['forder_price']		= 'Единична цена';
	$lang['forder_options']		= 'Опции';
	$lang['forder_delete']		= 'Премахни';
	$lang['forder_continue']	= 'Продължи да пазаруваш';
	$lang['forder_total_price']	= 'Крайна цена';
	$lang['forder_empty']		= 'Все още нямате добавени картини в кошницата.';
	$lang['forder_address']		= 'Адрес';
	$lang['forder_naseleno_mqsto']	= 'Населено място';
	$lang['forder_ime_fam']		= 'Име и фамилия';
	$lang['avt_name']			= 'Венетка Нитова';
	$lang['avt_name_index']		= 'Венетка Нитова';
	
	$lang['index_text_1'] = 'Върви си човекът по земния път и се редуват завои и прави, сменят се слънчеви с мрачни дни, изживяваме падения и успехи, изгаряме в любов и изневери, и така до деня, в който пътуването свърши... И няма ги вече страстите, копнежите, мечтите... Край?!<br />
О-о, не!<br />
Пътят продължава, но вече без теб. Остават само твоите деца и твоите дела. Остава това, което си успял да създадеш, да съградиш или посадиш докато си се смял и плакал на тази земя...';

	$lang['index_text_2'] = 'Старая се да рисувам картини с богат колорит, които да украсяват дома или офиса. Но понякога водена от емоции създавам творби, които задават въпроси, разказват история или събуждат чувста и размисли. <br /><br />
  Мръсотията и злото ни заобикалят от всички страни и постоянно ни се натрапват. Красотата и доброто трябва да се търсят и отглеждат като крехко цвете. За щастието и любовта е нужно да се грижим ежедневно.';
	
	$lang['forder_success'] 	= 'Благодарим за Вашата поръчка!<br /> Поръчката е УСПЕШНА!';
	$lang['forder_number'] 		= 'Номер';
	$lang['forder_status'] 		= 'Статус';
	$lang['forder_new'] 		= 'Нова';
	$lang['forder_data'] 		= 'Дата';
	$lang['forder_client'] 			= 'Клиент';
	$lang['forder_address_send'] 	= 'Доставка до';
	$lang['forder_info_plashtane'] 	= 'Информация за плащането';
	$lang['forder_metod'] 			= 'Начин на плащане';
	$lang['forder_message'] 		= 'Забележка';
	$lang['forder_nalojen'] 		= 'PayPal';
	$lang['forder_info_delivery'] 	= 'Информация за доставка';
	$lang['forder_metod_dostavka'] 	= 'Начин на доставка';
	$lang['forder_col']				= 'Количество';
	$lang['forder_contact_phone']	= 'Телефон за контакт';
	
	// Error register
	$lang['register_erroremail_1'] = 'Изисква се имейл адрес!';
	$lang['register_erroremail_2'] = 'Този имейл адрес вече е регистриран!';
	$lang['register_erroremail_3'] = '* Задължително поле!';
	$lang['register_erroremail_4'] = 'Невалиден иемйл адрес';
	
	$lang['obshti_uslovia'] = 'Общи условия';
	
	$lang['order_email_text'] = '<br>Вашата поръчка с номер <b>###idorder##</b> е успешно направена!<br> 
След преглед от модератор, ще се свържем с вас.<br> 
Благодарим ви, че пазарувахте от нас!';

$lang['dostavka_text'] = '<b>За България:</b>
<ul class="dostavka">
<li>При поръчка на обща стойност над 50 лева доставката е БЕЗПЛАТНА за цяла България.</li>
<li>При покупка под 50 лева, доставката е за сметка на клиента.</li>
<li>Доставката се извършва в срок от 5 работни дни чрез фирма Еконт Експрес.</li>
<li>Плащането може да се извърши чрез наложен платеж или системата PayPal.</li>
<li>Клиентът има правото да върне продукта в срок от  7 дни след  датата на получаване.</li>
</ul>
<br /><b>За чужбина:</b><br />
<ul class="dostavka">
<li>При поръчки извън страната  доставката е за сметка на клиента.</li>
<li>Доставката се извършва чрез фирма  DHL Express.</li>
<li>Плащането се извършва чрез системата PayPal  или наложен платеж.</li>
<li>Периода и цената на доставка са различни за отделните държави и региони и зависят от разстоянието до адреса на получателя и броя на избраните продукти.</li>
</ul>';

$lang['menu_last'] = 'Последни';
$lang['menu_pred'] = 'Предстоящи';

$lang['menu_pred_title'] = 'Предстоящи изяви';
?>