<?php

	// Language name
	$lang['language'] = "English";
	$lang['lg'] = "en";

    // Pager
    $lang['first'] = "First";
    $lang['before'] = "Prev";
    $lang['next'] = "Next";
    $lang['last'] = "Last";

	// Timespan
	$lang['posted_on'] = "Submitted on ";
	$lang['posted'] = "Submitted ";
	$lang['timespan_just_now'] = "just now";
	$lang['timespan_second_ago'] = "second ago";
	$lang['timespan_seconds_ago'] = "seconds ago";
	$lang['timespan_minute_ago'] = "minute ago";
	$lang['timespan_minutes_ago'] = "minutes ago";
	$lang['timespan_hour_ago'] = "hour ago";
	$lang['timespan_hours_ago'] = "hours ago";
	$lang['timespan_day_ago'] = "day ago";
	$lang['timespan_days_ago'] = "days ago";
	$lang['timespan_week_ago'] = "week ago";
	$lang['timespan_weeks_ago'] = "weeks ago";
	$lang['timespan_month_ago'] = "month ago";
	$lang['timespan_months_ago'] = "months ago";
	$lang['timespan_year_ago'] = "year ago";
	$lang['timespan_years_ago'] = "years ago";
	$lang['timespan_hours'] = "hours";
	$lang['timespan_days'] = "days";
	$lang['timespan_weeks'] = "weeks";
	// Menu
	$lang['za_nas'] 		= 'ABOUT THE AUTHOR';
	$lang['kontakti'] 		= 'CONTACTS';
	$lang['home'] 			= 'HOME';
	$lang['price']	 		= 'Price';
	$lang['gallery']		= 'GALLERY';
	$lang['news']			= 'News';
	$lang['sitemap']		= 'Site Map';
	$lang['map']			= 'Map';
	$lang['archive']		= 'ARCHIVE';

	$lang['url_product_menu']	= '';

	$lang['header_cart']			= 'BASKET';
	$lang['header_cards']			= 'CARDS';
	$lang['header_dekoracii']		= 'DECORATIONS';
	$lang['header_events']			= 'EVENTS';

	$lang['cart_city']				= 'City';
	$lang['index_history'] 			= 'History....';
	$lang['index_za_men']			= 'About me';
	$lang['index_posledni_izqvi'] 	= 'LAST EVENTS';
	$lang['index_tehniki']			= 'USED TECHNIQUES AND MATERIALS';
	$lang['index_molid']			= 'Charcoal/ pencil';
	$lang['index_boi']				= 'Oils';
	$lang['index_igla']				= 'With needle and thread';
	$lang['index_kviling'] 			= 'Quilling';
	$lang['index_drugi']			= 'Other';
	$lang['index_pishete_mi']		= 'WRITE ME';
	$lang['index_facebook']			= 'SEE FACEBOOK';

	$lang['breadcrumb_home'] 		= 'Home';
	$lang['page_number']			= 'Page';
	$lang['page_ot']				= 'from';
	$lang['page_next_page']			= 'Next page';
	$lang['page_prev_page']			= 'Previous page';

	$lang['view_sm']				= 'sm';
	$lang['view_zaqvka']			= 'Sold out';
	$lang['view_same']				= 'Similar pictures';
	$lang['view_order']				= 'Buy';


	// Errors
	$lang['error_interface_404_title'] 		= "Page not found";
	$lang['error_interface_404_message'] 	= "Oops ...SORRY! The page you are looking for does not exist; It may have been moved, or removed altogether.";
	$lang['interface_return_to_homepage'] 	= "Back on the home page evinarts.com";

	$lang['vsichki_prava_zapazeni']	= 'All rights reserved.';
	$lang['index_home']			= 'Welcome!';
	$lang['view_all']			= 'view all';
	$lang['nazad'] 				= 'Back';
	$lang['more']				= 'more ';
	$lang['order_product']		= 'Buy';
	$lang['order_title']		= 'Order';

	$lang['page_share']			= 'Share:';
	$lang['page_back']			= 'Back';

	// Contacts
	$lang['contact_title'] 	= 'Feedback form';
	$lang['contact_forma'] 		= 'Question';
	$lang['contact_fields'] 	= '<span class="required">*</span> required field.';
	$lang['contact_name'] 		= 'Name';
	$lang['contact_phone'] 		= 'Telephone';
	$lang['contact_comentar'] 	= 'Inquiry';
	$lang['contact_security'] 	= 'Code';
	$lang['contact_send'] 		= 'Send';
	$lang['contact_men']		= 'Contact me';
	$lang['contact_address_info']= 'village Trudovets, Botevgrad, Sofia Region';

	// Final page
	$lang['forder_title'] 		= 'The order was made successfully!';
	$lang['forder_porychka'] 	= 'Order';
	$lang['forder_final'] 		= 'Details';
	$lang['forder_prod']		= 'Painting';
	$lang['forder_price']		= 'Unit price';
	$lang['forder_options']		= 'Options';
	$lang['forder_delete']		= 'Remove';
	$lang['forder_continue']	= 'Continue shopping';
	$lang['forder_total_price']	= 'Final price';
	$lang['forder_empty']		= 'There are no pictures in the shopping cart.';
	$lang['forder_address']		= 'Address';
	$lang['forder_naseleno_mqsto']	= 'Populated place';
	$lang['forder_ime_fam']		= 'Name and surname';
	$lang['avt_name']			= 'Veneta Nitova';
	$lang['avt_name_index']		= 'Veneta Nitova';

	$lang['index_text_1'] = 'Man is treading their earthy road with alternating turns and does, sunny days changing with cloudy ones, we are going through successes and failures, burn in love and infidelity, and so to the day on which the trip is over .... And passions, longings, dreams gone!. End ?!<br />
Oh, no!<br />
The road continues, but already without you. Only your children and your works stay behind. What you have managed to create, to build or plant while laughing and crying on this earth , remains behind you.';

	$lang['index_text_2'] = 'I am trying to paint pictures rich in colour to decorate your home or office. But sometimes led by emotions, I create works that ask questions, tell a story or rouse feelings and thoughts.<br /><br />
Dirt and evil surround us all around and are constantly intruding upon us. Beauty and good should be sought and cultivated as a brittle flower. We should care for love and happiness every single day.';

	$lang['forder_success'] 	= 'Thank you for your order!<br /> The order is successfully';
	$lang['forder_number'] 		= 'Number';
	$lang['forder_status'] 		= 'Status';
	$lang['forder_new'] 		= 'New';
	$lang['forder_data'] 		= 'Date';
	$lang['forder_client'] 			= 'Customer';
	$lang['forder_address_send'] 	= 'Delivery to';
	$lang['forder_info_plashtane'] 	= 'Payment information';
	$lang['forder_metod'] 			= 'Method of payment';
	$lang['forder_message'] 		= 'Remark';
	$lang['forder_nalojen'] 		= 'PayPal';
	$lang['forder_info_delivery'] 	= 'Delivery information';
	$lang['forder_metod_dostavka'] 	= 'Shipping and payment';
	$lang['forder_col']				= 'Quantity';
	$lang['forder_contact_phone']	= 'Contact phone';

	// Error register
	$lang['register_erroremail_1'] = 'Email is required!';
	$lang['register_erroremail_2'] = 'This Email is already registered!';
	$lang['register_erroremail_3'] = '* Required field!';
	$lang['register_erroremail_4'] = 'Invalid Email address';

	$lang['obshti_uslovia'] = 'General provisions';

	$lang['order_email_text'] = '<br>Your order with number <b>###idorder##</b> is successfully done!<br>
After a review of moderator, we will contact you.<br>
Thank you for the shopping from us!';

$lang['dostavka_text'] ='<b>For shipment in Bulgaria:</b><br />
<ul class="dostavka">
<li>For orders totaling over <b>50 levs (BGN)</b> delivery is <b>FREE</b> throughout Bulgaria.</li>
<li>For purchases under <b>50 levs (BGN)</b>, delivery expenses are borne by the Customer.</li>
<li>Delivery is carried out within 5 working days by <b>Econt Express</b> shipping company.</li>
<li>Payment can be made by cash or via <b>PayPal</b>.</li>
<li>The Customer has the right to return the product within 7 days after its receipt.</li>
</ul>

<br /><b>For shipment abroad:</b><br />
<ul class="dostavka">
<li>For orders outside the country delivery shall be borne by the Customer.</li>
<li>Delivery is carried out by <b>DHL Express</b> shipping company.</li>
<li>Payment is via <b>PayPal</b> system or by cash.</li>
<li>The term and price of deliver differ for different countries and regions and depend on the distance to the destination address and the number of selected products.</li>
</ul>';

$lang['menu_last'] = 'Last';
$lang['menu_pred'] = 'Upcoming';

$lang['menu_pred_title'] = 'Upcoming events';
?>