<?php 
	
	// Language name
	$lang["language"] = "French";
	$lang["lg"] = "fr";
	
    // Pager
    $lang["first"] = "First";
    $lang["before"] = "Prev";
    $lang["next"] = "Next";
    $lang["last"] = "Last";
	
	// Timespan
	$lang["posted_on"] = "Submitted on ";
	$lang["posted"] = "Submitted ";
	$lang["timespan_just_now"] = "just now";
	$lang["timespan_second_ago"] = "second ago";
	$lang["timespan_seconds_ago"] = "seconds ago";
	$lang["timespan_minute_ago"] = "minute ago";
	$lang["timespan_minutes_ago"] = "minutes ago";
	$lang["timespan_hour_ago"] = "hour ago";
	$lang["timespan_hours_ago"] = "hours ago";
	$lang["timespan_day_ago"] = "day ago";
	$lang["timespan_days_ago"] = "days ago";
	$lang["timespan_week_ago"] = "week ago";
	$lang["timespan_weeks_ago"] = "weeks ago";
	$lang["timespan_month_ago"] = "month ago";
	$lang["timespan_months_ago"] = "months ago";
	$lang["timespan_year_ago"] = "year ago";
	$lang["timespan_years_ago"] = "years ago";
	$lang["timespan_hours"] = "hours";
	$lang["timespan_days"] = "days";
	$lang["timespan_weeks"] = "weeks";
	
	$lang["za_nas"] 		= "POUR L'AUTEUR";
	$lang["kontakti"] 		= "CONTACT";
	$lang["home"] 		    = "ACCUEIL";
	$lang["price"]	 		= "Prix";
	$lang["gallery"]		= "GALERIE";
	$lang["news"]			= "Nouvelles";
	$lang["sitemap"]		= "Plan de site";
	$lang["map"]			= "Carte";
	$lang["archive"]		= "ARCHIVES";
	
	$lang["url_product_menu"]	= "";
	
	$lang["header_cart"]			= "PANIER";
	$lang["header_cards"]			= "CARTES";
	$lang["header_dekoracii"]		= "DECORATIONS";
	$lang["header_events"]			= "EVENEMENTS";	
		
	$lang['cart_city']				= 'City';
	$lang["index_history"] 			= "l'Histoire....";
	$lang["index_za_men"]			= "POUR MOI";
	$lang["index_posledni_izqvi"] 	= "EVENEMENTS RECENTS";
	$lang["index_tehniki"]			= "TECHNIQUES ET MATERIAUX UTILISES";
	$lang["index_molid"]			= "Charbon/crayon";
	$lang["index_boi"]				= "Huils";
	$lang["index_igla"]				= "Avec une aiguille et du fil";
	$lang["index_kviling"] 			= "Quilling";
	$lang["index_drugi"]			= "Autre";
	$lang["index_pishete_mi"]		= "ECRIS MOI";
	$lang["index_facebook"]			= "VOIR FACEBOOK";
	
	$lang["breadcrumb_home"] 		= "Home";
	$lang["page_number"]			= "Page";
	$lang["page_ot"]				= "de";
	$lang["page_next_page"]			= "Page suivante";
	$lang["page_prev_page"]			= "Page précédente";
	
	$lang["view_sm"]				= "sm";
	$lang["view_zaqvka"]			= "Sur demande";
	$lang["view_same"]				= "Images similaires";
	$lang["view_order"]				= "Acheter";
	
	
	// Errors
	$lang["error_interface_404_title"] 		= "Page non trouvée";
	$lang["error_interface_404_message"] 	= "Oups ...Désolé! La page que vous cherchez ne pas exister; Il a peut-être été déplacé ou retiré complètement.";
	$lang["interface_return_to_homepage"] 	= "Retour sur la page d'accueil evinarts.com";
	
	$lang["vsichki_prava_zapazeni"]	= "Tous droits réservés.";
	$lang["index_home"]			= "Bienvenue!";
	$lang["view_all"]			= "voir toit";
	$lang["nazad"] 				= "RETOUR";
	$lang["more"]				= "plus";
	$lang["order_product"]		= "Acheter";
	$lang["order_title"]		= "Commande";
	
	$lang["page_share"]			= "Partager:";
	$lang["page_back"]			= "En arrière";
	
	// Contacts
	$lang["contact_title"] 	= "Formulaire de commentaires";
	$lang["contact_forma"] 		= "Demande";
	$lang["contact_fields"] 	= " champ requis.";
	$lang["contact_name"] 		= "Nom";
	$lang["contact_phone"] 		= "Téléphone";
	$lang["contact_comentar"] 	= "Demande";
	$lang["contact_security"] 	= "Code";
	$lang["contact_send"] 		= "Envoyer";
	$lang["contact_men"]		= "Contactez moi";
	$lang["contact_address_info"]= "Troudovets de village, Communauté de Botevgrad, Sofia Region";
	
	// Final page
	$lang["forder_title"] 		= "L'ordonnance a été fait avec succès!";
	$lang["forder_porychka"] 	= "Demmande";
	$lang["forder_final"] 		= "Détails";
	$lang["forder_prod"]		= "Tableau";
	$lang["forder_price"]		= "Prix unitaire";
	$lang["forder_options"]		= "Options";
	$lang["forder_delete"]		= "Supprimer";
	$lang["forder_continue"]	= "Continuer vos achats";
	$lang["forder_total_price"]	= "Prix final";
	$lang["forder_empty"]		= "Il n'y a pas des tableaux dans le pannier.";
	$lang["forder_address"]		= "Adresse";
	$lang["forder_naseleno_mqsto"]	= "Lieu habité";
	$lang["forder_ime_fam"]		= "Nom et surnom";
	$lang['avt_name']			= 'Veneta Nitova';
	$lang['avt_name_index']		= 'Veneta Nitova';
	
	$lang['index_text_1'] = ' On suit son chemin terrestre, les virages et les droites se succèdent, les jours ensoleillés succèdent aux jours ténébreux...Nous vivons des chutes et des succès, nous brûlons d’amour et d’infidélités et cela jusqu’au jour où le voyage s’arrête...Plus de passions, de désirs ardents et de rêves...Est-ce la fin?<br />
   Mais, non!<br />
   Le chemin continue, cette fois sans toi. Il ne reste que tes enfants et tes oeuvres - ce que tu as réussi de créer, bâtir ou planter aux bons et aux mauvais moments de ta vie.';

	$lang['index_text_2'] = ' J’essaie de créeer des tableaux riches en couleurs pour décorer une maison ou un bureau. Mais parfois, menée par les émotions, je crée des oevres qui posent des questions, racontent des histoires, éveillent des sensations et provoquent des réflexions.<br /><br />

  Le laid et le mal s’imposent  autour de nous. La beauté et le bien doivent être cultivés comme une fleur délicate. Le bonheur et l’amour doivent être soignés tous les jours.';
	
	$lang["forder_success"] 	= "Nous VOUS remercions de votre commande!<br /> La commande est réussie!";
	$lang["forder_number"] 		= "Numéro";
	$lang["forder_status"] 		= "Statut";
	$lang["forder_new"] 		= "Nouveau";
	$lang["forder_data"] 		= "Date";
	$lang["forder_client"] 			= "Client";
	$lang["forder_address_send"] 	= "Livraison à";
	$lang["forder_info_plashtane"] 	= "Informations de payement";
	$lang["forder_metod"] 			= "Moyen de payement";
	$lang["forder_message"] 		= "Remarque";
	$lang["forder_nalojen"] 		= "PayPal";
	$lang["forder_info_delivery"] 	= "Information de livraison";
	$lang["forder_metod_dostavka"] 	= "Mode de livraison";
	$lang['forder_col']				= 'Quantity';
	$lang['forder_contact_phone']	= 'Contact phone';
	
	// Error register
	$lang["register_erroremail_1"] = "Email est requis!";
	$lang["register_erroremail_2"] = "Cet Email est déjà enregistré!";
	$lang["register_erroremail_3"] = "* Champ requis!";
	$lang["register_erroremail_4"] = "Email adresse est ivalide";
	
	$lang['obshti_uslovia'] = 'General provisions';
	
	
	$lang['order_email_text'] = '<br>Votre commande avec  numéro <b>###idorder##</b> elle est fait avec succès!<br> 
Après examen par un modérateur, nous  prendrons  contact avec vous.<br> 
Merci, que vous avez fait  des achats de nous!';

	$lang['dostavka_text'] ='
	<b>Pour Bulgarie:</b>
	<ul class="dostavka">	
<li>Pour  une commande totalisant plus de 50 leva,  la livraison est gratuite pour tout le territoire de la Bulgarie.</li>
<li>Pour un achat de moins de 50 leva,  la livraison est à la charge du client.</li>
<li>La livraison s’effectue dans les 5 jours ouvrables par la société Ekont Express.</li>
<li>Le paiement peut être effectué contre remboursement ou par le système PayPal.</li>
<li>Le client a le droit de retourner le produit dans les 7 jours suivant la réception.</li>
</ul>
<b>Pour étranger:</b>
<ul class="dostavka">
<li>Pour les commandes pour étranger la livraison est à la charge de client.</li>
<li>La livraison s’effectue  par la société DHL Express.</li>
<li>Le paiement est effectué par le système PayPal ou contre remboursement.</li>
<li>La période et le coût de la livraison sont différents pour les différents pays et régions et dépendent de la distance à l’adresse de destination et le nombre des produits sélectionnés.</li>
</ul>';

$lang['menu_last'] = 'Derniers';
$lang['menu_pred'] = 'A venir';

$lang['menu_pred_title'] = 'évènements à venir';
?>