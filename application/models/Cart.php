<?php

/**
 *
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved.
 *
 * Cart model
 *
 * @file			Cart.php
 * @category		Application
 * @author			Elenko Ivanov
 * @email			elenko.ivanov@gmail.com
 *
 */
class Cart {

    // Generate search string from search array
    public static function whereStringFromArray($array = "") {

        // If we don't have search conditions
        if (!is_array($array) OR empty($array))
            return;

        // Form search array
        foreach ($array AS $k => $v) {
			if(is_array($v)){
				foreach ($v AS $a) {
					$input[] = $k . " != '" . $a . "'";
				}
			}else{
				if (strstr($v, "%")) {
					$input[] = $k . " LIKE '" . $v . "'";
				} else if (strstr($v, "!")) {
					$input[] = $k . " != '" . str_replace('!', '', trim($v)) . "'";
				} else if (strstr($v, "<")) {
					$input[] = $k . " < '" . str_replace('<', '', trim($v)) . "'";
				} else if (strstr($v, ">")) {
					$input[] = $k . " > '" . str_replace('>', '', trim($v)) . "'";
				} else if (strstr($v, "<=")) {
					$input[] = $k . " <= '" . str_replace('<=', '', trim($v)) . "'";
				} else if (strstr($v, ">=")) {
					$input[] = $k . " >= '" . str_replace('>=', '', trim($v)) . "'";
				} else {
					$input[] = $k . " = '" . $v . "'";
				}
			}
        }

        // Form search string
        $where = " WHERE " . implode(" AND ", $input);

        // Return search string
        return $where;
    }
    
    // Generate order string from array
    public function orderStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " ORDER BY " . $array['by'] . " " . $array['type'];
        }
    }

    // Generate limit string from array
    public function limitStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " LIMIT " . $array['from'] . "," . $array['count'];
        }
    }
   
    // List products in cart
    public static function listProductCart($idcart){
		
		$db = Registry::get("db");
		$lg = Registry::get("lg");
		
		return $db->fetchAll("SELECT cp.*, p.`title_" . $lg . "` as title, p.`active`, p.`picture`, p.`alias_" . $lg . "` as alias, p.idproduct
								FROM `carts_products` as cp
								LEFT JOIN `products` p ON p.idproduct = cp.id_product
								WHERE cp.`id_cart`=" . $idcart . " 
								ORDER BY cp.`id` ASC");
	}
   
   
    // Create cart
    public static function createCart($session_id){
    	
		$db = Registry::get("db");
		
		// Check cart if already added
		$check_exists = $db->fetchOne("SELECT `id_carts` FROM `carts` WHERE `phpsession`='" . $session_id . "' AND `status`=1 ");

		if($check_exists) return $check_exists;
		
		// Create fields
		$fields['phpsession'] 	= $session_id;
		$fields['date_added']	= date('Y-m-d H:i:s');
		$fields['ip']			= $_SERVER['REMOTE_ADDR'];
		$fields['status'] 		= 1;

		// Insert in database
		$db->insert('carts', $fields, false);
	
		$id = $db->lastInsertId();
		
		return $id;
	}

	// Update status or buyer info(name, address)
	function updateCart($data, $id_cart){
	
		$db = Registry::get("db");
		
		// Update cart
		$db->update('carts', $data, ' `id_carts` = ' . $id_cart);
	
		return $id_cart;
	}

	function getColPrice($cart_id){
		
		$db = Registry::get("db");
		$lg = Registry::get("lg");
		
		Loader::loadClass('Common');
		
		// Cart data
		$cart_db_info = $db->fetchRow("SELECT * FROM `carts` WHERE `phpsession`='" . $cart_id . "' AND `status`=1");
		
		$data['price'] 	= 0;
		$data['col']	= 0;
		
		if(!$cart_db_info){	
			return $data;
		}

		// Get all products in cart
		$prod_in_cart = $db->fetchAll("SELECT cp.*, p.`title_" . $lg . "` as title, p.idproduct, p.`alias_" . $lg . "` as alias, p.active, p.picture 
										FROM `carts_products` as cp
										LEFT JOIN `products` p ON p.idproduct=cp.id_product
										WHERE cp.`id_cart`=" . $cart_db_info['id_carts']);
		
		if($prod_in_cart){
			foreach($prod_in_cart as $k=>$v){
				
				$price = $v['price'];
				 
				$data['products'][$k] = $v;
				$data['products'][$k]['price'] = $price;
				
				$col_price 	= $v['col']*$price;
				
				$data['price'] += $col_price;
				$data['col']   += $v['col'];
			}
		}
		
		return $data;
	}

	function insertProdInCart($id_cart, $data) {
		
		$db = Registry::get("db");

		foreach($data as $key=>$value){
			$fields[$key] = trim($value);
		}

		$fields['date_added'] 	= date('Y-m-d H:i:s');
		$fields['id_cart'] 		= (int)$id_cart;
		
		// Check if product exist in cart
		$check_exists = $db->fetchOne("SELECT `id` FROM `carts_products` WHERE 
												`id_cart`=" . $id_cart . " AND 
												`id_product`=" . $fields['id_product']
										);
		if($check_exists){
			// Update data base
			// $db->query("UPDATE `carts_products` SET `col`=`col`+" . $data['col'] . " WHERE id= " . $check_exists);
			
			return $check_exists;
		}
		else {
			// Insert in database
			$db->insert('carts_products', $fields);
		
			$insert_id = $db->lastInsertId();
		}
		
		return $insert_id;
	}

	function updateProdInCart($data, $id_cart, $id_prod, $id) {
		$db = Registry::get("db");

		foreach($data as $key=>$value) {
			$post_data[$key] = trim($value);
		}

		// Update cart
		if($db->update('carts_products', $post_data, ' `id_cart` = ' . $id_cart . " AND `id_product`=" . $id_prod . " AND `id`=" . $id))
			return 'OK';
		else 
			return 'NOK';
	}

	// Delete product from cart
	function deleteProdFromCart($id_cart, $id_del) {
	
		$db = Registry::get("db");
		
		if($db->query("DELETE FROM `carts_products` WHERE `id_cart`=" . $id_cart . " and `id`=" . $id_del))
			return 'OK';
		else
			return 'NOK';
	}

	// Remove carts
	function deleteCart($id) {
		
		$db = Registry::get("db");

		$db->query("DELETE FROM `carts` WHERE `id_carts` =" . $id);
		$db->query("DELETE FROM `carts_products` WHERE `id_cart` =" . $id);
	}
	
	// Взимаме информацията за текущата поръчка
    public static function getCartInfo($where = "", $order = "", $limit = "") {
        
        $db = Registry::get("db");

        // Generate where clause
        $where = self::whereStringFromArray($where);
        
        // Generate order clause
        $order = self::orderStringFromArray($order);
        
        // Generate limit clause
        $limit = self::limitStringFromArray($limit);

        // Get category
        return $db->fetchAll("SELECT *
								FROM `carts`
								$where $order $limit");
    }
}
?>