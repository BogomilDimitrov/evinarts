<?php

/**
 *
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved.
 *
 * Gallery model
 *
 * @file			Gallery.php
 * @category		Application
 * @author			Elenko Ivanov
 *
 */
class Gallery {

    // Generate search string from search array
    public static function whereStringFromArray($array = "") {

        // If we don't have search conditions
        if (!is_array($array) OR empty($array))
            return;

        // Form search array
        foreach ($array AS $k => $v) {
            if (strstr($v, "%")) {
                $input[] = $k . " LIKE '" . $v . "'";
            } else if (strstr($v, "!")) {
                $input[] = $k . " != '" . str_replace('!', '', trim($v)) . "'";
            } else if (strstr($v, "<")) {
                $input[] = $k . " < " . str_replace('<', '', trim($v));
            } else if (strstr($v, ">")) {
                $input[] = $k . " > " . str_replace('>', '', trim($v));
            } else if (strstr($v, "<=")) {
                $input[] = $k . " <= " . str_replace('<=', '', trim($v));
            } else if (strstr($v, ">=")) {
                $input[] = $k . " >= " . str_replace('>=', '', trim($v));
            } else {
                $input[] = $k . " = '" . $v . "'";
            }
        }

        // Form search string
        $where = " WHERE " . implode(" AND ", $input);

        // Return search string
        return $where;
    }
    
    // Generate order string from array
    public function orderStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else if ($array['by'] == 'RAND'){
			return " ORDER BY RAND()";	
        } else {
            return " ORDER BY " . $array['by'] . " " . $array['type'];
        }
    }

    // Generate limit string from array
    public function limitStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " LIMIT " . $array['from'] . "," . $array['count'];
        }
    }

    // Get News
    public static function getGallery($where = "", $order = "", $limit = "", $ids= "") {
	
		// Get parameters
        $db = Registry::get("db");
		$lg = Registry::get("lg");
		
        // Generate where clause
        $where = self::whereStringFromArray($where);
        
        // Generate order clause
        $order = self::orderStringFromArray($order);
        
        // Generate limit clause
        $limit = self::limitStringFromArray($limit);
		
        // Взимаме картините   
        return $db->fetchAll("SELECT `idproduct`, `idcategory`, `title_" . $lg . "` as title, `alias_" . $lg . "` as alias,
										`content_" . $lg . "` as content, `price_1`, `price_2`, `razmeri`, `status`, `picture`,
										`meta_title_" . $lg . "` as meta_title,
										`meta_keywords_" . $lg . "` as meta_keywords,
										`meta_description_" . $lg . "` as meta_description
								FROM `products`
								$where $ids $order $limit");
    }
		
    // Взимаме всички продукти
    public static function countGallery($where = "", $ids= "") {
        $db = Registry::get("db");

		// Generate where clause
        $where = self::whereStringFromArray($where);
		
        // Count category
        return $db->fetchOne("SELECT COUNT(*)
								FROM `products`
								$where $ids");
    }
	
	// Взимаме категориите
	public static function getCategory($where= "", $order="", $limit=""){
		
		// Get parameters
        $db = Registry::get("db");
		$lg = Registry::get("lg");
		
		$where['title_' . $lg] = '! ';
		
        // Generate where clause
        $where = self::whereStringFromArray($where);
        
        // Generate order clause
        $order = self::orderStringFromArray($order);
        
         // Generate limit clause
        $limit = self::limitStringFromArray($limit);
        	
		 // Взимаме категориите      
        return $db->fetchAll("SELECT `idcategory`, `idparrent`, `title_" . $lg . "` as title, `slug_" . $lg . "` as alias, 
        								`content_" . $lg . "` as content, `active`,
										`meta_title_" . $lg . "` as meta_title,
										`meta_keywords_" . $lg . "` as meta_keywords,
										`meta_description_" . $lg . "` as meta_description
								FROM `category`
								$where $order $limit");
		
	}
}

?>