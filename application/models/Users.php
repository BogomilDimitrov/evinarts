<?php

/**
 *
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2011 All Rights Reserved.
 *
 * Users model
 *
 * @file			Users.php
 * @category		Application
 * @author			Martin Tsvetanov
 *
 */
class Users {

    // Generate search string from search array
    public static function whereStringFromArray($array = "") {

        // If we don't have search conditions
        if (!is_array($array) OR empty($array))
            return;

        // Form search array
        foreach ($array AS $k => $v) {
			if(is_array($v)){
				foreach ($v AS $a) {
					$input[] = $k . " != '" . $a . "'";
				}
			}else{
				if (strstr($v, "%")) {
					$input[] = $k . " LIKE '" . $v . "'";
				} else if (strstr($v, "!")) {
					$input[] = $k . " != '" . str_replace('!', '', trim($v)) . "'";
				} else if (strstr($v, "<")) {
					$input[] = $k . " < '" . str_replace('<', '', trim($v)) . "'";
				} else if (strstr($v, ">")) {
					$input[] = $k . " > '" . str_replace('>', '', trim($v)) . "'";
				} else if (strstr($v, "<=")) {
					$input[] = $k . " <= '" . str_replace('<=', '', trim($v)) . "'";
				} else if (strstr($v, ">=")) {
					$input[] = $k . " >= '" . str_replace('>=', '', trim($v)) . "'";
				} else {
					$input[] = $k . " = '" . $v . "'";
				}
			}
        }

        // Form search string
        $where = " WHERE " . implode(" AND ", $input);

        // Return search string
        return $where;
    }
    
    // Generate order string from array
    public function orderStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " ORDER BY " . $array['by'] . " " . $array['type'];
        }
    }

    // Generate limit string from array
    public function limitStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " LIMIT " . $array['from'] . "," . $array['count'];
        }
    }

    // Get users
    public static function getUsers($where = "", $order = "", $limit = "") {
        $db = Registry::get("db");

        // Generate where clause
        $where = self::whereStringFromArray($where);
        
        // Generate order clause
        $order = self::orderStringFromArray($order);
        
        // Generate limit clause
        $limit = self::limitStringFromArray($limit);
		
        // Get users
        return $db->fetchAll("SELECT u.*
								FROM `users` AS u
								$where $order $limit");
    }
	
	// Count user login attempts
	public function countLoginAttempts($where = "", $group = "", $order = "", $limit = "") {
	
        $db = Registry::get("db");

        // Generate where clause
        $where = self::whereStringFromArray($where);
        
        // Generate order clause
        $order = self::orderStringFromArray($order);
        
        // Generate limit clause
        $limit = self::limitStringFromArray($limit);

        // Get cities
        return $db->fetchAll("SELECT *
								FROM `users_login`
                                $where $group $order $limit");
	}
	
    // Get blocked users
    public static function getBlockedUsers($where = "", $order = "", $limit = "", $group = "") {
        $db = Registry::get("db");

        // Generate where clause
        $where = self::whereStringFromArray($where);
        
        // Generate order clause
        $order = self::orderStringFromArray($order);
        
        // Generate limit clause
        $limit = self::limitStringFromArray($limit);
		
        // Get post comments
        return $db->fetchColumn("SELECT idblocked
								FROM `users_blocked`
								$where $group $order $limit");
    }
	
    // Get banned IP
    public static function getBannedIP($where = "", $order = "", $limit = "", $group = "") {
        $db = Registry::get("db");

        // Generate where clause
        $where = self::whereStringFromArray($where);
        
        // Generate order clause
        $order = self::orderStringFromArray($order);
        
        // Generate limit clause
        $limit = self::limitStringFromArray($limit);
		
        // Get post comments
        return $db->fetchOne("SELECT * 
								FROM `banned_ips`
								$where $group $order $limit");
    }
	
    // Count users
    public static function countUsers($where = "") {
        $db = Registry::get("db");

		// Generate where clause
        $where = self::whereStringFromArray($where);
		
        // Count posts
        return $db->fetchOne("SELECT COUNT(*)
								FROM `users`
								$where");
    }
}

?>