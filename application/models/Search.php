<?php

/**
 *
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved.
 *
 * Search model
 *
 * @file			Search.php
 * @category		Application
 * @author			Elenko Ivanov
 * @email			elenko.ivanov@gmail.com
 *
 */
class Search {

    // Generate search string from search array
    public static function whereStringFromArray($array = "") {

        // If we don't have search conditions
        if (!is_array($array) OR empty($array))
            return;

        // Form search array
        foreach ($array AS $k => $v) {
			if(is_array($v)){
				foreach ($v AS $a) {
					$input[] = $k . " != '" . $a . "'";
				}
			}else{
				if (strstr($v, "%")) {
					$input[] = $k . " LIKE '" . $v . "'";
				} else if (strstr($v, "!")) {
					$input[] = $k . " != '" . str_replace('!', '', trim($v)) . "'";
				} else if (strstr($v, "<")) {
					$input[] = $k . " < '" . str_replace('<', '', trim($v)) . "'";
				} else if (strstr($v, ">")) {
					$input[] = $k . " > '" . str_replace('>', '', trim($v)) . "'";
				} else if (strstr($v, "<=")) {
					$input[] = $k . " <= '" . str_replace('<=', '', trim($v)) . "'";
				} else if (strstr($v, ">=")) {
					$input[] = $k . " >= '" . str_replace('>=', '', trim($v)) . "'";
				} else {
					$input[] = $k . " = '" . $v . "'";
				}
			}
        }

        // Form search string
        $where = " WHERE " . implode(" AND ", $input);

        // Return search string
        return $where;
    }
    
    // Generate order string from array
    public function orderStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " ORDER BY " . $array['by'] . " " . $array['type'];
        }
    }

    // Generate limit string from array
    public function limitStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " LIMIT " . $array['from'] . "," . $array['count'];
        }
    }

    // Search product
    public static function searchProduct($query, $order = "", $limit = "", $group = "", $condition_where = "") {
        	
        $db = Registry::get("db");
		$lg = Registry::get("lg");

        // Generate order clause
        $order = self::orderStringFromArray($order);

        // Generate limit clause
        $limit = self::limitStringFromArray($limit);

		// Replace chars
		$query = trim(str_replace(array('-','$','(',')','%','#','$','^','+','|','!','&'), ' ', $query));
				
        // Get data 
        return $db->fetchAll("SELECT p.`idproduct`, p.`idcategory`, p.`price_1`, p.`active`, p.`picture`, 
	        						p.`title_" . $lg ."` as title, p.`alias_" . $lg ."` as alias, p.`short_info_" . $lg ."` as short_info,
	        						p.`description_" . $lg ."` as description, 
	        						p.`meta_title_" . $lg ."` as meta_title, p.`meta_description_" . $lg ."` as meta_description, p.`meta_keywords_" . $lg ."` as meta_keywords
								FROM `products` AS p
								WHERE (p.title_" . $lg . " LIKE '%$query%' OR p.description_" . $lg . " LIKE '%$query%' OR p.short_info_" . $lg . " LIKE '%$query%')
								AND p.active != '4'
								$condition_where $group $order $limit");
    }
   
    // Функция която връща само id-то на продукта
    public static function searchProductId($query, $order = "", $limit = "", $group = "") {
        	
        $db = Registry::get("db");
		$lg = Registry::get("lg");
		
        // Generate order clause
        $order = self::orderStringFromArray($order);

        // Generate limit clause
        $limit = self::limitStringFromArray($limit);

		// Replace chars
		$query = trim(str_replace(array('-','$','(',')','%','#','$','^','+','|','!','&'), ' ', $query));

        // Get data 
        return $db->fetchColumn("SELECT p.idproduct
								FROM `products` AS p
								WHERE p.title_" . $lg . " LIKE '%$query%' OR p.code LIKE '%$query%'
								AND p.active != '4'
								$group $order $limit");
   }
  
}
?>