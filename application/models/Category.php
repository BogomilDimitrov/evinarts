<?php

/**
 *
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved.
 *
 * Category model
 *
 * @file			Category.php
 * @category		Application
 * @author			Elenko Ivanov
 *
 */
class Category {

    // Generate search string from search array
    public static function whereStringFromArray($array = "") {

        // If we don't have search conditions
        if (!is_array($array) OR empty($array))
            return;

        // Form search array
        foreach ($array AS $k => $v) {
            if (strstr($v, "%")) {
                $input[] = $k . " LIKE '" . $v . "'";
            } else if (strstr($v, "!")) {
                $input[] = $k . " != '" . str_replace('!', '', trim($v)) . "'";
            } else if (strstr($v, "<")) {
                $input[] = $k . " < " . str_replace('<', '', trim($v));
            } else if (strstr($v, ">")) {
                $input[] = $k . " > " . str_replace('>', '', trim($v));
            } else if (strstr($v, "<=")) {
                $input[] = $k . " <= " . str_replace('<=', '', trim($v));
            } else if (strstr($v, ">=")) {
                $input[] = $k . " >= " . str_replace('>=', '', trim($v));
            } else {
                $input[] = $k . " = '" . $v . "'";
            }
        }

        // Form search string
        $where = " WHERE " . implode(" AND ", $input);

        // Return search string
        return $where;
    }
    
    // Generate order string from array
    public function orderStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " ORDER BY " . $array['by'] . " " . $array['type'];
        }
    }

    // Generate limit string from array
    public function limitStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " LIMIT " . $array['from'] . "," . $array['count'];
        }
    }

    // Get category
    public static function getCategory($where = "", $order = "", $limit = "") {
       	
        $db = Registry::get("db");
		$lg = Registry::get("lg");
		
		$where['title_' . $lg] = '! ';
		
        // Generate where clause
        $where = self::whereStringFromArray($where);
        
        // Generate order clause
        $order = self::orderStringFromArray($order);
        
        // Generate limit clause
        $limit = self::limitStringFromArray($limit);

        // Get category
        return $db->fetchAll("SELECT `idcategory`, `idparrent`, `title_" . $lg ."` as title, `alias_" . $lg ."` as alias, `position`, 
        							`meta_title_" . $lg ."` as meta_title, `meta_keywords_" . $lg ."` as meta_keywords, 
        							`meta_description_" . $lg ."` as meta_description, picture
								FROM `category`
								$where $order $limit");
    }
	
	// Get category by ids
    public static function getCategoryById($ids) {
        $db = Registry::get("db");
		$lg = Registry::get("lg");
		
        // Get category
        return $db->fetchAll("SELECT `idcategory`, `idparrent`, `title_" . $lg ."` as title, `slug_" . $lg ."` as slug, `content_" . $lg ."` as content, 
        							`position`, `left_menu`, `footer_menu`, 
        							`meta_title_" . $lg ."` as meta_title, `meta_keywords_en`, `meta_description_en`, `active`
								FROM `category`
								WHERE `idcategory` IN (" . $ids . ") and active='1' ORDER BY `position` ASC");
    }
	
	// Get category ids
    public static function getCategoryIds($idcategory) {
        $db = Registry::get("db");

        $listCategory = $db->fetchColumn("SELECT `idcategory`
								FROM `category`
								WHERE `idparrent` = " . $idcategory);
		
		// If no data, return empty
		if(empty($listCategory)) return '';
		
		/*$ids = array();
		
		foreach($listCategory as $k=>$category){
			$ids[] = $category['idcategory'];
		}
		*/
		
		return implode(',', $listCategory);
    }
		
    // Count Category
    public static function countCategory($where = "") {
        $db = Registry::get("db");

		// Generate where clause
        $where = self::whereStringFromArray($where);
		
        // Count category
        return $db->fetchOne("SELECT COUNT(*)
								FROM `category`
								$where");
    }
	
	// get Brands
    public static function getBrands($where = "", $order = "", $limit = "", $ids = "") {
        $db = Registry::get("db");
		$lg = Registry::get("lg");

        // Generate where clause
        $where = self::whereStringFromArray($where);
        
        // Generate order clause
        $order = self::orderStringFromArray($order);
        
        // Generate limit clause
        $limit = self::limitStringFromArray($limit);
		
        // Get brands
        return $db->fetchAll("SELECT `idbrand`, `title_" . $lg ."` as title, `slug_" . $lg ."` as slug, `content_" . $lg ."` as content, `url_" . $lg ."` as url, 
        							`position`, `picture`, `topPage`, `active`
								FROM `brands`
								$where $ids $order $limit");
    }
	
}

?>