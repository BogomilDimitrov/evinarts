<?php

/**
 *
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved.
 *
 * Products model
 *
 * @file			Products.php
 * @category		Application
 * @author			Elenko Ivanov
 *
 */
class Products {

    // Generate search string from search array
    public static function whereStringFromArray($array = "") {

        // If we don't have search conditions
        if (!is_array($array) OR empty($array))
            return;

        // Form search array
        foreach ($array AS $k => $v) {
			if(is_array($v)){
				foreach ($v AS $a) {
					$input[] = $k . " != '" . $a . "'";
				}
			}else{
				if (strstr($v, "%")) {
					$input[] = $k . " LIKE '" . $v . "'";
				} else if (strstr($v, "!")) {
					$input[] = $k . " != '" . str_replace('!', '', trim($v)) . "'";
				} else if (strstr($v, "<=")) {
					$input[] = $k . " <= '" . str_replace('<=', '', trim($v)) . "'";
				} else if (strstr($v, ">=")) {
					$input[] = $k . " >= '" . str_replace('>=', '', trim($v)) . "'";
				} else if (strstr($v, "<")) {
					$input[] = $k . " < '" . str_replace('<', '', trim($v)) . "'";
				} else if (strstr($v, ">")) {
					$input[] = $k . " > '" . str_replace('>', '', trim($v)) . "'";
				} else if (strstr($v, "BETWEEN")) {
					$input[] = "(".$k." BETWEEN " . str_replace('BETWEEN', '', trim($v)).")";
				} else {
					$input[] = $k . " = '" . $v . "'";
				}
			}
        }

        // Form search string
        $where = " WHERE " . implode(" AND ", $input);

        // Return search string
        return $where;
    }
    
    // Generate order string from array
    public static function orderStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } 
		else if ($array['by'] == 'RAND'){
			return " ORDER BY RAND()";
		}
		else if (is_array($array['by'])){
			foreach($array['by'] as $k=>$v)
				$massOrder[] = $v . " " . $array['type'][$k];
			
			return " ORDER BY " . implode(", ", $massOrder);
		}
		else {
            return " ORDER BY " . $array['by'] . " " . $array['type'];
        }
    }

    // Generate limit string from array
    public static function limitStringFromArray($array) {

        // If we don't have limits
        if (empty($array)) {
            return;
        } else {
            return " LIMIT " . $array['from'] . "," . $array['count'];
        }
    }

    // Get Products
    public static function getProducts($where = "", $order = "", $limit = "", $group = "", $ids = "") {
        
		$db = Registry::get("db");
		$lg = Registry::get("lg");

        // Generate where clause
		if(is_array($where)) $where = self::whereStringFromArray($where);
        
        // Generate order clause
		if(is_array($order)) $order = self::orderStringFromArray($order);

        // Generate limit clause
        $limit = self::limitStringFromArray($limit);
				
        // Get Products
        return $db->fetchAll("SELECT  p.`idproduct`, p.`price_1`, p.`active`, p.`picture`, p.idcategory,
        						p.`title_" . $lg ."` as title, p.`alias_" . $lg ."` as alias, p.`short_info_" . $lg ."` as info,
        						p.`description_" . $lg ."` as description, p.prod_warranty,
        						p.powercold, p.powerhot, p.efcold, p.efhot, p.inverter, p.ecodesign,
        						p.`meta_title_" . $lg ."` as meta_title, p.`meta_description_" . $lg ."` as meta_description,
        						p.`meta_keywords_" . $lg ."` as meta_keywords
                                FROM `products` AS p
                                $where $ids $group $order $limit");
    }
	
	// Get maxPrice
    public static function getMaxPriceProducts($where = "", $ids = "") {
        
		$db = Registry::get("db");

        // Generate where clause
		if(is_array($where)) $where = self::whereStringFromArray($where);
        
        // Get Products
        return $db->fetchOne("SELECT max(p.price_1) as maxAmount
                                FROM `products` AS p
                                $where $ids LIMIT 1");
    }
	
	// Get minPrice
    public static function getMinPriceProducts($where = "", $ids = "") {
        
		$db = Registry::get("db");

        // Generate where clause
		if(is_array($where)) $where = self::whereStringFromArray($where);
        
        // Get Products
        return $db->fetchOne("SELECT min(p.price_1) as maxAmount
                                FROM `products` AS p
                                $where $ids LIMIT 1");
    }
	
	// Get products ids
    public static function getProductsIds($where = '', $ids = '') {
        $db = Registry::get("db");

		// Generate where clause
        $where = self::whereStringFromArray($where);	
				
		$listProduct = $db->fetchColumn("SELECT Distinct pc.idproduct
								FROM `products` AS p
								LEFT JOIN `products_category` as pc ON pc.idproduct = p.idproduct
								$where $ids");
			
		// If no data, return empty
		if(empty($listProduct)) return '';
		
		/*
		$ids = array();
		
		foreach($listProduct as $k=>$product){
			$ids[] = $product['idproduct'];
		}
		*/
		
		return implode(',', $listProduct);
    }
	
    // Get Products by algorithm
/*
    public static function getPostsByAlgorithm($where = "", $limit = "") {
	
		// Get objects
        $db = Registry::get("db");
		$config = Registry::get("config");

        // Generate where clause
		$where = self::whereStringFromArray($where);
		
        // Generate limit clause
        $limit = self::limitStringFromArray($limit);
		
		// Get posts
		return $db->fetchAll("SELECT p.*, u.username,
								(( SUM((SELECT COUNT(*) FROM `posts_views` WHERE idpost = p.idpost) * " . $config['algorithm_views_weight'] . ") + SUM((p.total_comments)*" . $config['algorithm_comments_weight'] . ") + SUM((p.total_shares)*" . $config['algorithm_shares_weight'] . ") + 
								   SUM((SELECT COUNT(*) FROM `posts_vote` WHERE idpost = p.idpost) * " . $config['algorithm_vote_weight'] . ")) / (Unix_Timestamp(CURRENT_TIMESTAMP) - Unix_Timestamp(p.date_added))) AS score 
								FROM `posts` AS p 
								LEFT JOIN `users` AS u ON u.iduser = p.iduser 
								$where
								GROUP BY p.idpost 
								ORDER BY score DESC, p.idpost DESC $limit");
    }
*/
	
    // Count Products
    public static function countProducts($where = "", $ids = "") {
        
		$db = Registry::get("db");

		// Generate where clause
        $where = self::whereStringFromArray($where);
		
        // Count Products
        return $db->fetchOne("SELECT COUNT(Distinct pc.idproduct)
								FROM `products` AS p
								LEFT JOIN `products_category` as pc ON pc.idproduct = p.idproduct
								$where $ids");
    }
	
	// Count Products by brands
    public static function countProductsBrand($where = "", $ids = ""){
		
		$db = Registry::get("db");

		// Generate where clause
        $where = self::whereStringFromArray($where);
		
		// Count products
		return $db->fetchOne("SELECT COUNT(*)
								FROM `products` AS p
								$where $ids");
	}
	
	// Взимаме категориите на определен продукт от тип масив
	public static function getProductCategory($where = ""){
	
		$db = Registry::get("db");

		// Generate where clause
        $where = self::whereStringFromArray($where);
		
		$listCat = array();
		
		$cat = $db->fetchAll("SELECT idcategory FROM `products_category` $where");
		
		if($cat){
			foreach($cat as $k=>$v){
				$listCat[$v['idcategory']] = $v['idcategory'];
			}
		}
		
		return $listCat;
	}
	
	// Взимаме категориите на определени продукти и връщаме данните от този тип 1,2,3,4,5
	public static function getProductCategoryIds($ids){
	
		$db = Registry::get("db");

		// Generate where clause
        $where = self::whereStringFromArray($where);
		
		$listCat = array();
		
		$cat = $db->fetchColumn("SELECT DISTINCT idcategory FROM `products_category` WHERE idproduct in (" . $ids . ")");
		
		if($cat)
			return implode(",", $cat);
	}
	
	// Взимаме продуктите от определена категория от тип 1,2,3,4,5
	public static function getCategoryProductIds($ids){
	
		$db = Registry::get("db");

		$prod = $db->fetchColumn("SELECT DISTINCT idproduct FROM `products_category` WHERE idcategory in (" . $ids . ")");
		
		if($prod)
			return implode(",", $prod);
	}
	
	// Get Related Products
    public static function getRelatedProducts($where = "", $order = "", $limit = "") {
        
		$db = Registry::get("db");
		$lg = Registry::get("lg");

        // Generate where clause
		if(is_array($where)) $where = self::whereStringFromArray($where);
        
        // Generate order clause
		if(is_array($order)) $order = self::orderStringFromArray($order);

        // Generate limit clause
        $limit = self::limitStringFromArray($limit);
		
        // Get Products
        return $db->fetchAll("SELECT p.`idproduct`, p.`idcategory`, p.`idbrand`, p.`price_1`, p.`price_2`, p.`colichestvo`, p.`teglo`, 
        							p.`active`, p.`status`, p.`varianti`, p.`vodesht`, p.`picture`, p.`code`, 
        						p.`title_" . $lg ."` as title, p.`alias_" . $lg ."` as alias
                                FROM `products_related` as rp
								LEFT JOIN `products` AS p ON p.idproduct = rp.idproduct
                                $where $order $limit");
    }
}
?>