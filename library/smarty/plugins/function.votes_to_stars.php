<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {votes_to_stars} function plugin
 *
 * Type:     function<br>
 * Name:     Votes to stars<br>
 * Purpose:  handle votes to stars in template<br>
 * @author   Jordan Trifonov
 * @param 	 array
 * @param 	 Smarty
 * @return	 string
 */
function smarty_function_votes_to_stars($params, &$smarty)
{
	if($params['votes_points'] > 0 AND $params['votes_count'] > 0){
		
		$total_perfect = $params['votes_count'] * 5;
		$total_now = $params['votes_points'];
		$total = $total_now / $total_perfect;

		if($total <= 0.2){
			$stars = "onestar";
		} else if($total <= 0.4){
			$stars = "twostar";
		} else if($total <= 0.6){
			$stars = "threestar";
		} else if($total <= 0.8){
			$stars = "fourstar";
		} else {
			$stars = "fivestar";
		}
		
	} else $stars = "nostar";
	
	echo $stars;
}

?>