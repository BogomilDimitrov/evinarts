<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */
function smarty_modifier_date_convert($date, $time = false, $time_prefix = " - ", $time_suffix = "")
{
   
    // Get first part - only date
    $date_parts = reset(explode(" ", $date));
    $date_parts = explode("-", $date_parts);
    
    // Check month
    switch($date_parts[1]){
        case "01":
            $month = "Януари";
            break;
         case "02":
            $month = "Февруари";
            break;
        case "03":
            $month = "Март";
            break;
        case "04":
            $month = "Април";
            break;
        case "05":
            $month = "Май";
            break;
        case "06":
            $month = "Юни";
            break;
        case "07":
            $month = "Юли";
            break;
        case "08":
            $month = "Август";
            break;
        case "09":
            $month = "Септември";
            break;
        case "10":
            $month = "Октомври";
            break;
        case "11":
            $month = "Ноември";
            break;
        case "12":
            $month = "Декември";
            break;
    }
    
    // Return date with time
	if($time){

		return $date_parts['2']." ".$month." ".$date_parts[0]. $time_prefix . date("H:m", $date) . $time_suffix;
		
	// Return date withour time
	} else {
	
		return $date_parts['2']." ".$month." ".$date_parts[0];
		
	}
}

/* vim: set expandtab: */

?>
