<?php 
/** 
 * Smarty plugin 
 * @package Smarty 
 * @subpackage plugins 
 */ 

/** 
 * Smarty date_bg modifier plugin 
 * 
 * Type:     modifier
 * Name:     date_bg
 * Return date -> 28 май 2014
 * @author   Elenko Ivanov
 */ 
function smarty_modifier_date_bg($date)
{
    
	$date_array = array (
		"01" => "Януари",
		"02" => "Февруари",
		"03" => "Март",
		"04" => "Април",
		"05" => "Май",
		"06" => "Юни",
		"07" => "Юли",
		"08" => "Август",
		"09" => "Септември",
		"10" => "Октомври",
		"11" => "Ноември",
		"12" => "Декември",
	);
	
	$date_explode = explode("-", $date);
		
	$date_format = substr($date_explode[2], 0, 2) . ' ' . $date_array[$date_explode[1]] . ' ' . $date_explode[0]; 
	
    return $date_format; 
}

/* vim: set expandtab: */ 
?>