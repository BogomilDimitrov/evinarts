<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty plugin
 *
 * @author Jordan Trifonov
 * @param string
 * @return string
 */
function smarty_modifier_smilies($string)
{
	$replace = array(
		":)"			=>	"1.gif",
		":("			=>	"2.gif",
		":D"			=>	"3.gif",
		"8-)"			=>	"4.gif",
		";)"			=>	"5.gif",
		";("			=>	"6.gif",
		"(sweat)"		=>	"7.gif",
		":*"			=>	"8.gif",
		":P"			=>	"9.gif",
		"(blush)"		=>	"10.gif",
		"|-)"			=>	"11.gif",
		"(dull)"		=>	"12.gif",
		"(inlove)"		=>	"13.gif",
		"(grin)"		=>	"14.gif",
		"(talk)"		=>	"15.gif",
		"(yawn)"		=>	"16.gif",
		"(puke)"		=>	"17.gif",
		":@"			=>	"18.gif",
		"(wasntme)"		=>	"19.gif",
		"(party)"		=>	"20.gif",
		"(clap)"		=>	"21.gif",
		"(rofl)"		=>	"22.gif",
		"(call)"		=>	"23.gif",
		"(devil)"		=>	"24.gif",
		"(envy)"		=>	"25.gif",
		"(hug)"			=>	"26.gif",
		"(makeup)"		=>	"27.gif",
		"(pizza)"		=>	"28.gif",
		"(cash)"		=>	"29.gif",
		"(headbang)"	=>	"30.gif",
	);
	
	foreach($replace AS $k => $v){
		$string = str_replace($k, "<img src='/htdocs/images/smilies/".$v."'>", $string);
	}

	return $string;
}

/* vim: set expandtab: */

?>
