<?php

// Convert date in human readable format
function smarty_modifier_date_before($date){

	// Start and end date
	$then = strtotime($date);
	$now = time();
	
	// Calculate difference
	$difference = $now - $then;

	// Get periods lengths
	$periods = array(
		"31570560"		=>	array("one"	=>	"година", 		"many"	=>	"години"),
		"2630880"		=>	array("one"	=>	"месец", 		"many"	=>	"месеца"),
		"604800"		=>	array("one"	=>	"седмица", 		"many"	=>	"седмици"),
		"86400"			=>	array("one"	=>	"ден", 			"many"	=>	"дни"),
		"3400"			=>	array("one"	=>	"час", 			"many"	=>	"часа"),
		"60"			=>	array("one"	=>	"минута", 		"many"	=>	"минути"),
		"1"				=>	array("one"	=>	"секунда", 		"many"	=>	"секунди")
	);
	
	// Returned string
	$output = "";
	
	// For each field
	foreach($periods AS $seconds => $names_array){
		
		// Get time difference for this field
		$result = $difference / $seconds;
		
		// If the result is more than one
		if($result > 1){
		
			// If the result is less than 2
			if($result < 2){
				//$output .= floor($result)." ".$names_array['one']." ";
				$output[] = floor($result)." ".$names_array['one']." ";
				
			// If the result is more than 2
			} else {
				//$output .= floor($result)." ".$names_array['many']." ";
				$output[] = floor($result)." ".$names_array['many']." ";
			}
			
			// Decrement difference with the taken time
			$difference = $difference - floor($result) * $seconds;
		}
		
		
	}
	
	if($output[1]){
		$output = $output[0]." и ".$output[1];
	} else {
		$output = $output[0];
	}
	
	
	// Return difference
	return $output;

}

?>
