<?php 
/** 
 * Smarty plugin 
 * @package Smarty 
 * @subpackage plugins 
 */ 

/** 
 * Smarty excerpt modifier plugin 
 * 
 * Type:     modifier
 * Name:     excerpt
 * Purpose:  generate excerpt
 * @author   Martin Tsvetanov
 */ 
function smarty_modifier_excerpt($html, $maxLength = 100, $end = '...', $isUtf8 = true){ 
	
	// Define variables
    $printedLength = 0;
    $position = 0;
    $tags = array();

    // For UTF-8, we need to count multibyte sequences as one character.
    $re = $isUtf8
        ? '{</?([a-z]+)[^>]*>|&#?[a-zA-Z0-9]+;|[\x80-\xFF][\x80-\xBF]*}'
        : '{</?([a-z]+)[^>]*>|&#?[a-zA-Z0-9]+;}';

    // Loop through all characters
	while ($printedLength < $maxLength && preg_match($re, $html, $match, PREG_OFFSET_CAPTURE, $position)) {
	
		// Get character
        list($tag, $tagPosition) = $match[0];

        // Print text leading up to the tag.
        $str = substr($html, $position, $tagPosition - $position);
        if ($printedLength + strlen($str) > $maxLength){
            $response .= substr($str, 0, $maxLength - $printedLength);
            $printedLength = $maxLength;
            break;
        }

        $response .= $str;
        $printedLength += strlen($str);
        if ($printedLength >= $maxLength) break;

		// Pass the entity or UTF-8 multibyte sequence through unchanged.
        if ($tag[0] == '&' || ord($tag) >= 0x80){
            $response .= $tag;
            $printedLength++;
        }
		
		// Handle the tag.
        else {
            $tagName = $match[1][0];
			
			// This is a closing tag.
            if ($tag[1] == '/'){
			
				// Check that tags are properly nested.
                $openingTag = array_pop($tags);
                assert($openingTag == $tagName); 
                $response .= $tag;
            }
			
			// Self-closing tag.
            else if ($tag[strlen($tag) - 2] == '/') {
                $response .= $tag;
            }
			
			// Opening tag.
            else {
                $response .= $tag;
                $tags[] = $tagName;
            }
        }

        // Continue after the tag.
        $position = $tagPosition + strlen($tag);
    }

    // Print any remaining text.
    if ($printedLength < $maxLength && $position < strlen($html)) $response .= substr($html, $position, $maxLength - $printedLength);

	// Add ending chars if text is longer than the max length
	if (strlen($html) > $maxLength) $response .= $end;
	
    // Close any open tags.
    while (!empty($tags)) $response .= sprintf('</%s>', array_pop($tags));
	
	// Append the ending and return
	return $response;
} 

/* vim: set expandtab: */ 
?>