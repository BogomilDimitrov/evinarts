<?php 
/** 
 * Smarty plugin 
 * @package Smarty 
 * @subpackage plugins 
 */ 

/** 
 * Smarty strip_tags modifier plugin 
 * 
 * Type:     modifier
 * Name:     strip_tags
 * Purpose:  strip html tags from text 
 * @author   Martin Tsvetanov
 */ 
function smarty_modifier_strip_tags($string, $allowed_tags = null) 
{ 
    
    return strip_tags($string, $allowed_tags); 
} 

/* vim: set expandtab: */ 
?>