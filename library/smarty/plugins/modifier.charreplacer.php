<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty plugin
 *
 * @author Martin Tsvetanov
 * @param string
 * @return string
 */
function smarty_modifier_charreplacer($string)
{
	if($_SESSION['language'] == 'farsi'){
	
		$replace = array(
			'1'	=>	'١',
			'2'	=>	'۲',
			'3'	=>	'۳',
			'4'	=>	'۴',
			'5'	=>	'۵',
			'6'	=>	'۶',
			'7'	=>	'۷',
			'8'	=>	'۸',
			'9'	=>	'۹',
			'0'	=>	'۰'
		);
		
		foreach($replace AS $k => $v){
			$string = str_replace($k, $v, $string);
		}
	}
	else {
		$string = str_replace('&', '&amp;', $string);
	}
	
	return $string;
}

/* vim: set expandtab: */

?>
