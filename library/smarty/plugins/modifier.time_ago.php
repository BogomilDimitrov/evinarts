<?php 
/** 
 * Smarty plugin 
 * @package Smarty 
 * @subpackage plugins 
 */ 

/** 
 * Smarty date modifier plugin 
 * Purpose:  converts unix timestamps or datetime strings to words 
 * Type:     modifier
 * Name:     timeAgo
 * @author   Stephan Otto 
 * @param string 
 * @return string 
 */ 

function smarty_modifier_time_ago($date) { 

	$lang = Registry::get('lang');

	$timeStrings = array($lang['timespan_just_now'],
					$lang['timespan_second_ago'], $lang['timespan_seconds_ago'],	// 1,1 
					$lang['timespan_minute_ago'], $lang['timespan_minutes_ago'],	// 3,3 
					$lang['timespan_hour_ago'], $lang['timespan_hours_ago'],		// 5,5 
					$lang['timespan_day_ago'], $lang['timespan_days_ago'],			// 7,7 
					$lang['timespan_week_ago'], $lang['timespan_weeks_ago'],		// 9,9 
					$lang['timespan_month_ago'], $lang['timespan_months_ago'],		// 11,12 
					$lang['timespan_year_ago'], $lang['timespan_years_ago']);		// 13,14 
	$debug = false; 
	$sec = time() - (( strtotime($date)) ? strtotime($date) : $date); 

	// $lang['posted'] .
	
	if ( $sec <= 0) return  $timeStrings[0]; 

	if ( $sec < 2) return  $sec." ".$timeStrings[1]; 
	if ( $sec < 60) return $sec." ".$timeStrings[2]; 

	$min = $sec / 60; 
	if ( floor($min+0.5) < 2) return floor($min+0.5)." ".$timeStrings[3]; 
	if ( $min < 60) return  floor($min+0.5)." ".$timeStrings[4]; 

	$hrs = $min / 60; 
	echo ($debug == true) ? $lang['timespan_hours'] . ": ".floor($hrs+0.5)."<br />" : ''; 
	if ( floor($hrs+0.5) < 2) return floor($hrs+0.5)." ".$timeStrings[5]; 
	
	if ( $hrs <= 48) return  floor($hrs+0.5)." ".$timeStrings[6];
	else return  date('F  d, Y', strtotime($date));
} 

?> 
