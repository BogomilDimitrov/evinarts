<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty escape modifier plugin
 *
 * Type:     modifier<br>
 * Name:     escape<br>
 * Purpose:  Escape the string according to escapement type
 * @link http://smarty.php.net/manual/en/language.modifier.escape.php
 *          escape (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @param html|htmlall|url|quotes|hex|hexentity|javascript
 * @return string
 */
function smarty_modifier_convert($string, $lang = "latin")
{
    $cyr  = array('а','б','в','г','д','е','ж','з','и','й','к','л','м','н','о','п','р','с','т','у', 'ф','х','ц','ч','ш','щ','ъ','ь', 'ю','я','А','Б','В','Г','Д','Е','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У', 'Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ь', 'Ю','Я' );
	$lat = array( 'a','b','v','g','d','e','zh','z','i','y','k','l','m','n','o','p','r','s','t','u',
	'f' ,'h' ,'ts' ,'ch','sh' ,'sht' ,'a' ,'y' ,'yu' ,'ya','A','B','V','G','D','E','Zh','Z','I','Y','K','L','M','N','O','P','R','S','T','U',
	'F' ,'H' ,'Ts' ,'Ch','Sh' ,'Sht' ,'A' ,'Y' ,'Yu' ,'Ya' );
	
	if($lang == 'latin'){

		$string = str_replace($cyr, $lat, $string);
		
		// Remove any unwanted chars in the url
		$string = preg_replace("/[^a-zA-Z0-9\/_| -]/", '', $string);
		
	} else if ($lang == 'cyrilic'){
		$string = str_replace($lat, $cyr, $string);
	}
	
	$string = str_replace(" ", "-", $string);
	
	// Replace more than one - with nothing
	$dashes = array(
		"--",
		"---",
		"----",
		"-----",
		"------",
		"-------",
		"--------",
		"---------",
		"----------",
	);

	return str_replace($dashes, "", $string);

}

/* vim: set expandtab: */

?>
