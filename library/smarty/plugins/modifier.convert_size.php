<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty escape modifier plugin
 *
 * Type:     modifier<br>
 * Name:     convert_size<br>
 */
function smarty_modifier_convert_size($size)
{
	
	// If it's more than 1 MB - show file size in MB
    if($size / 1048576 >= 1){
		return (round($size / 1048576, 2))." MB";
		
	// If it's less than 1 MB - show file size in Kb
	} else {
		return round($size / 1024)." Kb"; 
	}
}

?>
