<?php

/**
 * Balkanec.bg 1.0
 *
 * This file may not be redistributed in whole or significant part.
 * --------------- File IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 2013 All Rights Reserved.
 *
 * Pager - Pagination for the data
 *
 * @file            Pager.php
 * @subpackage      Pager
 * @category        Packages
 * @author          Elenko Ivanov
 *
 */
class Pager {

    public $link;
    public $replace;
    public $page = 0;
    public $rows_per_page = 12;
    public $max_links_shown = 10;
    public $limit;
    public $lang;

    // Paginate
    public function paginate($all) {
	
        $this->lang = Registry::get('lang');
		Loader::loadClass('Common');
		
        $return = "";
        $this->limit = ($this->page * $this->rows_per_page) - $this->rows_per_page;
		
        $totalPage = ceil($all / $this->rows_per_page);
        if ($totalPage > 1) {
			
            if (($this->max_links_shown % 2) != 0) {
                $first_page_link = $this->page - (($this->max_links_shown - 1) / 2);
                $last_page_link = $this->page + (($this->max_links_shown - 1) / 2);
            } else {
                $first_page_link = $this->page - ($this->max_links_shown / 2) + 1;
                $last_page_link = $this->page + ($this->max_links_shown / 2);
            }

            if ($first_page_link < 1) {
                for ($i = $first_page_link; $i < 1; $i++) {
                    $last_page_link++;
                }
                $first_page_link = 1;
            }

            if ($last_page_link > $totalPage) {
                $last_page_link = $totalPage;
            }

            if ($this->page != 1) {
                if (strpos($this->link, $this->replace)) {
                    $prev_link = str_replace($this->replace, ($this->page - 1), $this->link);
                } else
                    $prev_link = $this->link . ($this->page - 1);
            }

            for ($i = $first_page_link; $i <= $last_page_link; $i++) {
                if ($i == $this->page) {
                    $return .= '<span class="selected-page">' . $i . '</span>';
                } else {

                    if (strpos($this->link, $this->replace)) {
                        $return .= '<span><a href="' . str_replace($this->replace, $i, $this->link) . '" >' . $i . '</a></span>';
                    } else
                        $return .= '<span><a href="' . $this->link . $i . '" >' . $i . '</a></span>';
                }
            }

            if ($this->page != $totalPage) {
                if (strpos($this->link, $this->replace)) {
                    $next_link = str_replace($this->replace, ($this->page + 1), $this->link);
                } else
                    $next_link = $this->link . ($this->page + 1);
            }
        }

        return array($return, $totalPage, $prev_link, $next_link);
    }
}

?>