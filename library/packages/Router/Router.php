<?php

/**
 * This file may not be redistributed in whole or significant part.
 * --------------- THIS IS NOT FREE SOFTWARE ----------------
 *
 * Copyright 20013 All Rights Reserved.
 *
 * Router class
 *
 * @file             System.class.php
 * @subpackage       Main
 * @category         Router
 * @author           Elenko Ivanov
 *
 */
class Router {

    // Check if we have a rewrite rule for the current url
    public static function checkURL() {

        // Get the current url
        $url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $url = urldecode($url);
		$rq = urldecode($_SERVER['REQUEST_URI']);
		
		// Get only first parameter
		$params = explode('/', substr($rq, 1));
		
		$db = Registry::get('db');
		$lg = Registry::get('lg');
		
        // Pages router
		if($params[1] == 'aboutus' || $params[1] == 'terms'){

			// Set controller and action
			$rewrite['controller'] = 'pages';
			$rewrite['action'] = 'view';
			
			// Array to return
			$data = array(
				'idpage' 	=> $params[1]
			);
			
			// Set router params - later we will get them in the controllers
			Registry::set($data, 'router');
		}
		else if($params[1] == 'sitemap'){
			
			// Set controller and action
			$rewrite['controller'] 	= 'pages';
			$rewrite['action'] 		= 'sitemap';
			
			// Set router params - later we will get them in the controllers
			Registry::set($data, 'router');
		}
		
		else if($params[1] == 'contacts'){
			
			// Set controller and action
			$rewrite['controller'] 	= 'pages';
			$rewrite['action'] 		= 'contacts';
			
			// Set router params - later we will get them in the controllers
			Registry::set($data, 'router');
		}
		else if($params[1] == 'archive'){
			
			// Set controller and action
			$rewrite['controller'] 	= 'gallery';
			$rewrite['action'] 		= 'index';
			
			$data = array(
				'idcat' => 24
			);		
				
			// Set router params - later we will get them in the controllers
			Registry::set($data, 'router');
		}
		
		else if($params[1] == 'cards'){
			
			// Set controller and action
			$rewrite['controller'] 	= 'gallery';
			$rewrite['action'] 		= 'index';
			
			$data = array(
				'idcat' => 28
			);		
				
			// Set router params - later we will get them in the controllers
			Registry::set($data, 'router');
		}
		
		// Категории декорации
		else if( $params[1] == 'decorations' AND !empty($params[2]) AND $params[2] != 'index' ){
			
			// If viewing category
			if(stristr($params[2], '-') !== false){
				
				// Separate dashes
				$dashSeparated = explode('-', $params[2]);

				// Get idcategory id
				$idcategory = end($dashSeparated);

				// Remove product id from string
				array_pop($dashSeparated);

				// Get product slug
				$slug = implode('-', $dashSeparated);

				// If eveything is ok
				if(!empty($slug) AND !empty($idcategory)){

					// Array to return
					$data = array(
						'idcategory' => $idcategory,
						'slug'		=> $slug
					);
				}
				
				// Set controller and action
				$rewrite['controller'] 	= 'gallery';
				$rewrite['action'] 		= 'category';
			}
			
			// Set router params - later we will get them in the controllers
            Registry::set($data, 'router');
		}
		
		else if($params[1] == 'decorations'){
			
			// Set controller and action
			$rewrite['controller'] 	= 'gallery';
			$rewrite['action'] 		= 'index';
			
			$data = array(
				'idcat' => 21
			);		
				
			// Set router params - later we will get them in the controllers
			Registry::set($data, 'router');
		}
		
		// Преглед на картина
		else if(Loader::controllerExists($params[1]) == false AND !empty($params[1]) 
										AND ( empty($params[2])) ){
						
			$dashSeparated = explode('-', $params[1]);

			// Get product id
			$id = (int)end($dashSeparated);

			// Remove product id from string
			array_pop($dashSeparated);

			// Get product slug
			$slug = strip_tags(trim(implode('-', $dashSeparated)));
			
			// If eveything is ok
			if(!empty($slug) AND !empty($id)){

				// Array to return
				$data = array(
					'idproduct' => $id,
					'slug'		=> $slug
				);
			}
				
			// Set controller and action
			$rewrite['controller'] 	= 'gallery';
			$rewrite['action'] 		= 'view';
						
            // Set router params - later we will get them in the controllers
            Registry::set($data, 'router');
        }
										
		// Категории картини
		else if( $params[1] == 'gallery' AND !empty($params[2]) AND $params[2] != 'index' ){
			
			// If viewing category
			if(stristr($params[2], '-') !== false){
				
				// Separate dashes
				$dashSeparated = explode('-', $params[2]);

				// Get idcategory id
				$idcategory = end($dashSeparated);

				// Remove product id from string
				array_pop($dashSeparated);

				// Get product slug
				$slug = implode('-', $dashSeparated);

				// If eveything is ok
				if(!empty($slug) AND !empty($idcategory)){

					// Array to return
					$data = array(
						'idcategory' => $idcategory,
						'slug'		=> $slug
					);
				}
				
				// Set controller and action
				$rewrite['controller'] 	= 'gallery';
				$rewrite['action'] 		= 'category';
			}
			
			// Set router params - later we will get them in the controllers
            Registry::set($data, 'router');
		}
			
		// News preview
		else if( $params[1] == 'news' AND !empty($params[1]) AND !empty($params[2]) ){
			
			// If viewing news
			if(stristr($params[2], '-') !== false){
				
				// Separate dashes
				$dashSeparated = explode('-', $params[2]);

				// Get news id
				$idnews = end($dashSeparated);

				// Remove news id from string
				array_pop($dashSeparated);

				// Get news slug
				$slug = implode('-', $dashSeparated);

				// If eveything is ok
				if(!empty($slug) AND !empty($idnews)){

					// Array to return
					$data = array(
						'idnews' => $idnews,
						'slug'	 => $slug
					);
				}

				// Set controller and action
				$rewrite['controller'] 	= 'news';
				$rewrite['action'] 		= 'view';
			}
			
			// Set router params - later we will get them in the controllers
            Registry::set($data, 'router');
		}	
		// Gallery preview
		else if( $params[1] == 'gallery' AND !empty($params[2]) AND $params[2] != 'page' ){
			
			// If viewing news
			if(stristr($params[2], '-') !== false){
				
				// Separate dashes
				$dashSeparated = explode('-', $params[2]);

				// Get gallery id
				$idgallery = end($dashSeparated);

				// Remove news id from string
				array_pop($dashSeparated);

				// Get news slug
				$slug = implode('-', $dashSeparated);

				// If eveything is ok
				if(!empty($slug) AND !empty($idgallery)){

					// Array to return
					$data = array(
						'idgallery' => $idgallery,
						'slug'	 	=> $slug
					);
				}
				
				// Set controller and action
				$rewrite['controller'] 	= 'gallery';
				$rewrite['action'] 		= 'view';
			}
			
			// Set router params - later we will get them in the controllers
            Registry::set($data, 'router');
		}
		
        // Return rewrite array
        return $rewrite;
    }
}

?>
