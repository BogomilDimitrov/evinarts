<?php
/**
* X3M 1.0
*    
* This file may not be redistributed in whole or significant part.
* --------------- X3M IS NOT FREE SOFTWARE ----------------
* 
* Copyright 2008-2009 All Rights Reserved.
* 
* Sort class
* 
* @file			Arrays.php
* @subpackage	Main
* @category		Packages
* @author		Jordan Trifonov
*   	
*/
class Arrays{

	// Sort 2D arrays (the result from a fetch all query for example)
	public function sort2d($array, $index, $order='asc', $natsort = FALSE, $case_sensitive = FALSE){

		if(is_array($array) && count($array)>0){

			foreach(array_keys($array) AS $key){
				$temp[$key] = $array[$key][$index];
			}

			if(!$natsort){
				($order == 'asc') ? asort($temp) : arsort($temp);
			} else {
				($case_sensitive) ? natsort($temp) : natcasesort($temp);
				if($order! = 'asc') 
					$temp = array_reverse($temp, TRUE);
			}

			foreach(array_keys($temp) AS $key) 
				(is_numeric($key)) ? $sorted[] = $array[$key] : $sorted[$key] = $array[$key];

			return $sorted;
		}

		return $array;
	}

}

?>
