<?php
/**
* X3M 1.0
*    
* This file may not be redistributed in whole or significant part.
* --------------- X3M IS NOT FREE SOFTWARE ----------------
* 
* Copyright 2008-2009 All Rights Reserved.
* 
* Crypt
* 
* @file			Crypt.php
* @subpackage	Crypt
* @category		Packages
* @author		Jordan Trifonov
*   	
*/
class Crypt {

	
    // Encrypts the data with MD5
	public function encrypt($data){
		return md5($data);
	}

	// SimpleCrypt
    function X3MEncrypt($string, $key='1rAn1aNpR0j3cTcRack1FUcAn88') {
        $result = '';
        for($i=0; $i<strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key))-1, 1);
            $ordChar = ord($char);
            $ordKeychar = ord($keychar);
            $sum = $ordChar + $ordKeychar;
            $char = chr($sum);
            $result.=$char;
        }
        return base64_encode($result);
    }
 
    function X3MDecrypt($string, $key='1rAn1aNpR0j3cTcRack1FUcAn88') {
        $result = '';
        $string = base64_decode($string);
        for($i=0; $i<strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key))-1, 1);
            $ordChar = ord($char);
            $ordKeychar = ord($keychar);
            $sum = $ordChar - $ordKeychar;
            $char = chr($sum);
            $result.=$char;
        }
        return $result;
    }
}

?>