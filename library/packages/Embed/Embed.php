<?php
/**
* X3M 1.0
*    
* This file may not be redistributed in whole or significant part.
* --------------- X3M IS NOT FREE SOFTWARE ----------------
* 
* Copyright 2008-2009 All Rights Reserved.
* 
* Embed
* 
* @file			Embed.php
* @subpackage	Embed
* @category		Packages
* @author		Martin Tsvetanoff
*   	
*/
class Embed {

	// Find video
	public function findEmbedVideo($data, $getDetails = false){
	
		// If there are video URLs
		if (preg_match_all('%https?:\/\/(?:[0-9A-Z-]+\.)?((?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})|vimeo.com\/([^(\&|$)]*))%i', $data, $matches)){

			// Loop all matches
			foreach($matches[0] as $i => $match){
			
				// Execute video embedding
				$embed = self::embedVideo($match, $getDetails);
			
				// Replace URL with video embed
				$data = str_replace($match, $embed['video'], $data);
			}
		}
		
		// Return result
		return $data;
	}

	// Detect video
	public function embedVideo($source, $getDetails = false, $preview = false){
		
		// define width, height
		$width = 595;
		$height = 350;
		
		// Change width
		if($preview){
			$width = 350;
			$height = 240;
		}
		
		// YouTube
		if(strpos(strtolower($source), 'youtube.com') !== false || strpos(strtolower($source), 'youtu.be') !== false){
			
			// Get video ID
			$video_id = self::getYouTubeVideoID($source);
			
			if(!empty($video_id)){
			
				// Construct result
				$result = array(
					'source'	=> 'youtube',
					'video' 	=> '<iframe width="'. $width .'" height="'. $height .'" src="http://www.youtube.com/embed/' . $video_id . '?wmode=transparent&amp;autohide=1&amp;egm=0&amp;hd=1&amp;iv_load_policy=3&amp;modestbranding=1&amp;rel=0&amp;showinfo=0&amp;showsearch=0&amp;autoplay=0" frameborder="0" allowfullscreen=""></iframe>',
					'thumb'		=> 'http://img.youtube.com/vi/' . $video_id . '/0.jpg'
				);
				
				// Get title and description if set
				if($getDetails == true){ 
					
					// Get video metadata
					$metadata = self::getYouTubeVideoDetails($video_id);
					
					// If OK Merge arrays
					if(!empty($metadata)) $result = array_merge($result, $metadata);
					
					// If error
					else $result = 'Error occured while trying to fetch video metadata';
				}
			}
			
			// Invalid video ID
			else $result = 'Invalid video ID';
		}

		// Unknown video source
		else $result = 'Unsupported video source';
		
		// Return result
		return $result;
	}

	// Get YouTube video ID
	private function getYouTubeVideoID($source) {

		preg_match('/https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/ytscreeningroom\?v=|\/feeds\/api\/videos\/|\/user\S*[^\w\-\s]|\S*[^\w\-\s]))([\w\-]{11})[?=&+%\w-]*/i', $source, $match);
		return $match[1];
	}
	
	// Get YouTube video details
	private function getYouTubeVideoDetails($video_id){

		// Get metadata
		$temp['video_raw_details'] = file_get_contents('http://gdata.youtube.com/feeds/api/videos/' . $video_id);
		
		// Check for errors
		if(strpos($temp['video_raw_details'], 'Invalid id')) return false;
		
		// Video title
		if(preg_match("/<media:title type=\'plain\'>(.*?)<\/media:title>/", $temp['video_raw_details'], $matches) != 0) $video_details['title'] = $matches[1];
		
		// Video description
		if(preg_match("/<media:description type=\'plain\'>(.*?)<\/media:description>/", $temp['video_raw_details'], $matches) != 0) $video_details['description'] = $matches[1];

		// Return response
		return $video_details;		
	}

	// Get Vimeo video ID
	private function getVimeoVideoID($source) {

		preg_match("/vimeo.com\/([^(\&|$)]*)/", $source, $match);
		return $match[1];
	}
	
	// Get Vimeo video details
	private function getVimeoVideoDetails($video_id){

		// Get metadata
		$temp['video_raw_details'] = file_get_contents('http://vimeo.com/api/v2/video/' . $video_id . '.json');
		
		// Check for errors
		if(strpos($temp['video_raw_details'], 'not found')) return false;
		
		// Decode video data
		else $temp['video_details'] = (array) reset(json_decode($temp['video_raw_details']));

		// Video title
		if(!empty($temp['video_details']['title'])) $video_details['title'] = $temp['video_details']['title'];
		
		// Video description
		if(!empty($temp['video_details']['description'])) $video_details['description'] = $temp['video_details']['description'];

		// Video thumbnail
		if(!empty($temp['video_details']['thumbnail_large'])) $video_details['thumbnail_large'] = $temp['video_details']['thumbnail_large'];
		
		// Return response
		return $video_details;		
	}
}

?>