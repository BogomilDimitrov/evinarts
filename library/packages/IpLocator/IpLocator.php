<?php
/**
* X3M 1.0
*    
* This file may not be redistributed in whole or significant part.
* --------------- X3M IS NOT FREE SOFTWARE ----------------
* 
* Copyright 2008-2009 All Rights Reserved.
* 
* Ip Locator - Detect country code, country and city
* 
* @file			IpLocator.class.php
* @subpackage	IpLocator
* @category		Packages
* @author		Jordan Trifonov
*   	
*/
Class IpLocator{
	
	// Get all location data
	public function getLocation(){
		
		include(ROOT_PATH . "library/packages/IpLocator/geoipcity.inc");
		include(ROOT_PATH . "library/packages/IpLocator/geoipregionvars.php");

		$gi = geoip_open(ROOT_PATH . "library/packages/IpLocator/GeoIPCity.dat", GEOIP_STANDARD);
		$record = geoip_record_by_addr($gi, $_SERVER['REMOTE_ADDR']);
		
		$return = array(
			"code" => $record->country_code,
			"country" => $record->country_name,
			"city" => $record->city
		);
		
		geoip_close($gi);
		return $return;
	}
}

?>
