<?php
/**
* Balkanec.bg v.1.1
*    
* This file may not be redistributed in whole or significant part.
* --------------- Balkanec.bg IS NOT FREE SOFTWARE ----------------
* 
* Copyright 2013 All Rights Reserved.
* 
* Common
* 
* @file			Common.php
* @subpackage	Common
* @category		Packages
* @author		Elenko Ivanov
*   	
*/
class Common {
	
	// Build post content
	public static function buildProduct($post, $productPreview = ''){
		
		// Get registry variables
		$db 		= Registry::get('db');
		$config 	= Registry::get('config');
		$lg 		= Registry::get('lg');
		
		//$memcache = Registry::get('memcache');
		
		// Get homepage blogs from memcache
		//$memcache_post = $memcache->get('idproduct_' . $post['idproduct']);

		// If no cache or has expired
		if(empty($memcache_post)){
		
			// Build URL
			$post['url'] = self::buildUrl($post, 'product');
				
			$post['price_1'] = self::calculatePrice($post['price_1']);
			
			// If post has ID and preview posts
			if(!empty($productPreview)){
				
				// Select all images
				$post['images'] = $db->fetchAll("SELECT `idpicture`, `picture`
													FROM `products_picture` WHERE `idproduct`=" . $post['idproduct']);
			}
			
			// Return built post
			return $post;
		}
		
		// If we've got cache
		else {
		
			// Return cached post
			return $memcache_post;
		}
	}
	
	// Calculate price
	public static function calculatePrice($price){
		
		// Get current currency price
		if(!$_SESSION['curenncyData']){
			
			// Get registry variables
			$db = Registry::get('db');
		
			$_SESSION['curenncyData'] = $db->fetchRow("SELECT * FROM `currency` WHERE `gbp` !='' and `usd` != '' ORDER BY `date_added` DESC LIMIT 1");
		}
		
		$priceDb = $_SESSION['curenncyData'];
		
		if($_SESSION['currency'] == 'leva') return $price;
		else if($_SESSION['currency'] == 'gbp'){
			
			$priceLv 	= $price * 2;
			$priceReal = round($priceLv / $priceDb['gbp'], 2);
		}
		else if ($_SESSION['currency'] == 'usd'){
		
			$priceLv 	= $price * 2;
			$priceReal = round($priceLv / $priceDb['usd'], 2);
		}
		else if ($_SESSION['currency'] == 'eur'){
			$priceLv 	= $price / 2;
			$priceReal = round($priceLv, 0);
		}
		
		return $priceReal;
	}
	
	// Build show number
	public static function numShowProd($section, $show){
	
		// Define array
		$showArr = array(20=>20, 40=>40, 60=>60);
		
		if($showArr[$show])
			$_SESSION[$section]['show'] = $show; 
		else if (!$_SESSION[$section]['show'])
			$_SESSION[$section]['show'] = 20;

	}
	
	// Build price order
	public static function numPriceProd($section, $price){
	
		// Define price array
		$priceArr = array( 1 => 99, 2 => 'ASC', 3 => 'DESC');
		
		if($priceArr[$price] == 99){ // Сортира се по подразбиране, по дата DESC
			$_SESSION[$section]['priceOrder'] = 1;
			$_SESSION[$section]['price'] = '';
		}
		else if($priceArr[$price]){
			$_SESSION[$section]['priceOrder'] = $price;
			$_SESSION[$section]['price'] = $priceArr[$price]; 
		}
		else if (!$_SESSION[$section]['price']){ // Сортира се по подразбиране, по дата DESC
			$_SESSION[$section]['priceOrder'] = 1;
			$_SESSION[$section]['price'] = '';
		}
	}
		
		
	// Build product/comment/event url
	public static function buildUrl($data, $type = 'product'){
	
		// Get registry variables
		$config = Registry::get('config');
		$lg		= Registry::get('lg');
	
		// Product URL
		if($type == 'product'){
			// Build product URL
			return $config['site_url'] . $lg . '/' . $data['alias'] . '-' . $data['idproduct'];
		}
	}
	
		
	// Insert user login attempt
	public static function insertLoginAttempt($iduser, $idsession, $type, $status){
	
		// Get registry variables
		$db = Registry::get('db');
	
		// Prepare data for inserting in login table
		$data = array(
			'iduser' 		=> $iduser,
			'idsession'		=> $idsession,
			'type'			=> $type,
			'status'		=> $status,
			'ip_address'	=> $_SERVER['REMOTE_ADDR'],
			'date_login'	=> date("Y-m-d H:i:s")
		);	

		// Insert in database
		$db->insert('users_login', $data, false);
	}
	
	// Email templates
	public static function getEmailTemplate($type, $data = '', $show_template = true){
	
		// Get registry variables
		$smarty = Registry::get('smarty');
		$config = Registry::get('config');
		$lang 	= Registry::get('lang');

		// Switch between email types
		switch($type){
			
			// Промяна на статуса
			case 'status-confirm':
			
			break;
			
			// Темплейт когато е направена поръчка, до клиента
			case 'order-complete':
				// Set email content
				$email = array(
					'title' 			=> '',
					'slogan'			=> '',
					'phone'				=> $data['phone'],
					'date_order'		=> $data['date_create'],
					'idorder'			=> $data['idorder'],
					
					'orderTotal' 		=> $data['orderTotal'],
					'orderDeliveryPay'	=> $data['orderDeliveryPay'],
					'total'				=> $data['orderTotal'] + $data['orderDeliveryPay'], 
					
					'shipping_method' 	=> $data['orderDelivery'], 
					'pay_method' 		=> $data['orderPay'],
					
					'orderDiscount' 	=> $data['orderDiscount'],
					'orderPercentPromo'	=> $data['orderPercentPromo'],
					
					'description'		=> $data['description'],
					'shipping'			=> $data['shipping'],
					'payment'			=> $data['payment'],
					'products'			=> $data['products'],
					'currency'			=> $data['currency'],
					'content'			=> str_replace('##idorder##', $data['idorder'], $lang['order_email_text']),
				);
				
				// Fetch email design from template
				$smarty->assign('email', $email);
				$smarty->assign('config', $config);
				
				// Show template
				$email = $smarty->fetch('pages/email_order.tpl.html');
				
				return $email;
			
			break;
		}
		
		// Show email template
		if($show_template == true){
		
			// Fetch email design from template
			$smarty->assign('email', $email);
			$smarty->assign('config', $config);
			
			// Show template
			$email = $smarty->fetch('pages/email.tpl.html');
		}
		
		// Return response
		return $email;
	}

    // Get remote file via cURL
    public function curlGetFile($url) {
	
		// Parse URL
		$parsed_url = parse_url($url);
		
		// Build referer
		$referer = $parsed_url['scheme'] . '://' . $parsed_url['host'];

        // Get remote file content
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 Windows NT 5.1 AppleWebKit/536.5 KHTML, like Gecko Chrome/19.0.1084.56 Safari/536.5');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_REFERER, $referer); 
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $content = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

		// Return data
        return array(
            'content' => $content,
            'info' => $info
        );
    }

	// Generate random string
	public static function generateRandomString($length) {
		
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$string = '';
		
		for ($p = 0; $p < $length; $p++) {
			$string .= $characters[mt_rand(0, mb_strlen($characters, 'UTF-8'))];
		} 

		return $string;
	}
	
	// Timespan convertion
	public static function timeAgo($date){

		$lang = Registry::get('lang');

		$timeStrings = array($lang['timespan_just_now'],
						$lang['timespan_second_ago'], $lang['timespan_seconds_ago'],	// 1,1 
						$lang['timespan_minute_ago'], $lang['timespan_minutes_ago'],	// 3,3 
						$lang['timespan_hour_ago'], $lang['timespan_hours_ago'],		// 5,5 
						$lang['timespan_day_ago'], $lang['timespan_days_ago'],			// 7,7 
						$lang['timespan_week_ago'], $lang['timespan_weeks_ago'],		// 9,9 
						$lang['timespan_month_ago'], $lang['timespan_months_ago'],		// 11,12 
						$lang['timespan_year_ago'], $lang['timespan_years_ago']);		// 13,14 
		$debug = false; 
		$sec = time() - (( strtotime($date)) ? strtotime($date) : $date); 

		if ( $sec <= 0) return $timeStrings[0]; 

		if ( $sec < 2) return $sec." ".$timeStrings[1]; 
		if ( $sec < 60) return $sec." ".$timeStrings[2]; 

		$min = $sec / 60; 
		if ( floor($min+0.5) < 2) return floor($min+0.5)." ".$timeStrings[3]; 
		if ( $min < 60) return floor($min+0.5)." ".$timeStrings[4]; 

		$hrs = $min / 60; 
		echo ($debug == true) ? $lang['timespan_hours'] . ": ".floor($hrs+0.5)."<br />" : ''; 
		if ( floor($hrs+0.5) < 2) return floor($hrs+0.5)." ".$timeStrings[5]; 
		
		if ( $hrs <= 48) return floor($hrs+0.5)." ".$timeStrings[6];
		else return date('F  d, Y', strtotime($date)); 
	}
	
	// Генерираме цената да е с .00 накрая, ако няма.
	public function roundPreDi($value){
		//sprintf("%01.2f", $money)
		$newRound = sprintf("%01.2f", round($value,2));
		return $newRound;
	}
	
	// 404 Not Found
	public function showPage($idpage) {
	
		// Get registry variables
		$smarty = Registry::get('smarty');
	
		// Switch different page types
		switch($idpage){
		
			// 404
			case 404:
			
				// Return error header
				header('HTTP/1.0 404 Not Found', false, 404);
				
				$meta['title'] = 'Page is not found';
				$smarty->assign('meta', $meta);
				
				// Assign values and display template
				$smarty->assign('page', 'error');
				$smarty->assign('error', 'Error 404');
				$smarty->display('pages/errors/404.tpl.html');

			break;
		}
	
		// Terminate application
		exit();
	}
	
	// Лимитиране на стринга
	public function str_cut($string, $max_length){
		$string = strip_tags($string);
		if (mb_strlen($string, 'UTF-8') > $max_length){
			$string = mb_substr($string, 0, $max_length, 'UTF-8');
			$pos = mb_strrpos($string, " ", " ", 'UTF-8');
			if($pos === false) {
				return mb_substr($string, 0, $max_length, 'UTF-8')."...";
			}
			return mb_substr($string, 0, $pos, 'UTF-8')."...";
		} else{
			return $string;
		}
	}
	
	/**
	 * Редактира цена
	 * пр. 30000 = 30,000	 
	 * 	 	 	 	 
	 * @author		Elenko Ivanov	 	 
	 * @date		30.04.2014
	 * @version		1.0.0	 
	 */	
	public static function splitPrice($price){
		if( strlen($price) > 3 AND strlen($price) < 7 ){
			$b = substr($price, -3, 3);
			$f = substr($price, 0, -3);
			
			return "$f,$b"; 
		}
		elseif( strlen($price) > 7){
			$b = substr($price, -3, 3);
			$f = substr($price, -6, -3);
			$l = substr($price, 0, -6);			
			
			return "$l,$f,$b"; 
		}
		else{
			return $price;
		}		
	}
}
?>