<?php
/**
* Balkanec.bg
*    
* This file may not be redistributed in whole or significant part.
* --------------- Balkanec.bg IS NOT FREE SOFTWARE ----------------
* 
* Copyright 2015 All Rights Reserved.
* 
* Common
* 
* @file			Rss.php
* @subpackage	Rss
* @category		Packages
* @author		Elenko Ivanov
*   	
*/
class Rss {

	private $xml;

	// Initialization function
	public function init($website){
	
		// Set header type to XML
		header('Content-Type: text/xml');

		// Construct XML header
		$this->xml  = '<?xml version="1.0" encoding="UTF-8"?>';
		$this->xml .= '<rss version="2.0">';
		$this->xml .= '<channel>';
		$this->xml .= '<title>' . $website['title'] . '</title>';
		$this->xml .= '<description>' . $website['description'] . '</description>';
		$this->xml .= '<link>' . $website['link'] . '</link>';
	}

	// Add items to feed
	public function addItem($itemsArray){
	
		// Add items to feed
		foreach($itemsArray AS $item){
		
			$this->xml .= '<item>';
			$this->xml .= '<title>' . Filter::sanitize($item['title']) . '</title>';
			$this->xml .= '<description><![CDATA[' . Filter::sanitize($item['content']) . ' ]]></description>';
			$this->xml .= '<link>' . $item['link'] . '</link>';
			
			/* $this->xml .= '<image>';
			$this->xml .= '<url>http://www.w3schools.com/images/logo.gif</url>';
			$this->xml .= '<title>W3Schools.com</title>';
			$this->xml .= '<link>http://www.w3schools.com</link>';
			$this->xml .= '</image>'; */
			
			$this->xml .= '<pubDate>' . $item['date_added'] . ' GMT</pubDate>';
			$this->xml .= '</item>';
		}
	}
	
	// Show feed
	public function show(){

		// Finalize feed
		$this->xml .= '</channel>';
		$this->xml .= '</rss>';
		
		// Return feed
		return $this->xml;
	}
}
?>