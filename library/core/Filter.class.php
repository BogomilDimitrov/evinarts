<?php

/**
 * X3M 1.0
 *    
 * This file may not be redistributed in whole or significant part.
 * --------------- X3M IS NOT FREE SOFTWARE ----------------
 * 
 * Copyright 2008-2009 All Rights Reserved.
 * 
 * Filter class - Filtering the input data
 * 
 * @file			Filter.class.php
 * @subpackage	Main
 * @category		Core
 * @author		Jordan Trifonov
 *   	
 */
class Filter {

    // Return only letters and numbers
    public static function noChars($str = "") {
        return preg_replace('/[^a-z0-9]+/i', "", $str);
    }

    // Return only letters
    public static function getAlpha($filter) {
        return preg_replace("/[^[:alpha]]/", "", $filter);
    }

    // Return only numbers
    public static function getNums($filter) {
        return preg_replace("/[^\d]/", "", $filter);
    }
	 
    // Return only letters, numbers, dots and underscores
    public static function noSpecialChars($str = "") {
        return preg_replace('/[^a-z0-9._]+/i', "", $str);
    }

    // Clear the html tags
    public static function clear($str = "") {
        return strip_tags(trim($str));
    }
	
    // Sanitize data
    public static function sanitize($str, $allowed_tags = '', $linkify = false, $remove_empty = false) {

		require_once('library/packages/HTMLPurifier/HTMLPurifier.auto.php');
		$config = HTMLPurifier_Config::createDefault();
		
		$str = htmlspecialchars_decode($str);
		$str = stripslashes($str);
		$str = trim($str);
		
		if($linkify == true){
			$config->set('AutoFormat.Linkify', true);
		}
		
		if($remove_empty == true){
			$config->set('AutoFormat.RemoveEmpty', true);
		}

		$config->set('HTML.TargetBlank', true);
		
		if(!empty($allowed_tags)){
			$config->set('AutoFormat.AutoParagraph', true);
			$config->set('HTML.FlashAllowFullScreen', true);
			$config->set('HTML.SafeEmbed', true);
			$config->set('HTML.SafeObject', true);
			$config->set('HTML.SafeIframe', true);
			$config->set('URI.SafeIframeRegexp', '%^http://(www.youtube.com/embed/|player.vimeo.com/video/)%');
			$config->set('HTML.AllowedElements', $allowed_tags);
			$config->set('HTML.AllowedAttributes', 'href,src,width,height,alt,class,style,frameborder');
			$config->set('CSS.AllowedProperties', 'float,text-align,padding-left,padding-right,margin-bottom');
			$config->set('Output.FlashCompat', true);
		} else {
			$str = strip_tags($str);
		}

		$purifier = new HTMLPurifier($config);
		$str = $purifier->purify($str);
		$str = htmlspecialchars($str, ENT_QUOTES, 'UTF-8', false);
		
		return $str;
    }

	// Unsanitize data
	public static function unsanitize($data){
	
		if(is_array($data))		
			foreach ($data as $k => $v) $result[$k] = is_array($v) ? self::unsanitize($v) : htmlspecialchars_decode($v, ENT_QUOTES);
		else
			$result = htmlspecialchars_decode($data, ENT_QUOTES);
		return $result;
	}
	
    // Check user email
    public static function checkEmail($email) {
        if ((preg_match('/(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/', $email)) || (preg_match('/^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/', $email)))
            return true;
        else
            return false;
    }

    // Check IP address
    public static function checkIp($ip) {
        return preg_match("/^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])" . "(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$/", $ip);
    }

	// Check if URL is valid
	public static function isValidURL($url) {
		return preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $url);
	}

}

?>