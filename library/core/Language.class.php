<?php
/**
* X3M 1.0
*    
* This file may not be redistributed in whole or significant part.
* --------------- X3M IS NOT FREE SOFTWARE ----------------
* 
* Copyright 2008-2009 All Rights Reserved.
* 
* Language class - loads current language
* 
* @file			Language.class.php
* @subpackage	Main
* @category		Core
* @author		Jordan Trifonov
*   	
*/
class Language{

	// Load language
	public static function load($lang_code){
		
		// Get language files path
		$path = ROOT_PATH . "application/lang/" . strtolower($lang_code) . "/";

		// Create a handler for the directory
		$handler = opendir($path);

		// Keep going until all files in directory have been read
		while ($file = readdir($handler)) {

			// If the results is file
			if ($file != '.' && $file != '..' && $file != '.svn'){
			
				// Include the language file
				include_once($path . $file);
				
			}
		}

		// Close the handler
		closedir($handler);
		
		// Return language files data
		return $lang;
	}

}

?>