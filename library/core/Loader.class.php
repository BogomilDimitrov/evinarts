<?php
/**
* X3M 1.0
*    
* This file may not be redistributed in whole or significant part.
* --------------- X3M IS NOT FREE SOFTWARE ----------------
* 
* Copyright 2008-2009 All Rights Reserved.
* 
* Loader class - Load the models, pachages and etc.
* 
* @file			Loader.class.php
* @subpackage	Main
* @category		Core
* @author		Jordan Trifonov
*   	
*/
class Loader{

	// Find and load class
	public static function loadClass($class){
		
		// If the class or the interface are not there return
		if (class_exists($class, false) || interface_exists($class, false)) {
			return;
		}
		
		// Check libraries paths
		if(is_file("library/packages/".$class."/".$class.".php")){

			// If everything is ok include the class file
			return include_once "library/packages/".$class."/".$class.".php";
		}
		
		// Check models paths
		else if(is_file("application/models/".$class.".php")){
		
			// If everything is ok include the class file
			return include_once "application/models/".$class.".php";
		}
	}
	
	// Checks if a controller exists
	public static function controllerExists($controller){
	
		$file_path = "application/controllers/" . ucfirst(strtolower($controller)) . "Controller.php";
		
		// Check if the controller file exists
		if(is_file($file_path)){
		
			// Controller exists - return true
			return true;
		}else{
		
			// Controller does not exist - return false
			return false;
		}
	}
}

?>