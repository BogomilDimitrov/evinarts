<?php
	
	// Turn off script execution time limit
	set_time_limit(0);

	// Configuration
	ini_set('memory_limit', '1164M');
	ini_set('display_errors', 0); 
	ini_set('zend.ze1_compatibility_mode', 'off');
	
	// Define root path
	//define('ROOT_PATH', dirname( __FILE__ ) ."/../" );

	// Load main framework classes
	require_once('/www/vento-k.com/www/root/library/core/Filter.class.php');
	require_once('/www/vento-k.com/www/root/library/core/Database.class.php');
	require_once('/www/vento-k.com/www/root/library/packages/System/System.php');
	require_once('/www/vento-k.com/www/root/library/core/Registry.class.php');

	// Load config
	require_once('/www/vento-k.com/www/root/application/config.php');

	// Initialize db object for the queries
	$db = new Database($db_config);
	
	// Start the registry
	Registry::set($db, 'db');
	Registry::set($config, 'config');
	
	
		$filename = $config['site_dir'] . 'sitemap.xml';
		
        $rssfeed = '<?xml version="1.0" encoding="UTF-8"?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
   		$rssfeed .="\n";
		$rssfeed .='<url>';
		$rssfeed .="\n";
      	$rssfeed .='<loc>' . $config['site_url'] . 'bg/production</loc>';
		$rssfeed .="\n";
      	$rssfeed .='<changefreq>monthly</changefreq>';
		$rssfeed .="\n";
		$rssfeed .='<priority>0.3</priority>';
	   	$rssfeed .="\n";
   		$rssfeed .='</url>';
   		$rssfeed .="\n";	
		
		$productsList = $db->fetchAll("SELECT idproduct, alias_bg FROM products WHERE active!=4 and idcategory = 7"); 
		
		foreach($productsList as $k=>$product){
			
			$rssfeed .='<url>';
			$rssfeed .="\n";
	      	$rssfeed .='<loc>'. $config['site_url'] . 'bg/production/' . $product['alias_bg'] .'-'. $product['idproduct'] .'</loc>';
			$rssfeed .="\n";
	      	$rssfeed .='<changefreq>weekly</changefreq>';
			$rssfeed .="\n";
			$rssfeed .='<priority>0.6</priority>';
	   		$rssfeed .="\n";
	   		$rssfeed .='</url>';
	   		$rssfeed .="\n";
		}
		
		$rssfeed .='<url>';
		$rssfeed .="\n";
      	$rssfeed .='<loc>' . $config['site_url'] . 'bg/clients</loc>';
		$rssfeed .="\n";
      	$rssfeed .='<changefreq>monthly</changefreq>';
		$rssfeed .="\n";
		$rssfeed .='<priority>0.3</priority>';
	   	$rssfeed .="\n";
   		$rssfeed .='</url>';
   		$rssfeed .="\n";
		
		$rssfeed .='<url>';
		$rssfeed .="\n";
      	$rssfeed .='<loc>' . $config['site_url'] . 'bg/references</loc>';
		$rssfeed .="\n";
      	$rssfeed .='<changefreq>monthly</changefreq>';
		$rssfeed .="\n";
		$rssfeed .='<priority>0.3</priority>';
	   	$rssfeed .="\n";
   		$rssfeed .='</url>';
   		$rssfeed .="\n";
		
		$rssfeed .='<url>';
		$rssfeed .="\n";
      	$rssfeed .='<loc>' . $config['site_url'] . 'bg/gallery</loc>';
		$rssfeed .="\n";
      	$rssfeed .='<changefreq>monthly</changefreq>';
		$rssfeed .="\n";
		$rssfeed .='<priority>0.3</priority>';
	   	$rssfeed .="\n";
   		$rssfeed .='</url>';
   		$rssfeed .="\n";	
			
		$rssfeed .='<url>';
		$rssfeed .="\n";
      	$rssfeed .='<loc>' . $config['site_url'] . 'bg/catalogues</loc>';
		$rssfeed .="\n";
      	$rssfeed .='<changefreq>monthly</changefreq>';
		$rssfeed .="\n";
		$rssfeed .='<priority>0.3</priority>';
	   	$rssfeed .="\n";
   		$rssfeed .='</url>';
   		$rssfeed .="\n";
		
		$rssfeed .='<url>';
		$rssfeed .="\n";
      	$rssfeed .='<loc>' . $config['site_url'] . 'bg/contacts</loc>';
		$rssfeed .="\n";
      	$rssfeed .='<changefreq>monthly</changefreq>';
		$rssfeed .="\n";
		$rssfeed .='<priority>0.3</priority>';
	   	$rssfeed .="\n";
   		$rssfeed .='</url>';
   		$rssfeed .="\n";
		
		$rssfeed .='<url>';
		$rssfeed .="\n";
      	$rssfeed .='<loc>' . $config['site_url'] . 'bg/promotion</loc>';
		$rssfeed .="\n";
      	$rssfeed .='<changefreq>monthly</changefreq>';
		$rssfeed .="\n";
		$rssfeed .='<priority>0.3</priority>';
	   	$rssfeed .="\n";
   		$rssfeed .='</url>';
   		$rssfeed .="\n";
		
		$rssfeed .='<url>';
		$rssfeed .="\n";
      	$rssfeed .='<loc>' . $config['site_url'] . 'bg/client-services</loc>';
		$rssfeed .="\n";
      	$rssfeed .='<changefreq>monthly</changefreq>';
		$rssfeed .="\n";
		$rssfeed .='<priority>0.3</priority>';
	   	$rssfeed .="\n";
   		$rssfeed .='</url>';
   		$rssfeed .="\n";
		
		$rssfeed .='<url>';
		$rssfeed .="\n";
      	$rssfeed .='<loc>' . $config['site_url'] . 'bg/leasing</loc>';
		$rssfeed .="\n";
      	$rssfeed .='<changefreq>monthly</changefreq>';
		$rssfeed .="\n";
		$rssfeed .='<priority>0.3</priority>';
	   	$rssfeed .="\n";
   		$rssfeed .='</url>';
   		$rssfeed .="\n";
		
		$rssfeed .='<url>';
		$rssfeed .="\n";
      	$rssfeed .='<loc>' . $config['site_url'] . 'bg/terms-and-conditions</loc>';
		$rssfeed .="\n";
      	$rssfeed .='<changefreq>monthly</changefreq>';
		$rssfeed .="\n";
		$rssfeed .='<priority>0.3</priority>';
	   	$rssfeed .="\n";
   		$rssfeed .='</url>';
   		$rssfeed .="\n";
		
		$rssfeed .='<url>';
		$rssfeed .="\n";
      	$rssfeed .='<loc>' . $config['site_url'] . 'bg/our-history</loc>';
		$rssfeed .="\n";
      	$rssfeed .='<changefreq>monthly</changefreq>';
		$rssfeed .="\n";
		$rssfeed .='<priority>0.3</priority>';
	   	$rssfeed .="\n";
   		$rssfeed .='</url>';
   		$rssfeed .="\n";
		
		$rssfeed .='<url>';
		$rssfeed .="\n";
      	$rssfeed .='<loc>' . $config['site_url'] . 'bg/news</loc>';
		$rssfeed .="\n";
      	$rssfeed .='<changefreq>weekly</changefreq>';
		$rssfeed .="\n";
		$rssfeed .='<priority>0.3</priority>';
	   	$rssfeed .="\n";
   		$rssfeed .='</url>';
   		$rssfeed .="\n";
		
		$newsList = $db->fetchAll("SELECT idnews, alias_bg FROM news WHERE status = 'active' ORDER BY date_added DESC");
		
		foreach($newsList as $k=>$news){
			$rssfeed .='<url>';
			$rssfeed .="\n";
	      	$rssfeed .='<loc>'. $config['site_url'] . 'bg/news/' . $news['alias_bg'] .'-'. $news['idnews'] .'</loc>';
			$rssfeed .="\n";
	      	$rssfeed .='<changefreq>weekly</changefreq>';
			$rssfeed .="\n";
			$rssfeed .='<priority>0.6</priority>';
	   		$rssfeed .="\n";
	   		$rssfeed .='</url>';
	   		$rssfeed .="\n";
		}
	
		$categoryList = $db->fetchAll("SELECT idcategory, alias_bg FROM category WHERE status=1");
		
		foreach($categoryList as $k=>$cat){
			
			$rssfeed .='<url>';
			$rssfeed .="\n";
	      	$rssfeed .='<loc>'. $config['site_url'] . 'bg/products/' . $cat['alias_bg'] .'-'. $cat['idcategory'] .'</loc>';
			$rssfeed .="\n";
	      	$rssfeed .='<changefreq>weekly</changefreq>';
			$rssfeed .="\n";
			$rssfeed .='<priority>0.9</priority>';
	   		$rssfeed .="\n";
	   		$rssfeed .='</url>';
	   		$rssfeed .="\n";
		}
		
		$productsList = $db->fetchAll("SELECT idproduct, alias_bg FROM products WHERE active!=4 and idcategory != 7"); 
		
		foreach($productsList as $k=>$product){
			
			$rssfeed .='<url>';
			$rssfeed .="\n";
	      	$rssfeed .='<loc>'. $config['site_url'] . 'bg/' . $product['alias_bg'] .'-'. $product['idproduct'] .'</loc>';
			$rssfeed .="\n";
	      	$rssfeed .='<changefreq>weekly</changefreq>';
			$rssfeed .="\n";
			$rssfeed .='<priority>1.0</priority>';
	   		$rssfeed .="\n";
	   		$rssfeed .='</url>';
	   		$rssfeed .="\n";
		}
		
		$rssfeed .= '</urlset>';
		
		$handle = fopen($filename, 'w');
        fwrite($handle, trim($rssfeed));
        fclose($handle);
?>